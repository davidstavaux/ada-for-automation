#include <stdio.h>
#include <conio.h>
#include <windows.h>
#include <mmsystem.h>

/*****************************************************************************/
/*! The main function                                                        */
/*   \return 0 on success                                                    */
/*****************************************************************************/
int main(int argc, char* argv[])
{

  MMRESULT Result;

  UNREFERENCED_PARAMETER(argc);
  UNREFERENCED_PARAMETER(argv);

  Result = timeBeginPeriod(1);
  if(Result != TIMERR_NOERROR)
  {
    printf("timeBeginPeriod Error!\r\n");
  }

  printf("Hit any key to stop the process!\r\n");
  while(!kbhit())
  {
    Sleep(10);
  }

  Result = timeEndPeriod(1);
  if(Result != TIMERR_NOERROR)
  {
    printf("timeEndPeriod Error!\r\n");
  }

  return 0;
}
