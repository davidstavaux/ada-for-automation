var transMatrix = [1, 0, 0, 1, 0, 0];
var svgDoc = null;

function pan_zoom_init(evt) {
  svgDoc = evt.target.getElementById("syno1");
  mapMatrix = evt.target.getElementById("map-matrix");
  width = svgDoc.getAttributeNS(null, "width");
  height = svgDoc.getAttributeNS(null, "height");
}

function pan_zoom_init() {
  svgDoc = document.getElementById("syno1");
  mapMatrix = document.getElementById("map-matrix");
  width = svgDoc.getAttributeNS(null, "width");
  height = svgDoc.getAttributeNS(null, "height");
}

function pan(dx, dy) {
  if (svgDoc == null) pan_zoom_init();
  transMatrix[4] += dx;
  transMatrix[5] += dy;

  newMatrix = "matrix(" + transMatrix.join(' ') + ")";
  mapMatrix.setAttributeNS(null, "transform", newMatrix);
}

function zoom(scale) {
  if (svgDoc == null) pan_zoom_init();
  for (var i = 0; i < transMatrix.length; i++) {
    transMatrix[i] *= scale;
  }

  transMatrix[4] += (1 - scale) * width / 2;
  transMatrix[5] += (1 - scale) * height / 2;

  newMatrix = "matrix(" + transMatrix.join(' ') + ")";
  mapMatrix.setAttributeNS(null, "transform", newMatrix);
}
