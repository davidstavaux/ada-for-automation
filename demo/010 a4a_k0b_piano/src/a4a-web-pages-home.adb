
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Base;

with A4A.User_Controllers; use A4A.User_Controllers;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

package body A4A.Web.Pages.Home is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      Web_Page.Home_View.IPB1_View.Update_View;
      Web_Page.Home_View.IPB2_View.Update_View;
      Web_Page.Home_View.IPB3_View.Update_View;
      Web_Page.Home_View.IPB4_View.Update_View;

      for I in Web_Page.Home_View.Input_IPBs_View_Byte_0'Range loop
         Web_Page.Home_View.Input_IPBs_View_Byte_0 (I).Update_View;
      end loop;

      for I in Web_Page.Home_View.Input_IPBs_View_Byte_1'Range loop
         Web_Page.Home_View.Input_IPBs_View_Byte_1 (I).Update_View;
      end loop;

      for I in Web_Page.Home_View.Output_LEDs_View_Byte_0'Range loop
         Web_Page.Home_View.Output_LEDs_View_Byte_0 (I).Update_View;
      end loop;

      for I in Web_Page.Home_View.Output_LEDs_View_Byte_1'Range loop
         Web_Page.Home_View.Output_LEDs_View_Byte_1 (I).Update_View;
      end loop;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.Home_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("block");
         Web_Page.Home_View.Side_Nav_Synos_View.Top ("0px");
      end Side_Nav_Synos_Open_On_Click;

      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Close_On_Click;

      procedure Synos_Close;
      procedure Synos_Close is
      begin
         Web_Page.Home_View.Syno1_View.Display ("none");
         Web_Page.Home_View.Syno2_View.Display ("none");
      end Synos_Close;

      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno1_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno1_On_Click;

      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno2_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View
           .Inner_HTML ("Inputs / Outputs View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno2_On_Click;

      procedure Set_Controllers;
      procedure Set_Controllers is
      begin
         Web_Page.Home_View.IPB1_View.Set_Controller
           (IPB1'Unrestricted_Access);
         Web_Page.Home_View.IPB2_View.Set_Controller
           (IPB1'Unrestricted_Access);
         Web_Page.Home_View.IPB3_View.Set_Controller
           (IPB3'Unrestricted_Access);
         Web_Page.Home_View.IPB4_View.Set_Controller
           (IPB4'Unrestricted_Access);

         for I in Web_Page.Home_View.Input_IPBs_View_Byte_0'Range loop
            Web_Page.Home_View.Input_IPBs_View_Byte_0 (I).Set_Controller
              (Input_IPBs_Byte_0 (I)'Unrestricted_Access);
         end loop;

         for I in Web_Page.Home_View.Input_IPBs_View_Byte_1'Range loop
            Web_Page.Home_View.Input_IPBs_View_Byte_1 (I).Set_Controller
              (Input_IPBs_Byte_1 (I)'Unrestricted_Access);
         end loop;

         for I in Web_Page.Home_View.Output_LEDs_View_Byte_0'Range loop
            Web_Page.Home_View.Output_LEDs_View_Byte_0 (I).Set_Controller
              (Output_LEDs_Byte_0 (I)'Unrestricted_Access);
         end loop;

         for I in Web_Page.Home_View.Output_LEDs_View_Byte_1'Range loop
            Web_Page.Home_View.Output_LEDs_View_Byte_1 (I).Set_Controller
              (Output_LEDs_Byte_1 (I)'Unrestricted_Access);
         end loop;

      end Set_Controllers;

      procedure Set_Handlers;
      procedure Set_Handlers is
      begin
         Web_Page.Home_View.Side_Nav_Synos_Open_View.On_Click_Handler
           (Side_Nav_Synos_Open_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Close_View.On_Click_Handler
           (Side_Nav_Synos_Close_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno1_View.On_Click_Handler
           (Side_Nav_Synos_Syno1_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno2_View.On_Click_Handler
           (Side_Nav_Synos_Syno2_On_Click'Unrestricted_Access);

         Web_Page.Home_View.IPB1_View.On_Click_Handler
           (IPB_Views.On_Click'Unrestricted_Access);

         Web_Page.Home_View.IPB3_View.On_Click_Handler
           (IPB_Views.On_Click'Unrestricted_Access);

         Web_Page.Home_View.IPB4_View.On_Mouse_Down_Handler
           (IPB_Views.On_Mouse_Down'Unrestricted_Access);

         Web_Page.Home_View.IPB4_View.On_Mouse_Up_Handler
           (IPB_Views.On_Mouse_Up'Unrestricted_Access);

         for I in Web_Page.Home_View.Input_IPBs_View_Byte_0'Range loop
            Web_Page.Home_View.Input_IPBs_View_Byte_0 (I).On_Mouse_Down_Handler
              (IPB_Views.On_Mouse_Down'Unrestricted_Access);

            Web_Page.Home_View.Input_IPBs_View_Byte_0 (I).On_Mouse_Up_Handler
              (IPB_Views.On_Mouse_Up'Unrestricted_Access);
         end loop;

         for I in Web_Page.Home_View.Input_IPBs_View_Byte_1'Range loop
            Web_Page.Home_View.Input_IPBs_View_Byte_1 (I).On_Click_Handler
              (IPB_Views.On_Click'Unrestricted_Access);
         end loop;
      end Set_Handlers;

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "050-main.html");

      Web_Page.Home_View.Side_Nav_Synos_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos");

      Web_Page.Home_View.Side_Nav_Synos_Open_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-open");

      Web_Page.Home_View.Side_Nav_Synos_Close_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-close");

      Web_Page.Home_View.Side_Nav_Synos_Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno1");

      Web_Page.Home_View.Side_Nav_Synos_Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno2");

      Web_Page.Home_View.Main_Header_Title_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-header-title");

      Web_Page.Home_View.Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno1");

      Web_Page.Home_View.Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno2");

      Synos_Close;
      Web_Page.Home_View.Current_Syno_View :=
        Web_Page.Home_View.Syno1_View'Access;
      Web_Page.Home_View.Current_Syno_View.Display ("block");
      Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");

      Web_Page.Home_View.IPB1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "led1");

      Web_Page.Home_View.IPB2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "led2");

      Web_Page.Home_View.IPB3_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "led3");

      Web_Page.Home_View.IPB4_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "led4");

      for I in Web_Page.Home_View.Input_IPBs_View_Byte_0'Range loop
         Web_Page.Home_View.Input_IPBs_View_Byte_0 (I).Attach_Using_Parent
           (Web_Page.Common_View.View, "input_leds_byte_0_"
            & Gnoga.Left_Trim (I'Img));
      end loop;

      for I in Web_Page.Home_View.Input_IPBs_View_Byte_1'Range loop
         Web_Page.Home_View.Input_IPBs_View_Byte_1 (I).Attach_Using_Parent
           (Web_Page.Common_View.View, "input_leds_byte_1_"
            & Gnoga.Left_Trim (I'Img));
      end loop;

      for I in Web_Page.Home_View.Output_LEDs_View_Byte_0'Range loop
         Web_Page.Home_View.Output_LEDs_View_Byte_0 (I).Attach_Using_Parent
           (Web_Page.Common_View.View, "output_leds_byte_0_"
            & Gnoga.Left_Trim (I'Img));
      end loop;

      for I in Web_Page.Home_View.Output_LEDs_View_Byte_1'Range loop
         Web_Page.Home_View.Output_LEDs_View_Byte_1 (I).Attach_Using_Parent
           (Web_Page.Common_View.View, "output_leds_byte_1_"
            & Gnoga.Left_Trim (I'Img));
      end loop;

      Set_Controllers;

      Set_Handlers;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.Home;
