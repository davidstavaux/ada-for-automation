
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time;

--  with A4A.Memory.MBTCP_IOServer;
with A4A.Memory.MBTCP_IOScan;
use A4A.Memory;

with A4A.Library.Conversion; use A4A.Library.Conversion;
with A4A.User_Objects; use A4A.User_Objects;

--  tag::functions[]
package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is  --  <1>
   begin

      Booleans_To_Byte (Boolean_in00 => MBTCP_IOScan.Bool_Inputs (0),
                        Boolean_in01 => MBTCP_IOScan.Bool_Inputs (1),
                        Boolean_in02 => MBTCP_IOScan.Bool_Inputs (2),
                        Boolean_in03 => MBTCP_IOScan.Bool_Inputs (3),
                        Boolean_in04 => MBTCP_IOScan.Bool_Inputs (4),
                        Boolean_in05 => MBTCP_IOScan.Bool_Inputs (5),
                        Boolean_in06 => MBTCP_IOScan.Bool_Inputs (6),
                        Boolean_in07 => MBTCP_IOScan.Bool_Inputs (7),
                        Byte_out     => Cmd_Byte);

      Booleans_To_Byte (Boolean_in00 => MBTCP_IOScan.Bool_Inputs (8),
                        Boolean_in01 => MBTCP_IOScan.Bool_Inputs (9),
                        Boolean_in02 => MBTCP_IOScan.Bool_Inputs (10),
                        Boolean_in03 => MBTCP_IOScan.Bool_Inputs (11),
                        Boolean_in04 => MBTCP_IOScan.Bool_Inputs (12),
                        Boolean_in05 => MBTCP_IOScan.Bool_Inputs (13),
                        Boolean_in06 => MBTCP_IOScan.Bool_Inputs (14),
                        Boolean_in07 => MBTCP_IOScan.Bool_Inputs (15),
                        Byte_out     => Pattern_Byte);

   end Map_Inputs;

   procedure Map_Outputs is --  <2>
   begin

      Byte_To_Booleans (Byte_in       => Cmd_Byte,
                        Boolean_out00 => MBTCP_IOScan.Bool_Outputs (0),
                        Boolean_out01 => MBTCP_IOScan.Bool_Outputs (1),
                        Boolean_out02 => MBTCP_IOScan.Bool_Outputs (2),
                        Boolean_out03 => MBTCP_IOScan.Bool_Outputs (3),
                        Boolean_out04 => MBTCP_IOScan.Bool_Outputs (4),
                        Boolean_out05 => MBTCP_IOScan.Bool_Outputs (5),
                        Boolean_out06 => MBTCP_IOScan.Bool_Outputs (6),
                        Boolean_out07 => MBTCP_IOScan.Bool_Outputs (7));

      Byte_To_Booleans (Byte_in       => Output_Byte,
                        Boolean_out00 => MBTCP_IOScan.Bool_Outputs (8),
                        Boolean_out01 => MBTCP_IOScan.Bool_Outputs (9),
                        Boolean_out02 => MBTCP_IOScan.Bool_Outputs (10),
                        Boolean_out03 => MBTCP_IOScan.Bool_Outputs (11),
                        Boolean_out04 => MBTCP_IOScan.Bool_Outputs (12),
                        Boolean_out05 => MBTCP_IOScan.Bool_Outputs (13),
                        Boolean_out06 => MBTCP_IOScan.Bool_Outputs (14),
                        Boolean_out07 => MBTCP_IOScan.Bool_Outputs (15));

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

   procedure Process_IO is --  <3>

      Elapsed_TON_1 : Ada.Real_Time.Time_Span;

   begin

      if First_Cycle then

         Output_Byte := Pattern_Byte;

         First_Cycle := False;

      end if;

      Tempo_TON_1.Cyclic (Start   => not TON_1_Q,
                          Preset  => Ada.Real_Time.Milliseconds (500),
                          Elapsed => Elapsed_TON_1,
                          Q       => TON_1_Q);

      if TON_1_Q then

         case Cmd_Byte is

         when 0 =>
            Output_Byte := ROR (Value => Output_Byte, Amount => 1);

         when 1 =>
            Output_Byte := ROL (Value => Output_Byte, Amount => 1);

         when others => Output_Byte := Pattern_Byte;

         end case;

      end if;

   end Process_IO;

end A4A.User_Functions;
--  end::functions[]
