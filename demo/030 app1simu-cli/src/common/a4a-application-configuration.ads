
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;
with A4A.Configuration; use A4A.Configuration;

package A4A.Application.Configuration is

   --------------------------------------------------------------------
   --  Main Task Configuration
   --------------------------------------------------------------------

   Application_Main_Task_Priority : System.Priority :=
     System.Priority'Last - 10;
   --  System.Default_Priority;

   --  can be cyclic
--     Application_Main_Task_Type     : constant Main_Task_Type := Cyclic;
   Application_Main_Task_Delay_MS : constant := 100;

   --  or periodic
   Application_Main_Task_Type : constant Main_Task_Type := Periodic;
   Application_Main_Task_Period_MS : constant := 50;

   --------------------------------------------------------------------
   --  Periodic Task 1 Configuration
   --------------------------------------------------------------------

   Application_Periodic_Task_1_Priority : System.Priority :=
     System.Default_Priority;
   Application_Periodic_Task_1_Period_MS : constant := 500;

end A4A.Application.Configuration;
