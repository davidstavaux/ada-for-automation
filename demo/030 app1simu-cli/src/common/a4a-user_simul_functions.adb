
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Simul_Objects; use A4A.User_Simul_Objects;

package body A4A.User_Simul_Functions is

   --------------------------------------------------------------------
   --  User simulation functions
   --------------------------------------------------------------------

   procedure Simulate is
      Tank_Filling_Increment : constant := 100;
      Tank_Filling_Decrement : constant := 200;

      Threshoold_SL : constant :=  1000;
      Threshoold_SH : constant := 65000;

      Tank_Level_Min : constant :=   500;
      Tank_Level_Max : constant := 65200;
   begin

      --  Auto
      Auto := Auto_Simu;

      --  Manu
      Manu := Manu_Simu;

      --  Faults Acknowledgement
      Ack_Faults := Ack_Faults_Simu;

      if Tank_Simu_Auto then

         if Valve_14_Pos_Open then
            Level_Transmitter_10_Measure :=
              Level_Transmitter_10_Measure + Tank_Filling_Increment;
         end if;

         if MyPump13_FeedBack then
            Level_Transmitter_10_Measure :=
              Level_Transmitter_10_Measure - Tank_Filling_Decrement;
         end if;

         if Level_Transmitter_10_Measure < Tank_Level_Min then
            Level_Transmitter_10_Measure := Tank_Level_Min;
         elsif Level_Transmitter_10_Measure > Tank_Level_Max then
            Level_Transmitter_10_Measure := Tank_Level_Max;
         end if;

         Level_Switch_11 := Level_Transmitter_10_Measure < Threshoold_SH;
         Level_Switch_12 := Level_Transmitter_10_Measure > Threshoold_SL;

      else

         Level_Switch_11 := Level_Switch_11_Simu_On;
         Level_Switch_12 := Level_Switch_12_Simu_On;

      end if;

      MyPump13_Simu.Cyclic
        (Auto      => MyPump13_Simu_Auto,
         FB_Manu   => MyPump13_Simu_FB_Manu,
         Coil      => MyPump13_Coil,
         Feed_Back => MyPump13_FeedBack);

      Valve_14_Simu.Cyclic
        (Auto       => Valve_14_Simu_Auto,
         ZO_Manu    => Valve_14_Simu_Pos_Open_Manu,
         ZF_Manu    => Valve_14_Simu_Pos_Closed_Manu,
         Coil       => Valve_14_Coil,
         Pos_Open   => Valve_14_Pos_Open,
         Pos_Closed => Valve_14_Pos_Closed);

   end Simulate;

end A4A.User_Simul_Functions;
