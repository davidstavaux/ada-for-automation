
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.MBTCP_IOServer;
use A4A.Memory;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Simul_Objects; use A4A.User_Simul_Objects;

package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
--        Temp_Bools : array (0..15) of Boolean := (others => False);
   begin

--        Word_To_Booleans
--          (Word_in       => MBTCP_IOServer.Registers(20),
--           Boolean_out00 => MyPump13_Coil,
--           Boolean_out01 => Valve_14_Coil,
--           Boolean_out02 => Temp_Bools(2) , -- Spare
--           Boolean_out03 => Temp_Bools(3) , -- Spare
--           Boolean_out04 => Temp_Bools(4) , -- Spare
--           Boolean_out05 => Temp_Bools(5) , -- Spare
--           Boolean_out06 => Temp_Bools(6) , -- Spare
--           Boolean_out07 => Temp_Bools(7) , -- Spare
--
--           Boolean_out08 => Temp_Bools(8) , -- Spare
--           Boolean_out09 => Temp_Bools(9) , -- Spare
--           Boolean_out10 => Temp_Bools(10), -- Spare
--           Boolean_out11 => Temp_Bools(11), -- Spare
--           Boolean_out12 => Temp_Bools(12), -- Spare
--           Boolean_out13 => Temp_Bools(13), -- Spare
--           Boolean_out14 => Temp_Bools(14), -- Spare
--           Boolean_out15 => Temp_Bools(15)  -- Spare
--          );

      MyPump13_Coil := MBTCP_IOServer.Bool_Coils (0);
      Valve_14_Coil := MBTCP_IOServer.Bool_Coils (1);

   end Map_Inputs;

   procedure Map_Outputs is
   begin

--        Booleans_To_Word
--          (Boolean_in00 => Auto,
--           Boolean_in01 => Manu,
--           Boolean_in02 => Ack_Faults,
--           Boolean_in03 => Level_Switch_11,
--           Boolean_in04 => Valve_14_Pos_Open,
--           Boolean_in05 => Valve_14_Pos_Closed,
--           Boolean_in06 => Level_Switch_12,
--           Boolean_in07 => MyPump13_FeedBack,
--           -- others => Spare
--           Word_out     => MBTCP_IOServer.Input_Registers(0)
--          );

      MBTCP_IOServer.Bool_Inputs (0) := Auto;
      MBTCP_IOServer.Bool_Inputs (1) := Manu;
      MBTCP_IOServer.Bool_Inputs (2) := Ack_Faults;
      MBTCP_IOServer.Bool_Inputs (3) := Level_Switch_11;
      MBTCP_IOServer.Bool_Inputs (4) := Valve_14_Pos_Open;
      MBTCP_IOServer.Bool_Inputs (5) := Valve_14_Pos_Closed;
      MBTCP_IOServer.Bool_Inputs (6) := Level_Switch_12;
      MBTCP_IOServer.Bool_Inputs (7) := MyPump13_FeedBack;

      MBTCP_IOServer.Input_Registers (0) := Level_Transmitter_10_Measure;

   end Map_Outputs;

   procedure Map_HMI_Inputs is
--        Temp_Bools : array (0..15) of Boolean := (others => False);
   begin

--        Word_To_Booleans
--          (Word_in       => MBTCP_IOServer.Registers(0),
--           Boolean_out00 => Auto_Simu,
--           Boolean_out01 => Manu_Simu,
--           Boolean_out02 => Ack_Faults_Simu,
--           Boolean_out03 => Level_Switch_11_Simu_On,
--           Boolean_out04 => Level_Switch_12_Simu_On,
--           Boolean_out05 => Valve_14_Simu_Auto,
--           Boolean_out06 => Valve_14_Simu_Pos_Open_Manu,
--           Boolean_out07 => Valve_14_Simu_Pos_Closed_Manu,
--
--           Boolean_out08 => MyPump13_Simu_Auto,
--           Boolean_out09 => MyPump13_Simu_FB_Manu,
--           Boolean_out10 => Tank_Simu_Auto,
--           Boolean_out11 => Temp_Bools(11), -- Spare
--           Boolean_out12 => Temp_Bools(12), -- Spare
--           Boolean_out13 => Temp_Bools(13), -- Spare
--           Boolean_out14 => Temp_Bools(14), -- Spare
--           Boolean_out15 => Temp_Bools(15)  -- Spare
--          );

      Auto_Simu                     := MBTCP_IOServer.Bool_Coils (100);
      Manu_Simu                     := MBTCP_IOServer.Bool_Coils (101);
      Ack_Faults_Simu               := MBTCP_IOServer.Bool_Coils (102);
      Level_Switch_11_Simu_On       := MBTCP_IOServer.Bool_Coils (103);
      Level_Switch_12_Simu_On       := MBTCP_IOServer.Bool_Coils (104);
      Valve_14_Simu_Auto            := MBTCP_IOServer.Bool_Coils (105);
      Valve_14_Simu_Pos_Open_Manu   := MBTCP_IOServer.Bool_Coils (106);
      Valve_14_Simu_Pos_Closed_Manu := MBTCP_IOServer.Bool_Coils (107);
      MyPump13_Simu_Auto            := MBTCP_IOServer.Bool_Coils (108);
      MyPump13_Simu_FB_Manu         := MBTCP_IOServer.Bool_Coils (109);
      Tank_Simu_Auto                := MBTCP_IOServer.Bool_Coils (110);

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

end A4A.User_Functions;
