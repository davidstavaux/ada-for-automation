
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Simulation.Contactor;
with A4A.Library.Simulation.Valve;

use A4A.Library.Simulation;

package A4A.User_Simul_Objects is

   --------------------------------------------------------------------
   --  User Simulation Objects creation
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  HMI Inputs
   --------------------------------------------------------------------

   Auto_Simu                : Boolean := False;
   Manu_Simu                : Boolean := False;

   Ack_Faults_Simu          : Boolean := False;
   --  Faults Acknowledgement

   Tank_Simu_Auto           : Boolean := False;

   Level_Switch_11_Simu_On  : Boolean := False;
   Level_Switch_12_Simu_On  : Boolean := False;

   Valve_14_Simu_Auto            : Boolean := False;
   Valve_14_Simu_Pos_Open_Manu   : Boolean := False;
   Valve_14_Simu_Pos_Closed_Manu : Boolean := False;

   MyPump13_Simu_Auto       : Boolean := False;
   MyPump13_Simu_FB_Manu    : Boolean := False;

   --------------------------------------------------------------------
   --  Simulation Instances
   --------------------------------------------------------------------

   MyPump13_Simu : Contactor.Instance :=
     Contactor.Create (TON_Preset => 500);
   --  Pump Simulation Instance

   Valve_14_Simu : Valve.Instance :=
     Valve.Create (TON_Preset => 500);
   --  Valve Simulation Instance

end A4A.User_Simul_Objects;
