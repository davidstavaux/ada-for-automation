
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Application.Identification is

   function Get_Application_Name return String;

   function Get_Application_Version return String;

   function Get_Application_Description return String;

private

   Application_Name : String := "Test Application 1 Simulation with Web UI";

   Application_Version : String := "2022/01/16";

   Application_Description : String :=
     "<p>This is a simulation application for the test application 1.</p>"
     & CRLF
     & "<p>The purpose of this application is to simulate the operative part."
     & "</p>"
     & CRLF
     & "<p>There is a Web UI provided by Gnoga.</p>";

end A4A.Application.Identification;
