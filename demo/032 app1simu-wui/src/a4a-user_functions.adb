
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.MBTCP_IOServer;
use A4A.Memory;

with A4A.User_Objects; use A4A.User_Objects;

--  tag::functions[]
package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is  --  <1>
   begin

      MyPump13_Coil := MBTCP_IOServer.Bool_Coils (0);
      Valve_14_Coil := MBTCP_IOServer.Bool_Coils (1);

   end Map_Inputs;

   procedure Map_Outputs is --  <2>
   begin

      MBTCP_IOServer.Bool_Inputs (0) := Auto;
      MBTCP_IOServer.Bool_Inputs (1) := Manu;
      MBTCP_IOServer.Bool_Inputs (2) := Ack_Faults;
      MBTCP_IOServer.Bool_Inputs (3) := Level_Switch_11;
      MBTCP_IOServer.Bool_Inputs (4) := Valve_14_Pos_Open;
      MBTCP_IOServer.Bool_Inputs (5) := Valve_14_Pos_Closed;
      MBTCP_IOServer.Bool_Inputs (6) := Level_Switch_12;
      MBTCP_IOServer.Bool_Inputs (7) := MyPump13_FeedBack;

      MBTCP_IOServer.Input_Registers (0) := Level_Transmitter_10_Measure;

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

end A4A.User_Functions;
--  end::functions[]
