
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  tag::objects[]
package A4A.User_Objects is

   --------------------------------------------------------------------
   --  User Objects creation
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  Inputs => Simulation Outputs
   --------------------------------------------------------------------

   Auto                : aliased Boolean := False;  --  <1>
   Manu                : aliased Boolean := False;

   Ack_Faults          : aliased Boolean := False;
   --  Faults Acknowledgement

   Level_Transmitter_10_Measure : Word    := 0;

   Level_Switch_11     : aliased Boolean := False;
   Level_Switch_12     : aliased Boolean := False;
   MyPump13_FeedBack   : aliased Boolean := False;
   Valve_14_Pos_Open   : aliased Boolean := False;
   Valve_14_Pos_Closed : aliased Boolean := False;

   --------------------------------------------------------------------
   --  Outputs => Simulation Inputs
   --------------------------------------------------------------------
   MyPump13_Coil       : aliased Boolean := False;  --  <2>
   Valve_14_Coil       : aliased Boolean := False;

end A4A.User_Objects;
--  end::objects[]
