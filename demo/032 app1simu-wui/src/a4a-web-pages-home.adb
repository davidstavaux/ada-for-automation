
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Base;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Controllers; use A4A.User_Controllers;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

package body A4A.Web.Pages.Home is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      --
      --  General Panel views
      --

      Web_Page.Home_View.Auto_Manu_Simu_Switch3_View.Update_View;
      Web_Page.Home_View.Ack_Faults_Simu_IPB_View.Update_View;

      Web_Page.Home_View.Auto_LED_View.Update_View;
      Web_Page.Home_View.Manu_LED_View.Update_View;
      Web_Page.Home_View.Ack_Faults_LED_View.Update_View;

      --
      --  Tank and Level Switches views
      --

      Web_Page.Home_View.Tank_Simu_Auto_IPB_View.Update_View;
      Web_Page.Home_View.LS_11_Simu_On_IPB_View.Update_View;
      Web_Page.Home_View.LS_12_Simu_On_IPB_View.Update_View;

      Web_Page.Home_View.LS_11_On_LED_View.Update_View;
      Web_Page.Home_View.LS_12_On_LED_View.Update_View;

      if Level_Transmitter_10_Measure /= Web_Page.Home_View.LT_10_Measure then
         Web_Page.Home_View.Level_10_View.Inner_HTML
           (Level_Transmitter_10_Measure'Img);
         Web_Page.Home_View.LT_10_Measure := Level_Transmitter_10_Measure;
      end if;

      --
      --  Valve 14 views
      --

      Web_Page.Home_View.V14_Simu_Auto_IPB_View.Update_View;
      Web_Page.Home_View.V14_Simu_Pos_Open_Manu_IPB_View.Update_View;
      Web_Page.Home_View.V14_Simu_Pos_Closed_Manu_IPB_View.Update_View;

      Web_Page.Home_View.V14_Pos_Open_LED_View.Update_View;
      Web_Page.Home_View.V14_Pos_Closed_LED_View.Update_View;
      Web_Page.Home_View.V14_Coil_LED_View.Update_View;

      --
      --  Pump 13 views
      --

      Web_Page.Home_View.P13_Simu_Auto_IPB_View.Update_View;
      Web_Page.Home_View.P13_Simu_FB_Manu_IPB_View.Update_View;

      Web_Page.Home_View.P13_FB_LED_View.Update_View;
      Web_Page.Home_View.P13_Coil_LED_View.Update_View;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.Home_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("block");
         Web_Page.Home_View.Side_Nav_Synos_View.Top ("0px");
      end Side_Nav_Synos_Open_On_Click;

      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Close_On_Click;

      procedure Synos_Close;
      procedure Synos_Close is
      begin
         Web_Page.Home_View.Syno1_View.Display ("none");
         Web_Page.Home_View.Syno2_View.Display ("none");
      end Synos_Close;

      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno1_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno1_On_Click;

      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno2_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View
           .Inner_HTML ("Inputs / Outputs View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno2_On_Click;

      procedure Set_Handlers;
      procedure Set_Handlers is
      begin
         Web_Page.Home_View.Side_Nav_Synos_Open_View.On_Click_Handler
           (Side_Nav_Synos_Open_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Close_View.On_Click_Handler
           (Side_Nav_Synos_Close_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno1_View.On_Click_Handler
           (Side_Nav_Synos_Syno1_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno2_View.On_Click_Handler
           (Side_Nav_Synos_Syno2_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Ack_Faults_Simu_IPB_View.On_Mouse_Down_Handler
           (IPB_Views.On_Mouse_Down'Access);
         Web_Page.Home_View.Ack_Faults_Simu_IPB_View.On_Mouse_Up_Handler
           (IPB_Views.On_Mouse_Up'Access);

         Web_Page.Home_View.Tank_Simu_Auto_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
         Web_Page.Home_View.LS_11_Simu_On_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
         Web_Page.Home_View.LS_12_Simu_On_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);

         Web_Page.Home_View.V14_Simu_Auto_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
         Web_Page.Home_View.V14_Simu_Pos_Open_Manu_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
         Web_Page.Home_View.V14_Simu_Pos_Closed_Manu_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);

         Web_Page.Home_View.P13_Simu_Auto_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
         Web_Page.Home_View.P13_Simu_FB_Manu_IPB_View.On_Click_Handler
           (IPB_Views.On_Click'Access);
      end Set_Handlers;

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "050-main.html");

      Web_Page.Home_View.Side_Nav_Synos_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos");

      Web_Page.Home_View.Side_Nav_Synos_Open_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-open");

      Web_Page.Home_View.Side_Nav_Synos_Close_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-close");

      Web_Page.Home_View.Side_Nav_Synos_Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno1");

      Web_Page.Home_View.Side_Nav_Synos_Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno2");

      Web_Page.Home_View.Main_Header_Title_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-header-title");

      Web_Page.Home_View.Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno1");

      Web_Page.Home_View.Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno2");

      Synos_Close;
      Web_Page.Home_View.Current_Syno_View :=
        Web_Page.Home_View.Syno1_View'Access;
      Web_Page.Home_View.Current_Syno_View.Display ("block");
      Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");

      --
      --  General Panel views
      --

      Web_Page.Home_View.Auto_Manu_Simu_Switch3_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Auto_Manu_Simu_Switch3'Access,
         Mobile_Id  => "auto-manu-switch3-mobile",
         Area0_Id   => "auto-manu-switch3-stop",
         Area1_Id   => "auto-manu-switch3-auto",
         Area2_Id   => "auto-manu-switch3-manu");

      Web_Page.Home_View.Ack_Faults_Simu_IPB_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Ack_Faults_Simu_IPB'Access,
         Id         => "ack-button");

      Web_Page.Home_View.Auto_LED_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Auto_LED'Access,
         Id         => "auto-led");

      Web_Page.Home_View.Manu_LED_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Manu_LED'Access,
         Id         => "manu-led");

      Web_Page.Home_View.Ack_Faults_LED_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Ack_Faults_LED'Access,
         Id         => "ack-led");

      --
      --  Tank and Level Switches views
      --

      Web_Page.Home_View.Tank_Simu_Auto_IPB_View.Setup
        (Web_Page.Common_View.View, Tank_Simu_Auto_IPB'Access,
         "tank-simu-auto-button");

      Web_Page.Home_View.LS_11_Simu_On_IPB_View.Setup
        (Web_Page.Common_View.View, LS_11_Simu_On_IPB'Access, "ls11-button");

      Web_Page.Home_View.LS_12_Simu_On_IPB_View.Setup
        (Web_Page.Common_View.View, LS_12_Simu_On_IPB'Access, "ls12-button");

      Web_Page.Home_View.LS_11_On_LED_View.Setup
        (Web_Page.Common_View.View, LS_11_On_LED'Access, "ls11-led");

      Web_Page.Home_View.LS_12_On_LED_View.Setup
        (Web_Page.Common_View.View, LS_12_On_LED'Access, "ls12-led");

      Web_Page.Home_View.Level_10_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "level10");

      --
      --  Valve 14 views
      --

      Web_Page.Home_View.V14_Simu_Auto_IPB_View.Setup
        (Web_Page.Common_View.View, V14_Simu_Auto_IPB'Access,
         "valve14-simu-auto-button");

      Web_Page.Home_View.V14_Simu_Pos_Open_Manu_IPB_View.Setup
        (Web_Page.Common_View.View, V14_Simu_Pos_Open_Manu_IPB'Access,
         "valve14-simu-open-button");

      Web_Page.Home_View.V14_Simu_Pos_Closed_Manu_IPB_View.Setup
        (Web_Page.Common_View.View, V14_Simu_Pos_Closed_Manu_IPB'Access,
         "valve14-simu-close-button");

      Web_Page.Home_View.V14_Pos_Open_LED_View.Setup
        (Web_Page.Common_View.View, V14_Pos_Open_LED'Access,
         "valve14-open-led");

      Web_Page.Home_View.V14_Pos_Closed_LED_View.Setup
        (Web_Page.Common_View.View, V14_Pos_Closed_LED'Access,
         "valve14-closed-led");

      Web_Page.Home_View.V14_Coil_LED_View.Setup
        (Web_Page.Common_View.View, V14_Coil_LED'Access,
         "valve14-coil-led");

      --
      --  Pump 13 views
      --

      Web_Page.Home_View.P13_Simu_Auto_IPB_View.Setup
        (Web_Page.Common_View.View, P13_Simu_Auto_IPB'Access,
         "pump13-simu-auto-button");

      Web_Page.Home_View.P13_Simu_FB_Manu_IPB_View.Setup
        (Web_Page.Common_View.View, P13_Simu_FB_Manu_IPB'Access,
         "pump13-simu-on-button");

      Web_Page.Home_View.P13_FB_LED_View.Setup
        (Web_Page.Common_View.View, P13_FB_LED'Access, "pump13-fb-led");

      Web_Page.Home_View.P13_Coil_LED_View.Setup
        (Web_Page.Common_View.View, P13_Coil_LED'Access, "pump13-coil-led");

      Set_Handlers;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.Home;
