
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

with A4A.Web.Views.LED_Views;
with A4A.Web.Views.IPB_Views;
with A4A.Web.Views.Switch3_Views;
use A4A.Web.Views;

package A4A.Web.Pages.Home is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   --  Views

   type Home_View_Type is
      record
         Side_Nav_Synos_View        : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Open_View   : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Close_View  : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Syno1_View  : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Syno2_View  : Gnoga.Gui.Element.Element_Type;

         Main_Header_Title_View     : Gnoga.Gui.Element.Element_Type;

         Syno1_View        : aliased Gnoga.Gui.Element.Element_Type;
         Syno2_View        : aliased Gnoga.Gui.Element.Element_Type;

         Current_Syno_View : Gnoga.Gui.Element.Element_Access;

         --
         --  General Panel views
         --

         Auto_Manu_Simu_Switch3_View : Switch3_Views.Instance;
         Ack_Faults_Simu_IPB_View    : IPB_Views.Instance;

         Auto_LED_View        : LED_Views.Instance;
         Manu_LED_View        : LED_Views.Instance;
         Ack_Faults_LED_View  : LED_Views.Instance;

         --
         --  Tank and Level Switches views
         --

         Tank_Simu_Auto_IPB_View   : IPB_Views.Instance;
         LS_11_Simu_On_IPB_View    : IPB_Views.Instance;
         LS_12_Simu_On_IPB_View    : IPB_Views.Instance;

         LS_11_On_LED_View    : LED_Views.Instance;
         LS_12_On_LED_View    : LED_Views.Instance;

         Level_10_View : Gnoga.Gui.Element.Element_Type;
         LT_10_Measure : Word := 0;
         --
         --  Valve 14 views
         --

         V14_Simu_Auto_IPB_View            : IPB_Views.Instance;
         V14_Simu_Pos_Open_Manu_IPB_View   : IPB_Views.Instance;
         V14_Simu_Pos_Closed_Manu_IPB_View : IPB_Views.Instance;

         V14_Pos_Open_LED_View   : LED_Views.Instance;
         V14_Pos_Closed_LED_View : LED_Views.Instance;
         V14_Coil_LED_View       : LED_Views.Instance;

         --
         --  Pump 13 views
         --

         P13_Simu_Auto_IPB_View    : IPB_Views.Instance;
         P13_Simu_FB_Manu_IPB_View : IPB_Views.Instance;

         P13_FB_LED_View      : LED_Views.Instance;
         P13_Coil_LED_View    : LED_Views.Instance;

      end record;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;
         Home_View   : Home_View_Type;

         My_Connection_ID : Gnoga.Types.Connection_ID;
      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

end A4A.Web.Pages.Home;
