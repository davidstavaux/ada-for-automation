
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time;
with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Logger;
with A4A.Memory.MBTCP_IOServer;
with A4A.Memory.MBTCP_IOScan;
use A4A.Memory;

with A4A.Application.Main_Periodic1;
use A4A.Application.Main_Periodic1;

with A4A.Library.Analog; use A4A.Library.Analog;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Functions; use A4A.User_Functions;

package body A4A.Application is

   procedure Cold_Start is
   begin

      null;

   end Cold_Start;

   procedure Closing is
   begin

      null;

   end Closing;

   procedure Main_Cyclic is
      My_Ident : constant String := "A4A.Application.Main_Cyclic";
      Elapsed_TON_2 : Ada.Real_Time.Time_Span;
--        Bug : Integer := 10;
   begin
--        A4A.Log.Logger.Put
--          (Who  => My_Ident,
--           What => "Yop ! ********************** "
--           & Integer'Image(Integer(MBTCP_IOScan_Inputs(0))));

--        Bug := Bug / (Bug - 10);
      Map_Inputs;

      Map_HMI_Inputs;

      --  Playing with tasks interface
      Main_Outputs.X := Main_Inputs.A;
      Main_Outputs.Y := Main_Inputs.B;
      Main_Outputs.Z := Main_Inputs.C;

      --  Working mode
      Mode_Auto := Auto and not Manu;
      Mode_Manu := not Auto and Manu;

      if Mode_Auto then
         Valve_14_Manu_Cmd_Open := False;
         MyPump13_Manu_Cmd_On := False;
      end if;

      --  Level Transmitter 10
      Level_Transmitter_10_Value := Scale_In
        (X    => Integer (Level_Transmitter_10_Measure),
         Xmin => 0,
         Xmax => 65535,
         Ymin => 0.0,
         Ymax => 100.0);

      if not Level_Transmitter_10_InitDone then
         Level_Transmitter_10_Thresholds.Initialise
           (Hysteresis => Level_Transmitter_10_Hyst,
            HHH_T      => Level_Transmitter_10_HHH_T,
            HH_T       => Level_Transmitter_10_HH_T,
            H_T        => Level_Transmitter_10_H_T,
            L_T        => Level_Transmitter_10_L_T,
            LL_T       => Level_Transmitter_10_LL_T,
            LLL_T      => Level_Transmitter_10_LLL_T);

         Level_Transmitter_10_InitDone := True;
      end if;

      Level_Transmitter_10_Thresholds.Cyclic
        (Value => Level_Transmitter_10_Value,
         HHH   => Level_Transmitter_10_XHHH,
         HH    => Level_Transmitter_10_XHH,
         H     => Level_Transmitter_10_XH,
         L     => Level_Transmitter_10_XL,
         LL    => Level_Transmitter_10_XLL,
         LLL   => Level_Transmitter_10_XLLL);

      LS11_AH.Cyclic (Alarm_Cond => not Level_Switch_11,
                      Ack        => Ack_Faults,
                      Inhibit    => False);

      LS12_AL.Cyclic (Alarm_Cond => not Level_Switch_12,
                      Ack        => Ack_Faults,
                      Inhibit    => False);

      --  Valve_14 Command
      Valve_14_Condition_Perm := not LS11_AH.is_Faulty;
      Valve_14_Condition_Auto := Mode_Auto;
      Valve_14_Condition_Manu := Mode_Manu;

      if Level_Transmitter_10_XL then
         Valve_14_Auto_Cmd_Open := True;
      elsif Level_Transmitter_10_XHH then
         Valve_14_Auto_Cmd_Open := False;
      end if;

      Valve_14_Cmd_Open := Valve_14_Condition_Perm and
        ((Valve_14_Condition_Manu and Valve_14_Manu_Cmd_Open)
        or (Valve_14_Condition_Auto and Valve_14_Auto_Cmd_Open));

      Valve_14.Cyclic (Pos_Open   => Valve_14_Pos_Open,
                       Pos_Closed => Valve_14_Pos_Closed,
                       Ack        => Ack_Faults,
                       Cmd_Open   => Valve_14_Cmd_Open,
                       Coil       => Valve_14_Coil);

      --  MyPump13 Command
      MyPump13_Condition_Perm := not LS12_AL.is_Faulty;
      MyPump13_Condition_Auto := Mode_Auto;
      MyPump13_Condition_Manu := Mode_Manu;

      if Level_Transmitter_10_XHH then
         MyPump13_Auto_Cmd_On := True;
      elsif Level_Transmitter_10_XLL then
         MyPump13_Auto_Cmd_On := False;
      end if;

      MyPump13_Cmd_On := MyPump13_Condition_Perm and
        ((MyPump13_Condition_Auto and MyPump13_Auto_Cmd_On)
        or (MyPump13_Condition_Manu and MyPump13_Manu_Cmd_On));

      MyPump13.Cyclic (Feed_Back => MyPump13_FeedBack,
                       Ack       => Ack_Faults,
                       Cmd_On    => MyPump13_Cmd_On,
                       Coil      => MyPump13_Coil);

      --  Status use example
      MyPump13_Is_On := MyPump13.is_On;

--        A4A.Log.Logger.Put
--          (Who  => My_Ident,
--           What => "Here is MyPump Id : " & MyPump13.Get_Id);
      --  A little test
      Tempo_TON_2.Cyclic (Start   => not TON_2_Q,
                          Preset  => Ada.Real_Time.Milliseconds (10000),
                          Elapsed => Elapsed_TON_2,
                          Q       => TON_2_Q);

      if TON_2_Q then
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Tempo_TON_2 elapsed!",
                             Log_Level => A4A.Logger.Level_Verbose);
      end if;

      --  Modbus TCP IO Scanning test
      for Index in 10 .. 19 loop
         A4A.Memory.MBTCP_IOScan.Word_Outputs (Index) := Word (Index);
      end loop;

      for Index in 20 .. 29 loop
         A4A.Memory.MBTCP_IOScan.Word_Outputs (Index) :=
           A4A.Memory.MBTCP_IOScan.Word_Inputs (Index);
      end loop;

      A4A.Memory.MBTCP_IOScan.Word_Outputs (30) :=
        A4A.Memory.MBTCP_IOScan.Word_Outputs (30) + 1;

      A4A.Memory.MBTCP_IOScan.Bool_Outputs (0 .. 15) :=
        A4A.Memory.MBTCP_IOScan.Bool_Inputs (32 .. 47);

      --  Modbus TCP Server test
      A4A.Memory.MBTCP_IOServer.Input_Registers (5 .. 19) :=
        A4A.Memory.MBTCP_IOServer.Registers (5 .. 19);

      Map_Outputs;

      Map_HMI_Outputs;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Program_Fault_Flag := True;

   end Main_Cyclic;

   procedure Periodic1_Cyclic is
      My_Ident : constant String := "A4A.Application.Periodic1_Cyclic";
--        Elapsed_TON_1 : Ada.Real_Time.Time_Span;
      My_PID_1_Init : constant Boolean := False;
      My_PID_1_SP   : constant Float := 10.0;
      My_PID_1_PV   : constant Float := 20.0;
      My_PID_1_MV   : Float := 0.0;
      My_PID_1_Kp   : constant Float := 1.0;
      My_PID_1_Ki   : constant Float := 0.0;
      My_PID_1_Kd   : constant Float := 0.0;
   begin
--        A4A.Log.Logger.Put (Who  => My_Ident,
--                            What => "Hi !");

      --  Do something useful here
      --  Could be simulate

      --  Playing with tasks interface
      Periodic1_Outputs.A := not Periodic1_Inputs.X;
      Periodic1_Outputs.B := Periodic1_Inputs.Y + 2;
      Periodic1_Outputs.C := Periodic1_Inputs.Z + 1;

      My_PID_1.Cyclic
        (Set_Point              => My_PID_1_SP,
         Process_Value          => My_PID_1_PV,
         Kp                     => My_PID_1_Kp,
         Ki                     => My_PID_1_Ki,
         Kd                     => My_PID_1_Kd,
         Initialise             => My_PID_1_Init,
         Period_In_Milliseconds => 100,
         Manipulated_Value      => My_PID_1_MV
        );

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Program_Fault_Flag := True;

   end Periodic1_Cyclic;

   function Program_Fault return Boolean is
   begin
      return Program_Fault_Flag;
   end Program_Fault;

end A4A.Application;
