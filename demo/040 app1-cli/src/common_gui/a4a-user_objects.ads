
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Timers.TON; use A4A.Library.Timers;
with A4A.Library.Devices.Contactor; use A4A.Library.Devices;
with A4A.Library.Devices.Alarm_Switch;
with A4A.Library.Devices.Valve;
with A4A.Library.Analog.PID; use A4A.Library.Analog;
with A4A.Library.Analog.Threshold;

package A4A.User_Objects is

   --------------------------------------------------------------------
   --  User Objects creation
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  Inputs
   --------------------------------------------------------------------

   Auto                : Boolean := False;
   Manu                : Boolean := False;

   Ack_Faults          : Boolean := False;
   --  Faults Acknowledgement

   Level_Transmitter_10_Measure : Word    := 0;

   Level_Switch_11     : Boolean := False;
   Level_Switch_12     : Boolean := False;
   MyPump13_FeedBack   : Boolean := False;
   Valve_14_Pos_Open   : Boolean := False;
   Valve_14_Pos_Closed : Boolean := False;

   --------------------------------------------------------------------
   --  Outputs
   --------------------------------------------------------------------
   MyPump13_Coil       : Boolean := False;
   Valve_14_Coil       : Boolean := False;

   --------------------------------------------------------------------
   --  HMI Inputs
   --------------------------------------------------------------------

   MyPump13_Manu_Cmd_On     : Boolean := False;
   Valve_14_Manu_Cmd_Open   : Boolean := False;

   --------------------------------------------------------------------
   --  Internal
   --------------------------------------------------------------------

   Tempo_TON_1      : TON.Instance;
   --  My Tempo TON 1

   Tempo_TON_2      : TON.Instance;
   --  My Tempo TON 2

   TON_2_Q          : Boolean := False;

   Mode_Auto        : Boolean := False;
   --  Working Mode is Auto

   Mode_Manu        : Boolean := False;
   --  Working Mode is Manu

   Level_Transmitter_10_Value : Float    :=  0.0;
   Level_Transmitter_10_Hyst  : Float    :=  5.0;
   Level_Transmitter_10_HHH_T : Float    := 99.0;
   Level_Transmitter_10_HH_T  : Float    := 90.0;
   Level_Transmitter_10_H_T   : Float    := 85.0;
   Level_Transmitter_10_L_T   : Float    := 15.0;
   Level_Transmitter_10_LL_T  : Float    := 10.0;
   Level_Transmitter_10_LLL_T : Float    :=  1.0;

   Level_Transmitter_10_InitDone : Boolean  := False;

   Level_Transmitter_10_XHHH : Boolean  := False;
   Level_Transmitter_10_XHH  : Boolean  := False;
   Level_Transmitter_10_XH   : Boolean  := False;
   Level_Transmitter_10_XL   : Boolean  := False;
   Level_Transmitter_10_XLL  : Boolean  := False;
   Level_Transmitter_10_XLLL : Boolean  := False;

   Valve_14_Condition_Perm : Boolean := False;
   Valve_14_Condition_Auto : Boolean := False;
   Valve_14_Condition_Manu : Boolean := False;

   Valve_14_Auto_Cmd_Open  : Boolean := False;

   Valve_14_Cmd_Open : Boolean := False;
   --  Valve Command

   MyPump13_Condition_Perm   : Boolean := False;
   MyPump13_Condition_Auto   : Boolean := False;
   MyPump13_Condition_Manu   : Boolean := False;

   MyPump13_Auto_Cmd_On  : Boolean := False;

   MyPump13_Cmd_On  : Boolean := False;
   --  Pump Command

   MyPump13_Is_On   : Boolean := False;
   --  Pump Status

   My_PID_1         : PID.Instance;
   --  My PID Controller 1

   Level_Transmitter_10_Thresholds  : Threshold.Instance;
   --  Level_Transmitter_10 Thresholds Box

   --------------------------------------------------------------------
   --  Devices Instances
   --------------------------------------------------------------------

   MyPump13         : Contactor.Instance :=
     Contactor.Create (Id => "Pump13");
   --  Pump Instance

   LS11_AH          : Alarm_Switch.Instance :=
     Alarm_Switch.Create
       (Id         => "LS11",
        TON_Preset => 2000);
   --  Level Alarm Switch Instance

   LS12_AL          : Alarm_Switch.Instance :=
     Alarm_Switch.Create
       (Id         => "LS12",
        TON_Preset => 2000);
   --  Level Alarm Switch Instance

   Valve_14         : Valve.Instance :=
     Valve.Create
       (Id         => "XV14",
        TON_Preset => 5000); -- a slow valve
   --  Valve Instance

end A4A.User_Objects;
