= Ada for Automation Demo Application: 052 a4a_hilscherx_piano
Stéphane LOS
v2022.05, 2022-05-31
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images
:title-logo-image: image:../../../book/images/A4A_logo1-4 TakeOff.png[Logo, pdfwidth=80%]

include::../../book/A4A_Links.asciidoc[]

== Description

=== Ada for Automation

include::../../book/A4A_Description-en.asciidoc[]

=== This demo application

This is a demo application featuring:

* a basic command line interface,

* a basic web user interface making use of Gnoga,

* a kernel managing one Hilscher cifX channel (K6),

* a trivial application that mimics 16 push buttons and 16 LEDs with a web interface.

== Projects diagram

The following picture shows the diagram of projects :

[plantuml, diagram-classes, png]     
----
@startuml

class Shared 		<< Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class LibcifX 	<< Project >>
note left: Hilscher cifX\nlibrary

class Gnoga 		<< Project >>
note left: Ada Web Framework\nDOM Manipulation library

class A4A 		<< Project >>

class A4A_K6 		<< Project >>
note right: Kernel 6\none Hilscher cifX channel

class A4A_HilscherX_Piano_cifX 	<< Project >>
note right: Application with\nCommand Line and WUI Interfaces

Shared - A4A : uses <
A4A <|-- A4A_K6
Shared - A4A_K6 : uses <
Shared_HilscherX -- A4A_K6 : uses <
LibcifX - A4A_K6 : uses <
A4A_K6 <|-- A4A_HilscherX_Piano_cifX
Gnoga - A4A_HilscherX_Piano_cifX : uses <

@enduml
----

include::../A4A_DemoAppsLicence-en.asciidoc[]

== Building

The provided makefile uses {gprbuild} and provides six targets:

* all : builds the executable,

* app_doc : creates the documentation of the source code,

* clean : cleans the space.

Additionally one can generate some documentation using {adoc} with :

* read_me_html : generates the README in HTML format,

* read_me_pdf : generates the README in PDF format,

* read_me : generates the README in both formats.

== Running

Of course, this application is of interest only if a master application is talking to it.

Good candidates are a SCADA or a PLC but, if none is at your disposal, you could use one of :

* 062 a4a-k3-wui,

* your own.

In a console:

Build the application:

`make`

Optionally create the documentation:

`make app_doc`

Run the application:

`make run`

Use Ctrl+C to exit.

Optionally clean all:

`make clean`

== Directories

*bin* +
Where you will find the executable.

*doc* +
The place where {gnatdoc} would create the documentation.

*obj* +
Build artifacts go here.

*src* +
Application source files.

== Application

This is a basic *Ada for Automation* application, managing one Hilscher cifX channel, that mimics 16 push buttons and 16 LEDs with a web interface.

It has a Command Line and Web User Interfaces and communicates with any master using major standard industrial protocols.

The Hilscher cifX channel can be any of a cifX board, comX communication module, or a netX SoC.

The application expects the fieldbus channel to be already configured using SYCON.net.

=== Deployment diagram

[plantuml, diagram-deployment, png]     
----
@startuml

node "HMI Station" as Node0 {
  component "Browser" as N0_Browser <<executable>>
}

node "Controller" as Node1 {
  component "062 a4a-k3-wui" as N1_App <<executable>>
  component "libmodbus" as N1_LibModbus <<library>>
  component "Gnoga" as N1_Gnoga <<library>>
  component "SimpleComponents" as N1_Simple <<library>>
  component "libcifx" as N1_cifX <<library>>
}
note left of Node1: Could also be :\n- your own\n- a SCADA\n- or a PLC

node "Device" as Node2 {
  component "052 a4a_hilscherx_piano" as N2_App <<executable>>
  component "Gnoga" as N2_Gnoga <<library>>
  component "SimpleComponents" as N2_Simple <<library>>
  component "libcifx" as N2_cifX <<library>>
}

N0_Browser -(0- N1_Simple : HTTP/HTTPS
N0_Browser -(0- N2_Simple : HTTP/HTTPS

N1_App -(0- N1_cifX
N1_App -up(0- N1_LibModbus
N1_Gnoga -0)- N1_App
N1_Simple -0)- N1_Gnoga

N2_App -(0- N2_cifX
N2_Gnoga -0)- N2_App
N2_Simple -0)- N2_Gnoga

N1_cifX -ri(0- N2_cifX : fieldbus

@enduml
----

<<<

=== Activity diagram

The Kernel manages the communication channel and provides an interface to it, namely the package "A4A.Memory.Hilscher_Fieldbus1".

[plantuml, diagram-activity-initialization, png]     
----
@startuml

|kernel|
start
partition Initialization {
  :setup;
  :init internal variables;
  :start communication;

  |#AntiqueWhite|application|
  :Cold_Start();

  |#Bisque|user|
  :nothing;

  |kernel|
  (A)
}

@enduml
----

[plantuml, diagram-activity-running, png]     
----
@startuml

|kernel|
partition Running {

  (A)
repeat : Main_Loop

  :check application watchdog;
  :check communication watchdog;
  :get inputs;

  |#AntiqueWhite|application|
  :Main_Cyclic();

  |#Bisque|user|
  :Map_Inputs();
  :Map_Outputs();

  |application|
  :return;

  |kernel|
  :set outputs;
  :housekeeping;

repeat while (Quit ?)

  (B)

}

@enduml
----

[plantuml, diagram-activity-finalization, png]     
----
@startuml

|kernel|
partition Finalization {

  (B)

  |#AntiqueWhite|application|
  :Closing();

  |#Bisque|user|
  :nothing;

  |kernel|
  :stop communication;
  :De-initialization;
}

stop

@enduml
----

<<<

=== Fieldbus Configuration

."./src/a4a-application-configuration.ads"
[source,ada]
----
include::./src/a4a-application-configuration.ads[tag=config]
----

<1> Channel configuration : board name

<2> Channel configuration : channel number

<<<

=== User objects Definition

."./src/a4a-user_objects.ads"
[source,ada]
----
include::./src/a4a-user_objects.ads[tag=objects]
----

<1> An array of 16 Input bits that a fieldbus master can read is defined.

<2> As well, an array of 16 Coils can be written by the master.

<<<

=== User Functions

."./src/a4a-user_functions.adb"
[source,ada]
----
include::./src/a4a-user_functions.adb[tag=functions]
----

User functions are defined to :

<1> get the inputs from the channel,

<2> set channel ouputs.

<<<

=== User Application

."./src/a4a-application.adb"
[source,ada]
----
include::./src/a4a-application.adb[tag=application]
----

The application cyclically :

<1> gets the inputs from the channel,

<2> sets channel ouputs.

=== Web server and User Interface

Hereafter is a diagram showing architecture and information flow for the Web UI.

An article is available, in French though : +
https://slo-ist.fr/ada4automation/a4a-modbus-tcp-server-web-hmi-a4a_piano

image:../../../book/images/A4A-Piano.png[]


