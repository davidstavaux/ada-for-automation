
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.Hilscher_Fieldbus1;
use A4A.Memory;

with A4A.Library.Conversion; use A4A.Library.Conversion;
with A4A.User_Objects; use A4A.User_Objects;

--  tag::functions[]
package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is  --  <1>
   begin

      Byte_To_Booleans (Byte_in       => Hilscher_Fieldbus1.Inputs (1),
                        Boolean_out00 => Coils (0),
                        Boolean_out01 => Coils (1),
                        Boolean_out02 => Coils (2),
                        Boolean_out03 => Coils (3),
                        Boolean_out04 => Coils (4),
                        Boolean_out05 => Coils (5),
                        Boolean_out06 => Coils (6),
                        Boolean_out07 => Coils (7));

      Byte_To_Booleans (Byte_in       => Hilscher_Fieldbus1.Inputs (0),
                        Boolean_out00 => Coils (8),
                        Boolean_out01 => Coils (9),
                        Boolean_out02 => Coils (10),
                        Boolean_out03 => Coils (11),
                        Boolean_out04 => Coils (12),
                        Boolean_out05 => Coils (13),
                        Boolean_out06 => Coils (14),
                        Boolean_out07 => Coils (15));

   end Map_Inputs;

   procedure Map_Outputs is --  <2>
   begin

      Booleans_To_Byte (Boolean_in00 => Input_Bits (0),
                        Boolean_in01 => Input_Bits (1),
                        Boolean_in02 => Input_Bits (2),
                        Boolean_in03 => Input_Bits (3),
                        Boolean_in04 => Input_Bits (4),
                        Boolean_in05 => Input_Bits (5),
                        Boolean_in06 => Input_Bits (6),
                        Boolean_in07 => Input_Bits (7),
                        Byte_out       => Hilscher_Fieldbus1.Outputs (1)
                        );

      Booleans_To_Byte (Boolean_in00 => Input_Bits (8),
                        Boolean_in01 => Input_Bits (9),
                        Boolean_in02 => Input_Bits (10),
                        Boolean_in03 => Input_Bits (11),
                        Boolean_in04 => Input_Bits (12),
                        Boolean_in05 => Input_Bits (13),
                        Boolean_in06 => Input_Bits (14),
                        Boolean_in07 => Input_Bits (15),
                        Byte_out       => Hilscher_Fieldbus1.Outputs (0)
                        );

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

end A4A.User_Functions;
--  end::functions[]
