
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Timers.TON; use A4A.Library.Timers;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Initiate;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Read;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Closed;

with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Initiate_FB;
with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Abort_FB;
with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Read_FB;
with A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2.Closed_FB;

use A4A.Protocols.HilscherX.Profibus_DPM;

package A4A.User_Objects is

   package DPV1C2_Ind_FB renames
     A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2;

   package DPV1C2_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2;

   --------------------------------------------------------------------
   --  User Objects creation
   --------------------------------------------------------------------

   Tempo_TON_1      : TON.Instance;
   --  My Tempo TON 1

   TON_1_Q          : Boolean := False;

   --------------------------------------------------------------------
   --  Inputs from cifX PROFIBUS DPM
   --------------------------------------------------------------------

   LT_Main_Value       : Float := 0.0;
   LT_2nd_Value        : Float := 0.0;
   LT_Main_Status      : Byte  :=   0;
   LT_2nd_Status       : Byte  :=   0;

   --------------------------------------------------------------------
   --  Outputs to cifX PROFIBUS DPM
   --------------------------------------------------------------------

   LT_Display_Value    : Float := 0.0;
   LT_Display_Status   : Byte  :=   0;

   --------------------------------------------------------------------
   --  Test PROFIBUS DPM DP V1 Class 2
   --------------------------------------------------------------------

   DPV1_FB : DPV1.Instance;

   Initiate_FB : DPV1C2_Req_FB.Initiate_FB.Instance;

   Remote_Address : DWord := 2;

   Do_Initiate    : Boolean := False;
   Initiate_Done  : Boolean := False;
   Initiate_Error : Boolean := False;

   Initiate_Cnf_Pos_Data  : DPV1C2.Initiate.PROFIBUS_FSPMM2_INITIATE_CNF_POS_T;

   Abort_FB : DPV1C2_Req_FB.Abort_FB.Instance;

   Do_Abort    : Boolean := False;
   Abort_Done  : Boolean := False;
   Abort_Error : Boolean := False;

   Abort_Cnf_Data  : DPV1C2.Abort_Req.PROFIBUS_FSPMM2_ABORT_CNF_T;

   Read_FB : DPV1C2_Req_FB.Read_FB.Instance;

   --  Software revision from E+H Level Meter
   Slot   : DWord :=  1;
   Index  : DWord := 73;
   Length : DWord := 16;

   Do_Read    : Boolean := False;
   Read_Done  : Boolean := False;
   Read_Error : Boolean := False;

   Read_Cnf_Pos_Data  : DPV1C2.Read.PROFIBUS_FSPMM2_READ_CNF_POS_T;

   Software_Revision : String (1 .. 16);

   DPV1C2_Closed_FB : aliased DPV1C2_Ind_FB.Closed_FB.Instance;

   Closed_Indication_Got : Boolean := False;

   Closed_Indication_Data : DPV1C2.Closed.PROFIBUS_FSPMM2_CLOSED_IND_T;

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Initiate DPV1C2 connection

      X02,
      --  Read data from slave

      X03,
      --  Abort connection

      X04,
      --  Wait for Closed Indication

      X05
      --  Terminate
     );

   Status : Block_Status := X00;

end A4A.User_Objects;
