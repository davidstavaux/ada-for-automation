
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.User_Objects is

   --------------------------------------------------------------------
   --  User Objects creation
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  Inputs
   --------------------------------------------------------------------

   Input_Bits : Bool_Array (0 .. 15) := (others => False);

   --------------------------------------------------------------------
   --  Outputs
   --------------------------------------------------------------------

   Coils      : Bool_Array (0 .. 15) := (others => False);

   --------------------------------------------------------------------
   --  Drive management
   --------------------------------------------------------------------

   Control_Word_Byte0 : Byte := 0;
   Control_Word_Byte1 : Byte := 0;
   Control_Word : Word := 0;

   Status_Word_Byte0 : Byte := 0;
   Status_Word_Byte1 : Byte := 0;
   Status_Word  : Word := 0;

   Operation_Mode_Display : SInt := 0;

   --  Target Velocity in rotations per minute
   Target_Velocity_RPM : DInt := 0;

   --  Target Velocity in Increments per second
   Target_Velocity_INC : DInt := 0;

   Increments_Per_Rotation : constant := 4096;

   Target_Velocity_RPM_Max   : constant := +600;
   Target_Velocity_RPM_Min   : constant := -600;

   --  Target Velocity variation in RPM per cycle
   Target_Velocity_Variation : DInt := 1;

   --  Target Velocity slope : True (+) / False (-)
   Target_Velocity_Slope     : Boolean := True;

   --  Actual Velocity in rotations per minute
   Actual_Velocity_RPM : DInt := 0;

   --  Actual Velocity in Increments per second
   Actual_Velocity_INC_DWord : DWord := 0;
   Actual_Velocity_INC : DInt := 0;

end A4A.User_Objects;
