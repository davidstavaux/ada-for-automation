
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A.Application.Identification; use A4A.Application.Identification;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Kernel.Main;

with A4A.User_Objects; use A4A.User_Objects;

with Gnoga.Gui.Base;
with Gnoga.Server;

with A4A.Web.Web_Server_Config;
with A4A.Web.Pages.General_Status;
with A4A.Web.Pages.cifX_Status;
use A4A.Web.Pages;

package body A4A.Web.Web_Server is

   procedure Connect_Data is
   begin
      IPB1.Set_Model
        (Status_Bit  => Input_Bits (0)'Access);

      IPB3.Set_Model
        (Status_Bit  => Input_Bits (1)'Access);

      for I in Input_IPBs_Byte_0'Range loop
         Input_IPBs_Byte_0 (I).Set_Model
           (Status_Bit  => Input_Bits (I)'Access);
      end loop;

      for I in Input_IPBs_Byte_1'Range loop
         Input_IPBs_Byte_1 (I).Set_Model
           (Status_Bit  => Input_Bits (I + 1 + Input_IPBs_Byte_0'Last)'Access);
      end loop;

      for I in Output_LEDs_Byte_0'Range loop
         Output_LEDs_Byte_0 (I).Set_Model
           (Status_Bit  => Coils (I)'Access);
      end loop;

      for I in Output_LEDs_Byte_1'Range loop
         Output_LEDs_Byte_1 (I).Set_Model
           (Status_Bit  => Coils (I + 1 + Output_LEDs_Byte_0'Last)'Access);
      end loop;

   end Connect_Data;

   task body Default_View_Updater is
      My_Ident : constant String := "A4A.Web.Web_Server.Default_View_Updater";
      Control_Word_Previous : Word  := 0;
      Status_Word_Previous  : Word  := 0;
      Target_Velocity_RPM_Previous  : DInt  := 0;
      Actual_Velocity_RPM_Previous  : DInt  := 0;
      Operation_Mode_Display_Previous  : SInt  := 0;

      function Word_Image (Item : Word) return String;
      function Word_Image (Item : Word) return String
      is
         Buffer1 : String  := "16#0000#";
         Index1  : Integer := Buffer1'Last - 1;  --  Escape last "#"
         Buffer2 : String  := "        ";
      begin
         Word_Text_IO.Put (To => Buffer2, Item => Item, Base => 16);

         for Index2 in reverse Buffer2'First .. Buffer2'Last - 1 loop
            case Buffer2 (Index2) is
               when '0' .. '9' | 'A' .. 'F' =>
                  Buffer1 (Index1) := Buffer2 (Index2);
                  Index1 := Index1 - 1;
               when others =>
                  exit;
            end case;
         end loop;

         return Buffer1;
      end Word_Image;

   begin

      accept Start;

      loop

         App_Data.Connection_Stats_View.Update_View;

         App_Data.IPB1_View.Update_View;
         App_Data.IPB2_View.Update_View;
         App_Data.IPB3_View.Update_View;
         App_Data.IPB4_View.Update_View;

         for I in App_Data.Input_IPBs_View_Byte_0'Range loop
            App_Data.Input_IPBs_View_Byte_0 (I).Update_View;
         end loop;

         for I in App_Data.Input_IPBs_View_Byte_1'Range loop
            App_Data.Input_IPBs_View_Byte_1 (I).Update_View;
         end loop;

         for I in App_Data.Output_LEDs_View_Byte_0'Range loop
            App_Data.Output_LEDs_View_Byte_0 (I).Update_View;
         end loop;

         for I in App_Data.Output_LEDs_View_Byte_1'Range loop
            App_Data.Output_LEDs_View_Byte_1 (I).Update_View;
         end loop;

         if Control_Word /= Control_Word_Previous then
            App_Data.Control_Word_View.Inner_HTML (Word_Image (Control_Word));
            Control_Word_Previous := Control_Word;
         end if;

         if Status_Word /= Status_Word_Previous then
            App_Data.Status_Word_View.Inner_HTML (Word_Image (Status_Word));
            Status_Word_Previous := Status_Word;
         end if;

         if Target_Velocity_RPM /= Target_Velocity_RPM_Previous then
            App_Data.Target_Velocity_RPM_View.Inner_HTML
              (Target_Velocity_RPM'Img);
            Target_Velocity_RPM_Previous := Target_Velocity_RPM;
         end if;

         if Actual_Velocity_RPM /= Actual_Velocity_RPM_Previous then
            App_Data.Actual_Velocity_RPM_View.Inner_HTML
              (Actual_Velocity_RPM'Img);
            Actual_Velocity_RPM_Previous := Actual_Velocity_RPM;
         end if;

         if Operation_Mode_Display /= Operation_Mode_Display_Previous then
            App_Data.Operation_Mode_Display_View.Inner_HTML
              (Operation_Mode_Display'Img);
            Operation_Mode_Display_Previous := Operation_Mode_Display;
         end if;

         select
            accept Stop;
            A4A.Log.Logger.Put (Who       => My_Ident,
                                What      => "I'm dead !",
                                Log_Level => Level_Info);

            Connection_Stats_Controller.Disconnection;
            exit;
         or
            delay 0.1;
         end select;

      end loop;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Default_View_Updater;

   procedure On_Connect_Default
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      App_Data : constant Default_View_App_Data_Access :=
        new Default_View_App_Data_Type;
      View_Updater_Task : Default_View_Updater (App_Data);

      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         App_Data.Side_Nav_Synos_View.Display ("block");
         App_Data.Side_Nav_Synos_View.Top ("0px");
      end Side_Nav_Synos_Open_On_Click;

      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         App_Data.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Close_On_Click;

      procedure Synos_Close;
      procedure Synos_Close is
      begin
         App_Data.Syno1_View.Display ("none");
         App_Data.Syno2_View.Display ("none");
      end Synos_Close;

      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         App_Data.Current_Syno_View.Display ("none");
         App_Data.Current_Syno_View := App_Data.Syno1_View'Access;
         App_Data.Current_Syno_View.Display ("block");
         App_Data.Main_Header_Title_View.Inner_HTML ("Process View");
         App_Data.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno1_On_Click;

      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         App_Data.Current_Syno_View.Display ("none");
         App_Data.Current_Syno_View := App_Data.Syno2_View'Access;
         App_Data.Current_Syno_View.Display ("block");
         App_Data.Main_Header_Title_View.Inner_HTML ("Inputs / Outputs View");
         App_Data.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno2_On_Click;

   begin
      Main_Window.Connection_Data (App_Data);
      App_Data.Main_Window := Main_Window'Unchecked_Access;

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);

      Connection_Stats_Controller.Connection;

      App_Data.View.Create (Main_Window, ID => "main-window-view");
      App_Data.View.Attribute ("style", "display:block");

      App_Data.View.Load_HTML
        (Gnoga.Server.HTML_Directory & "010-top.html", ID => "top");

      App_Data.View.Load_HTML
        (Gnoga.Server.HTML_Directory & "050-main.html", ID => "main-content");

      App_Data.View.Load_HTML
        (Gnoga.Server.HTML_Directory & "099-bottom.html", ID => "bottom");

      App_Data.Header_App_Name_View.Attach_Using_Parent
        (App_Data.View, "header-appname");

      App_Data.Header_App_Name_View.Inner_HTML (Get_Application_Name);

      App_Data.About_Box_App_Name_View.Attach_Using_Parent
        (App_Data.View, "about-box-appname");
      App_Data.About_Box_App_Desc_View.Attach_Using_Parent
        (App_Data.View, "about-box-appdesc");
      App_Data.About_Box_App_Version_View.Attach_Using_Parent
        (App_Data.View, "about-box-appversion");

      App_Data.About_Box_App_Name_View.Inner_HTML (Get_Application_Name);
      App_Data.About_Box_App_Desc_View.Inner_HTML
        (Get_Application_Description);
      App_Data.About_Box_App_Version_View.Inner_HTML (Get_Application_Version);

      App_Data.Side_Nav_Synos_View.Attach_Using_Parent
        (App_Data.View, "side-nav-synos");

      App_Data.Side_Nav_Synos_Open_View.Attach_Using_Parent
        (App_Data.View, "side-nav-synos-open");
      App_Data.Side_Nav_Synos_Open_View.On_Click_Handler
        (Side_Nav_Synos_Open_On_Click'Unrestricted_Access);

      App_Data.Side_Nav_Synos_Close_View.Attach_Using_Parent
        (App_Data.View, "side-nav-synos-close");
      App_Data.Side_Nav_Synos_Close_View.On_Click_Handler
        (Side_Nav_Synos_Close_On_Click'Unrestricted_Access);

      App_Data.Side_Nav_Synos_Syno1_View.Attach_Using_Parent
        (App_Data.View, "side-nav-synos-syno1");
      App_Data.Side_Nav_Synos_Syno1_View.On_Click_Handler
        (Side_Nav_Synos_Syno1_On_Click'Unrestricted_Access);

      App_Data.Side_Nav_Synos_Syno2_View.Attach_Using_Parent
        (App_Data.View, "side-nav-synos-syno2");
      App_Data.Side_Nav_Synos_Syno2_View.On_Click_Handler
        (Side_Nav_Synos_Syno2_On_Click'Unrestricted_Access);

      App_Data.Main_Header_Title_View.Attach_Using_Parent
        (App_Data.View, "main-header-title");

      App_Data.Syno1_View.Attach_Using_Parent
        (App_Data.View, "syno1");

      App_Data.Syno2_View.Attach_Using_Parent
        (App_Data.View, "syno2");

      Synos_Close;
      App_Data.Current_Syno_View := App_Data.Syno1_View'Access;
      App_Data.Current_Syno_View.Display ("block");
      App_Data.Main_Header_Title_View.Inner_HTML ("Process View");

      App_Data.Connection_Stats_View.Setup
        (Parent                             => App_Data.View,
         Current_Connections_Number_View_Id => "current_connections_number",
         Connections_Number_View_Id         => "connections_number",
         Disconnections_Number_View_Id      => "disconnections_number",
         Connections_Number_Max_View_Id     => "connections_number_max",
         Controller                    => Connection_Stats_Controller'Access);

      App_Data.IPB1_View.Set_Controller (IPB1'Unrestricted_Access);
      App_Data.IPB1_View.Attach_Using_Parent (App_Data.View, "led1");
      App_Data.IPB1_View.On_Click_Handler
        (IPB_Views.On_Click'Unrestricted_Access);

      App_Data.IPB2_View.Set_Controller (IPB1'Unrestricted_Access);
      App_Data.IPB2_View.Attach_Using_Parent (App_Data.View, "led2");

      App_Data.IPB3_View.Set_Controller (IPB3'Unrestricted_Access);
      App_Data.IPB3_View.Attach_Using_Parent (App_Data.View, "led3");
      App_Data.IPB3_View.On_Click_Handler
        (IPB_Views.On_Click'Unrestricted_Access);

      App_Data.IPB4_View.Set_Controller (IPB4'Unrestricted_Access);
      App_Data.IPB4_View.Attach_Using_Parent (App_Data.View, "led4");
      App_Data.IPB4_View.On_Mouse_Down_Handler
        (IPB_Views.On_Mouse_Down'Unrestricted_Access);
      App_Data.IPB4_View.On_Mouse_Up_Handler
        (IPB_Views.On_Mouse_Up'Unrestricted_Access);

      for I in App_Data.Input_IPBs_View_Byte_0'Range loop
         App_Data.Input_IPBs_View_Byte_0 (I).Set_Controller
           (Input_IPBs_Byte_0 (I)'Unrestricted_Access);
         App_Data.Input_IPBs_View_Byte_0 (I).Attach_Using_Parent
           (App_Data.View, "input_leds_byte_0_" & Gnoga.Left_Trim (I'Img));
         App_Data.Input_IPBs_View_Byte_0 (I).On_Click_Handler
           (IPB_Views.On_Click'Unrestricted_Access);
      end loop;

      for I in App_Data.Input_IPBs_View_Byte_1'Range loop
         App_Data.Input_IPBs_View_Byte_1 (I).Set_Controller
           (Input_IPBs_Byte_1 (I)'Unrestricted_Access);
         App_Data.Input_IPBs_View_Byte_1 (I).Attach_Using_Parent
           (App_Data.View, "input_leds_byte_1_" & Gnoga.Left_Trim (I'Img));
         App_Data.Input_IPBs_View_Byte_1 (I).On_Click_Handler
           (IPB_Views.On_Click'Unrestricted_Access);
      end loop;

      for I in App_Data.Output_LEDs_View_Byte_0'Range loop
         App_Data.Output_LEDs_View_Byte_0 (I).Set_Controller
           (Output_LEDs_Byte_0 (I)'Unrestricted_Access);
         App_Data.Output_LEDs_View_Byte_0 (I).Attach_Using_Parent
           (App_Data.View, "output_leds_byte_0_" & Gnoga.Left_Trim (I'Img));
      end loop;

      for I in App_Data.Output_LEDs_View_Byte_1'Range loop
         App_Data.Output_LEDs_View_Byte_1 (I).Set_Controller
           (Output_LEDs_Byte_1 (I)'Unrestricted_Access);
         App_Data.Output_LEDs_View_Byte_1 (I).Attach_Using_Parent
           (App_Data.View, "output_leds_byte_1_" & Gnoga.Left_Trim (I'Img));
      end loop;

      App_Data.Control_Word_View.Attach_Using_Parent
        (App_Data.View, "control-word");

      App_Data.Status_Word_View.Attach_Using_Parent
        (App_Data.View, "status-word");

      App_Data.Target_Velocity_RPM_View.Attach_Using_Parent
        (App_Data.View, "target-velocity-rpm");

      App_Data.Actual_Velocity_RPM_View.Attach_Using_Parent
        (App_Data.View, "actual-velocity-rpm");

      App_Data.Operation_Mode_Display_View.Attach_Using_Parent
        (App_Data.View, "operation-mode-display");

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect_Default;

   task body Scanner is
      My_Ident : constant String := "A4A.Web.Web_Server.Scanner";
   begin

      accept Start;

      loop

         IPB1.Update;
         IPB3.Update;

         for I in Input_IPBs_Byte_0'Range loop
            Input_IPBs_Byte_0 (I).Update;
         end loop;

         for I in Input_IPBs_Byte_1'Range loop
            Input_IPBs_Byte_1 (I).Update;
         end loop;

         for I in Output_LEDs_Byte_0'Range loop
            Output_LEDs_Byte_0 (I).Update;
         end loop;

         for I in Output_LEDs_Byte_1'Range loop
            Output_LEDs_Byte_1 (I).Update;
         end loop;

         select
            accept Stop;
            A4A.Log.Logger.Put (Who       => My_Ident,
                                What      => "I'm dead !",
                                Log_Level => Level_Info);
            exit;
         or
            delay 0.1;
         end select;

      end loop;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Scanner;

   task body Main_Task is
      My_Ident : constant String := "A4A.Web.Web_Server.Main_Task";
      Scanner_Task : Scanner;
      package cifX_Page1 is new A4A.Web.Pages.cifX_Status
        (Label => "cifX0 Channel 0 Status");
   begin

      accept Start;

      Gnoga.Application.Multi_Connect.Initialize
        (Event => On_Connect_Default'Access,
         Port  => A4A.Web.Web_Server_Config.Port,
         Boot  => "000-boot.html");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => General_Status.On_Connect'Access,
         Path => "general_status");

      cifX_Page1.Initialise
        (A4A.Kernel.Main.The_Main_Task_Interface.cifX_Drv_Status'Access,
         A4A.Kernel.Main.The_Main_Task_Interface.cifX_Board_Status'Access);

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => cifX_Page1.On_Connect'Unrestricted_Access,
         Path => "cifX_status");

      Gnoga.Application.Title ("Ada for Automation Piano with Gnoga");

      Gnoga.Application.HTML_On_Close
        ("<b>Connection to Application has been terminated</b>");

      Connect_Data;

      Scanner_Task.Start;

      if A4A.Web.Web_Server_Config.Open_URL_Flag then
         Gnoga.Application.Open_URL
           (URL => "http://127.0.0.1:"
            & Gnoga.Left_Trim (A4A.Web.Web_Server_Config.Port'Img));
      end if;

      Gnoga.Application.Multi_Connect.Message_Loop;

      Scanner_Task.Stop;

      Terminated_Flag := True;
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "terminated...",
                          Log_Level => Level_Info);
   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Scanner_Task.Stop;
         Quit;
         Terminated_Flag := True;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Main_Task;

   procedure Quit is
   begin
      Gnoga.Application.Multi_Connect.End_Application;
   end Quit;

   function is_Terminated return Boolean is
   begin
      return Terminated_Flag;
   end is_Terminated;

end A4A.Web.Web_Server;
