
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Types; use Gnoga.Types;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.View;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Web.Controllers.Connection_Stats_Controllers;
with A4A.Web.Controllers.LED_Controllers;
with A4A.Web.Controllers.IPB_Controllers;
use A4A.Web.Controllers;

with A4A.Web.Views.Connection_Stats_Views;
with A4A.Web.Views.LED_Views;
with A4A.Web.Views.IPB_Views;
use A4A.Web.Views;

package A4A.Web.Web_Server is

   task type Main_Task is
      entry Start;
   end Main_Task;

   procedure Quit;

   function is_Terminated return Boolean;

private
   Connection_Stats_Controller : aliased Connection_Stats_Controllers.Instance;

   IPB1 : aliased IPB_Controllers.Instance;
   IPB2 : aliased IPB_Controllers.Instance;
   IPB3 : aliased IPB_Controllers.Instance;
   IPB4 : aliased IPB_Controllers.Instance;

   type IPBs_Byte is array (0 .. 7) of aliased IPB_Controllers.Instance;
   type LEDs_Byte is array (0 .. 7) of aliased LED_Controllers.Instance;

   Input_IPBs_Byte_0  : IPBs_Byte;
   Input_IPBs_Byte_1  : IPBs_Byte;

   Output_LEDs_Byte_0 : LEDs_Byte;
   Output_LEDs_Byte_1 : LEDs_Byte;

   type IPBs_View_Byte is array (0 .. 7) of IPB_Views.Instance;
   type LEDs_View_Byte is array (0 .. 7) of LED_Views.Instance;

   type Default_View_App_Data_Type is new Connection_Data_Type with
      record
         Main_Window : Window.Pointer_To_Window_Class;
         View        : aliased Gnoga.Gui.View.View_Type;

         Header_App_Name_View    : Gnoga.Gui.Element.Element_Type;

         About_Box_App_Name_View    : Gnoga.Gui.Element.Element_Type;
         About_Box_App_Desc_View    : Gnoga.Gui.Element.Element_Type;
         About_Box_App_Version_View : Gnoga.Gui.Element.Element_Type;

         Side_Nav_Synos_View        : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Open_View   : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Close_View  : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Syno1_View  : Gnoga.Gui.Element.Element_Type;
         Side_Nav_Synos_Syno2_View  : Gnoga.Gui.Element.Element_Type;

         Main_Header_Title_View     : Gnoga.Gui.Element.Element_Type;

         Syno1_View        : aliased Gnoga.Gui.Element.Element_Type;
         Syno2_View        : aliased Gnoga.Gui.Element.Element_Type;

         Current_Syno_View : Gnoga.Gui.Element.Element_Access;

         Connection_Stats_View      : Connection_Stats_Views.Instance;

         IPB1_View        : IPB_Views.Instance;
         IPB2_View        : IPB_Views.Instance;
         IPB3_View        : IPB_Views.Instance;
         IPB4_View        : IPB_Views.Instance;

         Input_IPBs_View_Byte_0  : IPBs_View_Byte;
         Input_IPBs_View_Byte_1  : IPBs_View_Byte;

         Output_LEDs_View_Byte_0 : LEDs_View_Byte;
         Output_LEDs_View_Byte_1 : LEDs_View_Byte;

         Control_Word_View : Gnoga.Gui.Element.Element_Type;
         Status_Word_View  : Gnoga.Gui.Element.Element_Type;

         Target_Velocity_RPM_View  : Gnoga.Gui.Element.Element_Type;
         Actual_Velocity_RPM_View  : Gnoga.Gui.Element.Element_Type;
         Operation_Mode_Display_View  : Gnoga.Gui.Element.Element_Type;
      end record;
   type Default_View_App_Data_Access is access all Default_View_App_Data_Type;

   procedure Connect_Data;
   --  The role of this procedure is to connect Controllers to Model data.

   task type Scanner
   is
      entry Start;
      entry Stop;
   end Scanner;
   --  This task will update the Controllers with the Model data.

   task type Default_View_Updater
     (App_Data : Default_View_App_Data_Access)
   is
      entry Start;
      entry Stop;
   end Default_View_Updater;
   --  This task will update the Views from Controllers data.

   procedure On_Connect_Default
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);
   --  For each connection from a browser a view is created holding all View
   --  components. All View components are bound to DOM IDs and corresponding
   --  Controllers. Then, a View_Updater task is started.

   Terminated_Flag : Boolean := False;

end A4A.Web.Web_Server;
