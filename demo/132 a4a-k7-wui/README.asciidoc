= Ada for Automation Demo Application: 132 a4a-k7-wui
Stéphane LOS
v2022.05, 2022-05-31
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images
:title-logo-image: image:../../../book/images/A4A_logo1-4 TakeOff.png[Logo, pdfwidth=80%]

include::../../book/A4A_Links.asciidoc[]

== Description

=== Ada for Automation

include::../../book/A4A_Description-en.asciidoc[]

=== This demo application

This is a demo application featuring:

* a basic command line interface,

* a basic web user interface making use of Gnoga,

* a kernel with Modbus TCP IO Scanning and one Hilscher cifX channel (K7),

* a trivial gateway application that plays with 16 push buttons and 16 LEDs.

This application implements a gateway with Modbus TCP IO Scanning connected to *010 a4a_piano*
 and one Hilscher cifX channel connected to *062 a4a-k3-wui*.

== Projects diagram

The following picture shows the diagram of projects :

[plantuml, diagram-classes, png]     
----
@startuml

class Shared 		<< Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class Gnoga 		<< Project >>
note left: Ada Web Framework\nDOM Manipulation library

class A4A 		<< Project >>

class A4A_K7 		  << Project >>
note right: Kernel 7\nModbus TCP IO Scanning\none Hilscher cifX channel

class A4A_K7_WUI 	<< Project >>
note right: Application with\nCommand Line and WUI Interfaces

Shared - A4A : uses <
A4A <|-- A4A_K7
Shared - A4A_K7 : uses <
Shared_HilscherX - A4A_K7 : uses <
Libmodbus -- A4A_K7 : uses <
LibcifX -- A4A_K7 : uses <
A4A_K7 <|-- A4A_K7_WUI
Gnoga - A4A_K7_WUI : uses <

@enduml
----

include::../A4A_DemoAppsLicence-en.asciidoc[]

== Building

The provided makefile uses {gprbuild} and provides six targets:

* all : builds the executable,

* app_doc : creates the documentation of the source code,

* clean : cleans the space.

Additionally one can generate some documentation using {adoc} with :

* read_me_html : generates the README in HTML format,

* read_me_pdf : generates the README in PDF format,

* read_me : generates the README in both formats.

== Running

This application is meant to play on one hand with *010 a4a_piano* and on the other hand with *062 a4a-k3-wui*.

Of course, it can also play with a physical device of your own.

In a console:

Build the application:

`make`

Optionally create the documentation:

`make app_doc`

Run the application:

`make run`

Use Ctrl+C to exit.

Optionally clean all:

`make clean`

== Directories

*bin* +
Where you will find the executable.

*doc* +
The place where {gnatdoc} would create the documentation.

*obj* +
Build artifacts go here.

*src* +
Application source files.

== Application

This application implements a gateway with Modbus TCP IO Scanning connected to *010 a4a_piano*
 and one Hilscher cifX channel connected to *062 a4a-k3-wui* as fieldbus Master.
It allows to play with the 16 push buttons and 16 LEDs of the Modbus TCP Server from the fieldbus Master application.

It has a Command Line and Web User Interfaces and sends Modbus TCP requests to the server reading Input bits and writing Coils, exchanging those data on the fieldbus interface.

=== Deployment diagram

[plantuml, diagram-deployment, png]     
----
@startuml

node "HMI Station" as Node0 {
  component "Browser" as N0_Browser <<executable>>
}

node "Controller" as Node1 {
  component "062 a4a-k3-wui" as N1_App <<executable>>
  component "libmodbus" as N1_LibModbus <<library>>
  component "Gnoga" as N1_Gnoga <<library>>
  component "SimpleComponents" as N1_Simple <<library>>
  component "libcifx" as N1_cifX <<library>>
}

node "Gateway" as Node2 {
  component "132 a4a-k7-wui" as N2_App <<executable>>
  component "libmodbus" as N2_LibModbus <<library>>
  component "Gnoga" as N2_Gnoga <<library>>
  component "SimpleComponents" as N2_Simple <<library>>
  component "libcifx" as N2_cifX <<library>>
}

node "Server" as Node3 {
  component "010 a4a_piano" as N3_App <<executable>>
  component "libmodbus" as N3_LibModbus <<library>>
  component "Gnoga" as N3_Gnoga <<library>>
  component "SimpleComponents" as N3_Simple <<library>>
}

note top of Node3 : Could also be some hardware.

N0_Browser -(0- N1_Simple : HTTP/HTTPS
N0_Browser -(0- N2_Simple : HTTP/HTTPS
N0_Browser -(0- N3_Simple : HTTP/HTTPS

N1_App -(0- N1_cifX
N1_App -up(0- N1_LibModbus
N1_Gnoga -0)- N1_App
N1_Simple -0)- N1_Gnoga

N2_App -(0- N2_cifX
N2_App -(0- N2_LibModbus
N2_Gnoga -0)- N2_App
N2_Simple -0)- N2_Gnoga

N3_App -(0- N3_LibModbus
N3_Gnoga -0)- N3_App
N3_Simple -0)- N3_Gnoga

N1_cifX -ri(0- N2_cifX : fieldbus

N2_LibModbus -ri(0- N3_LibModbus : Modbus TCP

@enduml
----

<<<

=== Activity diagram

The Kernel manages the communication channels and provides an interface to those, namely the packages "A4A.Memory.MBTCP_IOScan" and "A4A.Memory.Hilscher_Fieldbus1".

[plantuml, diagram-activity-initialization, png]     
----
@startuml

|kernel|
start
partition Initialization {
  :setup;
  :init internal variables;
  :start communication;

  |#AntiqueWhite|application|
  :Cold_Start();

  |#Bisque|user|
  :nothing;

  |kernel|
  (A)
}

@enduml
----

[plantuml, diagram-activity-running, png]     
----
@startuml

|kernel|
partition Running {

  (A)
repeat : Main_Loop

  :check application watchdog;
  :check communication watchdog;
  :get inputs;

  |#AntiqueWhite|application|
  :Main_Cyclic();

  |#Bisque|user|
  :Map_MB_Inputs();
  :Map_FB_Inputs();
  :Process_IO();
  :Map_MB_Outputs();
  :Map_FB_Outputs();

  |#AntiqueWhite|application|
  :return;

  |kernel|
  :set outputs;
  :housekeeping;

repeat while (Quit ?)

  (B)

}

@enduml
----

[plantuml, diagram-activity-finalization, png]     
----
@startuml

|kernel|
partition Finalization {

  (B)

  |#AntiqueWhite|application|
  :Closing();

  |#Bisque|user|
  :nothing;

  |kernel|
  :stop communication;
  :De-initialization;
}

stop

@enduml
----

<<<

=== Modbus TCP IO Scanning Configuration

For each Modbus TCP Server define one client configuration task and declare all clients configurations in the array

:sourcedir: .

."{sourcedir}/src/a4a-application-mbtcp_clients_config.ads"
[source,ada]
----
include::{sourcedir}/src/a4a-application-mbtcp_clients_config.ads[tag=config]
----

<1> Modbus TCP Server IP Address : 127.0.0.1 (localhost)

<2> Modbus TCP Server port : 1504 (A4A_Piano)

<3> Commands number : 2 (since we declare two commands in the array)

<4> Commands array : one read and one write commands

<5> Configurations array : add our configuration

<<<

=== Fieldbus Configuration

:sourcedir: .

."{sourcedir}/src/cifx/a4a-application-configuration.ads"
[source,ada]
----
include::{sourcedir}/src/cifx/a4a-application-configuration.ads[tag=config]
----

<1> Board name or Alias of Hilscher board : "cifX0"

<<<

=== User objects Definition

:sourcedir: .

."{sourcedir}/src/a4a-user_objects.ads"
[source,ada]
----
include::{sourcedir}/src/a4a-user_objects.ads[tag=objects]
----

<1> 8 Input bits are read that form the Command byte, which is also output in 8 Coils.

<2> 8 Input bits are read that form the Pattern byte.

<3> 8 Coils are written that reflect the Output byte.

<<<

=== User Functions

."{sourcedir}/src/a4a-user_functions.adb"
[source,ada]
----
include::{sourcedir}/src/a4a-user_functions.adb[tag=functions]
----

User functions are defined to :

<1> get the inputs from the IO Scanner,

<2> set IO Scanner ouputs,

<3> get the inputs from the Fieldbus,

<4> set the Fieldbus ouputs,

<5> process the data (nothing to do).

<<<

=== User Application

."{sourcedir}/src/a4a-application.adb"
[source,ada]
----
include::{sourcedir}/src/a4a-application.adb[tag=application]
----

The application cyclically :

<1> gets the inputs from the IO Scanner,

<2> get the inputs from the Fieldbus,

<3> processes the data (nothing to do),

<4> sets IO Scanner ouputs,

<5> set the Fieldbus ouputs.


