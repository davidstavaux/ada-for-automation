
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.MBTCP_IOScan;
with A4A.Memory.Hilscher_Fieldbus1;
use A4A.Memory;

with A4A.Library.Conversion; use A4A.Library.Conversion;
with A4A.User_Objects; use A4A.User_Objects;

--  tag::functions[]
package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_MB_Inputs is  --  <1>
   begin

      Booleans_To_Byte (Boolean_in00 => MBTCP_IOScan.Bool_Inputs (0),
                        Boolean_in01 => MBTCP_IOScan.Bool_Inputs (1),
                        Boolean_in02 => MBTCP_IOScan.Bool_Inputs (2),
                        Boolean_in03 => MBTCP_IOScan.Bool_Inputs (3),
                        Boolean_in04 => MBTCP_IOScan.Bool_Inputs (4),
                        Boolean_in05 => MBTCP_IOScan.Bool_Inputs (5),
                        Boolean_in06 => MBTCP_IOScan.Bool_Inputs (6),
                        Boolean_in07 => MBTCP_IOScan.Bool_Inputs (7),
                        Byte_out     => Cmd_Byte_In);

      Booleans_To_Byte (Boolean_in00 => MBTCP_IOScan.Bool_Inputs (8),
                        Boolean_in01 => MBTCP_IOScan.Bool_Inputs (9),
                        Boolean_in02 => MBTCP_IOScan.Bool_Inputs (10),
                        Boolean_in03 => MBTCP_IOScan.Bool_Inputs (11),
                        Boolean_in04 => MBTCP_IOScan.Bool_Inputs (12),
                        Boolean_in05 => MBTCP_IOScan.Bool_Inputs (13),
                        Boolean_in06 => MBTCP_IOScan.Bool_Inputs (14),
                        Boolean_in07 => MBTCP_IOScan.Bool_Inputs (15),
                        Byte_out     => Pattern_Byte);

   end Map_MB_Inputs;

   procedure Map_MB_Outputs is --  <2>
   begin

      Byte_To_Booleans (Byte_in       => Cmd_Byte_Out,
                        Boolean_out00 => MBTCP_IOScan.Bool_Outputs (0),
                        Boolean_out01 => MBTCP_IOScan.Bool_Outputs (1),
                        Boolean_out02 => MBTCP_IOScan.Bool_Outputs (2),
                        Boolean_out03 => MBTCP_IOScan.Bool_Outputs (3),
                        Boolean_out04 => MBTCP_IOScan.Bool_Outputs (4),
                        Boolean_out05 => MBTCP_IOScan.Bool_Outputs (5),
                        Boolean_out06 => MBTCP_IOScan.Bool_Outputs (6),
                        Boolean_out07 => MBTCP_IOScan.Bool_Outputs (7));

      Byte_To_Booleans (Byte_in       => Output_Byte,
                        Boolean_out00 => MBTCP_IOScan.Bool_Outputs (8),
                        Boolean_out01 => MBTCP_IOScan.Bool_Outputs (9),
                        Boolean_out02 => MBTCP_IOScan.Bool_Outputs (10),
                        Boolean_out03 => MBTCP_IOScan.Bool_Outputs (11),
                        Boolean_out04 => MBTCP_IOScan.Bool_Outputs (12),
                        Boolean_out05 => MBTCP_IOScan.Bool_Outputs (13),
                        Boolean_out06 => MBTCP_IOScan.Bool_Outputs (14),
                        Boolean_out07 => MBTCP_IOScan.Bool_Outputs (15));

   end Map_MB_Outputs;

   procedure Map_FB_Inputs is --  <3>
   begin

      Cmd_Byte_Out := Hilscher_Fieldbus1.Inputs (1);
      Output_Byte  := Hilscher_Fieldbus1.Inputs (0);

   end Map_FB_Inputs;

   procedure Map_FB_Outputs is --  <4>
   begin

      Hilscher_Fieldbus1.Outputs (1) := Cmd_Byte_In;
      Hilscher_Fieldbus1.Outputs (0) := Pattern_Byte;

   end Map_FB_Outputs;

   procedure Process_IO is --  <5>

   begin

      null;

   end Process_IO;

end A4A.User_Functions;
--  end::functions[]
