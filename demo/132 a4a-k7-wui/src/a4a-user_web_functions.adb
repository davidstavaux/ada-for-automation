
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Conversion; use A4A.Library.Conversion;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Web_Objects; use A4A.User_Web_Objects;

package body A4A.User_Web_Functions is

   --------------------------------------------------------------------
   --  User Web functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
   begin

      Byte_To_Booleans (Byte_in       => Cmd_Byte_In,
                        Boolean_out00 => Input_Bits (0),
                        Boolean_out01 => Input_Bits (1),
                        Boolean_out02 => Input_Bits (2),
                        Boolean_out03 => Input_Bits (3),
                        Boolean_out04 => Input_Bits (4),
                        Boolean_out05 => Input_Bits (5),
                        Boolean_out06 => Input_Bits (6),
                        Boolean_out07 => Input_Bits (7));

      Byte_To_Booleans (Byte_in       => Pattern_Byte,
                        Boolean_out00 => Input_Bits (8),
                        Boolean_out01 => Input_Bits (9),
                        Boolean_out02 => Input_Bits (10),
                        Boolean_out03 => Input_Bits (11),
                        Boolean_out04 => Input_Bits (12),
                        Boolean_out05 => Input_Bits (13),
                        Boolean_out06 => Input_Bits (14),
                        Boolean_out07 => Input_Bits (15));

   end Map_Inputs;

   procedure Map_Outputs is
   begin

      Byte_To_Booleans (Byte_in       => Cmd_Byte_Out,
                        Boolean_out00 => Coils (0),
                        Boolean_out01 => Coils (1),
                        Boolean_out02 => Coils (2),
                        Boolean_out03 => Coils (3),
                        Boolean_out04 => Coils (4),
                        Boolean_out05 => Coils (5),
                        Boolean_out06 => Coils (6),
                        Boolean_out07 => Coils (7));

      Byte_To_Booleans (Byte_in       => Output_Byte,
                        Boolean_out00 => Coils (8),
                        Boolean_out01 => Coils (9),
                        Boolean_out02 => Coils (10),
                        Boolean_out03 => Coils (11),
                        Boolean_out04 => Coils (12),
                        Boolean_out05 => Coils (13),
                        Boolean_out06 => Coils (14),
                        Boolean_out07 => Coils (15));

   end Map_Outputs;

end A4A.User_Web_Functions;
