var MW0_Gauge = new JustGage({
        id: "MW0-gauge",
        label: "units",
        value: 0,
        min: -32768,
        max: 32767,
        decimals: 0,
        gaugeWidthScale: 0.6
});

var DB1_DBW0_Gauge = new JustGage({
        id: "DB1-DBW0-gauge",
        label: "units",
        value: 0,
        min: -32768,
        max: 32767,
        decimals: 0,
        gaugeWidthScale: 0.6
});

var IW0_Gauge = new JustGage({
        id: "IW0-gauge",
        label: "units",
        value: 0,
        min: -32768,
        max: 32767,
        decimals: 0,
        gaugeWidthScale: 0.6
});

var QW0_Gauge = new JustGage({
        id: "QW0-gauge",
        label: "units",
        value: 0,
        min: -32768,
        max: 32767,
        decimals: 0,
        gaugeWidthScale: 0.6
});
