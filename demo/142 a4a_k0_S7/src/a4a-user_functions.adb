
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.MBTCP_IOServer;
use A4A.Memory;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Protocols.LibSnap7; use A4A.Protocols.LibSnap7;
with Interfaces.C; use Interfaces.C;
with A4A.Application.S7_Client_Config;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.Library.Conversion; use A4A.Library.Conversion;

--  tag::functions[]
package body A4A.User_Functions is
   My_Ident : constant String := "A4A.User_Functions";

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is  --  <1>
   begin

      null;

   end Map_Inputs;

   procedure Map_Outputs is --  <2>
   begin

      MBTCP_IOServer.Input_Registers (0) := MW0;
      MBTCP_IOServer.Input_Registers (1) := MW2;
      MBTCP_IOServer.Input_Registers (2) := MW4;

      DWord_To_Words (DWord_in => MD8,
                      LSW_Word => MBTCP_IOServer.Input_Registers (4),
                      MSW_Word => MBTCP_IOServer.Input_Registers (3));

      MBTCP_IOServer.Input_Registers (5) := IW0;
      MBTCP_IOServer.Input_Registers (6) := QW0;
      MBTCP_IOServer.Input_Registers (7) := DB1_W0;

      MBTCP_IOServer.Input_Registers (8) := T1;
      MBTCP_IOServer.Input_Registers (9) := C1;
   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

   procedure S7Client_On_Start is --  <3>
   begin

      Client := Cli_Create;

   end S7Client_On_Start;

   procedure S7Client_On_Stop is --  <4>
   begin

      if Connected then
         Result := Cli_Disconnect (Client => Client);

         if Result = 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_On_Stop",
               What      => "Disconnected !",
               Log_Level => Level_Info);

            Connected := False;
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_On_Stop",
               What      => "Not Disconnected ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);
         end if;

      end if;

      Cli_Destroy (Client'Access);

   end S7Client_On_Stop;

   procedure S7Client_On_Error is --  <5>
   begin

      if Connected then
         Result := Cli_Disconnect (Client => Client);

         if Result = 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_On_Error",
               What      => "Disconnected !",
               Log_Level => Level_Info);

            Connected := False;
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_On_Error",
               What      => "Not Disconnected ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);
         end if;

      end if;

   end S7Client_On_Error;

   procedure S7Client_Test is --  <6>
      use A4A.Application.S7_Client_Config;
   begin

      Result := Cli_GetConnected (Client      => Client,
                                  IsConnected => IsConnected);

      if Result /= 0 then
         A4A.Log.Logger.Put
           (Who       => My_Ident & ".S7Client_Test",
            What      => "Cli_GetConnected "
            & "Result = " & Cli_Error_Text (Error => Result),
            Log_Level => Level_Error);
      end if;

      if not Connected then
         Result := Cli_ConnectTo
           (Client     => Client,
            IP_Address => To_String (Server_IP_Address),
            Rack       => CPU_Rack,
            Slot       => CPU_Slot);

         if Result = 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Connected !",
               Log_Level => Level_Info);

            Connected := True;
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Not Connected ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected then

         Result := Cli_GetPlcStatus (Client  => Client, Status => Status);

         if Result = 0 then
            Running := (Status = 16#08#);
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Cli_GetPlcStatus failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected and not Order_Code_Got then

         Cli_Get_Order_Code (Client     => Client,
                             Order_Code => Order_Code,
                             Result     => Result);

         if Result = 0 then
            Order_Code_Got := True;
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Cli_Get_Order_Code failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected and not CPU_Info_Got then

         Cli_Get_Cpu_Info (Client   => Client,
                           Cpu_Info => CPU_Info,
                           Result   => Result);

         if Result = 0 then
            CPU_Info_Got := True;
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Cli_Get_Cpu_Info : Serial_Number : "
               & CPU_Info.Serial_Number,
               Log_Level => Level_Info);
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".S7Client_Test",
               What      => "Cli_Get_Cpu_Info failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      Test_Mementos;

      Test_Inputs;

      Test_Outputs;

      Test_DBs;

      Test_Timers;

      Test_Counters;

   end S7Client_Test;

   --------------------------------------------------------------------
   --  Mementos Read / Write tests
   --------------------------------------------------------------------

   procedure Test_Mementos is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_MBRead (Client  => Client,
                               Start   => 0,
                               Size    => MyUsrDataIn'Length,
                               UsrData => MyUsrDataIn);

         if Result = 0 then

            Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                           MSB_Byte => MyUsrDataIn (0),
                           Word_out => MW0);

            Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                           MSB_Byte => MyUsrDataIn (2),
                           Word_out => MW2);

            Bytes_To_Word (LSB_Byte => MyUsrDataIn (5),
                           MSB_Byte => MyUsrDataIn (4),
                           Word_out => MW4);

            Bytes_To_DWord (Byte0 => MyUsrDataIn (11),
                            Byte1 => MyUsrDataIn (10),
                            Byte2 => MyUsrDataIn (9),
                            Byte3 => MyUsrDataIn (8),
                            DWord_out => MD8);

            MD8F := DWord_To_Float (MD8);

         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Mementos",
               What      => "Read Mementos failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected then

         MyUsrDataOut (0 .. 3) := MyUsrDataIn (8 .. 11);

         Result := Cli_MBWrite (Client  => Client,
                                Start   => MyUsrDataIn'Length,
                                Size    => MyUsrDataOut'Length,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Mementos",
               What      => "Write Mementos failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected and MW4_Write then

         Word_To_Bytes (Word_in  => MW4_New,
                        LSB_Byte => MyUsrDataOut (1),
                        MSB_Byte => MyUsrDataOut (0));

         Result := Cli_MBWrite (Client  => Client,
                                Start   => 4,
                                Size    => 2,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Mementos",
               What      => "Write MW4 failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         else
            MW4_Write := False;
         end if;

      end if;

      if Connected and MD8F_Write then

         DWord_To_Bytes (DWord_in  => Float_To_DWord (MD8F_New),
                         Byte0 => MyUsrDataOut (3),
                         Byte1 => MyUsrDataOut (2),
                         Byte2 => MyUsrDataOut (1),
                         Byte3 => MyUsrDataOut (0));

         Result := Cli_MBWrite (Client  => Client,
                                Start   => 8,
                                Size    => 4,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Mementos",
               What      => "Write MD8F failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         else
            MD8F_Write := False;
         end if;

      end if;

   end Test_Mementos;

   --------------------------------------------------------------------
   --  Inputs Read / Write tests
   --------------------------------------------------------------------

   procedure Test_Inputs is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_EBRead (Client  => Client,
                               Start   => 0,
                               Size    => MyUsrDataIn'Length,
                               UsrData => MyUsrDataIn);

         if Result = 0 then
            Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                           MSB_Byte => MyUsrDataIn (0),
                           Word_out => IW0);

            MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Inputs",
               What      => "Read Inputs failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected then

         Result := Cli_EBWrite (Client  => Client,
                                Start   => 10,
                                Size    => MyUsrDataOut'Length,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Inputs",
               What      => "Write Inputs failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

   end Test_Inputs;

   --------------------------------------------------------------------
   --  Outputs Read / Write tests
   --------------------------------------------------------------------

   procedure Test_Outputs is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_ABRead (Client  => Client,
                               Start   => 0,
                               Size    => MyUsrDataIn'Length,
                               UsrData => MyUsrDataIn);

         if Result = 0 then
            Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                           MSB_Byte => MyUsrDataIn (0),
                           Word_out => QW0);

            MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Outputs",
               What      => "Read Outputs failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected then

         Result := Cli_ABWrite (Client  => Client,
                                Start   => 10,
                                Size    => MyUsrDataOut'Length,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Outputs",
               What      => "Write Outputs failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

   end Test_Outputs;

   --------------------------------------------------------------------
   --  Data Blocks Read / Write tests
   --------------------------------------------------------------------

   procedure Test_DBs is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_DBRead (Client   => Client,
                               DBNumber => 1,
                               Start    => 0,
                               Size     => MyUsrDataIn'Length,
                               UsrData  => MyUsrDataIn);

         if Result = 0 then
            Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                           MSB_Byte => MyUsrDataIn (0),
                           Word_out => DB1_W0);

            MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_DBs",
               What      => "Read Data Block failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected then

         Result := Cli_DBWrite (Client   => Client,
                                DBNumber => 1,
                                Start    => 10,
                                Size     => MyUsrDataOut'Length,
                                UsrData  => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_DBs",
               What      => "Write Data Block failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

   end Test_DBs;

   --------------------------------------------------------------------
   --  Timers Read / Write tests
   --------------------------------------------------------------------

   procedure Test_Timers is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_TMRead (Client  => Client,
                               Start   => 0,
                               Amount  => MyUsrDataIn'Length / 2,
                               UsrData => MyUsrDataIn);

         if Result = 0 then
            Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                           MSB_Byte => MyUsrDataIn (2),
                           Word_out => T1);

            MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
            MyUsrDataOut (3) := MyUsrDataIn (3) and 16#F0#; -- Let's play !
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Timers",
               What      => "Read Timers failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected and False then

         Result := Cli_TMWrite (Client  => Client,
                                Start   => 0,
                                Amount  => MyUsrDataOut'Length / 2,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Timers",
               What      => "Write Timers failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

   end Test_Timers;

   --------------------------------------------------------------------
   --  Counters Read / Write tests
   --------------------------------------------------------------------

   procedure Test_Counters is

   begin

      if Connected then

         MyUsrDataIn := (others => 0);

         Result := Cli_CTRead (Client  => Client,
                               Start   => 0,
                               Amount  => MyUsrDataIn'Length / 2,
                               UsrData => MyUsrDataIn);

         if Result = 0 then
            Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                           MSB_Byte => MyUsrDataIn (2),
                           Word_out => C1);

            MyUsrDataOut (0 .. 2) := MyUsrDataIn (0 .. 2);
            MyUsrDataOut (3) := MyUsrDataIn (3) and 16#F0#; -- Let's play !
         else
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Counters",
               What      => "Read Counters failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

      if Connected and False then

         Result := Cli_CTWrite (Client  => Client,
                                Start   => 0,
                                Amount  => MyUsrDataOut'Length / 2,
                                UsrData => MyUsrDataOut);

         if Result /= 0 then
            A4A.Log.Logger.Put
              (Who       => My_Ident & ".Test_Counters",
               What      => "Write Counters failed ! "
               & "Result = " & Cli_Error_Text (Error => Result),
               Log_Level => Level_Error);

            S7Client_On_Error;
         end if;

      end if;

   end Test_Counters;

end A4A.User_Functions;
--  end::functions[]
