
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  tag::objects[]
with A4A.Protocols.LibSnap7; use A4A.Protocols.LibSnap7;

with Interfaces.C; use Interfaces.C;

package A4A.User_Objects is

   --------------------------------------------------------------------
   --  User Objects creation
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  S7 Communication
   --------------------------------------------------------------------

   Client       : aliased S7Object;
   Result       : C.int := 0;
   IsConnected  : C.int := 0;
   Connected    : Boolean := False;
   Status       : C.int := 0;
   Running      : Boolean := False;

   Order_Code   : Order_Code_Type;        --  <1>
   Order_Code_Got : Boolean := False;

   CPU_Info     : CPU_Info_Type;          --  <2>
   CPU_Info_Got : Boolean := False;

   MyUsrDataIn  : aliased Byte_Array (0 .. 19) := (others => 0);
   --  Input Data Buffer

   MyUsrDataOut : Byte_Array (0 .. 19) := (others => 0);
   --  Output Data Buffer

   MW0          : Word  := 0;             --  <3>
   MW2          : Word  := 0;
   MW4          : Word  := 0;
   MD8          : DWord := 0;
   MD8F         : Float := 0.0;
   IW0          : Word  := 0;
   QW0          : Word  := 0;
   DB1_W0       : Word  := 0;
   T1           : Word  := 0;
   C1           : Word  := 0;

   MW4_New      : Word  := 0;
   MW4_Write    : Boolean := False;

   MD8F_New     : Float := 0.0;
   MD8F_Write   : Boolean := False;
   --  S7 PLC Data Read / Write

end A4A.User_Objects;
--  end::objects[]
