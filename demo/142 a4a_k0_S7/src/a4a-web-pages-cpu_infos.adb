
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.User_Objects; use A4A.User_Objects;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Application.S7_Client_Config;

package body A4A.Web.Pages.CPU_Infos is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      if Order_Code_Got /= Web_Page.CPU_Info_View.Order_Code_Got then
         Web_Page.CPU_Info_View.Order_Code_Code_View.Inner_HTML
           (Order_Code.Code);

         Web_Page.CPU_Info_View.Order_Code_Version_View.Inner_HTML
           ("V" & Order_Code.Version_V1'Img
            & "."  & Order_Code.Version_V2'Img
            & "."  & Order_Code.Version_V3'Img);

         Web_Page.CPU_Info_View.Order_Code_Got := Order_Code_Got;
      end if;

      if CPU_Info_Got /= Web_Page.CPU_Info_View.CPU_Info_Got then
         Web_Page.CPU_Info_View.CPU_Info_Module_Type_Name_View.Inner_HTML
           (CPU_Info.Module_Type_Name);
         Web_Page.CPU_Info_View.CPU_Info_Serial_Number_View.Inner_HTML
           (CPU_Info.Serial_Number);
         Web_Page.CPU_Info_View.CPU_Info_AS_Name_View.Inner_HTML
           (CPU_Info.AS_Name);
         Web_Page.CPU_Info_View.CPU_Info_Copyright_View.Inner_HTML
           (CPU_Info.Copyright);
         Web_Page.CPU_Info_View.CPU_Info_Module_Name_View.Inner_HTML
           (CPU_Info.Module_Name);

         Web_Page.CPU_Info_View.CPU_Info_Got := CPU_Info_Got;
      end if;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.CPU_Info_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "053-cpu-info.html");

      Web_Page.My_Connection_ID :=
        Web_Page.Common_View.Main_Window.Connection_ID;

      Web_Page.CPU_Info_View.IPAddress_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-ipaddress");
      Web_Page.CPU_Info_View.IPAddress_View.Inner_HTML
        (To_String (A4A.Application.S7_Client_Config.Server_IP_Address));

      Web_Page.CPU_Info_View.Port_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-port");
      Web_Page.CPU_Info_View.Port_View.Inner_HTML
        (A4A.Application.S7_Client_Config.Server_TCP_Port'Img);

      Web_Page.CPU_Info_View.Rack_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-rack");
      Web_Page.CPU_Info_View.Rack_View.Inner_HTML
        (A4A.Application.S7_Client_Config.CPU_Rack'Img);

      Web_Page.CPU_Info_View.Slot_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-slot");
      Web_Page.CPU_Info_View.Slot_View.Inner_HTML
        (A4A.Application.S7_Client_Config.CPU_Slot'Img);

      Web_Page.CPU_Info_View.Order_Code_Code_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "order-code-code");

      Web_Page.CPU_Info_View.Order_Code_Version_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "order-code-version");

      Web_Page.CPU_Info_View.CPU_Info_Module_Type_Name_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cpuinfo-moduletype");

      Web_Page.CPU_Info_View.CPU_Info_Serial_Number_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cpuinfo-serialnumber");

      Web_Page.CPU_Info_View.CPU_Info_AS_Name_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cpuinfo-asname");

      Web_Page.CPU_Info_View.CPU_Info_Copyright_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cpuinfo-copyright");

      Web_Page.CPU_Info_View.CPU_Info_Module_Name_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cpuinfo-module");

      delay 0.1;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.CPU_Infos;
