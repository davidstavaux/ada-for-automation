
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

package A4A.Web.Pages.CPU_Infos is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   type CPU_Info_View_Type is
      record
         IPAddress_View  : Gnoga.Gui.Element.Element_Type;
         Port_View       : Gnoga.Gui.Element.Element_Type;
         Rack_View       : Gnoga.Gui.Element.Element_Type;
         Slot_View       : Gnoga.Gui.Element.Element_Type;

         Order_Code_Code_View        : Gnoga.Gui.Element.Element_Type;
         Order_Code_Version_View     : Gnoga.Gui.Element.Element_Type;
         Order_Code_Got : Boolean := False;

         CPU_Info_Module_Type_Name_View   : Gnoga.Gui.Element.Element_Type;
         CPU_Info_Serial_Number_View      : Gnoga.Gui.Element.Element_Type;
         CPU_Info_AS_Name_View            : Gnoga.Gui.Element.Element_Type;
         CPU_Info_Copyright_View          : Gnoga.Gui.Element.Element_Type;
         CPU_Info_Module_Name_View        : Gnoga.Gui.Element.Element_Type;
         CPU_Info_Got : Boolean := False;

      end record;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;
         CPU_Info_View : CPU_Info_View_Type;

         My_Connection_ID : Gnoga.Types.Connection_ID;
      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

end A4A.Web.Pages.CPU_Infos;
