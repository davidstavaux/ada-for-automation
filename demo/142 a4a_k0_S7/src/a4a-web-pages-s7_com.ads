
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Base;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;
with Gnoga.Gui.Element.Form;

with A4A.Web.Elements.Bool_Elements;
with A4A.Web.Elements.Float_Elements;
with A4A.Web.Elements.DWord_Elements;
with A4A.Web.Elements.Word_Elements;
use A4A.Web.Elements;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

package A4A.Web.Pages.S7_Com is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   type S7_Com_View_Type is
      record
         Connection_View : Bool_Elements.Instance;
         Running_View    : Bool_Elements.Instance;

         MW0_View        : Gnoga.Gui.Element.Element_Type;
         MW2_View        : Word_Elements.Instance;
         MW4_View        : Word_Elements.Instance;
         MD8_View        : DWord_Elements.Instance;
         MD8F_View       : Float_Elements.Instance;

         IW0_View        : Word_Elements.Instance;
         QW0_View        : Word_Elements.Instance;

         DB1_W0_B10_View : Word_Elements.Instance;
         DB1_W0_B16_View : Word_Elements.Instance;
         DB1_W0_B02_View : Word_Elements.Instance;

         T1_View         : Gnoga.Gui.Element.Element_Type;
         C1_View         : Gnoga.Gui.Element.Element_Type;

         MW4_Slider      : Gnoga.Gui.Element.Form.Range_Type;
         MW4_Number      : Gnoga.Gui.Element.Form.Number_Type;

         MD8F_Slider     : Gnoga.Gui.Element.Form.Range_Type;
         MD8F_Number     : Gnoga.Gui.Element.Form.Number_Type;
         MD8F_Meter      : Gnoga.Gui.Element.Form.Number_Type;
         MD8F_Number2    : Gnoga.Gui.Element.Form.Number_Type;

         MW0          : Word  := 0;
         MW4          : Word  := 0;
         MD8F         : Float := 0.0;

         T1           : Word  := 0;
         C1           : Word  := 0;
         --  S7 PLC Data Read / Write
      end record;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;
         S7_Com_View : S7_Com_View_Type;

         My_Connection_ID : Gnoga.Types.Connection_ID;
      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

   procedure BCD_To_DEC (bcd_val : in Word;
                         dec_val : out Word);
   --  Convert a Binary Coded Decimal value to a Decimal one

   function S5Time_Image (s5time : Word) return String;

   procedure On_Change_Range (Object : in out Gnoga.Gui.Base.Base_Type'Class);

   procedure On_Change_Number (Object : in out Gnoga.Gui.Base.Base_Type'Class);

   procedure On_Change_Range_MD8F
     (Object : in out Gnoga.Gui.Base.Base_Type'Class);

   procedure On_Change_Number_MD8F
     (Object : in out Gnoga.Gui.Base.Base_Type'Class);

end A4A.Web.Pages.S7_Com;
