
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2020, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------

with Ada.Text_IO;

package body A4A.Dual_Port_Memory.DPMI is

   protected body Dual_Port_Memory is

      -- <<<---------
      procedure Inputs_Write (Inputs : in Byte_Array;
                              Offset : in Natural) is
         Inputs_Length : Integer := Inputs'Length;
         Inputs_First  : Integer := Inputs'First;
         Inputs_Last   : Integer := Inputs'Last;
         Data_In_First : Integer := Data_In'First;
         Data_In_Last  : Integer := Data_In'Last;
         Index : Natural := 0;

      begin
         Flag_New_Inputs := True;
         Ada.Text_IO.Put_Line("DPM.Inputs_Write :");
         Ada.Text_IO.Put_Line("Inputs'Length : " & Inputs_Length'Img);
         Ada.Text_IO.Put_Line("Inputs'First  : " & Inputs_First'Img);
         Ada.Text_IO.Put_Line("Inputs'Last   : " & Inputs_Last'Img);
         Ada.Text_IO.Put_Line("Data_In'First  : " & Data_In_First'Img);
         Ada.Text_IO.Put_Line("Data_In'Last   : " & Data_In_Last'Img);
         Ada.Text_IO.Put_Line("Offset : " & Offset'Img);

--           Data_In (Offset ..(Offset + Inputs'Length - 1)) := Inputs(Inputs'First..Inputs'Last);
--           Data_In (Offset..(Offset + Inputs'Length - 1)) := Inputs;
         loop
            Data_In (Offset + Index) := Inputs(Inputs'First + Index);
            Index := Index + 1;
            exit when Index > Inputs'Last;
         end loop;

         Ada.Text_IO.Put_Line("Data_In (0) : " & Data_In (0)'Img);
         Ada.Text_IO.Put_Line("Data_In (Offset) : " & Data_In (Offset)'Img);

      end Inputs_Write;

      procedure Get_Outputs (Outputs : out Byte_Array;
                             Offset  : in Natural) is
      begin
         Ada.Text_IO.Put_Line("DPM.Get_Outputs");
         Flag_New_Outputs := False;
--           Outputs (Outputs'First..Outputs'Last) := Data_Out (Offset..Outputs'Length - 1);
         Outputs (Outputs'First..Outputs'Last) := Data_Out (0..Outputs'Length - 1);
      end Get_Outputs;

      procedure New_Outputs (Status : out Boolean) is
      begin
         Ada.Text_IO.Put_Line("DPM.New_Outputs");
         Status := Flag_New_Outputs;
      end New_Outputs;

      -- --------->>>
      procedure Outputs_Write (Outputs : out Byte_Array) is
      begin
         Flag_New_Outputs := True;
         Data_Out := Outputs;
      end Outputs_Write;

      procedure Get_Inputs (Inputs : out Byte_Array) is
      begin
         Ada.Text_IO.Put_Line("DPM.Get_Inputs");
         Flag_New_Inputs := False;
         Inputs := Data_In;
      end Get_Inputs;

      procedure New_Inputs (Status : out Boolean) is
      begin
         Ada.Text_IO.Put_Line("DPM.New_Inputs");
         Status := Flag_New_Inputs;
      end New_Inputs;

   end Dual_Port_Memory;

end A4A.Dual_Port_Memory.DPMI;
