
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2020, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------


with Ada.Command_Line; use Ada.Command_Line;
with Ada.Text_IO; use Ada.Text_IO;

with A4A.GUI; use A4A.GUI;
with A4A.Generic_Periodic_Task;
with A4A.Kernel; use A4A.Kernel;

with Gtk.Main;
with Gtk.Widget;
with Gtk.Window, Gtk.Button, Gtk.Box, Gtk.Separator, Gtk.Label, Gtk.Box;
use  Gtk.Window, Gtk.Button, Gtk.Box, Gtk.Separator, Gtk.Label, Gtk.Box;

procedure a4a_gui_main is
   Window       : Gtk_Window;
   Hello_Button : Gtk_Button;
   Bye_Button   : Gtk_Button;
   Box1         : Gtk_Box;

   Periodic_Task_1 : Periodic_Task (Period => 10);
   Generic_Periodic_Task_1 : A4A.Kernel.P1.Periodic_Task (Task_Priority => 10,
                                                          Period_In_Milliseconds => 1000);

begin
   --  This is called in all GtkAda applications. Arguments are parsed
   --  from the command line and are returned to the application.
   Gtk.Main.Init;

   -- Let's print our Command Line
   Put("Started as : ");
   Put(Ada.Command_Line.Command_Name & " ");
   for Index in 1 .. Ada.Command_Line.Argument_Count loop
      Put(Ada.Command_Line.Argument(Index) & " ");
   end loop;
   New_Line;

   --  Creates a new window
   Gtk.Window.Gtk_New (Window);
   Gtk.Window.Set_Title (Window, "-- Ada For Automation --");

   --  When the window is given the "delete_event" signal (this is given
   --  by the window manager, usually by the "close" option, or on the
   --  titlebar), we ask it to call the Delete_Event function.
   Return_Handlers.Connect
     (Window, "delete_event",
      Return_Handlers.To_Marshaller (Delete_Event'Access));

   --  Here we connect the "destroy" event to a signal handler.
   --  This event occurs when we call Gtk.Widget.Destroy on the window,
   --  or if we return False in the "delete_event" callback.
   Handlers.Connect
     (Window, "destroy", Handlers.To_Marshaller (Destroy'Access));

   --  Sets the border width of the window.
   Gtk.Window.Set_Border_Width (Window, 10);

   Gtk_New_Vbox (Box => Box1, Homogeneous => False, Spacing => 0);

   --  Creates a new button with the label "Hello World".
   Gtk_New (Hello_Button, "Hello World");
   Pack_Start (Box1, Hello_Button, Expand => False, Fill => False, Padding => 0);
   --  When the button receives the "clicked" signal, it will call the
   --  procedure Hello_Callback.
   Handlers.Connect
     (Hello_Button, "clicked", Handlers.To_Marshaller (Hello_Callback'Access));

   --  Creates a new button with the label "Bye World".
   Gtk_New (Bye_Button, "Bye World");
   Pack_End (Box1, Bye_Button, Expand => False, Fill => False, Padding => 0);
   --  This will cause the window to be destroyed by calling
   --  Gtk.Widget.Destroy (Window) when "clicked".  Again, the destroy
   --  signal could come from here, or the window manager.
   Handlers.Object_Connect
     (Bye_Button,
      "clicked",
      Handlers.To_Marshaller (Gtk.Widget.Destroy_Cb'Access),
      Window);

   --  This packs the box into the window (a Gtk_Container).
   Gtk.Window.Add (Window, Box1);

   --  The final step is to display this newly created widget.
   -- Show (Hello_Button);
   -- Show (Bye_Button);

   --  and the window
   -- Show (Window);
   Show_All (Window);

   --  All GtkAda applications must have a Main. Control ends here
   --  and waits for an event to occur (like a key press or
   --  mouse event).
   Gtk.Main.Main;
end a4a_gui_main;
