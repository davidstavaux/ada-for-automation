
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with A4A; use A4A;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

procedure Test_CifX is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Input_Data  : Byte_Array (0 .. 9) := (others => 0);
   Output_Data : Byte_Array (0 .. 9) := (others => 0);

   My_Timer : Integer := 10;

   procedure Close;

   procedure Close is
   begin

      if cifX_Channel_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         Ada.Text_IO.Put_Line (Item => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;
   end Close;

begin

   Ada.Text_IO.Put_Line (Item => "Test_CifX : Basic data exchange...");

   Ada.Text_IO.Put_Line (Item => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX.Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "cifX0",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      Ada.Text_IO.Put_Line (Item => "IO Exchange");

      loop

         Result := cifX.Channel_IO_Read
           (Channel_Handle => Channel_Handle,
            Area_Number    => 0,
            Offset         => 0,
            Data_Length    => Input_Data'Length,
            Data_In        => Input_Data,
            Time_Out       => 10);

         Ada.Text_IO.Put_Line (Item => "Channel_IO_Read");

         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         else

            for Index in Input_Data'First .. 3 loop

               Ada.Text_IO.Put ("Input_Data(" & Index'Img & ") : ");
               Byte_Text_IO.Put (Input_Data (Index), Base => 16);
               Ada.Text_IO.New_Line;

               Output_Data (Index) := Input_Data (Index);
            end loop;

            Result := cifX.Channel_IO_Write
              (Channel_Handle => Channel_Handle,
               Area_Number    => 0,
               Offset         => 0,
               Data_Length    => Output_Data'Length,
               Data_Out       => Output_Data,
               Time_Out       => 10);

            Ada.Text_IO.Put_Line (Item => "Channel_IO_Write");

            if Result /= CIFX_NO_ERROR then
               cifX.Show_Error (Result);
            end if;

         end if;

         My_Timer := My_Timer - 1;
         exit when My_Timer <= 0;

         delay 1.0;
      end loop;

   end if;

   Close;

end Test_CifX;
