
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with A4A; use A4A;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Profibus_DPM;
use A4A.Protocols.HilscherX.Profibus_DPM;

procedure Test_DPM_Channel_Extended_Status_Block is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Extended_Status_Block : Extended_Status_Block_Type;

   procedure cifX_Show_Extended_Status_Block;

   procedure cifX_Show_Extended_Status_Block is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Extended_Status_Block" & CRLF
        & "Global Bits :" & CRLF
        & "Control_Error               : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Control_Error'Img & CRLF
        & "Auto_Clear_Error            : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Auto_Clear_Error'Img & CRLF
        & "Non_Exchange_Error          : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Non_Exchange_Error'Img & CRLF
        & "Fatal_Error                 : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Fatal_Error'Img & CRLF
        & "Host_Not_Ready_Notification : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Host_Not_Ready_Notification'Img & CRLF
        & "Timeout_Error               : "
        & Extended_Status_Block.Global_State_Field
        .Global_Bits.Timeout_Error'Img & CRLF
        & CRLF
        & "Master main state  : "
        & Extended_Status_Block.Global_State_Field.DPM_State'Img & CRLF
        & CRLF
        & "abSlaves_Config(0) : "
        & Extended_Status_Block.Global_State_Field
        .abSlaves_Config (0)'Img & CRLF
        & "abSlaves_State(0)  : "
        & Extended_Status_Block.Global_State_Field
        .abSlaves_State (0)'Img & CRLF
        & "abSlaves_Diag(0)   : "
        & Extended_Status_Block.Global_State_Field
        .abSlaves_Diag (0)'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      Ada.Text_IO.Put_Line (What);
   end cifX_Show_Extended_Status_Block;

   procedure Close;

   procedure Close is
   begin

      if cifX_Channel_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         Ada.Text_IO.Put_Line (Item => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;
   end Close;

begin

   Ada.Text_IO.Put_Line (Item => "Test_CifX : "
                         & "Getting PROFIBUS DPM Extended Status...");

   Ada.Text_IO.Put_Line (Item => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX.Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "cifX0",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      Ada.Text_IO.Put_Line (Item => "Getting PROFIBUS DPM Extended Status");
      Channel_Extended_Status_Block
        (Channel_Handle        => Channel_Handle,
         Extended_Status_Block => Extended_Status_Block,
         Error                 => Result);
      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Show_Extended_Status_Block;
      end if;

   end if;

   Close;

end Test_DPM_Channel_Extended_Status_Block;
