
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM.SDO_Download_FB;

with A4A.Protocols.HilscherX.Request_FB;
use A4A.Protocols.HilscherX;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

procedure Test_ECM_SDO_Download is

   My_Ident : constant String := "Test_ECM_SDO_Download";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package ECM_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Do_Command    : Boolean := False;
   Command_Done  : Boolean := False;
   Command_Error : Boolean := False;

   My_Channel_Messaging : aliased
     A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   SDO_Download_FB : ECM_Req_FB.SDO_Download_FB.Instance;

   FW_Start_Temporisation : constant Duration := 5.0;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure Close;

   procedure Close is
   begin

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "ECM",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

      SDO_Download_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      SDO_Download_FB.Set_Parameters
        (Node_Id    => 16#100#,
         Index      => 16#2000#,
         Sub_Index  => 3,
         Data_Count => 1,
         SDO_Data   => (1 => 1));

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Waiting for the firmware to start...");

      Result := cifX.Channel_Reset
        (Channel_Handle => Channel_Handle,
         Reset_Mode     => cifX.CIFX_CHANNELINIT,
         Time_Out       => 20);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      end if;

      delay FW_Start_Temporisation;

      loop

         Do_Command := not Command_Done;

         SDO_Download_FB.Cyclic
           (Do_Command => Do_Command,
            Done       => Command_Done,
            Error      => Command_Error);

         exit when not Do_Command;

         delay 0.1;

      end loop;

      if not Command_Error then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Got no error !");

      else

         SDO_Download_FB.Show_Error (Who => My_Ident);

      end if;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_ECM_SDO_Download;
