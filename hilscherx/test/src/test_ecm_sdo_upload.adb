
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.EtherCAT_ECM.SDO_Upload;

with A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM.SDO_Upload_FB;

use A4A.Protocols.HilscherX.EtherCAT_ECM;

with A4A.Protocols.HilscherX.Request_FB;
use A4A.Protocols.HilscherX;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

procedure Test_ECM_SDO_Upload is

   My_Ident : constant String := "Test_ECM_SDO_Upload";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package ECM_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Do_Command    : Boolean := False;
   Command_Done  : Boolean := False;
   Command_Error : Boolean := False;

   My_Channel_Messaging : aliased
     A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   SDO_Upload_FB : ECM_Req_FB.SDO_Upload_FB.Instance;

   SDO_Upload_Data :
   SDO_Upload.ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_POS_T;

   FW_Start_Temporisation : constant Duration := 5.0;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure cifX_Show_SDO_Upload_Data;

   procedure cifX_Show_SDO_Upload_Data is

      procedure Byte_String
        (Item : Byte; Buffer : in out String; Pos : in out Integer);

      procedure Byte_String
        (Item : Byte; Buffer : in out String; Pos : in out Integer) is

         Hex   : constant array
           (Byte range 0 .. 15) of Character := "0123456789ABCDEF";

      begin

         Buffer (Pos)     := Hex (Item / 16);
         Buffer (Pos + 1) := Hex (Item mod 16);

         Pos := Pos + 2;

      end Byte_String;

      function Data_String return String;

      function Data_String return String is

         Data_Count : constant Natural := Natural (SDO_Upload_Data.Data_Count);

         --  We want to display the data bytes in hex
         --  00, ff, ...
         --  00 is 2
         --  00, ff is 6
         --  00, aa, ff is 10

         Result : String (1 .. (Data_Count * 4) - 2) := (others => ' ');

         Pos   : Integer := Result'First;

         Index : Integer := SDO_Upload_Data.SDO_Data'First;

      begin

         if Data_Count = 0 then
            return "";
         end if;

         Byte_String
           (Item   => SDO_Upload_Data.SDO_Data (Index),
            Buffer => Result,
            Pos    => Pos);

         loop

            Index := Index + 1;

            exit when Index > SDO_Upload_Data.SDO_Data'First + Data_Count - 1;

            Result (Pos .. Pos + 1) := ", ";

            Pos := Pos + 2;

            Byte_String
              (Item   => SDO_Upload_Data.SDO_Data (Index),
               Buffer => Result,
               Pos    => Pos);

         end loop;

         return Result;

      end Data_String;

      function DWord_Img (Item : DWord) return String;

      function DWord_Img (Item : DWord) return String is

         Image : String (1 .. 20);

      begin

         DWord_Text_IO.Put
           (To   => Image,
            Item => Item,
            Base => 16);

         return Image;

      end DWord_Img;

      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          SDO Upload Data" & CRLF & CRLF
        & "Request :" & CRLF
        & "Node_Id      : "
        & DWord_Img (SDO_Upload_Data.Node_Id) & CRLF
        & "Index        : "
        & DWord_Img (SDO_Upload_Data.Index) & CRLF
        & "Sub_Index    : "
        & DWord_Img (SDO_Upload_Data.Sub_Index) & CRLF
        & "Data_Count   : "
        & SDO_Upload_Data.Data_Count'Img & CRLF
        & CRLF
        & "Data :" & CRLF
        & Data_String & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_SDO_Upload_Data;

   procedure Close;

   procedure Close is
   begin

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "ECM",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

      SDO_Upload_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      SDO_Upload_FB.Set_Parameters
        (Node_Id => 16#100#, Index => 16#3000#, Sub_Index => 1);

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Waiting for the firmware to start...");

      delay FW_Start_Temporisation;

      loop

         Do_Command := not Command_Done;

         SDO_Upload_FB.Cyclic
           (Do_Command => Do_Command,
            Done       => Command_Done,
            Error      => Command_Error);

         exit when not Do_Command;

         delay 0.1;

      end loop;

      if not Command_Error then

         SDO_Upload_Data := SDO_Upload_FB.Get_Data_Pos;

         cifX_Show_SDO_Upload_Data;

      else

         SDO_Upload_FB.Show_Error (Who => My_Ident);

      end if;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_ECM_SDO_Upload;
