
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C;

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Handle;

with A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Handle_FB;
with A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Conn_Info_FB;

use A4A.Protocols.HilscherX.rcX_Public;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

with A4A.Protocols.HilscherX.EtherCAT_ECM;
use A4A.Protocols.HilscherX.EtherCAT_ECM;

procedure Test_Generic_Diag_EtherCAT_ECM is

   My_Ident : constant String := "Test_Generic_Diag_EtherCAT_ECM";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package rcX_Public_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.rcX_Public;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Do_Get_Handles    : Boolean := False;
   Get_Handles_Done  : Boolean := False;
   Get_Handles_Error : Boolean := False;

   Do_Get_Info    : Boolean := False;
   Get_Info_Done  : Boolean := False;
   Get_Info_Error : Boolean := False;

   My_Channel_Messaging : aliased
     A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   rcX_Get_Slave_Handle_FB :
   rcX_Public_Req_FB.rcX_Get_Slave_Handle_FB.Instance;

   Handles_Num : Natural := 0;
   Param       : DWord   := 0;
   Some_Handles  : rcX_Get_Slave_Handle.Handles_Array (1 .. 10);

   rcX_Get_Slave_Conn_Info_FB :
   rcX_Public_Req_FB.rcX_Get_Slave_Conn_Info_FB.Instance;

   Bytes_Num   : Natural := 0;
   Handle      : DWord   := 0;
   Struct_ID   : DWord   := 0;
   Conn_Info_Bytes  : aliased ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T;

   FW_Start_Temporisation : constant Duration := 5.0;

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Get Slave Handles

      X02,
      --  Get Slave Connection Info

      X03
      --  Terminate
     );

   Status : Block_Status := X00;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure cifX_Show_Get_Handle_Results;

   procedure cifX_Show_Get_Handle_Results is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Get Handle Results" & CRLF & CRLF
        & "Handles Number    : "
        & Handles_Num'Img & CRLF
        & "Param             : "
        & Param'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_Get_Handle_Results;

   procedure cifX_Show_Get_Info_Results;

   procedure cifX_Show_Get_Info_Results is

      function DWord_Img (Item : DWord) return String;

      function DWord_Img (Item : DWord) return String is

         Image : String (1 .. 20);

      begin

         DWord_Text_IO.Put
           (To   => Image,
            Item => Item,
            Base => 16);

         return Image;

      end DWord_Img;

      Conn_Info_Access : constant
        ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T_Access :=
        From_Byte_Array (Conn_Info_Bytes'Unrestricted_Access);

      Emergency_Reported : constant Boolean :=
        (Conn_Info_Access.Emergency_Reported /= 0);
      --  Slave has send an emergency

      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Get Info Results" & CRLF
        & CRLF
        & "Bytes Number    : " & Bytes_Num'Img & CRLF
        & "Struct_ID       : " & Struct_ID'Img & CRLF
        & CRLF
        & "Station_Address     : "
        & DWord_Img (Conn_Info_Access.Station_Address) & CRLF
        & "Auto_Inc_Address    : "
        & DWord_Img (Conn_Info_Access.Auto_Inc_Address) & CRLF
        & "Current_State       : "
        & DWord_Img (Conn_Info_Access.Current_State) & CRLF
        & "Last_Error          : "
        & DWord_Img (Conn_Info_Access.Last_Error) & CRLF
        & "Slave_Name          : "
        & Interfaces.C.To_Ada (Item => Conn_Info_Access.Slave_Name) & CRLF
        & "Emergency_Reported  : " & Emergency_Reported'Img & CRLF
        & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_Get_Info_Results;

   procedure Close;

   procedure Close is
   begin

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "ECM",   --  "cifX0"
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

      rcX_Get_Slave_Handle_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      rcX_Get_Slave_Conn_Info_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Waiting for the firmware to start...");

      delay FW_Start_Temporisation;

      loop

         case Status is

            when X00 => -- Initial state

               if True then

                  rcX_Get_Slave_Handle_FB.Set_Parameters
                    (Param => rcX_Get_Slave_Handle.RCX_LIST_CONF_SLAVES);

                  Status := X01;

               end if;

            when X01 => -- Get Slave Handles

               if Get_Handles_Done then

                  if Get_Handles_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got an error for Slave Handles");

                     rcX_Get_Slave_Handle_FB.Show_Error (Who => My_Ident);

                     Status := X03;

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got Slave Handles");

                     rcX_Get_Slave_Handle_FB.Get_Data
                       (Handles_Num => Handles_Num,
                        Param       => Param,
                        Handles     => Some_Handles);

                     cifX_Show_Get_Handle_Results;

                     rcX_Get_Slave_Conn_Info_FB.Set_Parameters
                       (Handle => Some_Handles (Some_Handles'First));

                     Status := X02;

                  end if;

               end if;

            when X02 => -- Get Slave Connection Info

               if Get_Info_Done then

                  if Get_Info_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got an error for Connection Info");

                     rcX_Get_Slave_Conn_Info_FB.Show_Error (Who => My_Ident);

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got Slave Connection Info");

                     rcX_Get_Slave_Conn_Info_FB.Get_Data
                       (Bytes_Num => Bytes_Num,
                        Handle    => Handle,
                        Struct_ID => Struct_ID,
                        State     => Conn_Info_Bytes.Bytes);

                     cifX_Show_Get_Info_Results;

                  end if;

                  Status := X03;

               end if;

            when X03 => -- Terminating

               null;

         end case;

         Do_Get_Handles := (Status = X01);

         rcX_Get_Slave_Handle_FB.Cyclic
           (Do_Command => Do_Get_Handles,
            Done       => Get_Handles_Done,
            Error      => Get_Handles_Error);

         Do_Get_Info := (Status = X02);

         rcX_Get_Slave_Conn_Info_FB.Cyclic
           (Do_Command => Do_Get_Info,
            Done       => Get_Info_Done,
            Error      => Get_Info_Error);

         exit when (Status = X03);

         delay 0.1;

      end loop;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_Generic_Diag_EtherCAT_ECM;
