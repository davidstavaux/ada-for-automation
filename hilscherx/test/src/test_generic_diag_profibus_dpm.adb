
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Library.Conversion;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Handle;

with A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Handle_FB;
with A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Conn_Info_FB;

use A4A.Protocols.HilscherX.rcX_Public;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

with A4A.Protocols.HilscherX.Profibus_DPM;
use A4A.Protocols.HilscherX.Profibus_DPM;

procedure Test_Generic_Diag_Profibus_DPM is

   My_Ident : constant String := "Test_Generic_Diag_Profibus_DPM";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package rcX_Public_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.rcX_Public;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Do_Get_Handles    : Boolean := False;
   Get_Handles_Done  : Boolean := False;
   Get_Handles_Error : Boolean := False;

   Do_Get_Info    : Boolean := False;
   Get_Info_Done  : Boolean := False;
   Get_Info_Error : Boolean := False;

   My_Channel_Messaging : aliased
     A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   rcX_Get_Slave_Handle_FB :
   rcX_Public_Req_FB.rcX_Get_Slave_Handle_FB.Instance;

   Handles_Num : Natural := 0;
   Param       : DWord   := 0;
   Some_Handles  : rcX_Get_Slave_Handle.Handles_Array (1 .. 10);

   rcX_Get_Slave_Conn_Info_FB :
   rcX_Public_Req_FB.rcX_Get_Slave_Conn_Info_FB.Instance;

   Bytes_Num   : Natural := 0;
   Handle      : DWord   := 0;
   Struct_ID   : DWord   := 0;
   Conn_Info_Bytes  : aliased PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T;

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Get Slave Handles

      X02,
      --  Get Slave Connection Info

      X03
      --  Terminate
     );

   Status : Block_Status := X00;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure cifX_Show_Get_Handle_Results;

   procedure cifX_Show_Get_Handle_Results is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Get Handle Results" & CRLF & CRLF
        & "Handles Number    : "
        & Handles_Num'Img & CRLF
        & "Param             : "
        & Param'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_Get_Handle_Results;

   procedure cifX_Show_Get_Info_Results;

   procedure cifX_Show_Get_Info_Results is

      function Ident_Number_Image (Item : in Word) return String;

      function Ident_Number_Image (Item : in Word) return String is

         Ident_Number : Word;
         Ident_Number_LSB : Byte;
         Ident_Number_MSB : Byte;
         Ident_Number_String : String (1 .. 20);

      begin

         A4A.Library.Conversion.Word_To_Bytes
           (Word_in  => Item,
            LSB_Byte => Ident_Number_LSB,
            MSB_Byte => Ident_Number_MSB);

         A4A.Library.Conversion.Bytes_To_Word
           (LSB_Byte => Ident_Number_MSB,
            MSB_Byte => Ident_Number_LSB,
            Word_out => Ident_Number);

         Word_Text_IO.Put (To   => Ident_Number_String,
                           Item => Ident_Number,
                           Base => 16);

         return Ident_Number_String;

      end Ident_Number_Image;

      Conn_Info_Access : constant PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T_Access :=
        From_Byte_Array (Conn_Info_Bytes'Unrestricted_Access);

      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Get Info Results" & CRLF
        & CRLF
        & "Bytes Number    : " & Bytes_Num'Img & CRLF
        & "Struct_ID       : " & Struct_ID'Img & CRLF
        & CRLF
        & "Master Address  : " & Conn_Info_Access.Master_Add'Img & CRLF
        & "Ident Number (Hex) : "
        & Ident_Number_Image (Conn_Info_Access.Ident_Number) & CRLF
        & CRLF
        & "--  Station Status 1  --"  & CRLF
        & "Station_Non_Existent    : "
        & Conn_Info_Access.Station_Status_1.Station_Non_Existent'Img & CRLF
        & "Station_Not_Ready       : "
        & Conn_Info_Access.Station_Status_1.Station_Not_Ready'Img & CRLF
        & "Cfg_Fault               : "
        & Conn_Info_Access.Station_Status_1.Cfg_Fault'Img & CRLF
        & "Ext_Diag                : "
        & Conn_Info_Access.Station_Status_1.Ext_Diag'Img & CRLF
        & "Not_Supported           : "
        & Conn_Info_Access.Station_Status_1.Not_Supported'Img & CRLF
        & "Invalid_Response        : "
        & Conn_Info_Access.Station_Status_1.Invalid_Response'Img & CRLF
        & "Prm_Fault               : "
        & Conn_Info_Access.Station_Status_1.Prm_Fault'Img & CRLF
        & "Master_Lock             : "
        & Conn_Info_Access.Station_Status_1.Master_Lock'Img & CRLF
        & CRLF
        & "--  Station Status 2  --"  & CRLF
        & "Prm_Req                 : "
        & Conn_Info_Access.Station_Status_2.Prm_Req'Img & CRLF
        & "Stat_Diag               : "
        & Conn_Info_Access.Station_Status_2.Stat_Diag'Img & CRLF
        & "bTrue                   : "
        & Conn_Info_Access.Station_Status_2.bTrue'Img & CRLF
        & "Wd_On                   : "
        & Conn_Info_Access.Station_Status_2.Wd_On'Img & CRLF
        & "Freeze_Mode             : "
        & Conn_Info_Access.Station_Status_2.Freeze_Mode'Img & CRLF
        & "Sync_Mode               : "
        & Conn_Info_Access.Station_Status_2.Sync_Mode'Img & CRLF
        & "Reserved                : "
        & Conn_Info_Access.Station_Status_2.Reserved'Img & CRLF
        & "Deactivated             : "
        & Conn_Info_Access.Station_Status_2.Deactivated'Img & CRLF
        & CRLF
        & "--  Station Status 3  --"  & CRLF
        & "Reserved_1              : "
        & Conn_Info_Access.Station_Status_3.Reserved_1'Img & CRLF
        & "Reserved_2              : "
        & Conn_Info_Access.Station_Status_3.Reserved_2'Img & CRLF
        & "Reserved_3              : "
        & Conn_Info_Access.Station_Status_3.Reserved_3'Img & CRLF
        & "Reserved_4              : "
        & Conn_Info_Access.Station_Status_3.Reserved_4'Img & CRLF
        & "Reserved_5              : "
        & Conn_Info_Access.Station_Status_3.Reserved_5'Img & CRLF
        & "Reserved_6              : "
        & Conn_Info_Access.Station_Status_3.Reserved_6'Img & CRLF
        & "Reserved_7              : "
        & Conn_Info_Access.Station_Status_3.Reserved_7'Img & CRLF
        & "Ext_Diag_Overflow       : "
        & Conn_Info_Access.Station_Status_3.Ext_Diag_Overflow'Img & CRLF
        & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_Get_Info_Results;

   procedure Close;

   procedure Close is
   begin

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "DPM",   --  "cifX0"
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

      rcX_Get_Slave_Handle_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      rcX_Get_Slave_Conn_Info_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      loop

         case Status is

            when X00 => -- Initial state

               if True then

                  rcX_Get_Slave_Handle_FB.Set_Parameters
                    (Param => rcX_Get_Slave_Handle.RCX_LIST_CONF_SLAVES);

                  Status := X01;

               end if;

            when X01 => -- Get Slave Handles

               if Get_Handles_Done then

                  if Get_Handles_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got an error for Slave Handles");

                     rcX_Get_Slave_Handle_FB.Show_Error (Who => My_Ident);

                     Status := X03;

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got Slave Handles");

                     rcX_Get_Slave_Handle_FB.Get_Data
                       (Handles_Num => Handles_Num,
                        Param       => Param,
                        Handles     => Some_Handles);

                     cifX_Show_Get_Handle_Results;

                     rcX_Get_Slave_Conn_Info_FB.Set_Parameters
                       (Handle => Some_Handles (Some_Handles'First));

                     Status := X02;

                  end if;

               end if;

            when X02 => -- Get Slave Connection Info

               if Get_Info_Done then

                  if Get_Info_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got an error for Connection Info");

                     rcX_Get_Slave_Conn_Info_FB.Show_Error (Who => My_Ident);

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Got Slave Connection Info");

                     rcX_Get_Slave_Conn_Info_FB.Get_Data
                       (Bytes_Num => Bytes_Num,
                        Handle    => Handle,
                        Struct_ID => Struct_ID,
                        State     => Conn_Info_Bytes.Bytes);

                     cifX_Show_Get_Info_Results;

                  end if;

                  Status := X03;

               end if;

            when X03 => -- Terminating

               null;

         end case;

         Do_Get_Handles := (Status = X01);

         rcX_Get_Slave_Handle_FB.Cyclic
           (Do_Command => Do_Get_Handles,
            Done       => Get_Handles_Done,
            Error      => Get_Handles_Error);

         Do_Get_Info := (Status = X02);

         rcX_Get_Slave_Conn_Info_FB.Cyclic
           (Do_Command => Do_Get_Info,
            Done       => Get_Info_Done,
            Error      => Get_Info_Error);

         exit when (Status = X03);

         delay 0.1;

      end loop;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_Generic_Diag_Profibus_DPM;
