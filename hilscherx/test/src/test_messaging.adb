
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with Interfaces.C;

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify;
use A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify;

with A4A.Protocols.HilscherX.Messaging;
use A4A.Protocols.HilscherX;

procedure Test_Messaging is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   Request      : cifX.cifX_Packet_Access;
   Confirmation : cifX.cifX_Packet_Access;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Fw_Ident_Req : RCX_FIRMWARE_IDENTIFY_REQ_T_Access;
   Fw_Ident_Cnf : RCX_FIRMWARE_IDENTIFY_CNF_T_Access;

   Packet_Got : Boolean := False;
   Packet_Put : Boolean := False;

   procedure cifX_Show_Firmware_Identification;

   procedure cifX_Show_Firmware_Identification is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Firmware Identification" & CRLF & CRLF
        & "Firmware Version :" & CRLF
        & "Major    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Major'Img & CRLF
        & "Minor    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Minor'Img & CRLF
        & "Build    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Build'Img & CRLF
        & "Revision : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Revision'Img & CRLF
        & CRLF
        & "Firmware Name : "
        & Interfaces.C.To_Ada
        (Fw_Ident_Cnf.Data.Fw_Identification.Fw_Name.Name) & CRLF
        & CRLF
        & "Firmware Date :" & CRLF
        & "Year  : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Year'Img & CRLF
        & "Month : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Month'Img & CRLF
        & "Day   : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Day'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      Ada.Text_IO.Put_Line (What);
   end cifX_Show_Firmware_Identification;

   procedure Close;

   procedure Close is
   begin

      Messaging.Quit;

      delay 5.0;

      if cifX_Channel_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         Ada.Text_IO.Put_Line (Item => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX.Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         Ada.Text_IO.Put_Line (Item => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

   end Close;

begin

   Ada.Text_IO.Put_Line (Item => "Test_Messaging...");

   Ada.Text_IO.Put_Line (Item => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX.Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      Ada.Text_IO.Put_Line (Item => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "cifX0",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX.Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      Messaging.Initialise (Channel_Handle => Channel_Handle);

      Ada.Text_IO.Put_Line (Item => "Sending Request");

      Messaging.Message_Pool.Get_Packet
        (Item   => Request,
         Result => Packet_Got);

      if not Packet_Got then

         Ada.Text_IO.Put_Line
           ("Could not get Request packet from pool...");

      else

         Fw_Ident_Req := From_cifX_Packet (Request);

         Fw_Ident_Req.Head.Dest    := 16#20#;
         Fw_Ident_Req.Head.Src     := 0;
         Fw_Ident_Req.Head.Dest_Id := 0;
         Fw_Ident_Req.Head.Src_Id  := 0;
         Fw_Ident_Req.Head.Len     := 4;
         Fw_Ident_Req.Head.Id      := 1;
         Fw_Ident_Req.Head.State   := 0;
         Fw_Ident_Req.Head.Cmd     := RCX_FIRMWARE_IDENTIFY_REQ;
         Fw_Ident_Req.Head.Ext     := 0;
         Fw_Ident_Req.Head.Rout    := 0;

         Fw_Ident_Req.Data.Channel_Id := 0;

         Messaging.Send_Queue.Put
           (Item   => Request,
            Result => Packet_Put);

         if not Packet_Put then

            Ada.Text_IO.Put_Line
              ("Could not put Request packet in queue...");

            Messaging.Message_Pool.Return_Packet (Item => Request);

         else

            Messaging.Receive_Queue.Get (Item => Confirmation);

            Fw_Ident_Cnf := From_cifX_Packet (Confirmation);

            cifX_Show_Firmware_Identification;

            Messaging.Message_Pool.Return_Packet (Item => Confirmation);

         end if;

      end if;

   end if;

   Close;

exception

   when Error : others =>
      Ada.Text_IO.Put_Line (Exception_Information (Error));

      Close;

end Test_Messaging;
