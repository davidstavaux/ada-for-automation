

include makefile_vars

destination_dir = ./doc/ReadMe

all: read_me

read_me: read_me_html read_me_pdf

read_me_html:
	asciidoctor --require=asciidoctor-diagram --destination-dir=$(destination_dir) README.asciidoc

read_me_pdf:
	asciidoctor-pdf --attribute=allow-uri-read --require=asciidoctor-diagram --destination-dir=$(destination_dir) README.asciidoc

CI_CD_build: read_me
	# BusyBox don't like verbosity
	# rm --force --recursive ./doc/ReadMe/.asciidoctor
	rm -f -r ./doc/ReadMe/.asciidoctor
	cd ./book && make && make book_img
	# rm --force --recursive ./doc/book/.asciidoctor
	rm -f -r ./doc/book/.asciidoctor
	cd ./demo && make read_me
	# rm --force --recursive ./*/doc/ReadMe/.asciidoctor
	rm -f -r ./*/doc/ReadMe/.asciidoctor

CI_CD_deploy:
	# deploy GitLab Pages
	# BusyBox don't like verbosity
	# mkdir --parents ./public/docu
	mkdir -p ./public/docu
	cp -R ./www/GitLabPages/* ./public/
	
	# deploy Documentation Portal
	cp -R ./www/docu/* ./public/docu
	
	# deploy ReadMe
	cp -R ./doc/* ./public/docu
	
	# deploy Book
	cp -R ./book/doc/* ./public/docu
	
	# deploy Demos ReadMe
	mkdir -p ./public/docu/demo/$(demo_000b_path)
	cp -R ./demo/$(demo_000b_path)/doc ./public/docu/demo/$(demo_000b_path)
	
	mkdir -p ./public/docu/demo/$(demo_000_path)
	cp -R ./demo/$(demo_000_path)/doc ./public/docu/demo/$(demo_000_path)
	
	mkdir -p ./public/docu/demo/$(demo_001b_path)
	cp -R ./demo/$(demo_001b_path)/doc ./public/docu/demo/$(demo_001b_path)
	
	mkdir -p ./public/docu/demo/$(demo_001_path)
	cp -R ./demo/$(demo_001_path)/doc ./public/docu/demo/$(demo_001_path)
	
	mkdir -p ./public/docu/demo/$(demo_010b_path)
	cp -R ./demo/$(demo_010b_path)/doc ./public/docu/demo/$(demo_010b_path)
	
	mkdir -p ./public/docu/demo/$(demo_010_path)
	cp -R ./demo/$(demo_010_path)/doc ./public/docu/demo/$(demo_010_path)
	
	mkdir -p ./public/docu/demo/$(demo_020_path)
	cp -R ./demo/$(demo_020_path)/doc ./public/docu/demo/$(demo_020_path)
	
	mkdir -p ./public/docu/demo/$(demo_021_path)
	cp -R ./demo/$(demo_021_path)/doc ./public/docu/demo/$(demo_021_path)
	
	mkdir -p ./public/docu/demo/$(demo_022_path)
	cp -R ./demo/$(demo_022_path)/doc ./public/docu/demo/$(demo_022_path)
	
	mkdir -p ./public/docu/demo/$(demo_052_path)
	cp -R ./demo/$(demo_052_path)/doc ./public/docu/demo/$(demo_052_path)
	
	mkdir -p ./public/docu/demo/$(demo_062_path)
	cp -R ./demo/$(demo_062_path)/doc ./public/docu/demo/$(demo_062_path)
	
	mkdir -p ./public/docu/demo/$(demo_082_path)
	cp -R ./demo/$(demo_082_path)/doc ./public/docu/demo/$(demo_082_path)
	
	mkdir -p ./public/docu/demo/$(demo_132_path)
	cp -R ./demo/$(demo_132_path)/doc ./public/docu/demo/$(demo_132_path)
	
	mkdir -p ./public/docu/demo/$(demo_142_path)
	cp -R ./demo/$(demo_142_path)/doc ./public/docu/demo/$(demo_142_path)

clean:
	if [ -e ./doc ]; then cd ./doc && rm --force --recursive * ; fi

