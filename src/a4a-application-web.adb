
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;

with A4A.User_Controllers; use A4A.User_Controllers;

with A4A.User_Web_Objects; use A4A.User_Web_Objects;
with A4A.User_Web_Functions;

with A4A.Web.Pages.General_Status;
with A4A.Web.Pages.MBTCP_Server_Status;
with A4A.Web.Pages.MBTCP_Clients_Status;
with A4A.Web.Pages.Home;
use A4A.Web.Pages;

with A4A.Web.Web_Server_Config;

package body A4A.Application.Web is

   procedure Connect_Data is
   begin

      LED1.Set_Model
        (Status_Bit  => Input_Bits (0)'Access);

      LED3.Set_Model
        (Status_Bit  => Input_Bits (1)'Access);

      for I in Input_LEDs_Byte_0'Range loop
         Input_LEDs_Byte_0 (I).Set_Model
           (Status_Bit  => Input_Bits (I)'Access);
      end loop;

      for I in Input_LEDs_Byte_1'Range loop
         Input_LEDs_Byte_1 (I).Set_Model
           (Status_Bit  => Input_Bits (I + 1 + Input_LEDs_Byte_0'Last)'Access);
      end loop;

      for I in Output_LEDs_Byte_0'Range loop
         Output_LEDs_Byte_0 (I).Set_Model
           (Status_Bit  => Coils (I)'Access);
      end loop;

      for I in Output_LEDs_Byte_1'Range loop
         Output_LEDs_Byte_1 (I).Set_Model
           (Status_Bit  => Coils (I + 1 + Output_LEDs_Byte_0'Last)'Access);
      end loop;

   end Connect_Data;

   procedure Update_Data is
   begin

      A4A.User_Web_Functions.Map_Inputs;
      A4A.User_Web_Functions.Map_Outputs;

      LED1.Update;
      LED3.Update;

      for I in Input_LEDs_Byte_0'Range loop
         Input_LEDs_Byte_0 (I).Update;
      end loop;

      for I in Input_LEDs_Byte_1'Range loop
         Input_LEDs_Byte_1 (I).Update;
      end loop;

      for I in Output_LEDs_Byte_0'Range loop
         Output_LEDs_Byte_0 (I).Update;
      end loop;

      for I in Output_LEDs_Byte_1'Range loop
         Output_LEDs_Byte_1 (I).Update;
      end loop;

   end Update_Data;

   procedure On_Server_Start is
   begin

      Gnoga.Application.Multi_Connect.Initialize
        (Event => Home.On_Connect'Access,
         Port  => A4A.Web.Web_Server_Config.Port,
         Boot  => "000-boot.html");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => General_Status.On_Connect'Access,
         Path => "general_status");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => MBTCP_Server_Status.On_Connect'Access,
         Path => "mbtcp_server_status");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => MBTCP_Clients_Status.On_Connect'Access,
         Path => "mbtcp_clients_status");

      Gnoga.Application.Title ("Ada for Automation Web UI with Gnoga");

   end On_Server_Start;

end A4A.Application.Web;
