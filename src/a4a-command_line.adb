
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;
with Ada.Text_IO;

with A4A.Log;

package body A4A.Command_Line is

   --------------------------------------------------------------------
   --  Parsing
   --------------------------------------------------------------------

   procedure Parse_Arguments is
      My_Ident : constant String := "A4A.Command_Line.Parse_Arguments";
   begin
      GNAT.Command_Line.Define_Switch
        (Config      => CL_Config,
         Output      => Log_Level_Str'Access,
         Switch      => "-ll=",
         Long_Switch => "--log-level=",
         Help        => "Log Level. Arg is error, warning, info, verbose");

      GNAT.Command_Line.Define_Switch
        (Config      => CL_Config,
         Output      => Log_Color_Str'Access,
         Switch      => "-lc=",
         Long_Switch => "--log-color=",
         Help        => "Log Color. Arg is no, yes");

      GNAT.Command_Line.Getopt (Config => CL_Config);

      Ada.Text_IO.Put_Line ("Log Level = " & Log_Level_Str.all
                           & ", Log Color = " & Log_Color_Str.all);

      if Log_Level_Str.all = "error" then
         Log_Level := A4A.Logger.Level_Error;
      elsif  Log_Level_Str.all = "warning" then
         Log_Level := A4A.Logger.Level_Warning;
      elsif  Log_Level_Str.all = "info" then
         Log_Level := A4A.Logger.Level_Info;
      elsif  Log_Level_Str.all = "verbose" then
         Log_Level := A4A.Logger.Level_Verbose;
      else
         Log_Level := A4A.Logger.Level_Error;
      end if;

      GNAT.Strings.Free (Log_Level_Str);

      Ada.Text_IO.Put_Line ("Log_Level = " & Log_Level'Img);

      A4A.Log.Logger.Set_Level (Log_Level => Log_Level);

      if Log_Color_Str.all = "yes" then
         A4A.Log.Logger.Set_Console_Color_Support (Supported => True);
      else
         A4A.Log.Logger.Set_Console_Color_Support (Supported => False);
      end if;

      GNAT.Strings.Free (Log_Color_Str);

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

   end Parse_Arguments;

end A4A.Command_Line;
