
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with GNAT.Command_Line;
with GNAT.Strings;

with A4A.Logger; use A4A.Logger;

package A4A.Command_Line is

   --------------------------------------------------------------------
   --  Parsing
   --------------------------------------------------------------------

   procedure Parse_Arguments;

private

   CL_Config : GNAT.Command_Line.Command_Line_Configuration;

   Log_Level_Str : aliased GNAT.Strings.String_Access := new String'("error");
   Log_Level : A4A.Logger.Log_Level_Type := A4A.Logger.Level_Error;

   Log_Color_Str : aliased GNAT.Strings.String_Access := new String'("no");

end A4A.Command_Line;
