
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Dual_Port_Memory is

   protected type Bool_Dual_Port_Memory_Area (First, Last : Natural) is

      procedure Set_Data
        (Data_In : in Bool_Array;
         Offset  : in Natural);

      function Get_Data
        (Offset  : in Natural;
         Number  : in Natural) return Bool_Array;

   private
      Data : Bool_Array (First .. Last) := (others => False);
   end Bool_Dual_Port_Memory_Area;

   type Bool_Dual_Port_Memory (First, Last : Natural) is
      record
         Inputs : Bool_Dual_Port_Memory_Area
           (First => First,
            Last  => Last);

         Outputs : Bool_Dual_Port_Memory_Area
           (First => First,
            Last  => Last);
      end record;

   type Bool_Dual_Port_Memory_Access
     is access all Bool_Dual_Port_Memory;

   protected type Word_Dual_Port_Memory_Area (First, Last : Natural) is

      procedure Set_Data
        (Data_In : in Word_Array;
         Offset  : in Natural);
      function Get_Data
        (Offset  : in Natural;
         Number  : in Natural) return Word_Array;

   private
      Data : Word_Array (First .. Last) := (others => 0);
   end Word_Dual_Port_Memory_Area;

   type Word_Dual_Port_Memory (First, Last : Natural) is
      record
         Inputs : Word_Dual_Port_Memory_Area
           (First => First,
            Last  => Last);

         Outputs : Word_Dual_Port_Memory_Area
           (First => First,
            Last  => Last);
      end record;

   type Word_Dual_Port_Memory_Access
     is access all Word_Dual_Port_Memory;

end A4A.Dual_Port_Memory;
