
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Tasks.Interfaces; use A4A.Tasks.Interfaces;
with A4A.Tasks.Watchdog; use A4A.Tasks.Watchdog;

generic
   with procedure Run;
   --  The job of the periodic task

package A4A.Generic_Periodic_Task is

   Watchdog_Time_Out_MS : constant Natural := 2000;

   type Task_Interface is
      record
         Control : Task_Control;
         Status  : Task_Status;
         Statistics  : Task_Statistics;

         Control_Watchdog : Control_Watchdog_Type;
         Status_Watchdog  : Status_Watchdog_Type;

      end record;
   type Task_Itf_Access is access all Task_Interface;

   task type Periodic_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Task_Itf_Access;
      Period_In_Milliseconds  : Natural) is
      pragma Priority (Task_Priority);
   end Periodic_Task;
   type Periodic_Task_Access is access Periodic_Task;

end A4A.Generic_Periodic_Task;
