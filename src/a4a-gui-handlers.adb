
with Ada.Text_IO; use Ada.Text_IO;
with Glib;                use Glib;
with Gtk.Main;

package body A4A.GUI.Handlers is

   procedure Quit_Callback (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin

      Quit;

   end Quit_Callback;

   procedure Stop_Callback (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin
      Stop;
   end Stop_Callback;

   procedure Start_Callback (Widget : access Gtk_Widget_Record'Class)
   is
      pragma Unreferenced (Widget);
   begin
      Start;
   end Start_Callback;

   procedure Page_Switch_Callback
     (Notebook : access Gtk_Notebook_Record'Class;
      Params   : Gtk.Arguments.Gtk_Args)
   is
      Old_Page : constant Gint := Get_Current_Page (Notebook);
--        Pixmap   : Gtk_Image;
      Page_Num : constant Gint := Gint (To_Guint (Params, 2));
--        Widget   : Gtk_Widget;

   begin
      Put_Line ("Page_Switch_Callback : old / new : "
                & Old_Page'Img & " / " & Page_Num'Img);
--        Widget := Get_Nth_Page (Notebook, Page_Num);
--        Pixmap := Gtk_Image
--          (Get_Child (Gtk_Box (Get_Tab_Label (Notebook, Widget)), 0));
--        Set (Pixmap, Book_Open, Book_Open_Mask);
--        Pixmap := Gtk_Image
--          (Get_Child (Gtk_Box (Get_Menu_Label (Notebook, Widget)), 0));
--        Set (Pixmap, Book_Open, Book_Open_Mask);
--
--        if Old_Page >= 0 then
--           Widget := Get_Nth_Page (Notebook, Old_Page);
--           Pixmap := Gtk_Image
--             (Get_Child (Gtk_Box (Get_Tab_Label (Notebook, Widget)), 0));
--           Set (Pixmap, Book_Closed, Book_Closed_Mask);
--           Pixmap := Gtk_Image
--             (Get_Child (Gtk_Box (Get_Menu_Label (Notebook, Widget)), 0));
--           Set (Pixmap, Book_Closed, Book_Closed_Mask);
--        end if;
   end Page_Switch_Callback;

   function Delete_Event
     (Widget : access Gtk_Widget_Record'Class;
      Event  : Gdk_Event)
     return Boolean
   is
      pragma Unreferenced (Event);
      pragma Unreferenced (Widget);
   begin
      --  If you return False in the "delete_event" signal handler,
      --  GtkAda will emit the "destroy" signal. Returning True means
      --  you don't want the window to be destroyed. This is useful
      --  for popping up 'are you sure you want to quit?' type
      --  dialogs.

      Quit;

      --  Change True to False and the main window will be destroyed
      --  with a "delete_event".

      return True;
   end Delete_Event;

   --   Another callback
   procedure Destroy (Widget : access Gtk_Widget_Record'Class) is
      pragma Unreferenced (Widget);
   begin
      Gtk.Main.Main_Quit;
   end Destroy;

end A4A.GUI.Handlers;
