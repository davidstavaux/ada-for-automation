
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Calendar.Arithmetic; use Ada.Calendar.Arithmetic;
with Ada.Calendar.Formatting;

with Gtk.Enums;

with A4A.Configuration; use A4A.Configuration;
with A4A.Application.Configuration; use A4A.Application.Configuration;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Main; use A4A.Kernel.Main;

package body A4A.GUI_Elements.General_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  General Status Page
   --------------------------------------------------------------------

   function Create_Application_UI
     (Start_Time_In : Ada.Calendar.Time) return Application_UI_Type is
      Application_UI : Application_UI_Type;
   begin
      Start_Time := Start_Time_In;

      Gtk_New (Application_UI.Frame, "Application");
      Gtk_Label (Application_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Application</b>");
      Application_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Application_UI.VBox);

      Gtk_New_Hbox (Application_UI.HBox);

      Gtk_New (Application_UI.Start_Time_Label,
               "<big>Start time:</big>" & CRLF
               & Ada.Calendar.Formatting.Image
                 (Date                  => Start_Time,
                  Include_Time_Fraction => False,
                  Time_Zone             => The_Time_Offset));
      Application_UI.Start_Time_Label.Set_Use_Markup (True);

      Application_UI.HBox.Pack_Start
        (Application_UI.Start_Time_Label, Expand => False, Padding => 10);

      Gtk_New (Application_UI.Up_Time_Label,
               "<big>Up time:</big>" & CRLF
               & "hh:mm:ss");
      Application_UI.Up_Time_Label.Set_Use_Markup (True);

      Application_UI.HBox.Pack_Start
        (Application_UI.Up_Time_Label, Expand => False, Padding => 10);

      Application_UI.VBox.Pack_Start
        (Application_UI.HBox, Expand => False, Padding => 10);

      Application_UI.Frame.Add (Application_UI.VBox);

      return Application_UI;

   end Create_Application_UI;

   function Create_Main_Task_UI return Main_Task_UI_Type is
      Main_Task_UI : Main_Task_UI_Type;
   begin
      Gtk_New (Main_Task_UI.Frame, "Main Task");
      Gtk_Label (Main_Task_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Main Task</b>");
      Main_Task_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Main_Task_UI.VBox);

      Gtk_New_Hbox (Main_Task_UI.Config_HBox);

      Gtk_New (Main_Task_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF);
      Main_Task_UI.Config_Label.Set_Use_Markup (True);

      pragma Warnings
        (Off, "condition is always True");
      if Application_Main_Task_Type = Cyclic then
         Main_Task_UI.Config_Label.Set_Markup
           ("<big>Task Configuration</big>" & CRLF
            & "Cyclic type | "
            & "Delay : " & Integer'Image (Application_Main_Task_Delay_MS)
            & " ms");
      elsif Application_Main_Task_Type = Periodic then
         Main_Task_UI.Config_Label.Set_Markup
           ("<big>Task Configuration</big>" & CRLF
            & "Periodic type | "
            & "Period : " & Integer'Image (Application_Main_Task_Period_MS)
            & " ms");
      end if;
      pragma Warnings
        (On, "condition is always True");

      Main_Task_UI.Config_HBox.Pack_Start
        (Main_Task_UI.Config_Label, Expand => False, Padding => 10);

      Main_Task_UI.VBox.Pack_Start
        (Main_Task_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Main_Task_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Main_Task_UI.Watchdog_VBox);

      Gtk_New (Main_Task_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Main_Task_UI.Watchdog_Image, "./green-led.png");

      Main_Task_UI.Watchdog_VBox.Pack_Start
        (Main_Task_UI.Watchdog_Label, Expand => False, Padding => 10);

      Main_Task_UI.Watchdog_VBox.Pack_Start
        (Main_Task_UI.Watchdog_Image, Expand => False);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Main_Task_UI.Running_VBox);

      Gtk_New (Main_Task_UI.Running_Label, "Running");

      Gtk_New (Main_Task_UI.Running_Image, "./red-led.png");

      Main_Task_UI.Running_VBox.Pack_Start
        (Main_Task_UI.Running_Label, Expand => False, Padding => 10);

      Main_Task_UI.Running_VBox.Pack_Start
        (Main_Task_UI.Running_Image, Expand => False);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Running_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Main_Task_UI.Stats_VBox);

      Gtk_New (Main_Task_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Main_Task_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Main_Task_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Main_Task_UI.Stats_HBox);

      Gtk_New (Main_Task_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Main_Task_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Main_Task_UI.Stats_Values_Label, "Values");

      Main_Task_UI.Stats_HBox.Pack_Start
        (Main_Task_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Main_Task_UI.Stats_HBox.Pack_Start
        (Main_Task_UI.Stats_Values_Label, Expand => False);

      Main_Task_UI.Stats_Frame.Add (Main_Task_UI.Stats_HBox);

      Main_Task_UI.Stats_VBox.Pack_Start
        (Main_Task_UI.Stats_Frame, Expand => False, Padding => 10);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Stats_VBox, Expand => False, Padding => 10);

      --  Scheduling Stats

      Gtk_New_Vbox (Main_Task_UI.Sched_Stats_VBox);

      Gtk_New (Main_Task_UI.Sched_Stats_Frame, "Scheduling Stats (delay)");
      Gtk_Label (Main_Task_UI.Sched_Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Scheduling Stats (delay)</u>" & CRLF);
      Main_Task_UI.Sched_Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Main_Task_UI.Sched_Stats_HBox);

      Gtk_New (Main_Task_UI.Sched_Stats_Items_Label,
               "+ 100 µs : " & CRLF
               & "+ 01 ms : " & CRLF
               & "+ 10 ms : " & CRLF
               & "+ 20 ms : " & CRLF
               & "+ 30 ms : " & CRLF
               & "+ 40 ms : " & CRLF
               & "+ 50 ms : " & CRLF
               & "+ 60 ms : " & CRLF
               & "+ 70 ms : " & CRLF
               & "+ 80 ms : ");
      Main_Task_UI.Sched_Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Main_Task_UI.Sched_Stats_Values_Label, "Values");

      Main_Task_UI.Sched_Stats_HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Items_Label,
         Expand => False, Padding => 10);

      Main_Task_UI.Sched_Stats_HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Values_Label, Expand => False);

      Main_Task_UI.Sched_Stats_Frame.Add (Main_Task_UI.Sched_Stats_HBox);

      Main_Task_UI.Sched_Stats_VBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Frame, Expand => False, Padding => 10);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_VBox, Expand => False, Padding => 10);

      Main_Task_UI.VBox.Pack_Start
        (Main_Task_UI.HBox, Expand => False);

      Main_Task_UI.Frame.Add (Main_Task_UI.VBox);

      return Main_Task_UI;

   end Create_Main_Task_UI;

   function Create_Periodic_Task1_UI return Periodic_Task1_UI_Type is
      Periodic_Task1_UI : Periodic_Task1_UI_Type;
   begin
      Gtk_New (Periodic_Task1_UI.Frame, "Periodic Task 1");
      Gtk_Label (Periodic_Task1_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Periodic Task 1</b>");
      Periodic_Task1_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Periodic_Task1_UI.VBox);

      Gtk_New_Hbox (Periodic_Task1_UI.Config_HBox);

      Gtk_New (Periodic_Task1_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF
               & "Period : xxxx ms");
      Periodic_Task1_UI.Config_Label.Set_Use_Markup (True);

      Periodic_Task1_UI.Config_Label.Set_Markup
        ("<big>Task Configuration</big>" & CRLF
         & "Period : "
         & Integer'Image (Application_Periodic_Task_1_Period_MS)
         & " ms");

      Periodic_Task1_UI.Config_HBox.Pack_Start
        (Periodic_Task1_UI.Config_Label,
         Expand => False, Padding => 10);

      Periodic_Task1_UI.VBox.Pack_Start
        (Periodic_Task1_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Periodic_Task1_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Periodic_Task1_UI.Watchdog_VBox);

      Gtk_New (Periodic_Task1_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Periodic_Task1_UI.Watchdog_Image, "./green-led.png");

      Periodic_Task1_UI.Watchdog_VBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_Label, Expand => False, Padding => 10);

      Periodic_Task1_UI.Watchdog_VBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_Image, Expand => False);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Periodic_Task1_UI.Running_VBox);

      Gtk_New (Periodic_Task1_UI.Running_Label, "Running");

      Gtk_New (Periodic_Task1_UI.Running_Image, "./red-led.png");

      Periodic_Task1_UI.Running_VBox.Pack_Start
        (Periodic_Task1_UI.Running_Label, Expand => False, Padding => 10);

      Periodic_Task1_UI.Running_VBox.Pack_Start
        (Periodic_Task1_UI.Running_Image, Expand => False);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Running_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Periodic_Task1_UI.Stats_VBox);

      Gtk_New (Periodic_Task1_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Periodic_Task1_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Periodic_Task1_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Periodic_Task1_UI.Stats_HBox);

      Gtk_New (Periodic_Task1_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Periodic_Task1_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Periodic_Task1_UI.Stats_Values_Label,
               "Values");

      Periodic_Task1_UI.Stats_HBox.Pack_Start
        (Periodic_Task1_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Periodic_Task1_UI.Stats_HBox.Pack_Start
        (Periodic_Task1_UI.Stats_Values_Label, Expand => False);

      Periodic_Task1_UI.Stats_Frame.Add (Periodic_Task1_UI.Stats_HBox);

      Periodic_Task1_UI.Stats_VBox.Pack_Start
        (Periodic_Task1_UI.Stats_Frame, Expand => False, Padding => 10);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Stats_VBox, Expand => False, Padding => 10);

      Periodic_Task1_UI.VBox.Pack_Start
        (Periodic_Task1_UI.HBox, Expand => False);

      Periodic_Task1_UI.Frame.Add (Periodic_Task1_UI.VBox);

      return Periodic_Task1_UI;

   end Create_Periodic_Task1_UI;

   procedure Application_UI_Update
     (Application_UI : Application_UI_Type) is

      Days         : Ada.Calendar.Arithmetic.Day_Count;
      Seconds      : Duration;
      Leap_Seconds : Ada.Calendar.Arithmetic.Leap_Seconds_Count;

      Up_Time_Hours   : Integer;
      Up_Time_Minutes : Integer;
      Up_Time_Seconds : Integer;

      Sub_Seconds : Duration;

   begin

      Ada.Calendar.Arithmetic.Difference
        (Left         => Ada.Calendar.Clock,
         Right        => Start_Time,
         Days         => Days,
         Seconds      => Seconds,
         Leap_Seconds => Leap_Seconds);

      Ada.Calendar.Formatting.Split
        (Seconds    => Seconds,
         Hour       => Up_Time_Hours,
         Minute     => Up_Time_Minutes,
         Second     => Up_Time_Seconds,
         Sub_Second => Sub_Seconds);

      Up_Time_Hours := Up_Time_Hours + Integer (Days) * 24;

      Application_UI.Up_Time_Label.Set_Markup
        ("<big>Up time:</big>" & CRLF
         & Up_Time_Hours'Img & "h, "
         & Up_Time_Minutes'Img & "m,"
         & Up_Time_Seconds'Img & "s");

   end Application_UI_Update;

   procedure Main_Task_UI_Update
     (Main_Task_UI : in Main_Task_UI_Type;
      Terminating  : in Boolean) is
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Main_Task_UI_Update";
   begin

      if not Main_Watching then
         The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_Main_Task_Interface.Control.Start_Watching (True);
         Main_Watching := True;
      end if;

      The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => Main_Task_Watchdog_TON_Q);

      if Main_Task_Watchdog_TON_Q and not Main_Task_Watchdog_Error then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task Watchdog Time Out elapsed!");
         Main_Task_Watchdog_Error := True;
         Main_Task_UI.Watchdog_Image.Set ("./red-led.png");
      end if;

      if not Main_Running and The_Main_Task_Interface.Status.Running then
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Main task started!",
                             Log_Level => Level_Info);
         Main_Task_UI.Running_Image.Set ("./green-led.png");
         Main_Running := True;
      end if;

      if Main_Running and not The_Main_Task_Interface.Status.Running then
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Main task stopped!",
                             Log_Level => Level_Info);
         Main_Task_UI.Running_Image.Set ("./red-led.png");
         Main_Running := False;
      end if;

      Main_Task_Status := The_Main_Task_Interface.Status.Get_Status;
      Main_Task_Statistics :=
        The_Main_Task_Interface.Statistics.Get_Statistics;
      Main_Task_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (Main_Task_Statistics.Min_Duration * 1000)
         & CRLF
         & Duration'Image (Main_Task_Statistics.Max_Duration * 1000)
         & CRLF
         & Duration'Image (Main_Task_Statistics.Avg_Duration * 1000));

      Main_Task_Sched_Statistics :=
        The_Main_Task_Interface.Sched_Statistics.Get_Sched_Stats;
      Main_Task_UI.Sched_Stats_Values_Label.Set_Markup
        (Integer'Image (Main_Task_Sched_Statistics (1))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (2))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (3))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (4))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (5))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (6))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (7))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (8))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (9))
         & CRLF
         & Integer'Image (Main_Task_Sched_Statistics (10)));

   end Main_Task_UI_Update;

   procedure Periodic_Task1_UI_Update
     (Periodic_Task1_UI : in Periodic_Task1_UI_Type;
      Terminating  : in Boolean) is
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Periodic_Task1_UI_Update";
   begin

      if not Period1_Watching then
         The_GP_Task1_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_GP_Task1_Interface.Control.Start_Watching (True);
         Period1_Watching := True;
      end if;

      The_GP_Task1_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_GP_Task1_Interface.Status_Watchdog.Value,
         Error           => Period1_Task_Watchdog_TON_Q);

      if Period1_Task_Watchdog_TON_Q and not Period1_Task_Watchdog_Error then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Periodic task 1 Watchdog Time Out elapsed!");
         Period1_Task_Watchdog_Error := True;
         Periodic_Task1_UI.Watchdog_Image.Set ("./red-led.png");
      end if;

      if not Period1_Running and The_GP_Task1_Interface.Status.Running then
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Periodic task 1 started!",
                             Log_Level => Level_Info);
         Periodic_Task1_UI.Running_Image.Set ("./green-led.png");
         Period1_Running := True;
      end if;

      if Period1_Running and not The_GP_Task1_Interface.Status.Running then
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Periodic task 1 stopped!",
                             Log_Level => Level_Info);
         Periodic_Task1_UI.Running_Image.Set ("./red-led.png");
         Period1_Running := False;
      end if;

      Period1_Task_Status := The_GP_Task1_Interface.Status.Get_Status;
      Period1_Task_Statistics :=
        The_GP_Task1_Interface.Statistics.Get_Statistics;
      Periodic_Task1_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (Period1_Task_Statistics.Min_Duration * 1000)
         & CRLF
         & Duration'Image (Period1_Task_Statistics.Max_Duration * 1000)
         & CRLF
         & Duration'Image (Period1_Task_Statistics.Avg_Duration * 1000));

   end Periodic_Task1_UI_Update;

   overriding function Create
     return Instance
   is

   begin

      return Create (Ada.Calendar.Clock);

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance;
      Terminating : in Boolean)
   is

   begin

      Application_UI_Update (GUI_Element.Application_UI);
      Main_Task_UI_Update (GUI_Element.Main_UI, Terminating);
      Periodic_Task1_UI_Update (GUI_Element.Periodic1_UI, Terminating);

   end Update;

   function Create
     (Start_Time : Ada.Calendar.Time) return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "General Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.Application_UI :=
        Create_Application_UI (Start_Time);

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Application_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Main_UI := Create_Main_Task_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Main_UI.Frame,
         Expand => False);

      GUI_Element.Periodic1_UI := Create_Periodic_Task1_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Periodic1_UI.Frame,
         Expand => False);

      GUI_Element.Scrolled_Window.Add (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

end A4A.GUI_Elements.General_Status_Page;
