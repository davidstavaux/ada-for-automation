
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;          use Gtk.Widget;
with Gtk.Box;             use Gtk.Box;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Label;           use Gtk.Label;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;

package A4A.GUI_Elements.Ident_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Identity Page
   --------------------------------------------------------------------

   type Instance is new GUI_Element_Type with private;

   overriding function Create
     return Instance;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

private

   type Application_Info_UI_Type is
      record
         Frame        : Gtk_Frame;
         VBox         : Gtk_Vbox;
         HBox         : Gtk_Hbox;
         Label        : Gtk_Label;
      end record;

   function Create_Application_Info_UI return Application_Info_UI_Type;

   type Instance is new GUI_Element_Type with
      record
         Scrolled_Window : Gtk_Scrolled_Window;
         Page_Label      : Gtk_Label;

         Application_Info_UI : Application_Info_UI_Type;
      end record;

end A4A.GUI_Elements.Ident_Page;
