
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Glib; use Glib;
with Gtk.Enums;

with A4A.Protocols; use A4A.Protocols.Device_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;

with A4A.Kernel.Main; use A4A.Kernel.Main;

package body A4A.GUI_Elements.MMaster_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Modbus RTU Master Status Page
   --------------------------------------------------------------------

   function Create_MMaster_Status_UI return MMaster_Status_UI_Type is
      MMaster_Status_UI    : MMaster_Status_UI_Type;
      Baud_Rate : constant Baud_Rate_Type := Config.Baud_Rate;

      Label : Gtk_Label;
      Image : Gtk_Image;

      Command_Index : Positive;
   begin
      Raws_Number := 1 + ((Config.Command_Number - 1) / 10);

      if Config.Command_Number >= 10 then
         Columns_Number := 10;
      else
         Columns_Number := Config.Command_Number mod 10;
      end if;

      Gtk_New (MMaster_Status_UI.Frame, "Modbus RTU Master");
      Gtk_Label (MMaster_Status_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Modbus RTU Master on "
         & To_String (Config.Device)
         & " : ("
         & Integer (Baud_Rate'Enum_Rep)'Img
         & ", "
         & Config.Parity'Img
         & ", "
         & Config.Data_Bits'Img
         & ", "
         & Config.Stop_Bits'Img
         & ")</b>");
      MMaster_Status_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (MMaster_Status_UI.VBox);

      Gtk_New_Hbox (MMaster_Status_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (MMaster_Status_UI.Watchdog_VBox);

      Gtk_New (MMaster_Status_UI.Watchdog_Label, "Watchdog");

      Gtk_New (MMaster_Status_UI.Watchdog_Image, "./green-led.png");

      MMaster_Status_UI.Watchdog_VBox.Pack_Start
        (MMaster_Status_UI.Watchdog_Label, Expand => False, Padding => 10);

      MMaster_Status_UI.Watchdog_VBox.Pack_Start
        (MMaster_Status_UI.Watchdog_Image, Expand => False);

      --  Connection

      Gtk_New_Vbox (MMaster_Status_UI.Connection_VBox);

      Gtk_New (MMaster_Status_UI.Connection_Label, "Connection");

      Gtk_New (MMaster_Status_UI.Connection_Image, "./red-led.png");

      MMaster_Status_UI.Connection_VBox.Pack_Start
        (MMaster_Status_UI.Connection_Label, Expand => False, Padding => 10);

      MMaster_Status_UI.Connection_VBox.Pack_Start
        (MMaster_Status_UI.Connection_Image, Expand => False);

      --  Commands Status

      Gtk_New (MMaster_Status_UI.Commands_Status_Frame, "Commands Status");
      Gtk_Label (MMaster_Status_UI.Commands_Status_Frame.Get_Label_Widget)
        .Set_Markup ("<b>Commands Status</b>");
      MMaster_Status_UI.Commands_Status_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (MMaster_Status_UI.Commands_Status_VBox);

      Gtk_New_Hbox (MMaster_Status_UI.Commands_Status_HBox);

      Gtk_New (MMaster_Status_UI.Commands_Status_Table);
      --  One raw for columns labels
      --  One raw for each 10 commands
      --  One column for raws labels
      --  1 to 10 columns for commands

      for Col_Index in 1 .. Columns_Number loop
         Gtk_New (Label);
         Label.Set_Markup ("<b>" & Integer'Image (Col_Index) & "</b>");
         MMaster_Status_UI.Commands_Status_Table.Attach
           (Child  => Label,
            Left   => Gint (Col_Index),
            Top    => 0,
            Width  => 1,
            Height => 1);
      end loop;

      for Raw_Index in 1 .. Raws_Number loop
         Gtk_New (Label);
         Label.Set_Markup ("<b>"
                           & Integer'Image ((Raw_Index - 1) * 10) & "</b>");
         MMaster_Status_UI.Commands_Status_Table.Attach
           (Child  => Label,
            Left   => 0,
            Top    => Gint (Raw_Index),
            Width  => 1,
            Height => 1);
      end loop;

      Command_Index := 1;
      for Raw_Index in 1 .. Raws_Number loop
         for Col_Index in 1 .. Columns_Number loop

            Gtk_New (Image, Command_Status_Image_Unknown);

            MMaster_Status_UI.Commands_Status_Table.Attach
              (Child  => Image,
               Left   => Gint (Col_Index),
               Top    => Gint (Raw_Index),
               Width  => 1,
               Height => 1);

            MMaster_Status_UI.Commands_Status_Images (Command_Index) := Image;
            Command_Index := Command_Index + 1;

            exit when
              Command_Index > MMaster_Status_UI.Commands_Status_Images'Last;

         end loop;
      end loop;

      MMaster_Status_UI.Commands_Status_HBox.Pack_Start
        (MMaster_Status_UI.Commands_Status_Table,
         Expand => False, Padding => 10);

      MMaster_Status_UI.Commands_Status_VBox.Pack_Start
        (MMaster_Status_UI.Commands_Status_HBox,
         Expand => False, Padding => 10);

      MMaster_Status_UI.Commands_Status_Frame.Add
        (MMaster_Status_UI.Commands_Status_VBox);

      --  HBox

      MMaster_Status_UI.HBox.Pack_Start
        (MMaster_Status_UI.Watchdog_VBox, Expand => False, Padding => 10);

      MMaster_Status_UI.HBox.Pack_Start
        (MMaster_Status_UI.Connection_VBox, Expand => False, Padding => 10);

      --  VBox

      MMaster_Status_UI.VBox.Pack_Start
        (MMaster_Status_UI.HBox, Expand => False, Padding => 10);

      MMaster_Status_UI.VBox.Pack_Start
        (MMaster_Status_UI.Commands_Status_Frame,
         Expand => False, Padding => 10);

      --  Frame

      MMaster_Status_UI.Frame.Add (MMaster_Status_UI.VBox);

      return MMaster_Status_UI;

   end Create_MMaster_Status_UI;

   procedure MMaster_Status_UI_Update
     (MMaster_Status_UI : in MMaster_Status_UI_Type) is
      Image : Gtk_Image;
      Command_Index : Positive;
   begin

      if The_Main_Task_Interface.MMaster_Status.WD_Error
        and not Watchdog_Error
      then
         MMaster_Status_UI.Watchdog_Image.Set ("./red-led.png");
         Watchdog_Error := True;
      end if;

      Task_Status := MBRTU_Master_Task_Itf.Status.Get_Status;

      if Task_Status.Connected then
         if not Connected then
            MMaster_Status_UI.Connection_Image.Set ("./green-led.png");
         end if;
      else
         if Connected then
            MMaster_Status_UI.Connection_Image.Set ("./red-led.png");
         end if;
      end if;
      Connected := Task_Status.Connected;

      Command_Index := 1;
      for Raw_Index in 1 .. Raws_Number loop
         for Col_Index in 1 .. Columns_Number loop

            if Task_Status.Commands_Status (Command_Index)
              /= Commands_Status (Command_Index)
            then

               Image := MMaster_Status_UI.Commands_Status_Images
                 ((Raw_Index - 1) * 10 + Col_Index);

               case Task_Status.Commands_Status (Command_Index) is

                  when Unknown => Image.Set (Command_Status_Image_Unknown);

                  when Disabled => Image.Set (Command_Status_Image_Disabled);

                  when Fine => Image.Set (Command_Status_Image_Fine);

                  when Fault => Image.Set (Command_Status_Image_Fault);

               end case;

               Commands_Status (Command_Index) :=
                 Task_Status.Commands_Status (Command_Index);
            end if;

            Command_Index := Command_Index + 1;

            exit when
              Command_Index > MMaster_Status_UI.Commands_Status_Images'Last;

         end loop;
      end loop;

   end MMaster_Status_UI_Update;

   overriding function Create
     return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "Master Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.MMaster_Status_UI := Create_MMaster_Status_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.MMaster_Status_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Scrolled_Window.Add (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance)
   is

   begin

      MMaster_Status_UI_Update (GUI_Element.MMaster_Status_UI);

   end Update;

end A4A.GUI_Elements.MMaster_Status_Page;
