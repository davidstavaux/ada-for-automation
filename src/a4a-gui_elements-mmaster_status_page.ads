
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;          use Gtk.Widget;
with Gtk.Box;             use Gtk.Box;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Label;           use Gtk.Label;
with Gtk.Image;           use Gtk.Image;
with Gtk.Grid;            use Gtk.Grid;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;

with A4A.MBRTU_Master; use A4A.MBRTU_Master;
with A4A.Application.MBRTU_Master_Config;
use A4A.Application.MBRTU_Master_Config;

package A4A.GUI_Elements.MMaster_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Modbus RTU Master Status Page
   --------------------------------------------------------------------

   type Instance is new GUI_Element_Type with private;

   overriding function Create
     return Instance;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   procedure Update
     (GUI_Element : in out Instance);

private

   Command_Status_Image_Unknown   : constant String := "./pink-led.png";
   Command_Status_Image_Disabled  : constant String := "./yellow-led.png";
   Command_Status_Image_Fine      : constant String := "./green-led.png";
   Command_Status_Image_Fault     : constant String := "./red-led.png";

   Watchdog_Error    : Boolean := False;
   Connected         : Boolean := False;

   Task_Status       : A4A.MBRTU_Master.Task_Status_Type
     (Config.Command_Number);

   Raws_Number       : Positive;
   Columns_Number    : Positive;

   Commands_Status : Commands_Status_Type (Config.Commands'Range) :=
     (others => Unknown);

   type Commands_Status_Images_Type is array (Config.Commands'Range)
     of Gtk_Image;

   type MMaster_Status_UI_Type is
      record
         Frame        : Gtk_Frame;
         VBox         : Gtk_Vbox;
         HBox         : Gtk_Hbox;

         Watchdog_VBox  : Gtk_Vbox;
         Watchdog_Label : Gtk_Label;
         Watchdog_Image : Gtk_Image;

         Connection_VBox  : Gtk_Vbox;
         Connection_Label : Gtk_Label;
         Connection_Image : Gtk_Image;

         Commands_Status_Frame  : Gtk_Frame;
         Commands_Status_VBox   : Gtk_Vbox;
         Commands_Status_HBox   : Gtk_Hbox;

         Commands_Status_Table  : Gtk_Grid;
         Commands_Status_Images : Commands_Status_Images_Type;

      end record;

   type Instance is new GUI_Element_Type with
      record
         Scrolled_Window : Gtk_Scrolled_Window;
         Page_Label      : Gtk_Label;
         Page_VBox       : Gtk_Vbox;

         MMaster_Status_UI : MMaster_Status_UI_Type;
      end record;

   function Create_MMaster_Status_UI return MMaster_Status_UI_Type;

   procedure MMaster_Status_UI_Update
     (MMaster_Status_UI : in MMaster_Status_UI_Type);

end A4A.GUI_Elements.MMaster_Status_Page;
