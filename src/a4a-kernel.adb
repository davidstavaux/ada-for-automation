
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Application.Main_Periodic1;

with A4A.Application.Configuration; use A4A.Application.Configuration;

package body A4A.Kernel is

   function Quit return Boolean is
   begin
      return Quit_Flag;
   end Quit;

   procedure Periodic1_Run is
   begin
      A4A.Application.Main_Periodic1.Get_Periodic1_Inputs;
      A4A.Application.Periodic1_Cyclic;
      A4A.Application.Main_Periodic1.Set_Periodic1_Outputs;
   end Periodic1_Run;

   procedure Create_Generic_Periodic_Task_1 is
   begin
      if not Periodic_Task_1_Created then
         The_Generic_Periodic_Task_1 := new A4A.Kernel.P1.Periodic_Task
           (Task_Priority          => Application_Periodic_Task_1_Priority,
            Task_Itf               => The_GP_Task1_Interface'Access,
            Period_In_Milliseconds => Application_Periodic_Task_1_Period_MS);

         Periodic_Task_1_Created := True;
      end if;
   end Create_Generic_Periodic_Task_1;

end A4A.Kernel;
