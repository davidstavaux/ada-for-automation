
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Generic_Periodic_Task;

package A4A.Kernel is

   function Quit return Boolean;

   procedure Create_Generic_Periodic_Task_1;

   procedure Periodic1_Run;
   --  Helper

   package P1 is new A4A.Generic_Periodic_Task
     (Run => Periodic1_Run);

   --------------------------------------------------------------------
   --  Generic Periodic Task 1 Management
   --------------------------------------------------------------------

   The_GP_Task1_Interface      : aliased A4A.Kernel.P1.Task_Interface;

private

   Quit_Flag : Boolean := False;
   --  <description>
   --  When Quit is set, the application shall exit.
   --  Quit is set by SIGINT Handler.
   --  The Quit function returns its value.
   --  </description>

   The_Generic_Periodic_Task_1 : A4A.Kernel.P1.Periodic_Task_Access;
   Periodic_Task_1_Created     : Boolean := False;

end A4A.Kernel;
