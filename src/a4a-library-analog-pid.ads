
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package implements PID Controller.
--  </summary>
--  <description>
--  It provides :
--  - PID.
--  </description>
--  <group>Libraries</group>

package A4A.Library.Analog.PID is

   type Instance is tagged private;

   procedure Cyclic
     (This_PID                : in out Instance;
      Set_Point               : in Float;
      Process_Value           : in Float;
      Kp                      : in Float;
      Ki                      : in Float;
      Kd                      : in Float;
      Initialise              : in Boolean;
      Period_In_Milliseconds  : Natural;
      Manipulated_Value       : out Float
     );

private

   type Instance is tagged
      record
         Previous_Error : Float;
         Integral       : Float;
      end record;

end A4A.Library.Analog.PID;
