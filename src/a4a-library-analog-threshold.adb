
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Analog.Threshold is

   procedure Initialise
     (This_Threshold : in out Instance;
      Hysteresis     : Float;

      HHH_T          : Float;
      HH_T           : Float;
      H_T            : Float;
      L_T            : Float;
      LL_T           : Float;
      LLL_T          : Float
     ) is
   begin
      This_Threshold.Hysteresis := Hysteresis;

      This_Threshold.HHH_T := HHH_T;
      This_Threshold.HH_T  := HH_T;
      This_Threshold.H_T   := H_T;
      This_Threshold.L_T   := L_T;
      This_Threshold.LL_T  := LL_T;
      This_Threshold.LLL_T := LLL_T;
   end Initialise;

   procedure Cyclic
     (This_Threshold : in out Instance;
      Value          : in Float;
      HHH            : out Boolean;
      HH             : out Boolean;
      H              : out Boolean;
      L              : out Boolean;
      LL             : out Boolean;
      LLL            : out Boolean
     ) is
   begin

      if Value > This_Threshold.HHH_T then
         This_Threshold.HHH := True;
      elsif Value <= This_Threshold.HHH_T - This_Threshold.Hysteresis then
         This_Threshold.HHH := False;
      end if;

      if Value > This_Threshold.HH_T then
         This_Threshold.HH := True;
      elsif Value <= This_Threshold.HH_T - This_Threshold.Hysteresis then
         This_Threshold.HH := False;
      end if;

      if Value > This_Threshold.H_T then
         This_Threshold.H := True;
      elsif Value <= This_Threshold.H_T - This_Threshold.Hysteresis then
         This_Threshold.H := False;
      end if;

      if Value > This_Threshold.L_T + This_Threshold.Hysteresis then
         This_Threshold.L := False;
      elsif Value <= This_Threshold.L_T then
         This_Threshold.L := True;
      end if;

      if Value > This_Threshold.LL_T + This_Threshold.Hysteresis then
         This_Threshold.LL := False;
      elsif Value <= This_Threshold.LL_T then
         This_Threshold.LL := True;
      end if;

      if Value > This_Threshold.LLL_T + This_Threshold.Hysteresis then
         This_Threshold.LLL := False;
      elsif Value <= This_Threshold.LLL_T then
         This_Threshold.LLL := True;
      end if;

      HHH := This_Threshold.HHH;
      HH  := This_Threshold.HH;
      H   := This_Threshold.H;

      L   := This_Threshold.L;
      LL  := This_Threshold.LL;
      LLL := This_Threshold.LLL;
   end Cyclic;

end A4A.Library.Analog.Threshold;
