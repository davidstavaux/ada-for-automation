
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides a home for analog stuff.
--  </summary>
--  <description>
--  It provides :
--  - Scale_In,
--  - Scale_Out,
--  - Threshold,
--  - Limits,
--  - Filters.
--  </description>
--  <group>Libraries</group>

package A4A.Library.Analog is

   function Scale_In
     (X    : Integer;
      Xmin : Integer;
      Xmax : Integer;
      Ymin : Float;
      Ymax : Float)
      return Float;
   --  returns the value scaled :
   --  Y = Ymax + ((X - Xmax) * (Ymax - Ymin) / (Xmax - Xmin))

   function Scale_Out
     (X    : Float;
      Xmin : Float;
      Xmax : Float;
      Ymin : Integer;
      Ymax : Integer)
      return Integer;
   --  returns the value scaled :
   --  Y = Ymax + ((X - Xmax) * (Ymax - Ymin) / (Xmax - Xmin))

   function Limits
     (X    : Float;
      Ymin : Float;
      Ymax : Float)
      return Float;
   --  returns the value limited to the bounds

   type Gradient_Type is new Float range 0.0 .. Float'Last;

   procedure Ramp
     (Value_In    : in Float;
      Gradient    : in Gradient_Type;
      Value_Out   : in out Float
     );
   --  has to be called in a periodic task
   --  the gradient has to be calculated accordingly

end A4A.Library.Analog;
