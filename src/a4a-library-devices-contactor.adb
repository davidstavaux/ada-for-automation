
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Devices.Contactor is

   overriding function Create
     (Id : in String)
      return Instance is
      Tempo_Dis : TON.Instance;
   begin
      return (Limited_Controlled with Id => new String'(Id),
              Status    => Status_Off,
              Preset    => Milliseconds (Default_TON_Preset),
              Tempo_Dis => Tempo_Dis,
              IO_Status  => (False, False));
   end Create;

   function Create
     (Id         : in String;
      TON_Preset : in Positive)
      --  TON Preset in milliseconds
      return Instance is
      Tempo_Dis : TON.Instance;
   begin
      return (Limited_Controlled with Id => new String'(Id),
              Status    => Status_Off,
              Preset    => Milliseconds (TON_Preset),
              Tempo_Dis => Tempo_Dis,
              IO_Status  => (False, False));
   end Create;

   procedure Cyclic
     (Device    : in out Instance;
      Feed_Back : in Boolean;
      Ack       : in Boolean;
      Cmd_On    : in Boolean;
      Coil      : out Boolean) is
      Elapsed : Time_Span;
      Tempo_Q : Boolean;
   begin

      case Device.Status is
         when Status_Off =>
            if Cmd_On then
               Device.Status := Status_Going_On;
            end if;
         when Status_Going_On =>
            if Feed_Back then
               Device.Status := Status_On;
            end if;
         when Status_On =>
            if not Cmd_On then
               Device.Status := Status_Going_Off;
            end if;
         when Status_Going_Off =>
            if not Feed_Back then
               Device.Status := Status_Off;
            end if;
         when Status_Fault =>
            if Ack then
               Device.Status := Status_Off;
            end if;
      end case;

      Coil := (Device.Status = Status_Going_On) or (Device.Status = Status_On);

      Device.Tempo_Dis.Cyclic
        (Start      => (Coil and not Feed_Back) or (not Coil and Feed_Back),
         Preset     => Device.Preset,
         Elapsed    => Elapsed,
         Q          => Tempo_Q);

      if Tempo_Q then
         Device.Status := Status_Fault;
      end if;

      Device.IO_Status.Feed_Back := Feed_Back;
      Device.IO_Status.Coil := Coil;

   end Cyclic;

   function is_On
     (Device    : in Instance) return Boolean is
   begin
      return (Device.Status = Status_On);
   end is_On;

   function is_Faulty
     (Device    : in Instance) return Boolean is
   begin
      return (Device.Status = Status_Fault);
   end is_Faulty;

   function Get_IO_Status
     (Device    : in Instance) return IO_Status_Type is
   begin
      return Device.IO_Status;
   end Get_IO_Status;

end A4A.Library.Devices.Contactor;
