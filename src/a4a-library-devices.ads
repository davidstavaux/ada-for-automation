
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Finalization; use Ada.Finalization;
with Ada.Unchecked_Deallocation;

package A4A.Library.Devices is
   type Device_Type is abstract tagged limited private;
   type Device_Ptr is access Device_Type'Class;

   function Create
     (Id : in String)
      return Device_Type
      is abstract;

   function Get_Id
     (Device : in Device_Type)
      return String;

private

   type Device_Id is access String;
   type Device_Type is abstract new Limited_Controlled with
      record
         Id : Device_Id;
      end record;

   procedure Free_Device_Id is new Ada.Unchecked_Deallocation
     (Object => String, Name => Device_Id);

   overriding
   procedure Initialize (Device : in out Device_Type);

   overriding
   procedure Finalize   (Device : in out Device_Type);

   Device_Count : Integer := 0;

end A4A.Library.Devices;
