
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Simulation.Valve is

   function Create
     (TON_Preset : in Positive := Default_TON_Preset)
      --  TON Preset in milliseconds
      return Instance is
      Device : Instance;
   begin
      Device.Preset_Simu := Milliseconds (TON_Preset);
      return Device;
   end Create;

   procedure Cyclic
     (Device      : in out Instance;
      Auto        : in Boolean;
      ZO_Manu     : in Boolean;
      ZF_Manu     : in Boolean;
      Coil        : in Boolean;
      Pos_Open    : out Boolean;
      Pos_Closed  : out Boolean) is

      Elapsed     : Time_Span;
      Tempo_Q     : Boolean;
      Tempo_Start : Boolean;
   begin

      Tempo_Start :=
        (Device.Status = Status_Opening) or (Device.Status = Status_Closing);

      Device.Tempo_Simu.Cyclic
        (Start      => Tempo_Start,
         Preset     => Device.Preset_Simu,
         Elapsed    => Elapsed,
         Q          => Tempo_Q);

      case Device.Status is
         when Status_Closed =>
            if Coil then
               Device.Status := Status_Opening;
            end if;
         when Status_Opening =>
            if Tempo_Q then
               Device.Status := Status_Open;
            end if;
         when Status_Open =>
            if not Coil then
               Device.Status := Status_Closing;
            end if;
         when Status_Closing =>
            if Tempo_Q then
               Device.Status := Status_Closed;
            end if;
      end case;

      Pos_Open :=
        (Auto and (Device.Status = Status_Open)) or (not Auto and ZO_Manu);

      Pos_Closed :=
        (Auto and (Device.Status = Status_Closed)) or (not Auto and ZF_Manu);

   end Cyclic;

end A4A.Library.Simulation.Valve;
