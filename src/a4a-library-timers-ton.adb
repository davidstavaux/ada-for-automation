
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Timers.TON is

   procedure Cyclic
     (This_Timer : in out Instance;
      Start      : in Boolean;
      Preset     : in Time_Span;
      Elapsed    : out Time_Span;
      Q          : out Boolean
     ) is
   begin

      if not Start then
         This_Timer.Status := Status_Off;
      end if;

      case This_Timer.Status is
         when Status_Off =>
            if Start then
               This_Timer.Status     := Status_Running;
               This_Timer.Start_Time := Clock_Time;
               This_Timer.End_Time   := This_Timer.Start_Time + Preset;
               Elapsed := Time_Span_Zero;
            end if;
         when Status_Running =>
            Elapsed := Clock_Time - This_Timer.Start_Time;
            if Elapsed >= Preset then
               This_Timer.Status := Status_On;
            end if;
         when Status_On =>
            null;
      end case;

      Q := This_Timer.Status = Status_On;
   end Cyclic;

end A4A.Library.Timers.TON;
