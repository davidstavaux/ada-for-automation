
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Timers is

   task body Clock_Handler_Task_Type is
      Next_Time : Time := Clock;
      Period : constant Time_Span := Milliseconds (Period_In_Milliseconds);
   begin
      loop
         Next_Time := Next_Time + Period;

         Clock_Time_Val := Clock;

         exit when Task_Itf.Control.Quit;
         delay until Next_Time;
      end loop;

      Task_Itf.Status.Terminated (Task_Itf.Control.Quit);
   end Clock_Handler_Task_Type;

   function Clock_Time return Time is
   begin
      return Clock_Time_Val;
   end Clock_Time;

end A4A.Library.Timers;
