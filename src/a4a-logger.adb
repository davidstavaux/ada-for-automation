
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Calendar.Formatting;

package body A4A.Logger is

   protected body Logger_Type is

      function Get_Level return Log_Level_Type is
      begin
         return The_Log_Level;
      end Get_Level;

      procedure Set_Level (Log_Level : Log_Level_Type) is
      begin
         The_Log_Level := Log_Level;
      end Set_Level;

      function Get_Console_Color_Support
        return Boolean is
      begin
         return Console_Color_Supported;
      end Get_Console_Color_Support;

      procedure Set_Console_Color_Support
        (Supported : in Boolean) is
      begin
         Console_Color_Supported := Supported;
      end Set_Console_Color_Support;

      procedure Put
        (Who  : in String;
         What : in String;
         Log_Level : in Log_Level_Type := Level_Error) is

         Log_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;
         Log_Item : Log_String;

      begin

         if Log_Level > The_Log_Level then
            return;
         end if;

         declare
            Time_Stamp_Str : constant String := Ada.Calendar.Formatting.Image
              (Date                  => Log_Time,
               Include_Time_Fraction => True,
               Time_Zone             => The_Time_Offset);

            Info_Str : constant String := " => " & Who & " : " & What;
         begin
            if Console_Color_Supported then
               Log_Item := new String'
                 (Get_Console_Color (Log_Level)
                  & Time_Stamp_Str & Info_Str & NORMAL_CONSOLE);
            else
               Log_Item := new String'
                 (Time_Stamp_Str & " "
                  & Get_Console_String (Log_Level) & Info_Str);
            end if;
         end;

         Log_List.Append (Log_Item);

      end Put;

      function Get_First
        return String is
      begin
         return Log_List.First_Element.all;
      end Get_First;

      procedure Delete_First is
         Log_Item : Log_String;
      begin
         Log_Item := Log_List.First_Element;
         Free_Log_String (Log_Item);
         Log_List.Delete_First;
      end Delete_First;

      function Is_Empty
        return Boolean is
      begin
         return Log_List.Is_Empty;
      end Is_Empty;

   end Logger_Type;

   function Get_Console_Color
        (Log_Level : in Log_Level_Type)
         return String is
   begin
      case Log_Level is
         when Level_Error =>
            return RED_CONSOLE;
         when Level_Warning =>
            return YELLOW_CONSOLE;
         when Level_Info =>
            return GREEN_CONSOLE;
         when Level_Verbose =>
            return NORMAL_CONSOLE;
      end case;
   end Get_Console_Color;

   function Get_Console_String
        (Log_Level : in Log_Level_Type)
         return String is
   begin
      case Log_Level is
         when Level_Error =>
            return ERROR_STRING;
         when Level_Warning =>
            return WARN_STRING;
         when Level_Info =>
            return INFO_STRING;
         when Level_Verbose =>
            return VERB_STRING;
      end case;

   end Get_Console_String;
end A4A.Logger;
