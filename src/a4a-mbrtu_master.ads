
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Tasks.Interfaces;
with A4A.Tasks.Watchdog; use A4A.Tasks.Watchdog;

with A4A.Protocols; use A4A.Protocols.Device_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;
with A4A.Dual_Port_Memory; use A4A.Dual_Port_Memory;

package A4A.MBRTU_Master is

   Faults_Time_Out_MS   : Natural := 10000;

   type Command_Type (Action : Action_Type := Read_Registers) is
      record
         Enabled          : Boolean := True;
         Period_Multiple  : Positive;
         Shift            : Natural;

         Slave            : Slave_Address;

         case Action is

            when Read_Bits | Read_Input_Bits | Read_Registers |
                 Read_Input_Registers | Write_Bit | Write_Register |
                 Write_Bits | Write_Registers =>

               Number           : Positive;
               Offset_Remote    : Offset_Type;
               Offset_Local     : Offset_Type;

            when Write_Read_Registers =>

               Write_Number        : Positive;
               Write_Offset_Remote : Offset_Type;
               Write_Offset_Local  : Offset_Type;

               Read_Number         : Positive;
               Read_Offset_Remote  : Offset_Type;
               Read_Offset_Local   : Offset_Type;

         end case;

      end record;

   type Commands_Table is array (Positive range <>) of Command_Type;

   --  Command_Number : The number of commands in the table
   type Master_Configuration
     (Command_Number : Positive) is
      record
         Enabled           : Boolean := True;
         --  Enables the Master Scanning

         Debug_On          : Boolean := False;
         --  Enables Modbus traces

         Task_Period_MS    : Positive;
         --  The period of the Master Task

         Retries           : Natural;
         --  The task will retry "Retries" time before setting status to fault

         Timeout           : Duration := 0.5;
         --  Command Time Out

         Device            : Bounded_String;
         Baud_Rate         : Baud_Rate_Type := BR_9600;
         Parity            : Parity_Type    := Even;
         Data_Bits         : Data_Bits_Type := 8;
         Stop_Bits         : Stop_Bits_Type := 1;

         Commands          : Commands_Table (1 .. Command_Number);
      end record;
   type Master_Configuration_Access is access all Master_Configuration;

   type Master_Configuration_Access_Array is array (Positive range <>)
     of Master_Configuration_Access;

   type Command_Status_Type is
     (Unknown,
      Disabled,
      Fine,
      Fault);

   type Command_Data_Type is
      record
         Retries : Natural := 3;
      end record;

   type Commands_Status_Type is array (Positive range <>)
     of Command_Status_Type;

   type Commands_Data_Type is array (Positive range <>)
     of Command_Data_Type;

   type Task_Status_Type (Command_Number : Positive) is
      record
         Terminated  : Boolean  := False;

         Connected   : Boolean  := False;
         Commands_Status : Commands_Status_Type (1 .. Command_Number) :=
           (others => Unknown);
         Commands_Data : Commands_Data_Type (1 .. Command_Number);
      end record;

   protected type Task_Status (Command_Number : Positive) is

      procedure Terminated
        (Value : in Boolean);
      function Terminated return Boolean;

      procedure Connected
        (Value : in Boolean);
      function Connected return Boolean;

      procedure Set_Command_Status
        (Command    : in Positive;
         Cmd_Status : in Command_Status_Type);

      function Get_Command_Status
        (Command    : in Positive)
         return Command_Status_Type;

      procedure Set_Command_Retries
        (Command : in Positive;
         Retries : in Natural);

      function Get_Command_Retries
        (Command : in Positive)
         return Natural;

      function Get_Status return Task_Status_Type;

   private
      Status : Task_Status_Type (Command_Number);

   end Task_Status;

   type Task_Interface (Command_Number : Positive) is
      record
         Control : A4A.Tasks.Interfaces.Task_Control;
         Status  : Task_Status (Command_Number);

         Control_Watchdog : Control_Watchdog_Type;
         Status_Watchdog  : Status_Watchdog_Type;
      end record;
   type Task_Itf_Access is access all Task_Interface;

   task type Periodic_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Task_Itf_Access;
      Configuration           : Master_Configuration_Access;
      Bool_DPM_Access         : Bool_Dual_Port_Memory_Access;
      Word_DPM_Access         : Word_Dual_Port_Memory_Access) is

      pragma Priority (Task_Priority);
   end Periodic_Task;
   type Periodic_Task_Access is access Periodic_Task;

   type Periodic_Task_Access_Array is array (Positive range <>)
     of  Periodic_Task_Access;

private

   procedure Run
     (Configuration : Master_Configuration_Access;
      Task_Itf      : Task_Itf_Access;
      Bool_DPM_Access : Bool_Dual_Port_Memory_Access;
      Word_DPM_Access : Word_Dual_Port_Memory_Access);
   --  The job of the periodic task

end A4A.MBRTU_Master;
