
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;

with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.MBRTU_Slave is

   protected body Task_Status is

      procedure Terminated (Value : in Boolean) is
      begin
         Status.Terminated := Value;
      end Terminated;

      function Terminated return Boolean is
      begin
         return Status.Terminated;
      end Terminated;

      procedure Update_Commands_Status
        (Commands_Status : Commands_Status_Type) is
      begin
         Status.Commands_Status := Commands_Status;
      end Update_Commands_Status;

      function Get_Status return Task_Status_Type is
      begin
         return Status;
      end Get_Status;

   end Task_Status;

   procedure Inputs_Bits_Write
     (Inputs : in Bool_Array;
      Offset : in Natural) is
   begin
      Input_Bits_RW_Lock.Write_Lock;
      for Index in Inputs'Range loop
         if Inputs (Index) then
            Input_Bits (Offset + Index - Inputs'First) := LibModbus.C_TRUE;
         else
            Input_Bits (Offset + Index - Inputs'First) := LibModbus.C_FALSE;
         end if;
      end loop;
      Input_Bits_RW_Lock.Write_Unlock;
   end Inputs_Bits_Write;

   procedure Inputs_Bit_Write
     (Input  : in Boolean;
      Offset : in Natural) is
   begin
      Input_Bits_RW_Lock.Write_Lock;
      if Input then
         Input_Bits (Offset) := LibModbus.C_TRUE;
      else
         Input_Bits (Offset) := LibModbus.C_FALSE;
      end if;
      Input_Bits_RW_Lock.Write_Unlock;
   end Inputs_Bit_Write;

   procedure Inputs_Bit_Read
     (Input  : out Boolean;
      Offset : in Natural) is
   begin
      Input_Bits_RW_Lock.Read_Lock;
      if Input_Bits (Offset) = LibModbus.C_FALSE then
         Input := False;
      else
         Input := True;
      end if;
      Input_Bits_RW_Lock.Read_Unlock;
   end Inputs_Bit_Read;

   procedure Coils_Read
     (Outputs : out Bool_Array;
      Offset  : in Natural) is
   begin
      Coils_RW_Lock.Read_Lock;
      for Index in Outputs'Range loop
         if Coils (Offset + Index - Outputs'First) = LibModbus.C_FALSE then
            Outputs (Index) := False;
         else
            Outputs (Index) := True;
         end if;
      end loop;
      Coils_RW_Lock.Read_Unlock;
   end Coils_Read;

   procedure Coil_Read
     (Output  : out Boolean;
      Offset  : in Natural) is
   begin
      Coils_RW_Lock.Read_Lock;
      if Coils (Offset) = LibModbus.C_FALSE then
         Output := False;
      else
         Output := True;
      end if;
      Coils_RW_Lock.Read_Unlock;
   end Coil_Read;

   procedure Coils_Write
     (Inputs : in Bool_Array;
      Offset : in Natural) is
   begin
      Coils_RW_Lock.Write_Lock;
      for Index in Inputs'Range loop
         if Inputs (Index) then
            Coils (Offset + Index - Inputs'First) := LibModbus.C_TRUE;
         else
            Coils (Offset + Index - Inputs'First) := LibModbus.C_FALSE;
         end if;
      end loop;
      Coils_RW_Lock.Write_Unlock;
   end Coils_Write;

   procedure Coil_Write
     (Input  : in Boolean;
      Offset : in Natural) is
   begin
      Coils_RW_Lock.Write_Lock;
      if Input then
         Coils (Offset) := LibModbus.C_TRUE;
      else
         Coils (Offset) := LibModbus.C_FALSE;
      end if;
      Coils_RW_Lock.Write_Unlock;
   end Coil_Write;

   procedure Inputs_Registers_Write
     (Inputs : in Word_Array;
      Offset : in Natural) is
   begin
      Input_Registers_RW_Lock.Write_Lock;
      Input_Registers (Offset .. Offset + Inputs'Length - 1) := Inputs;
      Input_Registers_RW_Lock.Write_Unlock;
   end Inputs_Registers_Write;

   procedure Registers_Read
     (Outputs : out Word_Array;
      Offset  : in Natural) is
   begin
      Registers_RW_Lock.Read_Lock;
      Outputs := Registers (Offset .. Offset + Outputs'Length - 1);
      Registers_RW_Lock.Read_Unlock;
   end Registers_Read;

   procedure Registers_Write
     (Inputs : in Word_Array;
      Offset : in Natural) is
   begin
      Registers_RW_Lock.Write_Lock;
      Registers (Offset .. Offset + Inputs'Length - 1) := Inputs;
      Registers_RW_Lock.Write_Unlock;
   end Registers_Write;

   procedure Run
     (Configuration : Slave_Configuration_Access;
      Task_Itf      : Task_Itf_Access) is
      My_Ident : constant String := "A4A.MBRTU_Slave.Run";

      --  Supported Function Codes
      Read_Coils               : constant := 1;
      Read_Discrete_Inputs     : constant := 2;
      Read_Holding_Registers   : constant := 3;
      Read_Input_Registers     : constant := 4;
      Write_Single_Coil        : constant := 5;
      Write_Single_Register    : constant := 6;
      Write_Multiple_Coils     : constant := 15;
      Write_Multiple_Registers : constant := 16;
      Write_Read_Registers     : constant := 23;

      Function_Code_Index : constant := 1;
      --  Request(0) is slave address
      --  Request(1) is function code

      MyContext         : LibModbus.Context_Type;
      Result            : Interfaces.C.int;
      pragma Unreferenced (Result);
      Req_Len            : Interfaces.C.int;
      Request           : aliased Byte_Array :=
        (0 .. LibModbus.MODBUS_RTU_MAX_ADU_LENGTH => 0);

      Context_Ok : Boolean := False;
      Connect_Ok : Boolean := False;

      Connected_To_Master : Boolean  := False;

      Watchdog_TON_Q       : Boolean := False;
      Watchdog_Error       : Boolean := False;

      procedure Close;

      procedure Close is
         My_Ident : constant String := "A4A.MBRTU_Slave.Run.Close";
      begin
         if Connect_Ok then
            LibModbus.Close (Context => MyContext);
            Connect_Ok := False;
         end if;
         if Context_Ok then
            LibModbus.Free (Context => MyContext);
            Context_Ok := False;
         end if;
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Closing gracefully.",
                             Log_Level => Level_Info);
      end Close;

      task Monitoring_Task;
      task body Monitoring_Task is
         Period : constant := 0.1;
         Counter : Natural := 0;
         Master_Time_Out : constant := 10;
      begin
         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Monitoring task started!",
                             Log_Level => Level_Info);

         loop

            Task_Itf.Status_Watchdog.Watchdog
              (Watching         => Task_Itf.Control.Start_Watching,
               Control_Watchdog => Task_Itf.Control_Watchdog.Value,
               Error            => Watchdog_TON_Q);

            if Watchdog_TON_Q and not Watchdog_Error then
               A4A.Log.Logger.Put (Who       => My_Ident,
                                   What      => "Watchdog Time Out elapsed!",
                                   Log_Level => Level_Error);
               Watchdog_Error := True;
            end if;

            if Connected_To_Master = False then
               Counter := Counter + 1;
            else
               Counter := 0;
               Status.Commands_Status.Connected_To_Master := True;
            end if;

            if (Counter > Master_Time_Out) then
               Status.Commands_Status.Connected_To_Master := False;
            end if;

            Connected_To_Master := False;

            Task_Itf.Status.Update_Commands_Status (Status.Commands_Status);

            Status.Terminated := Task_Itf.Control.Quit;

            exit when Status.Terminated;

            delay Period;

         end loop;

         A4A.Log.Logger.Put (Who       => My_Ident,
                             What      => "Monitoring task terminated!",
                             Log_Level => Level_Info);

      end Monitoring_Task;

   begin

      --  if not initialized this way, LibModbus.Receive fails
      --  TO_DO : Test if Request(0) := 0; would suffice
      for Index in Request'Range loop
         Request (Index) := 0;
      end loop;

      MyContext := LibModbus.New_RTU
         (Device    => To_String (Configuration.Device),
          Baud_Rate => Configuration.Baud_Rate,
          Parity    => Configuration.Parity,
          Data_Bits => Configuration.Data_Bits,
          Stop_Bits => Configuration.Stop_Bits);
      Context_Ok := True;

      LibModbus.Set_Slave (Context => MyContext, Slave => Configuration.Slave);

      LibModbus.Set_Debug (Context => MyContext, On => Configuration.Debug_On);

      LibModbus.Connect (Context => MyContext);
      Connect_Ok := True;

      --  Wait forever a master connection until told to quit
      loop

         Req_Len := LibModbus.Receive (Context => MyContext,
                                       Request => Request);

         if (Req_Len > 0) then

            Connected_To_Master := True;

            case Request (Function_Code_Index) is

               when Read_Coils =>

                  Status.Commands_Status.Read_Coils_Count :=
                    Status.Commands_Status.Read_Coils_Count + 1;

                  Coils_RW_Lock.Read_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Coils_RW_Lock.Read_Unlock;

               when Read_Discrete_Inputs =>

                  Status.Commands_Status.Read_Input_Bits_Count :=
                    Status.Commands_Status.Read_Input_Bits_Count + 1;

                  Input_Bits_RW_Lock.Read_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Input_Bits_RW_Lock.Read_Unlock;

               when Read_Holding_Registers =>

                  Status.Commands_Status
                    .Read_Holding_Registers_Count :=
                      Status.Commands_Status.Read_Holding_Registers_Count + 1;

                  Registers_RW_Lock.Read_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Registers_RW_Lock.Read_Unlock;

               when Read_Input_Registers =>

                  Status.Commands_Status.Read_Input_Registers_Count :=
                    Status.Commands_Status.Read_Input_Registers_Count + 1;

                  Input_Registers_RW_Lock.Read_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Input_Registers_RW_Lock.Read_Unlock;

               when Write_Single_Coil =>

                  Status.Commands_Status.Write_Single_Coil_Count :=
                    Status.Commands_Status.Write_Single_Coil_Count + 1;

                  Coils_RW_Lock.Write_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Coils_RW_Lock.Write_Unlock;

               when Write_Single_Register =>

                  Status.Commands_Status
                    .Write_Single_Register_Count :=
                      Status.Commands_Status.Write_Single_Register_Count + 1;

                  Registers_RW_Lock.Write_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Registers_RW_Lock.Write_Unlock;

               when Write_Multiple_Coils =>

                  Status.Commands_Status.Write_Multiple_Coils_Count :=
                    Status.Commands_Status.Write_Multiple_Coils_Count + 1;

                  Coils_RW_Lock.Write_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Coils_RW_Lock.Write_Unlock;

               when Write_Multiple_Registers =>

                  Status.Commands_Status
                    .Write_Multiple_Registers_Count :=
                      Status.Commands_Status
                        .Write_Multiple_Registers_Count + 1;

                  Registers_RW_Lock.Write_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Registers_RW_Lock.Write_Unlock;

               when Write_Read_Registers =>

                  Status.Commands_Status.Write_Read_Registers_Count :=
                    Status.Commands_Status.Write_Read_Registers_Count + 1;

                  Registers_RW_Lock.Write_Lock;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

                  Registers_RW_Lock.Write_Unlock;

               when others =>

                  Status.Commands_Status.Unmanaged_Requests_Count :=
                    Status.Commands_Status.Unmanaged_Requests_Count + 1;

                  Result := LibModbus.Reply
                    (Context        => MyContext,
                     Request        => Request,
                     Request_Len    => Req_Len,
                     Mapping_Access => My_Mapping);

            end case;

         else
            --  Error
            A4A.Log.Logger.Put
              (Who  => My_Ident,
               What => "Got an error !");

         end if;

         exit when Status.Terminated;

      end loop;

      Close;

   exception
      when Error : LibModbus.Context_Error | LibModbus.Connect_Error =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "libmodbus exception: " & CRLF
                             & Exception_Information (Error));
         Close;

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Unexpected exception: " & CRLF
                             & Exception_Information (Error));
         Close;

   end Run;

   task body Periodic_Task is
      My_Ident : constant String := "A4A.MBRTU_Slave.Periodic_Task "
        & To_String (Configuration.Device) & ":"
        & Configuration.Slave'Img;

   begin
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Started !",
                          Log_Level => Level_Info);

      loop

         Run (Configuration, Task_Itf);

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Oups !");

         Status.Terminated := Task_Itf.Control.Quit;
         exit when Status.Terminated;

         delay Configuration.Retries * 1.0; -- Wait 3 seconds and retry

      end loop;

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Finished !",
                          Log_Level => Level_Info);

      Task_Itf.Status.Terminated (Status.Terminated);

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Unexpected exception: " & CRLF
                             & Exception_Information (Error));
   end Periodic_Task;

end A4A.MBRTU_Slave;
