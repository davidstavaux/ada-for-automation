
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Tasks.Interfaces;
with A4A.Tasks.Watchdog; use A4A.Tasks.Watchdog;

with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;
with A4A.Lock_Mechanisms; use A4A.Lock_Mechanisms;

generic
   Coils_Number           : Positive;
   Input_Bits_Number      : Positive;
   Input_Registers_Number : Positive;
   Registers_Number       : Positive;
package A4A.MBTCP_Server is

   package LibModbus renames A4A.Protocols.LibModbus;

   type Server_Configuration
   is
      record
         Server_Enabled    : Boolean := True;
         Debug_On          : Boolean := False;
         --  enables Modbus traces

         Retries           : Positive;
         --  wait Retries * 1s before trying again

         Server_IP_Address : Bounded_String;
         Server_TCP_Port   : TCP_Port_Type;
      end record;
   type Server_Configuration_Access is access all Server_Configuration;

   type Commands_Status_Type is
      record
         Connected_Clients               : Natural  := 0;
         Read_Coils_Count                : DWord    := 0; -- 01
         Read_Input_Bits_Count           : DWord    := 0; -- 02
         Read_Holding_Registers_Count    : DWord    := 0; -- 03
         Read_Input_Registers_Count      : DWord    := 0; -- 04
         Write_Single_Coil_Count         : DWord    := 0; -- 05
         Write_Single_Register_Count     : DWord    := 0; -- 06
         Write_Multiple_Coils_Count      : DWord    := 0; -- 15
         Write_Multiple_Registers_Count  : DWord    := 0; -- 16
         Write_Read_Registers_Count      : DWord    := 0; -- 23
         Unmanaged_Requests_Count        : DWord    := 0;
      end record;

   type Task_Status_Type is
      record
         Terminated  : Boolean  := False;

         Commands_Status : Commands_Status_Type;
      end record;

   protected type Task_Status is

      procedure Terminated (Value : in Boolean);
      function Terminated return Boolean;

      procedure Update_Commands_Status
        (Commands_Status : Commands_Status_Type);

      function Get_Status return Task_Status_Type;

   private
      Status : Task_Status_Type;

   end Task_Status;

   type Task_Interface is
      record
         Control : A4A.Tasks.Interfaces.Task_Control;
         Status  : Task_Status;

         Control_Watchdog : Control_Watchdog_Type;
         Status_Watchdog  : Status_Watchdog_Type;
      end record;
   type Task_Itf_Access is access all Task_Interface;

   task type Periodic_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Task_Itf_Access;
      Configuration           : Server_Configuration_Access
     ) is
      pragma Priority (Task_Priority);
   end Periodic_Task;
   type Periodic_Task_Access is access Periodic_Task;

   procedure Inputs_Bits_Write
     (Inputs : in Bool_Array;
      Offset : in Natural);

   procedure Inputs_Bit_Write
     (Input  : in Boolean;
      Offset : in Natural);

   procedure Inputs_Bit_Read
     (Input  : out Boolean;
      Offset : in Natural);

   procedure Coils_Read
     (Outputs : out Bool_Array;
      Offset  : in Natural);

   procedure Coil_Read
     (Output  : out Boolean;
      Offset  : in Natural);

   procedure Coils_Write
     (Inputs : in Bool_Array;
      Offset : in Natural);

   procedure Coil_Write
     (Input  : in Boolean;
      Offset : in Natural);

   procedure Inputs_Registers_Write
     (Inputs : in Word_Array;
      Offset : in Natural);

   procedure Registers_Read
     (Outputs : out Word_Array;
      Offset  : in Natural);

   procedure Registers_Write
     (Inputs : in Word_Array;
      Offset : in Natural);

private

   Coils                   : aliased Byte_Array :=
     (0 .. Coils_Number - 1 => 0);
   Input_Bits              : aliased Byte_Array :=
     (0 .. Input_Bits_Number - 1 => 0);
   Input_Registers         : aliased Word_Array :=
     (0 .. Input_Registers_Number - 1 => 0);
   Registers               : aliased Word_Array :=
     (0 .. Registers_Number - 1 => 0);

   My_Mapping : aliased LibModbus.Modbus_Mapping_Type :=
     (nb_bits               => Coils'Length,
      start_bits            => 0,
      nb_input_bits         => Input_Bits'Length,
      start_input_bits      => 0,
      nb_input_registers    => Input_Registers'Length,
      start_input_registers => 0,
      nb_registers          => Registers'Length,
      start_registers       => 0,
      tab_bits              => Coils'Address,
      tab_input_bits        => Input_Bits'Address,
      tab_input_registers   => Input_Registers'Address,
      tab_registers         => Registers'Address
     );

   Input_Bits_RW_Lock      : Simple_RW_Lock_Mechanism;
   Coils_RW_Lock           : Simple_RW_Lock_Mechanism;
   Input_Registers_RW_Lock : Simple_RW_Lock_Mechanism;
   Registers_RW_Lock       : Simple_RW_Lock_Mechanism;

   Status : Task_Status_Type;
   --  Private_Task_Status

   procedure Run
     (Configuration : Server_Configuration_Access;
      Task_Itf      : Task_Itf_Access);
   --  The job of the periodic task
end A4A.MBTCP_Server;
