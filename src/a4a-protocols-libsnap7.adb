
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings;

package body A4A.Protocols.LibSnap7 is

   function Cli_ConnectTo (Client     :  S7Object;
                           IP_Address : in String;
                           Rack       : in Natural;
                           Slot       : in Natural) return C.int is

      function Snap7_Cli_ConnectTo
        (Client     : in S7Object;
         IP_Address : in C.Strings.chars_ptr;
         Rack       : in C.int;
         Slot       : in C.int)
         return C.int;
      --  int S7API Cli_ConnectTo(S7Object Client, const char *Address,
      --                          int Rack, int Slot);
      pragma Import (C, Snap7_Cli_ConnectTo, "Cli_ConnectTo");

      IP_Address_ptr : C.Strings.chars_ptr;
      Result         : C.int;
   begin
      IP_Address_ptr := C.Strings.New_String (IP_Address);
      Result := Snap7_Cli_ConnectTo (Client     => Client,
                                     IP_Address => IP_Address_ptr,
                                     Rack       => C.int (Rack),
                                     Slot       => C.int (Slot));
      C.Strings.Free (IP_Address_ptr);

      return Result;
   end Cli_ConnectTo;

   function Cli_Error_Text (Error : C.int) return String is

      Buffer : aliased Interfaces.C.char_array :=
        (0 .. 1023 => Interfaces.C.nul);
      Result : C.int;
   begin
      Result := Cli_ErrorText
        (Error         => Error,
         Buffer        => Buffer (Buffer'First)'Address,
         Buffer_Length => Buffer'Length);

      if Result = 0 then
         return Interfaces.C.To_Ada (Buffer);
      else
         return "Error description not found !";
      end if;

   end Cli_Error_Text;

   procedure Cli_Get_Order_Code (Client     : in S7Object;
                                 Order_Code : out Order_Code_Type;
                                 Result     : out C.int)
   is
      C_Order_Code : C_Order_Code_Type;
      C_Result : C.int;
   begin
      C_Result := Cli_GetOrderCode (Client     => Client,
                                    Order_Code => C_Order_Code);

      if C_Result = 0 then
         Order_Code.Code := Interfaces.C.To_Ada (C_Order_Code.Code,
                                                 Trim_Nul => False);

         Order_Code.Version_V1 := C_Order_Code.Version_V1;
         Order_Code.Version_V2 := C_Order_Code.Version_V2;
         Order_Code.Version_V3 := C_Order_Code.Version_V3;
      end if;

      Result := C_Result;
   end Cli_Get_Order_Code;

   procedure Cli_Get_Cpu_Info (Client   : in S7Object;
                               Cpu_Info : out CPU_Info_Type;
                               Result   : out C.int)
   is
      C_CPU_Info : C_CPU_Info_Type;
      C_Result : C.int;
   begin
      C_Result := Cli_GetCpuInfo (Client     => Client,
                                  Cpu_Info => C_CPU_Info);

      if C_Result = 0 then
         Cpu_Info.Module_Type_Name :=
           Interfaces.C.To_Ada (C_CPU_Info.Module_Type_Name,
                                Trim_Nul => False);

         Cpu_Info.Serial_Number :=
           Interfaces.C.To_Ada (C_CPU_Info.Serial_Number, Trim_Nul => False);

         Cpu_Info.AS_Name :=
           Interfaces.C.To_Ada (C_CPU_Info.AS_Name, Trim_Nul => False);

         Cpu_Info.Copyright :=
           Interfaces.C.To_Ada (C_CPU_Info.Copyright, Trim_Nul => False);

         Cpu_Info.Module_Name :=
           Interfaces.C.To_Ada (C_CPU_Info.Module_Name, Trim_Nul => False);

      end if;

      Result := C_Result;
   end Cli_Get_Cpu_Info;

end A4A.Protocols.LibSnap7;
