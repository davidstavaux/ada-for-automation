
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is the home for the various field bus protocols.
--  </summary>
--  <description>
--  It provides :
--  - libmodbus binding,
--  - add more by yourself.
--  </description>
--  <group>Protocols</group>

with Ada.Strings.Bounded;

package A4A.Protocols is

   IP_Address_String_Length_Max : constant := 15; -- "000.000.000.000"
   package IP_Address_Strings is new
     Ada.Strings.Bounded.Generic_Bounded_Length (IP_Address_String_Length_Max);

   Device_String_Length_Max : constant := 15;
   --  "/dev/ttyUSB0"
   --  "/dev/ttyS0"
   --  "\\\\.\\COM10"
   package Device_Strings is new
     Ada.Strings.Bounded.Generic_Bounded_Length (Device_String_Length_Max);
   --  The device argument specifies the name of the serial port handled by the
   --  OS, eg. /dev/ttyS0 or /dev/ttyUSB0. On Windows, it is necessary to
   --  prepend COM name with \\.\ for COM number greater than 9,
   --  eg. \\\\.\\COM10.
   --  See http://msdn.microsoft.com/en-us/library/aa365247(v=vs.85).aspx for
   --  details

end A4A.Protocols;
