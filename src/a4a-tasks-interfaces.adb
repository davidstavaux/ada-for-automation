
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Tasks.Interfaces is

   protected body Task_Control is

      procedure Quit (Value : in Boolean) is
      begin
         Control.Quit := Value;
      end Quit;

      function Quit return Boolean is
      begin
         return Control.Quit;
      end Quit;

      procedure Start_Watching (Value : in Boolean) is
      begin
         Control.Start_Watching := Value;
      end Start_Watching;

      function Start_Watching return Boolean is
      begin
         return Control.Start_Watching;
      end Start_Watching;

      procedure Run (Value : in Boolean) is
      begin
         Control.Run := Value;
      end Run;

      function Run return Boolean is
      begin
         return Control.Run;
      end Run;

   end Task_Control;

   protected body Task_Status is

      procedure Ready (Value : in Boolean) is
      begin
         Status.Ready := Value;
      end Ready;

      function Ready return Boolean is
      begin
         return Status.Ready;
      end Ready;

      procedure Terminated (Value : in Boolean) is
      begin
         Status.Terminated := Value;
      end Terminated;

      function Terminated return Boolean is
      begin
         return Status.Terminated;
      end Terminated;

      procedure Running (Value : in Boolean) is
      begin
         Status.Running := Value;
      end Running;

      function Running return Boolean is
      begin
         return Status.Running;
      end Running;

      function Get_Status return Task_Status_Type is
      begin
         return Status;
      end Get_Status;

   end Task_Status;

   protected body Task_Statistics is

      procedure Set_Statistics (Value : in Task_Statistics_Type) is
      begin
         Statistics := Value;
      end Set_Statistics;

      function Get_Statistics return Task_Statistics_Type is
      begin
         return Statistics;
      end Get_Statistics;

   end Task_Statistics;

   protected body Task_Sched_Statistics is

      procedure Set_Sched_Stats  (Value : in Sched_Stats_Array) is
      begin
         Sched_Stats := Value;
      end Set_Sched_Stats;

      function Get_Sched_Stats return Sched_Stats_Array is
      begin
         return Sched_Stats;
      end Get_Sched_Stats;

   end Task_Sched_Statistics;

end A4A.Tasks.Interfaces;
