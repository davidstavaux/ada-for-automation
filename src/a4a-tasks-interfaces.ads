
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Tasks.Interfaces is

   protected type Task_Control is

      procedure Quit (Value : in Boolean);
      function Quit return Boolean;

      procedure Start_Watching (Value : in Boolean);
      function Start_Watching return Boolean;

      procedure Run (Value : in Boolean);
      function Run return Boolean;

   private

      Control : Task_Control_Type;

   end Task_Control;

   protected type Task_Status is

      procedure Ready (Value : in Boolean);
      function Ready return Boolean;

      procedure Terminated (Value : in Boolean);
      function Terminated return Boolean;

      procedure Running (Value : in Boolean);
      function Running return Boolean;

      function Get_Status return Task_Status_Type;

   private

      Status : Task_Status_Type;

   end Task_Status;

   protected type Task_Statistics is

      procedure Set_Statistics (Value : in Task_Statistics_Type);

      function Get_Statistics return Task_Statistics_Type;

   private

      Statistics : Task_Statistics_Type;

   end Task_Statistics;

   protected type Task_Sched_Statistics is

      procedure Set_Sched_Stats  (Value : in Sched_Stats_Array);

      function Get_Sched_Stats return Sched_Stats_Array;

   private

      Sched_Stats  : Sched_Stats_Array := (others => 0);

   end Task_Sched_Statistics;

end A4A.Tasks.Interfaces;
