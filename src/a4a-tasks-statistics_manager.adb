
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Tasks.Statistics_Manager is

   procedure Initialise
     (Stats         : in out Instance;
      Task_Duration : Duration) is
   begin
      Stats.Task_Statistics.Min_Duration := Task_Duration;
      Stats.Task_Statistics.Max_Duration := Task_Duration;
   end Initialise;

   procedure Update
     (Stats         : in out Instance;
      Task_Duration : Duration) is
   begin
      if Stats.Task_Statistics.Min_Duration > Task_Duration then
         Stats.Task_Statistics.Min_Duration := Task_Duration;
      elsif Stats.Task_Statistics.Max_Duration < Task_Duration then
         Stats.Task_Statistics.Max_Duration := Task_Duration;
      end if;

      Stats.Task_Duration_Moving_Mean :=
        Stats.Task_Duration_Moving_Mean + Task_Duration
          - Stats.Task_Duration_Samples (Stats.Oldest_Sample);
      Stats.Task_Duration_Samples (Stats.Oldest_Sample) := Task_Duration;
      Stats.Oldest_Sample := Stats.Oldest_Sample + 1;
      Stats.Task_Statistics.Avg_Duration :=
        Stats.Task_Duration_Moving_Mean / Stats.Task_Duration_Samples'Length;

   end Update;

   function Get_Statistics
     (Stats : in out Instance) return Task_Statistics_Type is
   begin
      return Stats.Task_Statistics;
   end Get_Statistics;

end A4A.Tasks.Statistics_Manager;
