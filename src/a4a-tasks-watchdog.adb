
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Tasks.Watchdog is

   --------------------------------------------------------------------
   --  Control Watchdog
   --------------------------------------------------------------------

   protected body Control_Watchdog_Type is

      procedure Watchdog
        (Watching        : in Boolean;
         Status_Watchdog : in DWord;
         Error           : out Boolean) is
      begin

         Watchdog_TON_Start :=
           Watching and (Watchdog_Value = Status_Watchdog);

         Watchdog_TON.Cyclic
           (Start   => Watchdog_TON_Start,
            Preset  => Watchdog_Time_Out,
            Elapsed => Watchdog_TON_Elapsed,
            Q       => Watchdog_TON_Q);

         Error := Watchdog_TON_Q;

         Watchdog_Value := Status_Watchdog;

      end Watchdog;

      procedure Set_Time_Out
        (Time_Out_MS : Natural := Default_Watchdog_Time_Out_MS) is
      begin
         Watchdog_Time_Out := Ada.Real_Time.Milliseconds (Time_Out_MS);
      end Set_Time_Out;

      function Value return DWord is
      begin
         return Watchdog_Value;
      end Value;

   end Control_Watchdog_Type;

   --------------------------------------------------------------------
   --  Status Watchdog
   --------------------------------------------------------------------

   protected body Status_Watchdog_Type is

      procedure Watchdog
        (Watching         : in Boolean;
         Control_Watchdog : in DWord;
         Error            : out Boolean) is
      begin

         Watchdog_TON_Start :=
           Watching and (Watchdog_Value /= Control_Watchdog);

         Watchdog_TON.Cyclic
           (Start   => Watchdog_TON_Start,
            Preset  => Watchdog_Time_Out,
            Elapsed => Watchdog_TON_Elapsed,
            Q       => Watchdog_TON_Q);

         Error := Watchdog_TON_Q;

         if Watchdog_Value = Control_Watchdog then
            Watchdog_Value := Watchdog_Value + 1;
         end if;

      end Watchdog;

      procedure Set_Time_Out
        (Time_Out_MS : Natural := Default_Watchdog_Time_Out_MS) is
      begin
         Watchdog_Time_Out := Ada.Real_Time.Milliseconds (Time_Out_MS);
      end Set_Time_Out;

      function Value return DWord is
      begin
         return Watchdog_Value;
      end Value;

   end Status_Watchdog_Type;

end A4A.Tasks.Watchdog;
