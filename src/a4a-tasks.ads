
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time; use Ada.Real_Time;

package A4A.Tasks is

   type Task_Control_Type is
      record
         Quit           : Boolean := False;
         Start_Watching : Boolean := False;
         Run            : Boolean := False;
      end record;

   type Task_Status_Type is
      record
         Ready        : Boolean  := False;
         Terminated   : Boolean  := False;
         Running      : Boolean  := False;
      end record;

   type Task_Statistics_Type is
      record
         Min_Duration : Duration := 0.0;
         Max_Duration : Duration := 0.0;
         Avg_Duration : Duration := 0.0;
      end record;

   subtype Sched_Stats_Index is Natural range 1 .. 14;
   type Sched_Stats_Array is array (Sched_Stats_Index) of Natural;
   Sched_Thresholds  : constant array (Sched_Stats_Index) of Duration :=
     (1  => To_Duration (Microseconds (100)),
      2  => To_Duration (Milliseconds (1)),
      3  => To_Duration (Milliseconds (2)),
      4  => To_Duration (Milliseconds (3)),
      5  => To_Duration (Milliseconds (4)),
      6  => To_Duration (Milliseconds (5)),
      7  => To_Duration (Milliseconds (10)),
      8  => To_Duration (Milliseconds (20)),
      9  => To_Duration (Milliseconds (30)),
      10 => To_Duration (Milliseconds (40)),
      11 => To_Duration (Milliseconds (50)),
      12 => To_Duration (Milliseconds (60)),
      13 => To_Duration (Milliseconds (70)),
      14 => To_Duration (Milliseconds (80)));

end A4A.Tasks;
