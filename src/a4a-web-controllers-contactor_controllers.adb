
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Controllers.Contactor_Controllers is

   procedure Toggle (Object : in out Instance) is
   begin
      Object.Cmd.Toggle;
   end Toggle;

   function Get_Status (Object : in out Instance)
                        return Status_Type is
   begin
      return Object.Status.Contactor_Status;
   end Get_Status;

   function Get_Status (Object : in out Instance)
                        return Contactor_Status_Record_Type is
   begin
      return Object.Status;
   end Get_Status;

   function Get_Device_Id (Object : in out Instance)
                           return String is
   begin
      return Object.Contactor_Model.Get_Id;
   end Get_Device_Id;

   function Get_Cmd_Ctrl (Object : in out Instance)
                          return IPB_Controllers.Instance_Access is
   begin
      return Object.Cmd'Unrestricted_Access;
   end Get_Cmd_Ctrl;

   procedure Set_Model (Object : in out Instance;
                        Contactor_Model : access Contactor.Instance := null;
                        Cmd_Model       : access Boolean := null) is
   begin
      Object.Contactor_Model := Contactor_Model;
      Object.Cmd.Set_Model (Status_Bit => Cmd_Model);
   end Set_Model;

   procedure Update (Object : in out Instance) is
   begin
      if Object.Contactor_Model /= null then

         if Object.Contactor_Model.is_Faulty then
            Object.Status.Contactor_Status := Faulty;
         elsif Object.Contactor_Model.is_On then
            Object.Status.Contactor_Status := On;
         else
            Object.Status.Contactor_Status := Off;
         end if;

         Object.Status.IO_Status := Object.Contactor_Model.Get_IO_Status;

      end if;

      Object.Cmd.Update;
      Object.Status.Cmd_Status := Object.Cmd.Get_Status;
   end Update;

end A4A.Web.Controllers.Contactor_Controllers;
