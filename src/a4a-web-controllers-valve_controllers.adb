
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Controllers.Valve_Controllers is

   procedure Toggle (Object : in out Instance) is
   begin
      Object.Cmd.Toggle;
   end Toggle;

   function Get_Status (Object : in out Instance)
                        return Status_Type is
   begin
      return Object.Status.Valve_Status;
   end Get_Status;

   function Get_Status (Object : in out Instance)
                        return Valve_Status_Record_Type is
   begin
      return Object.Status;
   end Get_Status;

   function Get_Device_Id (Object : in out Instance)
                           return String is
   begin
      return Object.Valve_Model.Get_Id;
   end Get_Device_Id;

   function Get_Cmd_Ctrl (Object : in out Instance)
                          return IPB_Controllers.Instance_Access is
   begin
      return Object.Cmd'Unrestricted_Access;
   end Get_Cmd_Ctrl;

   procedure Set_Model (Object : in out Instance;
                        Valve_Model  : access Valve.Instance := null;
                        Cmd_Model    : access Boolean := null) is
   begin
      Object.Valve_Model := Valve_Model;
      Object.Cmd.Set_Model (Status_Bit => Cmd_Model);
   end Set_Model;

   procedure Update (Object : in out Instance) is
   begin
      if Object.Valve_Model /= null then

         if Object.Valve_Model.is_Faulty then
            Object.Status.Valve_Status := Faulty;
         elsif Object.Valve_Model.is_Open then
            Object.Status.Valve_Status := Open;
         elsif Object.Valve_Model.is_Closed then
            Object.Status.Valve_Status := Closed;
         else
            null;
         end if;

         Object.Status.IO_Status := Object.Valve_Model.Get_IO_Status;

      end if;

      Object.Cmd.Update;
      Object.Status.Cmd_Status := Object.Cmd.Get_Status;
   end Update;

end A4A.Web.Controllers.Valve_Controllers;
