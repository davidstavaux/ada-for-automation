
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Protocols; use A4A.Protocols.Device_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;
with A4A.Kernel.Main; use A4A.Kernel.Main;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

package body A4A.Web.Pages.MBRTU_Master_Status is

   type Side_Type is
     (On_Left,
      On_Right);

   function To_HTML (Command : in Command_Type;
                     Side    : in Side_Type) return String;
   function To_HTML (Command : in Command_Type;
                     Side    : in Side_Type) return String is
      function Action_To_HTML return String;
      function Action_To_HTML return String is
      begin
         case Command.Action is

            when Read_Bits | Read_Input_Bits | Read_Registers |
                 Read_Input_Registers | Write_Bit | Write_Register |
                 Write_Bits | Write_Registers =>

               return
                 "<table class=""w3-table w3-small"">"
                 & "<tr>"
                 & "<th>Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Offset_Local'Img & "</td>"
                 & "</tr>"
                 & "</table>";

            when Write_Read_Registers =>

               return
                 "<table class=""w3-table w3-small"">"
                 & "<tr>"
                 & "<th>Write Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Write Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Write Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Offset_Local'Img & "</td>"
                 & "</tr>"

                 & "<tr>"
                 & "<th>Read Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Read Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Read Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Offset_Local'Img & "</td>"
                 & "</tr>"
                 & "</table>";

         end case;
      end Action_To_HTML;

      function Tooltip_Class (Side : in Side_Type) return String;
      function Tooltip_Class (Side : in Side_Type) return String is
         function Size_Class return String;
         function Size_Class return String is
         begin
            if Command.Action = Write_Read_Registers then
               return "tooltiptextbig";
            else
               return "tooltiptextsmall";
            end if;
         end Size_Class;

         function Side_Class return String;
         function Side_Class return String is
         begin
            case Side is
            when On_Left => return "tooltiptextleft";
            when On_Right => return "tooltiptextright";
            end case;
         end Side_Class;
      begin
         return Size_Class & " " & Side_Class;
      end Tooltip_Class;

   begin
      return
        "<div class=""" & Tooltip_Class (Side) & """>"
        & "<h3>" & Command.Action'Img & "</h3>"
        & "<p>Slave address: " & Command.Slave'Img & "</p>"
        & " " & Action_To_HTML
        & "</div>";
   end To_HTML;

   procedure Create_Master_Status_View
     (Parent             : in out Gnoga.Gui.Element.Element_Type;
      Master_Status_View : in out Master_Status_View_Type) is
      My_Ident : constant String :=
        "A4A.Web.Pages.MBRTU_Master_Status.Create_Master_Status_View";

      Command_Index : Positive;

      TR_Access : Gnoga.Gui.Element.Table.Table_Row_Access;
      TH_Access : Gnoga.Gui.Element.Table.Table_Heading_Access;
      TC_Access : Gnoga.Gui.Element.Table.Table_Column_Access;

      Side : Side_Type;
   begin

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Trace !",
                          Log_Level => Level_Verbose);

      Master_Status_View.Rows_Number := 1 + ((Config.Command_Number - 1) / 10);

      if Config.Command_Number >= 10 then
         Master_Status_View.Columns_Number := 10;
      else
         Master_Status_View.Columns_Number := Config.Command_Number;
      end if;

      Master_Status_View.Section1.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.Section,
         ID      => "master");
      Master_Status_View.Section1.Add_Class ("w3-section");
      Master_Status_View.Section1.Place_Inside_Bottom_Of (Parent);

      Master_Status_View.Header1.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.Header);
      Master_Status_View.Header1.Add_Class
        ("w3-container w3-light-blue w3-padding-8");
      Master_Status_View.Header1.Place_Inside_Top_Of
        (Master_Status_View.Section1);

      Master_Status_View.Title_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.H1);
      Master_Status_View.Title_View.Inner_HTML
        ("Master" & " | " & To_String (Config.Device));
      Master_Status_View.Title_View.Place_Inside_Top_Of
        (Master_Status_View.Header1);

      Master_Status_View.A_Previous.Create
        (Parent  => Parent,
         Link    => "#top",
         Content => "<i class=""fa fa-arrow-up w3-large w3-right""></i>");

      Master_Status_View.A_Previous.Place_Inside_Bottom_Of
        (Master_Status_View.Header1);

      Master_Status_View.A_Next.Create
        (Parent  => Parent,
         Link    => "#bottom",
         Content => "<i class=""fa fa-arrow-down w3-large w3-right""></i>");

      Master_Status_View.A_Next.Place_Inside_Bottom_Of
        (Master_Status_View.Header1);

      Master_Status_View.Div1.Create (Parent  => Parent);
      Master_Status_View.Div1.Add_Class ("w3-row");
      Master_Status_View.Div1.Place_Inside_Bottom_Of
        (Master_Status_View.Section1);

      Master_Status_View.Div_WD.Create
        (Parent  => Parent,
         Content => "<h3 style=""width:120px; padding: 10px;"
         & "text-align: center"">Watchdog</h3>");
      Master_Status_View.Div_WD.Add_Class ("w3-container w3-half");
      Master_Status_View.Div_WD.Style ("width", "140px");

      Master_Status_View.Watchdog_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.P,
         ID      => "master" & "-task-watchdog");
      Master_Status_View.Watchdog_View.Add_Class ("a4a-status1");
      Master_Status_View.Watchdog_View.Attribute ("status", "off");
      Master_Status_View.Watchdog_View.Inner_HTML ("Status");
      Master_Status_View.Watchdog_View.Place_Inside_Bottom_Of
        (Master_Status_View.Div_WD);

      Master_Status_View.Div_WD.Place_Inside_Top_Of
        (Master_Status_View.Div1);

      Master_Status_View.Div_CNX.Create
        (Parent  => Parent,
         Content => "<h3 style=""width:120px; padding: 10px;"
         & "text-align: center"">Connection</h3>");
      Master_Status_View.Div_CNX.Add_Class ("w3-container w3-half");
      Master_Status_View.Div_CNX.Style ("width", "140px");

      Master_Status_View.CNX_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.P,
         ID      => "master" & "-task-connection");
      Master_Status_View.CNX_View.Add_Class ("a4a-status1");
      Master_Status_View.CNX_View.Attribute ("status", "off");
      Master_Status_View.CNX_View.Inner_HTML ("Status");
      Master_Status_View.CNX_View.Place_Inside_Bottom_Of
        (Master_Status_View.Div_CNX);

      Master_Status_View.Div_CNX.Place_Inside_Bottom_Of
        (Master_Status_View.Div1);

      Master_Status_View.Div2.Create
        (Parent  => Parent,
         Content => "<h3>Commands Status</h3>");
      Master_Status_View.Div2.Add_Class ("w3-container");
      Master_Status_View.Div2.Style ("max-width", "500px");
      Master_Status_View.Div2.Place_Inside_Bottom_Of
        (Master_Status_View.Section1);

      --  One raw for columns labels
      --  One raw for each 10 commands
      --  One column for raws labels
      --  1 to 10 columns for commands

      Master_Status_View.Cmd_Table.Create
        (Parent => Parent);
      Master_Status_View.Cmd_Table.Add_Class
        ("w3-bordered w3-card-4 a4a-status2-table");

      Master_Status_View.Cmd_Table_Head.Create
        (Table => Master_Status_View.Cmd_Table);

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "I'm Fine!",
                          Log_Level => Level_Verbose);

      TR_Access := new Gnoga.Gui.Element.Table.Table_Row_Type;
      TR_Access.Dynamic;
      TR_Access.Create (Master_Status_View.Cmd_Table_Head);

      TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
      TH_Access.Dynamic;
      TH_Access.Create (TR_Access.all);

      for Col_Index in 1 .. Master_Status_View.Columns_Number loop
         TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
         TH_Access.Dynamic;
         TH_Access.Create (TR_Access.all,
                           Content => Gnoga.Left_Trim (Col_Index'Img));
      end loop;

      Command_Index := 1;
      for Row_Index in 1 .. Master_Status_View.Rows_Number loop

         TR_Access := new Gnoga.Gui.Element.Table.Table_Row_Type;
         TR_Access.Dynamic;
         TR_Access.Create (Master_Status_View.Cmd_Table);

         TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
         TH_Access.Dynamic;
         TH_Access.Create
           (TR_Access.all,
            Content => Gnoga.Left_Trim (Integer'Image ((Row_Index - 1) * 10)));

         for Col_Index in 1 .. Master_Status_View.Columns_Number loop

            TC_Access := new Gnoga.Gui.Element.Table.Table_Column_Type;
            TC_Access.Dynamic;
            TC_Access.Create (TR_Access.all);

            if Col_Index < 8 then
               Side := On_Right;
            else
               Side := On_Left;
            end if;

            Master_Status_View.Commands_Status_Indicators (Command_Index)
              .Create
                (Parent  => Master_Status_View.Cmd_Table,
                 Content => To_HTML (Config.Commands (Command_Index), Side),
                 ID      => "master"
                 & "-cmd" & Gnoga.Left_Trim (Command_Index'Img));

            Master_Status_View.Commands_Status_Indicators (Command_Index)
              .Place_Inside_Top_Of (TC_Access.all);

            Master_Status_View.Commands_Status_Indicators (Command_Index)
              .Add_Class ("a4a-status2 tooltip");

            Master_Status_View.Commands_Status_Indicators (Command_Index)
              .Attribute ("status", Command_Status_Unknown);

            Command_Index := Command_Index + 1;

            exit when
              Command_Index >
                Master_Status_View.Commands_Status_Indicators'Last;

         end loop;
      end loop;

      Master_Status_View.Cmd_Table.Place_Inside_Bottom_Of
        (Master_Status_View.Div2);

      Master_Status_View.Div3.Create
        (Parent  => Parent,
         Content => "<h3>Commands Status Legend</h3>");
      Master_Status_View.Div3.Add_Class ("w3-container");
      Master_Status_View.Div3.Style ("max-width", "500px");
      Master_Status_View.Div3.Place_Inside_Bottom_Of
        (Master_Status_View.Section1);

      Master_Status_View.Legend_Cmd_Table.Create
        (Parent => Parent);
      Master_Status_View.Legend_Cmd_Table.Add_Class
        ("w3-bordered w3-card-4 a4a-status2-table");

      for Row_Index in
        Master_Status_View.Legend_Cmd_Status_Indicators'First ..
          Master_Status_View.Legend_Cmd_Status_Indicators'Last loop

         TR_Access := new Gnoga.Gui.Element.Table.Table_Row_Type;
         TR_Access.Dynamic;
         TR_Access.Create (Master_Status_View.Legend_Cmd_Table);

         TC_Access := new Gnoga.Gui.Element.Table.Table_Column_Type;
         TC_Access.Dynamic;
         TC_Access.Create (TR_Access.all);

         Master_Status_View.Legend_Cmd_Status_Indicators (Row_Index)
           .Create
             (Parent  => Master_Status_View.Legend_Cmd_Table,
              Content => "",
              ID      => "");

         Master_Status_View.Legend_Cmd_Status_Indicators (Row_Index)
           .Place_Inside_Top_Of (TC_Access.all);

         Master_Status_View.Legend_Cmd_Status_Indicators (Row_Index)
           .Add_Class ("a4a-status2");

         Master_Status_View.Legend_Cmd_Status_Indicators (Row_Index)
           .Attribute ("status", Command_Status_Strings (Row_Index).all);

         TC_Access := new Gnoga.Gui.Element.Table.Table_Column_Type;
         TC_Access.Dynamic;
         TC_Access.Create (Row     => TR_Access.all,
                           Content => Command_Status_Strings (Row_Index).all);

      end loop;

      Master_Status_View.Legend_Cmd_Table.Place_Inside_Bottom_Of
        (Master_Status_View.Div3);

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Trace End!",
                          Log_Level => Level_Verbose);

   end Create_Master_Status_View;

   procedure Master_Status_View_Update
     (Master_Status_View : in out Master_Status_View_Type) is

      Command_Index : Positive;
   begin

      if The_Main_Task_Interface.MMaster_Status.WD_Error
        /= Master_Status_View.Watchdog_Error
      then
         if The_Main_Task_Interface.MMaster_Status.WD_Error then
            Master_Status_View.Watchdog_View.Attribute ("status", "off");
         else
            Master_Status_View.Watchdog_View.Attribute ("status", "on");
         end if;
      end if;
      Master_Status_View.Watchdog_Error :=
        The_Main_Task_Interface.MMaster_Status.WD_Error;

      Master_Status_View.Task_Status :=
        MBRTU_Master_Task_Itf.Status.Get_Status;

      if Master_Status_View.Task_Status.Connected
        /= Master_Status_View.Connected
      then
         if not Master_Status_View.Task_Status.Connected then
            Master_Status_View.CNX_View.Attribute ("status", "off");
         else
            Master_Status_View.CNX_View.Attribute ("status", "on");
         end if;
      end if;
      Master_Status_View.Connected := Master_Status_View.Task_Status.Connected;

      Command_Index := 1;
      for Row_Index in 1 .. Master_Status_View.Rows_Number loop
         for Col_Index in 1 .. Master_Status_View.Columns_Number loop

            if Master_Status_View.Task_Status.Commands_Status (Command_Index)
              /= Master_Status_View.Commands_Status (Command_Index)
            then

               case Master_Status_View
                 .Task_Status.Commands_Status (Command_Index)
               is

                  when Unknown =>
                     Master_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", Command_Status_Unknown);

                  when Disabled =>
                     Master_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", Command_Status_Disabled);

                  when Fine =>
                     Master_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", Command_Status_Fine);

                  when Fault =>
                     Master_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", Command_Status_Fault);

               end case;

               Master_Status_View.Commands_Status (Command_Index) :=
                 Master_Status_View.Task_Status.
                   Commands_Status (Command_Index);
            end if;

            Command_Index := Command_Index + 1;

            exit when
              Command_Index >
                Master_Status_View.Commands_Status_Indicators'Last;

         end loop;
      end loop;

   end Master_Status_View_Update;

   overriding
   procedure Update_Page (Web_Page : access Instance) is

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      Master_Status_View_Update
        (Master_Status_View => Web_Page.Master_Status_View);

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String :=
        "A4A.Web.Pages.MBRTU_Master_Status_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);
      Main_Window.Document.Load_CSS (TOOLTIP_CSS_URL);

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "140-mbrtu-master-status.html");
      --
      --  MBRTU Master View
      --

      Web_Page.Master_Status_Parent_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "mbrtu-master-status");

      Create_Master_Status_View
        (Parent             => Web_Page.Master_Status_Parent_View,
         Master_Status_View => Web_Page.Master_Status_View);

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.MBRTU_Master_Status;
