
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;
with Gnoga.Gui.Element;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Section;
with Gnoga.Gui.Element.Table;
use Gnoga.Gui;

with A4A.MBRTU_Master; use A4A.MBRTU_Master;
with A4A.Application.MBRTU_Master_Config;
use A4A.Application.MBRTU_Master_Config;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

package A4A.Web.Pages.MBRTU_Master_Status is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   Command_Status_Unknown   : aliased String := "unknown";
   Command_Status_Disabled  : aliased String := "disabled";
   Command_Status_Fine      : aliased String := "fine";
   Command_Status_Fault     : aliased String := "fault";

   type Command_Status_Strings_Type is array (1 .. 4) of access String;

   Command_Status_Strings : Command_Status_Strings_Type :=
     (Command_Status_Unknown'Access,
      Command_Status_Disabled'Access,
      Command_Status_Fine'Access,
      Command_Status_Fault'Access);

   type Commands_Status_Indicators_Type is array (Positive range <>)
     of Gnoga.Gui.Element.Common.DIV_Type;
   type Commands_Status_Indicators_Access is
     access all Commands_Status_Indicators_Type;

   type Master_Status_View_Type is
      record
         Section1      : Gnoga.Gui.Element.Section.Section_Type;
         Header1       : Gnoga.Gui.Element.Section.Section_Type;
         Title_View    : Gnoga.Gui.Element.Section.Section_Type;

         A_Previous    : Gnoga.Gui.Element.Common.A_Type;
         A_Next        : Gnoga.Gui.Element.Common.A_Type;

         Div1          : Gnoga.Gui.Element.Common.DIV_Type;
         Div_WD        : Gnoga.Gui.Element.Common.DIV_Type;
         Div_CNX       : Gnoga.Gui.Element.Common.DIV_Type;
         Watchdog_View : Gnoga.Gui.Element.Section.Section_Type;
         CNX_View      : Gnoga.Gui.Element.Section.Section_Type;

         Div2           : Gnoga.Gui.Element.Common.DIV_Type;
         Cmd_Table      : Gnoga.Gui.Element.Table.Table_Type;
         Cmd_Table_Head : Gnoga.Gui.Element.Table.Table_Header_Type;

         Commands_Status_Indicators :
         Commands_Status_Indicators_Type (Config.Commands'Range);

         Div3           : Gnoga.Gui.Element.Common.DIV_Type;
         Legend_Cmd_Table : Gnoga.Gui.Element.Table.Table_Type;
         Legend_Cmd_Status_Indicators :
         Commands_Status_Indicators_Type (Command_Status_Strings_Type'Range);

         Watchdog_Error : Boolean := True;
         Connected      : Boolean := False;

         Task_Status    : A4A.MBRTU_Master.Task_Status_Type
           (Config.Command_Number);

         Rows_Number    : Positive;
         Columns_Number : Positive;

         Commands_Status : Commands_Status_Type (Config.Commands'Range) :=
           (others => Unknown);

      end record;
   type Master_Status_View_Access is access all Master_Status_View_Type;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;

         Master_Status_Parent_View : Gnoga.Gui.Element.Element_Type;
         Master_Status_View        : Master_Status_View_Type;

      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

   procedure Create_Master_Status_View
     (Parent             : in out Gnoga.Gui.Element.Element_Type;
      Master_Status_View : in out Master_Status_View_Type);

   procedure Master_Status_View_Update
     (Master_Status_View : in out Master_Status_View_Type);

end A4A.Web.Pages.MBRTU_Master_Status;
