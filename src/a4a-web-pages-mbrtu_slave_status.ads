
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Application.MBRTU_Slave_Config;
use A4A.Application;

with A4A.Web.Elements.Bool_Elements;
with A4A.Web.Elements.DWord_Elements;
use A4A.Web.Elements;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

package A4A.Web.Pages.MBRTU_Slave_Status is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure Slave_Task_Status_Update
     (Watchdog_Error : Boolean;
      Status         : MBRTU_Slave_Config.Slave.Task_Status_Type);

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;

         Slave_Device_View  : Gnoga.Gui.Element.Element_Type;
         Slave_Address_View : Gnoga.Gui.Element.Element_Type;

         Slave_Task_Watchdog_View       : Bool_Elements.Instance;
         Slave_Connected_To_Master_View : Bool_Elements.Instance;

         Slave_FC01_View : DWord_Elements.Instance;
         Slave_FC02_View : DWord_Elements.Instance;
         Slave_FC03_View : DWord_Elements.Instance;
         Slave_FC04_View : DWord_Elements.Instance;
         Slave_FC05_View : DWord_Elements.Instance;
         Slave_FC06_View : DWord_Elements.Instance;
         Slave_FC15_View : DWord_Elements.Instance;
         Slave_FC16_View : DWord_Elements.Instance;
         Slave_FC23_View : DWord_Elements.Instance;

         Slave_Task_Status_Previous :
         MBRTU_Slave_Config.Slave.Task_Status_Type;
      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

   Slave_Task_Watchdog_Error    : Boolean  := False;
   Slave_Task_Status       : MBRTU_Slave_Config.Slave.Task_Status_Type;

end A4A.Web.Pages.MBRTU_Slave_Status;
