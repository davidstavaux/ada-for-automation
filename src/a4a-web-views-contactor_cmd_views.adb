
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Views.Contactor_Cmd_Views is
   use Contactor_Controllers;

   procedure Update_View (Object : in out Instance)
   is
      Status : Contactor_Controllers.Contactor_Status_Record_Type;
   begin

      Object.Command.Update_View;

      if Object.Contactor = null then
         return;
      end if;

      Status := Object.Contactor.Get_Status;
      if Object.First_Time
        or Status.Contactor_Status /= Object.Status_Previous.Contactor_Status
      then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         case Status.Contactor_Status is
            when Contactor_Controllers.Faulty =>
               Object.Contactor_View.Attribute ("status", "faulty");
            when Contactor_Controllers.On =>
               Object.Contactor_View.Attribute ("status", "on");
            when Contactor_Controllers.Off =>
               Object.Contactor_View.Attribute ("status", "off");
         end case;
         Object.Status_Previous.Contactor_Status := Status.Contactor_Status;

      end if;

      if Object.First_Time then
         Object.First_Time := False;
      end if;

      Object.Contactor_Feed_Back.Update_Element
        (Status   => Status.IO_Status.Feed_Back);

      Object.Contactor_Coil.Update_Element
        (Status   => Status.IO_Status.Coil);

   end Update_View;

   procedure Setup
     (Object : in out Instance;
      Parent     : in Gnoga.Gui.Base.Base_Type'Class;
      View_Id    : in String;
      Command_Id : in String;
      Title_Id   : in String;
      Contactor_View_Id : in String;
      Contactor_Feed_Back_Id : in String;
      Contactor_Coil_Id      : in String)
   is
   begin
      Object.View.Attach_Using_Parent (Parent, View_Id);

      Object.Title.Attach_Using_Parent (Parent, Title_Id);

      Object.Command.Attach_Using_Parent (Parent, Command_Id);
      Object.Command.On_Click_Handler (IPB_Views.On_Click'Unrestricted_Access);

      Object.Contactor_View.Attach_Using_Parent (Parent, Contactor_View_Id);

      Object.Contactor_Feed_Back.Attach_Using_Parent
        (Parent, Contactor_Feed_Back_Id);
      Object.Contactor_Coil.Attach_Using_Parent (Parent, Contactor_Coil_Id);

   end Setup;

   procedure Connect
     (Object : in out Instance;
      Controller : in Contactor_Controllers.Instance_Access := null) is
   begin
      Object.Contactor := Controller;

      if Object.Contactor = null then
         return;
      end if;

      Object.Command.Set_Controller (Object.Contactor.Get_Cmd_Ctrl);
      Object.Title.Inner_HTML (Object.Contactor.Get_Device_Id);
   end Connect;

   procedure Show (Object : in out Instance) is
   begin
      Object.View.Display ("block");
   end Show;

end A4A.Web.Views.Contactor_Cmd_Views;
