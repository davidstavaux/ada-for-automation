
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;

package body A4A.Web.Views.Contactor_Views is
   use Contactor_Cmd_Views;
   use Contactor_Controllers;

   procedure On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
   begin
      A4A.Log.Logger.Put (Who  => My_Ident & ".On_Click",
                          What => "Gnoga.Gui.Base.Base_Type");
      Instance (Object).On_Click;
   end On_Click;

   procedure On_Click (Object : in out Instance)
   is
   begin
      A4A.Log.Logger.Put (Who  => My_Ident & ".On_Click",
                          What => "Instance");
      if Object.Cmd_View = null then
         return;
      end if;

      Object.Cmd_View.Connect (Controller => Object.Contactor);
      Object.Cmd_View.Show;
   end On_Click;

   procedure Update_View (Object : in out Instance)
   is
      Status : Contactor_Controllers.Status_Type;
   begin
      if Object.Contactor = null then
         return;
      end if;

      Status := Object.Contactor.Get_Status;
      if Object.First_Time
        or Status /= Object.Status_Previous
      then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         case Status is
            when Contactor_Controllers.Faulty =>
               Object.Attribute ("status", "faulty");
            when Contactor_Controllers.On =>
               Object.Attribute ("status", "on");
            when Contactor_Controllers.Off =>
               Object.Attribute ("status", "off");
         end case;
         Object.Status_Previous := Status;
      end if;
   end Update_View;

   procedure Setup
     (Object     : in out Instance;
      Controller : in     Contactor_Controllers.Instance_Access := null;
      Cmd_View   : in     Contactor_Cmd_Views.Instance_Access   := null)
   is
   begin
      Object.Contactor := Controller;
      Object.Cmd_View  := Cmd_View;
   end Setup;

end A4A.Web.Views.Contactor_Views;
