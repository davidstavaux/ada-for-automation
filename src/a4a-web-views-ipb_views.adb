
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.Web.Views.IPB_Views is
   use IPB_Controllers;

   procedure Update_View (Object : in out Instance)
   is
      Status : IPB_Controllers.Status_Type;
   begin
      if Object.IPB /= null then
         Status := Object.IPB.Get_Status;
         if Object.First_Time or Status /= Object.IPB_Status_Previous then

            if Object.First_Time then
               Object.First_Time := False;
            end if;

            case Status is
            when IPB_Controllers.Off =>
               Object.Attribute ("status", "off");
            when IPB_Controllers.On =>
               Object.Attribute ("status", "on");
            end case;
            Object.IPB_Status_Previous := Status;
         end if;
      end if;
   end Update_View;

   procedure Setup
     (Object     : in out Instance;
      Parent     : in     Gnoga.Gui.Base.Base_Type'Class;
      Controller : in IPB_Controllers.Instance_Access;
      Id         : in String)
   is
   begin
      Object.IPB := Controller;

      Object.Attach_Using_Parent (Parent, Id);
   end Setup;

   procedure On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
   begin
      A4A.Log.Logger.Put (Who       => My_Ident & ".On_Click",
                          What      => "Gnoga.Gui.Base.Base_Type",
                          Log_Level => Level_Info);
      Instance (Object).On_Click;
   end On_Click;

   procedure On_Mouse_Down
     (Object      : in out Gnoga.Gui.Base.Base_Type'Class;
      Mouse_Event : in     Gnoga.Gui.Base.Mouse_Event_Record)
   is
      pragma Unreferenced (Mouse_Event);
   begin
      Instance (Object).On_Mouse_Down;
   end On_Mouse_Down;

   procedure On_Mouse_Up
     (Object      : in out Gnoga.Gui.Base.Base_Type'Class;
      Mouse_Event : in     Gnoga.Gui.Base.Mouse_Event_Record)
   is
      pragma Unreferenced (Mouse_Event);
   begin
      Instance (Object).On_Mouse_Up;
   end On_Mouse_Up;

   procedure On_Click (Object : in out Instance)
   is
   begin
      A4A.Log.Logger.Put (Who       => My_Ident & ".On_Click",
                          What      => "Instance",
                          Log_Level => Level_Info);
      if Object.IPB /= null then
         Object.IPB.Toggle;
      end if;
   end On_Click;

   procedure On_Mouse_Down (Object : in out Instance)
   is
   begin
      if Object.IPB /= null then
         Object.IPB.Turn_On;
      end if;
   end On_Mouse_Down;

   procedure On_Mouse_Up (Object : in out Instance)
   is
   begin
      if Object.IPB /= null then
         Object.IPB.Turn_Off;
      end if;
   end On_Mouse_Up;

   procedure Set_Controller (Object : in out Instance;
                             Controller : in IPB_Controllers.Instance_Access)
   is
   begin
      Object.IPB := Controller;
   end Set_Controller;
end A4A.Web.Views.IPB_Views;
