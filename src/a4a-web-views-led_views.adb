
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Views.LED_Views is
   use LED_Controllers;

   procedure Update_View (Object : in out Instance)
   is
      Status : LED_Controllers.Status_Type;
   begin
      Status := Object.LED.Get_Status;
      if Object.First_Time or Status /= Object.LED_Status_Previous then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         case Status is
            when LED_Controllers.Off =>
               Object.Attribute ("status", "off");
            when LED_Controllers.On =>
               Object.Attribute ("status", "on");
         end case;
         Object.LED_Status_Previous := Status;
      end if;
   end Update_View;

   procedure Setup
     (Object     : in out Instance;
      Parent     : in     Gnoga.Gui.Base.Base_Type'Class;
      Controller : in LED_Controllers.Instance_Access;
      Id         : in String)
   is
   begin
      Object.LED := Controller;

      Object.Attach_Using_Parent (Parent, Id);
   end Setup;

   procedure Set_Controller (Object     : in out Instance;
                             Controller : in LED_Controllers.Instance_Access)
   is
   begin
      Object.LED := Controller;
   end Set_Controller;
end A4A.Web.Views.LED_Views;
