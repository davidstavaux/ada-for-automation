
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Views.Switch3_Views is
   use Switch3_Controllers;

   procedure Area0_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
   begin
      Area_Instance (Object).View.On_Click0;
   end Area0_On_Click;

   procedure Area1_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
   begin
      Area_Instance (Object).View.On_Click1;
   end Area1_On_Click;

   procedure Area2_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
   begin
      Area_Instance (Object).View.On_Click2;
   end Area2_On_Click;

   procedure On_Click0 (Object : in out Instance)
   is
   begin
      Object.Switch3.Pos0;
   end On_Click0;

   procedure On_Click1 (Object : in out Instance)
   is
   begin
      Object.Switch3.Pos1;
   end On_Click1;

   procedure On_Click2 (Object : in out Instance)
   is
   begin
      Object.Switch3.Pos2;
   end On_Click2;

   procedure Update_View (Object : in out Instance)
   is
      Status : Switch3_Controllers.Status_Type;
   begin
      Status := Object.Switch3.Get_Status;
      if Object.First_Time or Status /= Object.Switch3_Status_Previous then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         case Status is
            when Switch3_Controllers.Pos0 =>
               Object.Mobile.Attribute ("transform", "rotate(90 30 50)");
            when Switch3_Controllers.Pos1 =>
               Object.Mobile.Attribute ("transform", "rotate(45 30 50)");
            when Switch3_Controllers.Pos2 =>
               Object.Mobile.Attribute ("transform", "rotate(135 30 50)");
         end case;
         Object.Switch3_Status_Previous := Status;
      end if;
   end Update_View;

   procedure Setup
     (Object     : in out Instance;
      Parent     : in     Gnoga.Gui.Base.Base_Type'Class;
      Controller : in Switch3_Controllers.Instance_Access;
      Mobile_Id  : in String;
      Area0_Id   : in String;
      Area1_Id   : in String;
      Area2_Id   : in String)
   is
   begin
      Object.Switch3 := Controller;

      Object.Mobile.Attach_Using_Parent (Parent, Mobile_Id);

      Object.Area0.Setup (View => Object'Unrestricted_Access);
      Object.Area0.Attach_Using_Parent (Parent, Area0_Id);
      Object.Area0.On_Click_Handler (Area0_On_Click'Unrestricted_Access);

      Object.Area1.Setup (View => Object'Unrestricted_Access);
      Object.Area1.Attach_Using_Parent (Parent, Area1_Id);
      Object.Area1.On_Click_Handler (Area1_On_Click'Unrestricted_Access);

      Object.Area2.Setup (View => Object'Unrestricted_Access);
      Object.Area2.Attach_Using_Parent (Parent, Area2_Id);
      Object.Area2.On_Click_Handler (Area2_On_Click'Unrestricted_Access);
   end Setup;

   procedure Setup
     (Object : in out Area_Instance;
      View   : in Instance_Access)
   is
   begin
      Object.View := View;
   end Setup;

end A4A.Web.Views.Switch3_Views;
