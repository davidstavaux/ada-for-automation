
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Views.Valve_Cmd_Views is
   use Valve_Controllers;

   procedure Update_View (Object : in out Instance)
   is
      Status : Valve_Controllers.Valve_Status_Record_Type;
   begin

      Object.Command.Update_View;

      if Object.Valve = null then
         return;
      end if;

      Status := Object.Valve.Get_Status;
      if Object.First_Time
        or Status.Valve_Status /= Object.Status_Previous.Valve_Status
      then

         case Status.Valve_Status is
            when Valve_Controllers.Faulty =>
               Object.Valve_View.Attribute ("status", "faulty");
            when Valve_Controllers.Open =>
               Object.Valve_View.Attribute ("status", "open");
            when Valve_Controllers.Closed =>
               Object.Valve_View.Attribute ("status", "closed");
         end case;

         Object.Status_Previous.Valve_Status := Status.Valve_Status;

      end if;

      if Object.First_Time then
         Object.First_Time := False;
      end if;

      Object.Valve_Pos_Open.Update_Element
        (Status   => Status.IO_Status.Pos_Open);

      Object.Valve_Pos_Closed.Update_Element
        (Status   => Status.IO_Status.Pos_Closed);

      Object.Valve_Coil.Update_Element
        (Status   => Status.IO_Status.Coil);

   end Update_View;

   procedure Setup
     (Object        : in out Instance;
      Parent        : in Gnoga.Gui.Base.Base_Type'Class;
      View_Id       : in String;
      Command_Id    : in String;
      Title_Id      : in String;
      Valve_View_Id : in String;
      Valve_Pos_Open_Id   : in String;
      Valve_Pos_Closed_Id : in String;
      Valve_Coil_Id       : in String)
   is
   begin
      Object.View.Attach_Using_Parent (Parent, View_Id);

      Object.Title.Attach_Using_Parent (Parent, Title_Id);

      Object.Command.Attach_Using_Parent (Parent, Command_Id);
      Object.Command.On_Click_Handler (IPB_Views.On_Click'Unrestricted_Access);

      Object.Valve_View.Attach_Using_Parent (Parent, Valve_View_Id);

      Object.Valve_Pos_Open.Attach_Using_Parent (Parent, Valve_Pos_Open_Id);
      Object.Valve_Pos_Closed.Attach_Using_Parent
       (Parent, Valve_Pos_Closed_Id);
      Object.Valve_Coil.Attach_Using_Parent (Parent, Valve_Coil_Id);

   end Setup;

   procedure Connect
     (Object : in out Instance;
      Controller : in Valve_Controllers.Instance_Access := null) is
   begin
      Object.Valve := Controller;

      if Object.Valve = null then
         return;
      end if;

      Object.Command.Set_Controller (Object.Valve.Get_Cmd_Ctrl);
      Object.Title.Inner_HTML (Object.Valve.Get_Device_Id);
   end Connect;

   procedure Show (Object : in out Instance) is
   begin
      Object.View.Display ("block");
   end Show;

end A4A.Web.Views.Valve_Cmd_Views;
