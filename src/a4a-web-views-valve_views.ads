
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Base;
with Gnoga.Gui.Element;
with A4A; use A4A;
with A4A.Web.Controllers.Valve_Controllers;
use A4A.Web.Controllers;
with A4A.Web.Views.Valve_Cmd_Views;
use A4A.Web.Views;

package A4A.Web.Views.Valve_Views is
   type Instance is new Gnoga.Gui.Element.Element_Type with private;

   procedure On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class);
   procedure Update_View (Object : in out Instance);
   procedure Setup
     (Object     : in out Instance;
      Controller : in     Valve_Controllers.Instance_Access := null;
      Cmd_View   : in     Valve_Cmd_Views.Instance_Access   := null);
private
   My_Ident : constant String := "A4A.Web.Views.Valve_Views";
   procedure On_Click (Object : in out Instance);
   type Instance is new Gnoga.Gui.Element.Element_Type with
      record
         Valve          : Valve_Controllers.Instance_Access;
         --  The controller

         Cmd_View   : Valve_Cmd_Views.Instance_Access := null;

         Status_Previous : Valve_Controllers.Status_Type;

         First_Time : Boolean := True;
      end record;

end A4A.Web.Views.Valve_Views;
