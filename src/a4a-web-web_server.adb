
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with Gnoga.Application.Multi_Connect; use Gnoga;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Web.Web_Server_Config;

with A4A.Application.Web; use A4A.Application.Web;

package body A4A.Web.Web_Server is

   task body Scanner is
      My_Ident : constant String := "A4A.Web.Web_Server.Scanner";
   begin

      accept Start;

      loop

         Update_Data;

         select
            accept Stop;
            A4A.Log.Logger.Put (Who       => My_Ident,
                                What      => "I'm dead !",
                                Log_Level => Level_Info);
            exit;
         or
            delay 0.1;
         end select;

      end loop;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Scanner;

   task body Main_Task is
      My_Ident : constant String := "A4A.Web.Web_Server.Main_Task";
      Scanner_Task : Scanner;
   begin

      accept Start;

      On_Server_Start;

      Gnoga.Application.HTML_On_Close
        ("<b>Connection to Application has been terminated</b>");

      Connect_Data;

      Scanner_Task.Start;

      if A4A.Web.Web_Server_Config.Open_URL_Flag then
         Gnoga.Application.Open_URL
           (URL => "http://127.0.0.1:"
            & A4A.Web.Web_Server_Config.Port'Img);
      end if;

      Gnoga.Application.Multi_Connect.Message_Loop;

      Scanner_Task.Stop;

      Terminated_Flag := True;
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "terminated...",
                          Log_Level => Level_Info);
   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Scanner_Task.Stop;
         Quit;
         Terminated_Flag := True;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Main_Task;

   procedure Quit is
   begin
      Gnoga.Application.Multi_Connect.End_Application;
   end Quit;

   function is_Terminated return Boolean is
   begin
      return Terminated_Flag;
   end is_Terminated;

end A4A.Web.Web_Server;
