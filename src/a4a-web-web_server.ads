
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Web.Web_Server is

   task type Main_Task is
      entry Start;
   end Main_Task;

   procedure Quit;

   function is_Terminated return Boolean;

private

   task type Scanner
   is
      entry Start;
      entry Stop;
   end Scanner;
   --  This task will update the Controllers with the Model data.

   --  For each connection from a browser a view is created holding all View
   --  components. All View components are bound to DOM IDs and corresponding
   --  Controllers. Then, a View_Updater task is started.

   Terminated_Flag : Boolean := False;

end A4A.Web.Web_Server;
