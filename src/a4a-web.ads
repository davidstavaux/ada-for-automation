
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Web is
   --------------------------------------------------------------------
   --  URLs
   --------------------------------------------------------------------

   W3_CSS_URL : String :=
     "https://www.w3schools.com/lib/w3.css";
   --  W3Schools CSS URL

   Font_Awesome_CSS_URL : String :=
     "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/"
         & "font-awesome.min.css";
   --  Font Awesome 4 CSS URL

   APP_CSS_URL : String := "/css/app.css";
   --  Application CSS URL

   TOOLTIP_CSS_URL : String := "/css/tooltip.css";
   --  Tooltip CSS URL

end A4A.Web;
