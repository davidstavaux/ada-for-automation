
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Application.Fieldbus2 is

   --------------------------------------------------------------------
   --  User Program
   --------------------------------------------------------------------

   procedure Cold_Start;
   --  This procedure is called once during Main Task initialisation.
   --  Its purpose is to provide for first time application initialisation.

   procedure Closing;
   --  This procedure is called once during Main Task termination.
   --  It allows application to terminate cleanly.

   procedure Main_Cyclic;
   --  This is application main entry point.
   --  It is called by Main Task when in Running state.

   function Program_Fault return Boolean;

private

   Program_Fault_Flag : Boolean := False;
   --  It is raised by exception handling in application and tested by
   --  Main Task.

end A4A.Application.Fieldbus2;
