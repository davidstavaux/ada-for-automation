
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Application.Main_Fieldbus1 is

   procedure Get_Main_Inputs is
   begin
      Main_Inputs := The_DPM.Inputs.Get_Data;
   end Get_Main_Inputs;

   procedure Set_Main_Outputs is
   begin
      The_DPM.Outputs.Set_Data (Main_Outputs);
   end Set_Main_Outputs;

   procedure Get_Fieldbus_Inputs is
   begin
      Fieldbus_Inputs := The_DPM.Outputs.Get_Data;
   end Get_Fieldbus_Inputs;

   procedure Set_Fieldbus_Outputs is
   begin
      The_DPM.Inputs.Set_Data (Fieldbus_Outputs);
   end Set_Fieldbus_Outputs;

end A4A.Application.Main_Fieldbus1;
