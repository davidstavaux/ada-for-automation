
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2021, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Get_DPM_IO_Info;
use A4A.Protocols.HilscherX.rcX_Public;

package A4A.Kernel.Fieldbus is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;
   use A4A.Protocols.HilscherX;

   protected type cifX_Driver_Status is

      procedure Set_Driver_Information
        (Value : in cifX.Driver_Information_Type);

      function Get_Driver_Information return cifX.Driver_Information_Type;

   private
      Driver_Information  : cifX.Driver_Information_Type;
   end cifX_Driver_Status;
   type cifX_Driver_Status_Access is access all cifX_Driver_Status;

   type cifX_Status_Type is
      record
         WD_Error : Boolean  := False;
         --  Watchdog error

         Communicating : Boolean  := False;
         --  Communicating
      end record;

   protected type cifX_Status is

      procedure WD_Error (Value : in Boolean);

      function WD_Error return Boolean;

      procedure Communicating (Value : in Boolean);

      function Communicating return Boolean;

      procedure Set_Channel_Information
        (Value : in cifX.Channel_Information_Type);

      function Get_Channel_Information return cifX.Channel_Information_Type;

      procedure Set_Common_Status_Block
        (Value : in cifX.Common_Status_Block_Type);

      function Get_Common_Status_Block return cifX.Common_Status_Block_Type;

   private
      Status : cifX_Status_Type;

      Channel_Information : cifX.Channel_Information_Type;
      Common_Status_Block : cifX.Common_Status_Block_Type;
   end cifX_Status;
   type cifX_Status_Access is access all cifX_Status;

   procedure cifX_Show_Error (Who   : in String;
                              Error : in DInt);

   procedure cifX_Show_Driver_Information
     (Who                : in String;
      Driver_Information : in cifX.Driver_Information_Type);

   procedure cifX_Show_Channel_Information
     (Who                 : in String;
      Channel_Information : in cifX.Channel_Information_Type);

   procedure cifX_Show_DPM_IO_Info
     (Who         : in String;
      DPM_IO_Info : rcX_Get_DPM_IO_Info.RCX_GET_DPM_IO_INFO_CNF_DATA_T);

end A4A.Kernel.Fieldbus;
