
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Tasks.Interfaces; use A4A.Tasks.Interfaces;
with A4A.Tasks.Watchdog; use A4A.Tasks.Watchdog;

with A4A.Protocols.HilscherX.cifX_User;

with A4A.Application.Configuration; use A4A.Application.Configuration;
with A4A.Configuration.Fieldbus; use A4A.Configuration.Fieldbus;
with A4A.Kernel.Fieldbus; use A4A.Kernel.Fieldbus;

package A4A.Kernel.Fieldbus2 is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   Watchdog_Time_Out_MS : constant Natural := 2000;

   procedure Create_Main_Task;

   type Main_Task_Interface is
      record
         Control : Task_Control;
         Status  : A4A.Tasks.Interfaces.Task_Status;
         Statistics  : Task_Statistics;

         Control_Watchdog  : Control_Watchdog_Type;
         Status_Watchdog   : Status_Watchdog_Type;

         cifX_Drv_Status   : aliased cifX_Driver_Status;
         cifX_Board_Status : aliased cifX_Status;
      end record;

   type Main_Task_Itf_Access is access all Main_Task_Interface;

   task type Main_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Main_Task_Itf_Access
     ) is
      pragma Priority (Task_Priority);
   end Main_Task;
   type Main_Task_Access is access Main_Task;

   --------------------------------------------------------------------
   --  Main Task Management
   --------------------------------------------------------------------

   The_Main_Task_Interface     : aliased Main_Task_Interface;

private

   The_Main_Task       : Main_Task_Access;
   Main_Task_Created   : Boolean := False;

   FB_Config : Fieldbus_Configuration_Access :=
     Fieldbuses_Configuration (2);

end A4A.Kernel.Fieldbus2;
