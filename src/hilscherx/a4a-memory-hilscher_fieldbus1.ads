
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Memory.Hilscher_Fieldbus1 is

   --------------------------------------------------------------------
   --  IO Areas
   --------------------------------------------------------------------

   Inputs_Size  : constant := 5760;
   subtype Inputs_Range is Integer range 0 .. Inputs_Size - 1;

   Outputs_Size  : constant := 5760;
   subtype Outputs_Range is Integer range 0 .. Outputs_Size - 1;

   Inputs  : Byte_Array (Inputs_Range)  := (others => 0);
   Outputs : Byte_Array (Outputs_Range) := (others => 0);

end A4A.Memory.Hilscher_Fieldbus1;
