
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

package body A4A.Protocols.HilscherX.Channel_Messaging is

   procedure Initialise
     (Channel        : in out Instance;
      Channel_Handle : in Channel_Handle_Type) is
   begin

      Channel.The_Channel_Handle := Channel_Handle;

      Channel.The_Packet_Pool.Initialise;

      Channel.The_Queue_Map.Initialise;

      Channel.Init_Flag := True;

   end Initialise;

   procedure Quit
     (Channel : in out Instance) is
   begin
      Channel.Quit_Flag := True;
   end Quit;

   function Is_Initialised
     (Channel : in Instance) return Boolean is
   begin

      return Channel.Init_Flag;

   end Is_Initialised;

   function Is_Terminated
     (Channel : in Instance) return Boolean is
   begin

      return Channel.Send_Task_Terminated and Channel.Receive_Task_Terminated;

   end Is_Terminated;

   procedure Get_Packet
     (Channel : in out Instance;
      Item    : out cifX_Packet_Access;
      Result  : out Boolean) is
   begin

      Channel.The_Packet_Pool.Get_Packet (Item, Result);

   end Get_Packet;

   procedure Return_Packet
     (Channel : in out Instance;
      Item    : in cifX_Packet_Access) is
   begin

      Channel.The_Packet_Pool.Return_Packet (Item);

   end Return_Packet;

   procedure Put
     (Channel : in out Instance;
      Item   : in Packet_Queue_Access;
      Index  : out DWord;
      Result : out Boolean) is
   begin

      Channel.The_Queue_Map.Put (Item, Index, Result);

   end Put;

   procedure Get
     (Channel : in out Instance;
      Index  : in DWord;
      Item   : out Packet_Queue_Access;
      Result : out Boolean) is
   begin

      Channel.The_Queue_Map.Get (Index, Item, Result);

   end Get;

   procedure Delete
     (Channel : in out Instance;
      Index  : in DWord;
      Result : out Boolean) is
   begin

      Channel.The_Queue_Map.Delete (Index, Result);

   end Delete;

   procedure Send
     (Channel : in out Instance;
      Item    : in cifX_Packet_Access;
      Result  : out Boolean) is
   begin

      Channel.Send_Queue.Put (Item, Result);

   end Send;

   procedure Receive
     (Channel : in out Instance;
      Item    : out cifX_Packet_Access;
      Result  : out Boolean) is
   begin

      Channel.Receive_Queue.Get (Item, Result);

   end Receive;

   function Get_Pending
     (Channel : in Instance) return DWord is
   begin

      return Channel.Receive_Queue.Get_Pending;

   end Get_Pending;

   function Get_Send_Pkt_Count
     (Channel : in Instance) return DWord is
   begin

      return Channel.Send_Pkt_Count;

   end Get_Send_Pkt_Count;

   task body Send_Msg_Task_Type is

      My_Ident : constant String := Package_Ident & ".Send_Msg_Task_Type";

      Packet    : cifX_Packet_Access;

      Result    : DInt;

      procedure cifX_Show_Error (Error : DInt);

      procedure cifX_Show_Error (Error : DInt) is
         Error_Image : String (1 .. 12);
      begin
         DWord_Text_IO.Put
           (To   => Error_Image,
            Item => DInt_To_DWord (Error),
            Base => 16);

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "(" & Error_Image & ")"
            & Driver_Get_Error_Description (Error));
      end cifX_Show_Error;

   begin

      loop

         delay 1.0;
         exit when Channel.Init_Flag or Channel.Quit_Flag;

      end loop;

      loop

         exit when Channel.Quit_Flag;

         select

            Channel.Send_Queue.Get (Item => Packet);

            Result := Channel_Put_Packet
              (Channel_Handle => Channel.The_Channel_Handle,
               Send_Packet    => Packet.all,
               Time_Out       => 5000);
            if Result /= CIFX_NO_ERROR then
               cifX_Show_Error (Result);
            end if;

            Channel.The_Packet_Pool.Return_Packet (Item => Packet);

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Packet sent.",
                                Log_Level => A4A.Logger.Level_Verbose);
         or

            delay 0.1;

--              A4A.Log.Logger.Put (Who  => My_Ident,
--                                  What => "No packet to send.",
--                                  Log_Level => A4A.Logger.Level_Verbose);
         end select;

      end loop;

      Channel.Send_Task_Terminated := True;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Channel.Send_Task_Terminated := True;

   end Send_Msg_Task_Type;

   task body Receive_Msg_Task_Type is

      My_Ident : constant String := Package_Ident & ".Receive_Msg_Task_Type";

      Packet     : cifX_Packet_Access;

      Result     : DInt;

      Queue_App  : DWord;
      Queue_Id   : DWord;

      Packet_Got : Boolean := False;
      Packet_Rcv : Boolean := False;
      Packet_Put : Boolean := False;

      Queue_Got  : Boolean := False;

      The_Packet_Queue_Access : Packet_Queue_Access;

      procedure Dump_Packet;

      procedure Dump_Packet is

         Dest_String    : String (1 .. 20);
         Src_String     : String (1 .. 20);
         Dest_Id_String : String (1 .. 20);
         Src_Id_String  : String (1 .. 20);
         Cmd_String     : String (1 .. 20);

      begin

         DWord_Text_IO.Put
           (To   => Dest_String,
            Item => Packet.Header.Dest,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_String,
            Item => Packet.Header.Src,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Dest_Id_String,
            Item => Packet.Header.Dest_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_Id_String,
            Item => Packet.Header.Src_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Cmd_String,
            Item => Packet.Header.Cmd,
            Base => 16);

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Got a message : " & CRLF
            & "Dest    : " & Dest_String & CRLF
            & "Src     : " & Src_String & CRLF
            & "Dest_Id : " & Dest_Id_String & CRLF
            & "Src_Id  : " & Src_Id_String & CRLF
            & "Cmd     : " & Cmd_String
           );

      end Dump_Packet;

   begin

      loop

         delay 1.0;
         exit when Channel.Init_Flag or Channel.Quit_Flag;

      end loop;

      loop

         exit when Channel.Quit_Flag;

         Result := Channel_Get_MBX_State
           (Channel_Handle => Channel.The_Channel_Handle,
            Recv_Pkt_Count => Channel.Recv_Pkt_Count'Access,
            Send_Pkt_Count => Channel.Send_Pkt_Count'Access);

         if not Packet_Got then

            Channel.The_Packet_Pool.Get_Packet
              (Item   => Packet,
               Result => Packet_Got);

         end if;

         if not Packet_Got then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Could not get packet from pool...");

         end if;

         if Packet_Got and not Packet_Rcv and Channel.Recv_Pkt_Count > 0 then

            Result := Channel_Get_Packet
              (Channel_Handle => Channel.The_Channel_Handle,
               Size           => cifX_Packet_Type'Size / 8,
               Recv_Packet    => Packet.all,
               Time_Out       => 5000);

            if Result = CIFX_NO_ERROR then

               Packet_Rcv := True;

               if A4A.Log.Logger.Get_Level = A4A.Logger.Level_Verbose then
                  Dump_Packet;
               end if;

               if (Packet.Header.Cmd and 1) = 0 then

                  --  It's an indication
                  Queue_App := Packet.Header.Dest;
                  Queue_Id  := Packet.Header.Dest_Id;

               else

                  --  It's a response
                  Queue_App := Packet.Header.Src;
                  Queue_Id  := Packet.Header.Src_Id;

               end if;

            elsif Result = CIFX_DEV_GET_TIMEOUT
              or Result = CIFX_DEV_GET_NO_PACKET
            then

               A4A.Log.Logger.Put (Who  => My_Ident,
                                   What => "No packet pending !!!",
                                   Log_Level => A4A.Logger.Level_Error);

            else

               Show_Error (Result);

            end if;

         end if;

         if Packet_Rcv then

            if Queue_App /= A4A_App_0 or Queue_Id = 0 then

               Channel.Receive_Queue.Put
                 (Item   => Packet,
                  Result => Packet_Put);

            else

               Channel.The_Queue_Map.Get
                 (Index  => Queue_Id,
                  Item   => The_Packet_Queue_Access,
                  Result => Queue_Got);

               if Queue_Got then

                  The_Packet_Queue_Access.Put
                    (Item   => Packet,
                     Result => Packet_Put);

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Got wrong Queue_Id !!!",
                     Log_Level => A4A.Logger.Level_Error);

               end if;

            end if;

            if Packet_Put then

               Packet_Got := False;
               Packet_Rcv := False;
               Packet_Put := False;

            end if;

         end if;

         delay 0.05;

      end loop;

      Channel.Receive_Task_Terminated := True;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Channel.Send_Task_Terminated := True;

   end Receive_Msg_Task_Type;

end A4A.Protocols.HilscherX.Channel_Messaging;
