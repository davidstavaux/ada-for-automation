
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides from "cifXErrors.h" :
--  - cifX driver error codes definition,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package A4A.Protocols.HilscherX.cifX_Errors is

-----------------------------------------------------------------------
   --  from "cifXErrors.h"
-----------------------------------------------------------------------

   --  #define CIFX_NO_ERROR            ((int32_t)0x00000000L)
   CIFX_NO_ERROR                       : constant := 0;

   --  #define CIFX_DEV_NOT_READY       ((int32_t)0x800C0011L)
   CIFX_DEV_NOT_READY                  : constant DInt :=
     DWord_To_DInt (16#800C0011#);

   --  #define CIFX_DEV_NOT_RUNNING     ((int32_t)0x800C0012L)
   CIFX_DEV_NOT_RUNNING                : constant DInt :=
     DWord_To_DInt (16#800C0012#);

   --  #define CIFX_DEV_PUT_TIMEOUT     ((int32_t)0x800C0017L)
   CIFX_DEV_PUT_TIMEOUT                : constant DInt :=
     DWord_To_DInt (16#800C0017#);

   --  #define CIFX_DEV_GET_TIMEOUT     ((int32_t)0x800C0018L)
   CIFX_DEV_GET_TIMEOUT                : constant DInt :=
     DWord_To_DInt (16#800C0018#);

   --  #define CIFX_DEV_GET_NO_PACKET   ((int32_t)0x800C0019L)
   CIFX_DEV_GET_NO_PACKET              : constant DInt :=
     DWord_To_DInt (16#800C0019#);

   --  #define CIFX_DEV_NO_COM_FLAG     ((int32_t)0x800C0021L)
   CIFX_DEV_NO_COM_FLAG                : constant DInt :=
     DWord_To_DInt (16#800C0021#);

end A4A.Protocols.HilscherX.cifX_Errors;
