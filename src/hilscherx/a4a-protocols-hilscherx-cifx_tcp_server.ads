
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package A4A.Protocols.HilscherX.cifX_TCP_Server is

   procedure Start;
   procedure Quit;

private

   Start_Flag : Boolean := False;
   Quit_Flag : Boolean := False;

   procedure Marshaller_Timer;
   --  void MarshallerTimer (void);

   procedure DeInit_Server;
   --  void DeInitServer(void);

   function Initialise return Integer;
   --  int Initialise();

   pragma Import (C, Marshaller_Timer, "MarshallerTimer");
   pragma Import (C, DeInit_Server, "DeInitServer");
   pragma Import (C, Initialise, "Initialise");

   task TCP_Server;

end A4A.Protocols.HilscherX.cifX_TCP_Server;
