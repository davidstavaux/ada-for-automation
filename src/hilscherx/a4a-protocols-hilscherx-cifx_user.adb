
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System; use System;

with Ada.Text_IO;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

package body A4A.Protocols.HilscherX.cifX_User is

   function Driver_Handle_Null return Driver_Handle_Type is
   begin
      return Driver_Handle_Type (System.Null_Address);
   end Driver_Handle_Null;

   function Channel_Handle_Null return Channel_Handle_Type is
   begin
      return Channel_Handle_Type (System.Null_Address);
   end Channel_Handle_Null;

   procedure Driver_Get_Information
     (Driver_Handle      : in Driver_Handle_Type;
      Driver_Information : out Driver_Information_Type;
      Error              : out DInt) is

      C_Driver_Info : C_Driver_Info_Type;
      Result : DInt;
   begin
      Result := cifX_Driver_Get_Information
        (Driver_Handle  => Driver_Handle,
         Size           => C_Driver_Info_Type'Size / 8,
         Driver_Info    => C_Driver_Info);

      if Result = CIFX_NO_ERROR then
         Driver_Information.Driver_Version :=
           Interfaces.C.To_Ada (C_Driver_Info.Driver_Version,
                                Trim_Nul => False);

         Driver_Information.Board_Cnt := Natural (C_Driver_Info.Board_Cnt);
      end if;

      Error := Result;
   end Driver_Get_Information;

   function Driver_Get_Error_Description (Error : DInt)
                                         return String is

      Buffer : aliased Interfaces.C.char_array :=
        (0 .. 1023 => Interfaces.C.nul);
      Result : DInt;
   begin
      Result := cifX_Driver_Get_Error_Description
        (Error         => Error,
         Buffer        => Buffer (Buffer'First)'Address,
         Buffer_Length => Buffer'Length);

      if Result = CIFX_NO_ERROR then
         return Interfaces.C.To_Ada (Buffer);
      else
         return "Error description not found !";
      end if;

   end Driver_Get_Error_Description;

   procedure Show_Error (Error : DInt) is
   begin
      Ada.Text_IO.Put ("Error ");
      DWord_Text_IO.Put (Item => DInt_To_DWord (Error), Base => 16);
      Ada.Text_IO.Put_Line (": " & Driver_Get_Error_Description (Error));
   end Show_Error;

   function Channel_Open
     (Driver_Handle          : in Driver_Handle_Type;
      Board_Name             : in String;
      Channel_Number         : in DWord;
      Channel_Handle_Access  : access Channel_Handle_Type)
      return DInt is

      Board_Name_ptr : Interfaces.C.Strings.chars_ptr;
      Result         : DInt;
   begin
      Board_Name_ptr := Interfaces.C.Strings.New_String (Board_Name);
      Result := cifX_Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => Board_Name_ptr,
         Channel_Number         => Channel_Number,
         Channel_Handle_Access  => Channel_Handle_Access);

      Interfaces.C.Strings.Free (Board_Name_ptr);

      return Result;
   end Channel_Open;

   procedure Channel_Info
     (Channel_Handle      : in Channel_Handle_Type;
      Channel_Information : out Channel_Information_Type;
      Error               : out DInt) is

      C_CI   : C_Channel_Information_Type;
      Result : DInt;
   begin

      C_CI.Device_Number := 0; -- to avoid warning "read but never assigned

      Result := cifX_Channel_Info
        (Channel_Handle => Channel_Handle,
         Size           => C_CI'Size / 8,
         Channel_Info   => C_CI);

      if Result = CIFX_NO_ERROR then
         Channel_Information.Board_Name :=
           Interfaces.C.To_Ada (C_CI.Board_Name, Trim_Nul => False);

         Channel_Information.Board_Alias :=
           Interfaces.C.To_Ada (C_CI.Board_Alias, Trim_Nul => False);

         Channel_Information.Device_Number := C_CI.Device_Number;
         Channel_Information.Serial_Number := C_CI.Serial_Number;

         Channel_Information.FW_Major := C_CI.FW_Major;
         Channel_Information.FW_Minor := C_CI.FW_Minor;
         Channel_Information.FW_Build := C_CI.FW_Build;
         Channel_Information.FW_Revision := C_CI.FW_Revision;

         Channel_Information.FW_Name :=
           Interfaces.C.To_Ada (C_CI.FW_Name, Trim_Nul => False);

         Channel_Information.FW_Year := C_CI.FW_Year;
         Channel_Information.FW_Month := C_CI.FW_Month;
         Channel_Information.FW_Day := C_CI.FW_Day;

      end if;

      Error := Result;
   end Channel_Info;

   procedure Channel_Common_Status_Block
     (Channel_Handle      : in Channel_Handle_Type;
      Common_Status_Block : out Common_Status_Block_Type;
      Error               : out DInt) is

      C_CSB  : C_Common_Status_Block_Type;
      Result : DInt;
   begin

      C_CSB.Communication_COS := 0;
      --  to avoid warning "read but never assigned

      Result := cifX_Channel_Common_Status_Block
        (Channel_Handle => Channel_Handle,
         Cmd            => CIFX_CMD_READ_DATA,
         Offset         => 0,
         Data_Len       => C_CSB'Size / 8,
         Data           => C_CSB);

      if Result = CIFX_NO_ERROR then
         Common_Status_Block.Communication_COS   := C_CSB.Communication_COS;
         Common_Status_Block.Communication_State := C_CSB.Communication_State;
         Common_Status_Block.Communication_Error := C_CSB.Communication_Error;

         Common_Status_Block.Version         := C_CSB.Version;
         Common_Status_Block.Watchdog_Time   := C_CSB.Watchdog_Time;
         Common_Status_Block.PDIn_Hsk_Mode   := C_CSB.PDIn_Hsk_Mode;
         Common_Status_Block.PDIn_Source     := C_CSB.PDIn_Source;
         Common_Status_Block.PDOut_Hsk_Mode  := C_CSB.PDOut_Hsk_Mode;
         Common_Status_Block.PDOut_Source    := C_CSB.PDOut_Source;
         Common_Status_Block.Host_Watchdog   := C_CSB.Host_Watchdog;
         Common_Status_Block.Error_Count     := C_CSB.Error_Count;
         Common_Status_Block.Error_Log_Ind   := C_CSB.Error_Log_Ind;
         Common_Status_Block.Error_PDIn_Cnt  := C_CSB.Error_PDIn_Cnt;
         Common_Status_Block.Error_PDOut_Cnt := C_CSB.Error_PDOut_Cnt;
         Common_Status_Block.Error_Sync_Cnt  := C_CSB.Error_Sync_Cnt;
         Common_Status_Block.Sync_Hsk_Mode   := C_CSB.Sync_Hsk_Mode;
         Common_Status_Block.Sync_Source     := C_CSB.Sync_Source;

         Common_Status_Block.netX_Master_Status.Slave_State :=
           C_CSB.netX_Master_Status.Slave_State;
         Common_Status_Block.netX_Master_Status.Slave_Err_Log_Ind :=
           C_CSB.netX_Master_Status.Slave_Err_Log_Ind;
         Common_Status_Block.netX_Master_Status.Num_Of_Config_Slaves :=
           C_CSB.netX_Master_Status.Num_Of_Config_Slaves;
         Common_Status_Block.netX_Master_Status.Num_Of_Active_Slaves :=
           C_CSB.netX_Master_Status.Num_Of_Active_Slaves;
         Common_Status_Block.netX_Master_Status.Num_Of_Diag_Slaves :=
           C_CSB.netX_Master_Status.Num_Of_Diag_Slaves;
      end if;

      Error := Result;
   end Channel_Common_Status_Block;

end A4A.Protocols.HilscherX.cifX_User;
