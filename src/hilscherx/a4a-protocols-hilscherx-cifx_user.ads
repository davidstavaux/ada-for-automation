
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with System;

with Interfaces.C.Strings;

package A4A.Protocols.HilscherX.cifX_User is

   --------------------------------------------------------------------
   --  from cifXUser.h
   --------------------------------------------------------------------

   --  /* General commands */
   --  #define CIFX_CMD_READ_DATA                    1
   CIFX_CMD_READ_DATA                  : constant := 1;
   --  #define CIFX_CMD_WRITE_DATA                   2
   CIFX_CMD_WRITE_DATA                 : constant := 2;

   --  /* HOST mode definition */
   --  #define CIFX_HOST_STATE_NOT_READY             0
   CIFX_HOST_STATE_NOT_READY           : constant := 0;
   --  #define CIFX_HOST_STATE_READY                 1
   CIFX_HOST_STATE_READY               : constant := 1;
   --  #define CIFX_HOST_STATE_READ                  2
   CIFX_HOST_STATE_READ                : constant := 2;

   --  /* WATCHDOG commands*/
   --  #define CIFX_WATCHDOG_STOP                    0
   CIFX_WATCHDOG_STOP                  : constant := 0;
   --  #define CIFX_WATCHDOG_START                    1
   CIFX_WATCHDOG_START                 : constant := 1;

   --  /* BUS state commands*/
   --  #define CIFX_BUS_STATE_OFF                    0
   CIFX_BUS_STATE_OFF                  : constant := 0;
   --  #define CIFX_BUS_STATE_ON                     1
   CIFX_BUS_STATE_ON                   : constant := 1;
   --  #define CIFX_BUS_STATE_GETSTATE               2
   CIFX_BUS_STATE_GETSTATE             : constant := 2;

   --  /* Reset definitions */
   --  #define CIFX_SYSTEMSTART                      1
   CIFX_SYSTEMSTART                    : constant := 1;
   --  #define CIFX_CHANNELINIT                      2
   CIFX_CHANNELINIT                    : constant := 2;
   --  #define CIFX_BOOTSTART                        3
   CIFX_BOOTSTART                      : constant := 3;

   --  #define CIFx_MAX_INFO_NAME_LENTH              16
   CIFx_MAX_INFO_NAME_LENTH            : constant := 16;

   type Driver_Handle_Type is private;
   function Driver_Handle_Null return Driver_Handle_Type;

   type Channel_Handle_Type is private;
   function Channel_Handle_Null return Channel_Handle_Type;

   type Driver_Information_Type is
      record
         Driver_Version : String (1 .. 32);
         --  Human readable driver name and version

         Board_Cnt : Natural;
         --  Number of handled boards

      end record;
   --  Driver Information

   type Driver_Information_Type_Access is access all Driver_Information_Type;

   subtype Board_Name_Type is String (1 .. CIFx_MAX_INFO_NAME_LENTH);
   --  Global board name type

   subtype Board_Alias_Type is String (1 .. CIFx_MAX_INFO_NAME_LENTH);
   --  Global board alias name type

   type Board_Information_Type is
      record
         Board_Error : DWord;
         --  Global Board error. Set when device specific data must not be used

         Board_Name : Board_Name_Type;
         --  Global board name

      end record;
   --  Board Information (to be continued !)

   type Board_Information_Type_Access is access all Board_Information_Type;

   type Channel_Information_Type is
      record
         Board_Name : Board_Name_Type;
         --  Global board name

         Board_Alias : Board_Name_Type;
         --  Global board alias name

         Device_Number : DWord;
         --  Global board device number

         Serial_Number : DWord;
         --  Global board serial number

         FW_Major : Word;
         --  Major Version of Channel Firmware

         FW_Minor : Word;
         --  Minor Version of Channel Firmware

         FW_Build : Word;
         --  Build number of Channel Firmware

         FW_Revision : Word;
         --  Revision of Channel Firmware

         FW_Name : String (1 .. 63);
         --  Firmware Name

         FW_Year : Word;
         --  Build Year of Firmware

         FW_Month : Byte;
         --  Build Month of Firmware (1..12)

         FW_Day : Byte;
         --  Build Day of Firmware (1..31)

      end record;
   --  Channel Information
   type Channel_Information_Type_Access is access all Channel_Information_Type;

   --  /*!< Maximum size of the RCX packet in bytes */
   --  #define CIFX_MAX_PACKET_SIZE               1596
   CIFX_MAX_PACKET_SIZE              : constant := 1596;

   --  /*!< Maximum size of the RCX packet header in bytes */
   --  #define CIFX_PACKET_HEADER_SIZE            40
   CIFX_PACKET_HEADER_SIZE           : constant := 40;

   --  /*!< Maximum RCX packet data size */
   --  #define CIFX_MAX_DATA_SIZE
   --          (CIFX_MAX_PACKET_SIZE - CIFX_PACKET_HEADER_SIZE)
   CIFX_MAX_DATA_SIZE                : constant :=
     CIFX_MAX_PACKET_SIZE - CIFX_PACKET_HEADER_SIZE;

   type cifX_Packet_Header_Type is
      record
         Dest       : DWord;
         --  destination of packet, process queue

         Src        : DWord;
         --  source of packet, process queue

         Dest_Id    : DWord;
         --  destination reference of packet

         Src_Id     : DWord;
         --  source reference of packet

         Len        : DWord;
         --  length of packet data without header

         Id         : DWord;
         --  identification handle of sender

         State      : DWord;
         --  status code of operation

         Cmd        : DWord;
         --  packet command

         Ext        : DWord;
         --  extension

         Rout       : DWord;
         --  router

      end record;
   pragma Convention (C, cifX_Packet_Header_Type);
   --  cifX Packet header

   type cifX_Packet_Type is
      record
         Header : cifX_Packet_Header_Type;
         Data   : Byte_Array (0 .. CIFX_MAX_DATA_SIZE - 1);
      end record;
   --  cifX Packet
   pragma Convention (C, cifX_Packet_Type);

   type cifX_Packet_Access is access all cifX_Packet_Type;

   --  from rcX_User.h
   --  /* Master Status definitions */
   --  #define RCX_SLAVE_STATE_UNDEFINED                           0x00000000
   RCX_SLAVE_STATE_UNDEFINED           : constant := 0;
   --  #define RCX_SLAVE_STATE_OK                                  0x00000001
   RCX_SLAVE_STATE_OK                  : constant := 1;
   --  #define RCX_SLAVE_STATE_FAILED                              0x00000002
   RCX_SLAVE_STATE_FAILED              : constant := 2;

   type netX_Master_Status_Type is
      record
         Slave_State          : DWord;
         --  Slave status

         Slave_Err_Log_Ind    : DWord;
         --  Slave error indication

         Num_Of_Config_Slaves : DWord;
         --  Number of configured slaves

         Num_Of_Active_Slaves : DWord;
         --  Number of active slaves

         Num_Of_Diag_Slaves   : DWord;
         --  Number of slaves in diag mode

         Reserved             : DWord;
         --  Number of slaves in diag mode

      end record;
   --  Default master status structure

   type Common_Status_Block_Type is
      record
         Communication_COS   : DWord := 0;
         --  Communication channel "Change Of State"

         Communication_State : DWord := 0;
         --  Actual communication state

         Communication_Error : DWord := 0;
         --  Actual communication error

         Version             : Word;
         --  Version of the diagnostic structure

         Watchdog_Time       : Word;
         --  Configured watchdog time

         PDIn_Hsk_Mode       : Byte;
         --  Input area handshake mode

         PDIn_Source         : Byte;
         --  Reserved. Set to zero

         PDOut_Hsk_Mode      : Byte;
         --  Output area handshake mode

         PDOut_Source        : Byte;
         --  Reserved. Set to zero

         Host_Watchdog       : DWord;
         --  Host watchdog

         Error_Count         : DWord;
         --  Number of errors since power-up

         Error_Log_Ind       : Byte;
         --  Counter of available Log Entries (not supported yet)

         Error_PDIn_Cnt      : Byte;
         --  Counter of input process data handshake handling errors

         Error_PDOut_Cnt     : Byte;
         --  Counter of output process data handshake handling errors

         Error_Sync_Cnt      : Byte;
         --  Counter of synchronization handshake handling errors

         Sync_Hsk_Mode       : Byte;
         --  Synchronization Handshake mode

         Sync_Source         : Byte;
         --  Synchronization source

         Reserved_0          : Word;
         --  Reserved[3]

         Reserved_1          : Word;
         --  Reserved[3]

         Reserved_2          : Word;
         --  Reserved[3]

         netX_Master_Status  : netX_Master_Status_Type;

      end record;
   --  Channel common status block

   function Driver_Open
     (Driver_Handle_Access : access Driver_Handle_Type)
      return DInt;
   --  int32_t xDriverOpen(CIFXHANDLE* phDriver);

   function Driver_Close
     (Driver_Handle : in Driver_Handle_Type)
      return DInt;
   --  int32_t xDriverClose(CIFXHANDLE hDriver);

   procedure Driver_Get_Information
     (Driver_Handle      : in Driver_Handle_Type;
      Driver_Information : out Driver_Information_Type;
      Error              : out DInt);
   --  int32_t APIENTRY xDriverGetInformation ( CIFXHANDLE  hDriver,
   --                                     uint32_t ulSize, void* pvDriverInfo);

   function Driver_Get_Error_Description
     (Error : DInt)
      return String;
   --  int32_t xDriverGetErrorDescription (int32_t lError, char* szBuffer,
   --                                      uint32_t ulBufferLen);

   function Channel_Open
     (Driver_Handle          : in Driver_Handle_Type;
      Board_Name             : in String;
      Channel_Number         : in DWord;
      Channel_Handle_Access  : access Channel_Handle_Type)
      return DInt;
   --  int32_t APIENTRY xChannelOpen ( CIFXHANDLE  hDriver, char* szBoard,
   --                               uint32_t ulChannel, CIFXHANDLE* phChannel);

   function Channel_Close
     (Channel_Handle : in Channel_Handle_Type)
      return DInt;
   --  int32_t xChannelClose ( CIFXHANDLE  hChannel);

   function Channel_Get_MBX_State
     (Channel_Handle : in Channel_Handle_Type;
      Recv_Pkt_Count : access DWord;
      Send_Pkt_Count : access DWord)
      return DInt;
   --  int32_t APIENTRY xChannelGetMBXState ( CIFXHANDLE  hChannel,
   --                                         uint32_t* pulRecvPktCount,
   --                                         uint32_t* pulSendPktCount);

   function Channel_Put_Packet
     (Channel_Handle : in Channel_Handle_Type;
      Send_Packet    : cifX_Packet_Type;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t APIENTRY xChannelPutPacket ( CIFXHANDLE  hChannel,
   --                                       CIFX_PACKET*  ptSendPkt,
   --                                       uint32_t ulTimeout);

   function Channel_Get_Packet
     (Channel_Handle : in Channel_Handle_Type;
      Size           : in DWord;
      Recv_Packet    : cifX_Packet_Type;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t APIENTRY xChannelGetPacket ( CIFXHANDLE  hChannel,
   --                                       uint32_t ulSize,
   --                                       CIFX_PACKET* ptRecvPkt,
   --                                       uint32_t ulTimeout);

   function Channel_Reset
     (Channel_Handle : in Channel_Handle_Type;
      Reset_Mode     : in DWord;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t APIENTRY xChannelReset ( CIFXHANDLE  hChannel,
   --                                   uint32_t ulResetMode,
   --                                   uint32_t ulTimeout);

   procedure Channel_Info
     (Channel_Handle      : in Channel_Handle_Type;
      Channel_Information : out Channel_Information_Type;
      Error               : out DInt);
   --  int32_t APIENTRY xChannelInfo ( CIFXHANDLE  hChannel,
   --                                  uint32_t ulSize, void* pvChannelInfo);

   function Channel_Watchdog
     (Channel_Handle : in Channel_Handle_Type;
      Command        : in DWord;
      Trigger        : access DWord)
      return DInt;
   --  int32_t APIENTRY xChannelWatchdog ( CIFXHANDLE  hChannel,
   --                                    uint32_t ulCmd, uint32_t* pulTrigger);

   function Channel_Host_State
     (Channel_Handle : in Channel_Handle_Type;
      Command        : in DWord;
      State          : access DWord;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t APIENTRY xChannelHostState ( CIFXHANDLE  hChannel,
   --                  uint32_t ulCmd, uint32_t* pulState, uint32_t ulTimeout);

   function Channel_Bus_State
     (Channel_Handle : in Channel_Handle_Type;
      Command        : in DWord;
      State          : access DWord;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t APIENTRY xChannelBusState ( CIFXHANDLE  hChannel,
   --                  uint32_t ulCmd, uint32_t* pulState, uint32_t ulTimeout);

   function Channel_IO_Read
     (Channel_Handle : in Channel_Handle_Type;
      Area_Number    : in DWord;
      Offset         : in DWord;
      Data_Length    : in DWord;
      Data_In        : Byte_Array;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t xChannelIORead ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber,
   --                           uint32_t ulOffset, uint32_t ulDataLen,
   --                           void* pvData, uint32_t ulTimeout);

   function Channel_IO_Write
     (Channel_Handle : in Channel_Handle_Type;
      Area_Number    : in DWord;
      Offset         : in DWord;
      Data_Length    : in DWord;
      Data_Out       : Byte_Array;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t xChannelIOWrite ( CIFXHANDLE  hChannel, uint32_t ulAreaNumber,
   --                            uint32_t ulOffset, uint32_t ulDataLen,
   --                            void* pvData, uint32_t ulTimeout);

   procedure Channel_Common_Status_Block
     (Channel_Handle      : in Channel_Handle_Type;
      Common_Status_Block : out Common_Status_Block_Type;
      Error               : out DInt);
   --  int32_t APIENTRY xChannelCommonStatusBlock
   --       ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
   --        uint32_t ulDataLen, void* pvData);

   procedure Show_Error (Error : DInt);

   function cifX_Channel_Extended_Status_Block
     (Channel_Handle : in Channel_Handle_Type;
      Cmd            : DWord;
      Offset         : DWord;
      Data_Len       : DWord;
      Data           : System.Address) return DInt;
   --  int32_t APIENTRY xChannelExtendedStatusBlock
   --       ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
   --        uint32_t ulDataLen, void* pvData);

private

   type Driver_Handle_Type is new System.Address;

   type Channel_Handle_Type is new System.Address;

   type C_Driver_Info_Type is
      record
         Driver_Version : Interfaces.C.char_array (1 .. 32) :=
           (others => Interfaces.C.nul);
         --  Human readable driver name and version

         Board_Cnt : DWord := 0;
         --  Number of handled boards

      end record;
   --  Driver Information
   pragma Convention (C, C_Driver_Info_Type);

   function cifX_Driver_Get_Information
     (Driver_Handle      : in Driver_Handle_Type;
      Size               : DWord;
      Driver_Info        : C_Driver_Info_Type) return DInt;
   --  int32_t APIENTRY xDriverGetInformation ( CIFXHANDLE  hDriver,
   --                                  uint32_t ulSize, void* pvDriverInfo);

   function cifX_Driver_Get_Error_Description
     (Error         : DInt;
      Buffer        : System.Address;
      Buffer_Length : DInt) return DInt;
   --  int32_t xDriverGetErrorDescription (int32_t lError, char* szBuffer,
   --                                     uint32_t ulBufferLen);

   function cifX_Channel_Open
     (Driver_Handle          : in Driver_Handle_Type;
      Board_Name             : in Interfaces.C.Strings.chars_ptr;
      Channel_Number         : in DWord;
      Channel_Handle_Access  : access Channel_Handle_Type)
      return DInt;
   --  int32_t APIENTRY xChannelOpen( CIFXHANDLE  hDriver, char* szBoard,
   --                             uint32_t ulChannel, CIFXHANDLE* phChannel);

   type C_Channel_Information_Type is
      record
         Board_Name :
         Interfaces.C.char_array (1 .. CIFx_MAX_INFO_NAME_LENTH) :=
           (others => Interfaces.C.nul);
         --  Global board name

         Board_Alias :
         Interfaces.C.char_array (1 .. CIFx_MAX_INFO_NAME_LENTH) :=
           (others => Interfaces.C.nul);
         --  Global board alias name

         Device_Number : DWord;
         --  Global board device number

         Serial_Number : DWord;
         --  Global board serial number

         FW_Major : Word;
         --  Major Version of Channel Firmware

         FW_Minor : Word;
         --  Minor Version of Channel Firmware

         FW_Build : Word;
         --  Build number of Channel Firmware

         FW_Revision : Word;
         --  Revision of Channel Firmware

         FW_NameLength : Byte;
         --  Length of FW Name

         FW_Name :
         Interfaces.C.char_array (1 .. 63) :=
           (others => Interfaces.C.nul);
         --  Firmware Name

         FW_Year : Word;
         --  Build Year of Firmware

         FW_Month : Byte;
         --  Build Month of Firmware (1..12)

         FW_Day : Byte;
         --  Build Day of Firmware (1..31)

         Channel_Error : DWord;
         --  Channel error

         Open_Cnt : DWord;
         --  Channel open counter

         Put_Packet_Cnt : DWord;
         --  Number of put packet commands

         Get_Packet_Cnt : DWord;
         --  Number of get packet commands

         Mailbox_Size : DWord;
         --  Mailbox packet size in bytes

         IO_In_Area_Cnt : DWord;
         --  Number of IO IN areas

         IO_Out_Area_Cnt : DWord;
         --  Number of IO OUT areas

         Hsk_Size : DWord;
         --  Size of the handshake cells

         Netx_Flags : DWord;
         --  Actual netX state flags

         Host_Flags : DWord;
         --  Actual Host flags

         Host_COS_Flags : DWord;
         --  Actual Host COS flags

         Device_COS_Flags : DWord;
         --  Actual Device COS flags

      end record;
   --  Channel Information
   pragma Convention (C, C_Channel_Information_Type);

   function cifX_Channel_Info
     (Channel_Handle : in Channel_Handle_Type;
      Size           : DWord;
      Channel_Info   : C_Channel_Information_Type) return DInt;
   --  int32_t APIENTRY xChannelInfo ( CIFXHANDLE  hChannel,
   --                                  uint32_t ulSize, void* pvChannelInfo);

   type C_netX_Master_Status_Type is
      record
         Slave_State          : DWord;
         --  Slave status

         Slave_Err_Log_Ind    : DWord;
         --  Slave error indication

         Num_Of_Config_Slaves : DWord;
         --  Number of configured slaves

         Num_Of_Active_Slaves : DWord;
         --  Number of active slaves

         Num_Of_Diag_Slaves   : DWord;
         --  Number of slaves in diag mode

         Reserved             : DWord;
         --  Number of slaves in diag mode

      end record;
   --  Default master status structure
   pragma Convention (C, C_netX_Master_Status_Type);

   type C_Common_Status_Block_Type is
      record
         Communication_COS   : DWord;
         --  Communication channel "Change Of State"

         Communication_State : DWord;
         --  Actual communication state

         Communication_Error : DWord;
         --  Actual communication error

         Version             : Word;
         --  Version of the diagnostic structure

         Watchdog_Time       : Word;
         --  Configured watchdog time

         PDIn_Hsk_Mode       : Byte;
         --  Input area handshake mode

         PDIn_Source         : Byte;
         --  Reserved. Set to zero

         PDOut_Hsk_Mode      : Byte;
         --  Output area handshake mode

         PDOut_Source        : Byte;
         --  Reserved. Set to zero

         Host_Watchdog       : DWord;
         --  Host watchdog

         Error_Count         : DWord;
         --  Number of errors since power-up

         Error_Log_Ind       : Byte;
         --  Counter of available Log Entries (not supported yet)

         Error_PDIn_Cnt      : Byte;
         --  Counter of input process data handshake handling errors

         Error_PDOut_Cnt     : Byte;
         --  Counter of output process data handshake handling errors

         Error_Sync_Cnt      : Byte;
         --  Counter of synchronization handshake handling errors

         Sync_Hsk_Mode       : Byte;
         --  Synchronization Handshake mode

         Sync_Source         : Byte;
         --  Synchronization source

         Reserved_0          : Word;
         --  Reserved[3]

         Reserved_1          : Word;
         --  Reserved[3]

         Reserved_2          : Word;
         --  Reserved[3]

         netX_Master_Status  : C_netX_Master_Status_Type;

      end record;
   --  Channel common status block
   pragma Convention (C, C_Common_Status_Block_Type);

   function cifX_Channel_Common_Status_Block
     (Channel_Handle : in Channel_Handle_Type;
      Cmd            : DWord;
      Offset         : DWord;
      Data_Len       : DWord;
      Data           : C_Common_Status_Block_Type) return DInt;
   --  int32_t APIENTRY xChannelCommonStatusBlock
   --       ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
   --        uint32_t ulDataLen, void* pvData);

   pragma Import (StdCall, Driver_Open, "xDriverOpen");
   pragma Import (StdCall, Driver_Close, "xDriverClose");
   pragma Import (StdCall, cifX_Driver_Get_Information,
                  "xDriverGetInformation");
   pragma Import (StdCall, cifX_Driver_Get_Error_Description,
                  "xDriverGetErrorDescription");

   pragma Import (StdCall, cifX_Channel_Open, "xChannelOpen");
   pragma Import (StdCall, Channel_Close, "xChannelClose");
   pragma Import (StdCall, Channel_Get_MBX_State, "xChannelGetMBXState");
   pragma Import (StdCall, Channel_Put_Packet, "xChannelPutPacket");
   pragma Import (StdCall, Channel_Get_Packet, "xChannelGetPacket");
   pragma Import (StdCall, Channel_Reset, "xChannelReset");
   pragma Import (StdCall, cifX_Channel_Info, "xChannelInfo");
   pragma Import (StdCall, Channel_Watchdog, "xChannelWatchdog");
   pragma Import (StdCall, Channel_Host_State, "xChannelHostState");
   pragma Import (StdCall, Channel_Bus_State, "xChannelBusState");
   pragma Import (StdCall, Channel_IO_Read, "xChannelIORead");
   pragma Import (StdCall, Channel_IO_Write, "xChannelIOWrite");
   pragma Import (StdCall, cifX_Channel_Common_Status_Block,
                  "xChannelCommonStatusBlock");
   pragma Import (StdCall, cifX_Channel_Extended_Status_Block,
                  "xChannelExtendedStatusBlock");

end A4A.Protocols.HilscherX.cifX_User;
