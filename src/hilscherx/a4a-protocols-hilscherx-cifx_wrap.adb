
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Application.Configuration; use A4A.Application.Configuration;

package body A4A.Protocols.HilscherX.cifX_Wrap is

   procedure Set_Driver
     (Driver_Handle  : in Driver_Handle_Type) is
   begin

      The_Driver_Handle := Driver_Handle;
      Initialised := True;

   end Set_Driver;

   function Driver_Open_Wrap
     (Driver_Handle_Access : access Driver_Handle_Type)
      return DInt is
   begin

      Driver_Handle_Access.all := The_Driver_Handle;

      A4A.Log.Logger.Put
        (Who  => Package_Ident & ".Driver_Open_Wrap",
         What => "Done.",
         Log_Level => A4A.Logger.Level_Info);

      return CIFX_NO_ERROR;

   end Driver_Open_Wrap;

   function Driver_Close_Wrap
     (Driver_Handle : in Driver_Handle_Type)
      return DInt is
      pragma Unreferenced (Driver_Handle);
   begin

      A4A.Log.Logger.Put
        (Who  => Package_Ident & ".Driver_Close_Wrap",
         What => "Done.",
         Log_Level => A4A.Logger.Level_Info);

      return CIFX_NO_ERROR;

   end Driver_Close_Wrap;

   function Get_Channel (Channel_Handle : Channel_Handle_Type)
                         return Channel_Messaging.Instance_Access is
   begin
      for I in Fieldbuses_Configuration'Range loop
         if Channel_Handle = Fieldbuses_Configuration (I).Channel_Handle
         then
            return Fieldbuses_Configuration (I).My_Channel;
         end if;
      end loop;
      return null;
   end Get_Channel;

   function Channel_Get_MBXState_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      RecvPktCount_Access : access DWord;
      SendPktCount_Access : access DWord)
      return DInt is
      My_Channel : constant Channel_Messaging.Instance_Access :=
        Get_Channel (Channel_Handle);
   begin

      if My_Channel = null then
         A4A.Log.Logger.Put
           (Who  => Package_Ident & ".Channel_Get_MBXState_Wrap",
            What => "Fieldbus other",
            Log_Level => A4A.Logger.Level_Verbose);

         return Channel_Get_MBX_State (Channel_Handle => Channel_Handle,
                                       Recv_Pkt_Count => RecvPktCount_Access,
                                       Send_Pkt_Count => SendPktCount_Access);
      end if;

      --  else
         A4A.Log.Logger.Put
           (Who  => Package_Ident & ".Channel_Get_MBXState_Wrap",
            What => "Fieldbus managed",
            Log_Level => A4A.Logger.Level_Verbose);

         RecvPktCount_Access.all := My_Channel.Get_Pending;
         SendPktCount_Access.all := My_Channel.Get_Send_Pkt_Count;

         return CIFX_NO_ERROR;

   end Channel_Get_MBXState_Wrap;

   function Channel_Put_Packet_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      Send_Packet    : cifX_Packet_Type;
      Time_Out       : in DWord)
      return DInt is

      My_Ident : constant String := Package_Ident & ".Channel_Put_Packet_Wrap";

      My_Channel : constant Channel_Messaging.Instance_Access :=
        Get_Channel (Channel_Handle);

      Packet_Got   : Boolean := False;
      Packet_Sent  : Boolean := False;

      Elapsed : DInt := DInt (Time_Out);

      procedure Dump_Packet;

      procedure Dump_Packet is

         Dest_String    : String (1 .. 20);
         Src_String     : String (1 .. 20);
         Dest_Id_String : String (1 .. 20);
         Src_Id_String  : String (1 .. 20);
         Id_String      : String (1 .. 20);
         Cmd_String     : String (1 .. 20);

      begin

         DWord_Text_IO.Put
           (To   => Dest_String,
            Item => Send_Packet.Header.Dest,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_String,
            Item => Send_Packet.Header.Src,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Dest_Id_String,
            Item => Send_Packet.Header.Dest_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_Id_String,
            Item => Send_Packet.Header.Src_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Id_String,
            Item => Send_Packet.Header.Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Cmd_String,
            Item => Send_Packet.Header.Cmd,
            Base => 16);

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Got a message : TO = " & Time_Out'Img & CRLF
            & "Dest    : " & Dest_String & CRLF
            & "Src     : " & Src_String & CRLF
            & "Dest_Id : " & Dest_Id_String & CRLF
            & "Src_Id  : " & Src_Id_String & CRLF
            & "Id      : " & Id_String & CRLF
            & "Cmd     : " & Cmd_String
           );

      end Dump_Packet;

   begin

      if My_Channel = null then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Fieldbus other",
            Log_Level => A4A.Logger.Level_Verbose);

         return Channel_Put_Packet (Channel_Handle, Send_Packet, Time_Out);
      end if;

      --  else
      if Send_Packet.Header.Src /= 0 then

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What =>
              ">>>Send_Packet.Header.Src : " & Send_Packet.Header.Src'Img,
            Log_Level => A4A.Logger.Level_Error);

      end if;

      if A4A.Log.Logger.Get_Level = A4A.Logger.Level_Verbose then

         case Send_Packet.Header.Cmd is

         when 16#00002F00# =>
            null;

         when others =>

            Dump_Packet;

         end case;

      end if;

      loop

         My_Channel.Get_Packet
           (Item   => Request,
            Result => Packet_Got);

         if Packet_Got then

            Request.Header := Send_Packet.Header;

            for I in Request.Data'Range loop
               exit when I >= Integer (Request.Header.Len);
               Request.Data (I) := Send_Packet.Data (I);
            end loop;

         else

            Elapsed := Elapsed - 100;

            delay 0.1;

         end if;

         exit when Packet_Got or Elapsed < 0;

      end loop;

      if Packet_Got then

         loop

            My_Channel.Send
              (Item   => Request,
               Result => Packet_Sent);

            if not Packet_Sent then

               Elapsed := Elapsed - 100;

               delay 0.1;

            end if;

            exit when Packet_Sent or Elapsed < 0;

         end loop;

      end if;

      if Packet_Sent then

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Packet sent.",
            Log_Level => A4A.Logger.Level_Verbose);

         return CIFX_NO_ERROR;

      else

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Put Time out !",
            Log_Level => A4A.Logger.Level_Error);

         return CIFX_DEV_PUT_TIMEOUT;

      end if;

   end Channel_Put_Packet_Wrap;

   function Channel_Get_Packet_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      Size           : in DWord;
      Recv_Packet_Address : System.Address;
      Time_Out       : in DWord)
      return DInt is

      My_Ident : constant String := Package_Ident & ".Channel_Get_Packet_Wrap";

      My_Channel : constant Channel_Messaging.Instance_Access :=
        Get_Channel (Channel_Handle);

      Recv_Packet    : cifX_Packet_Type;
      for Recv_Packet'Address use Recv_Packet_Address;

      Answer_Got   : Boolean := False;

      Elapsed : DInt := DInt (Time_Out);

      procedure Dump_Packet;

      procedure Dump_Packet is

         Dest_String    : String (1 .. 20);
         Src_String     : String (1 .. 20);
         Dest_Id_String : String (1 .. 20);
         Src_Id_String  : String (1 .. 20);
         Id_String      : String (1 .. 20);
         State_String   : String (1 .. 20);
         Cmd_String     : String (1 .. 20);

      begin

         DWord_Text_IO.Put
           (To   => Dest_String,
            Item => Recv_Packet.Header.Dest,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_String,
            Item => Recv_Packet.Header.Src,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Dest_Id_String,
            Item => Recv_Packet.Header.Dest_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Src_Id_String,
            Item => Recv_Packet.Header.Src_Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Id_String,
            Item => Recv_Packet.Header.Id,
            Base => 16);

         DWord_Text_IO.Put
           (To   => State_String,
            Item => Recv_Packet.Header.State,
            Base => 16);

         DWord_Text_IO.Put
           (To   => Cmd_String,
            Item => Recv_Packet.Header.Cmd,
            Base => 16);

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Got a message : TO = " & Time_Out'Img & CRLF
            & "Dest    : " & Dest_String & CRLF
            & "Src     : " & Src_String & CRLF
            & "Dest_Id : " & Dest_Id_String & CRLF
            & "Src_Id  : " & Src_Id_String & CRLF
            & "Id      : " & Id_String & CRLF
            & "State   : " & State_String & CRLF
            & "Cmd     : " & Cmd_String
           );

      end Dump_Packet;

   begin

      if My_Channel = null then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Fieldbus other",
            Log_Level => A4A.Logger.Level_Verbose);

         return Channel_Get_Packet (Channel_Handle => Channel_Handle,
                                    Size           => Size,
                                    Recv_Packet    => Recv_Packet,
                                    Time_Out       => Time_Out);
      end if;

      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Cmd received",
         Log_Level => A4A.Logger.Level_Verbose);

      loop

         My_Channel.Receive
           (Item   => Confirmation,
            Result => Answer_Got);

         if Answer_Got then

            Recv_Packet.Header := Confirmation.Header;

            for I in Confirmation.Data'Range loop
               exit when I >= Integer (Confirmation.Header.Len);
               Recv_Packet.Data (I) := Confirmation.Data (I);
            end loop;

            My_Channel.Return_Packet
              (Item => Confirmation);

         else

            Elapsed := Elapsed - 100;

            delay 0.1;

         end if;

         exit when Answer_Got or Elapsed < 0;

      end loop;

      if Answer_Got then

         if A4A.Log.Logger.Get_Level = A4A.Logger.Level_Verbose then

            case Recv_Packet.Header.Cmd is

            when 16#00002F01# =>
               null;

            when others =>

               Dump_Packet;

            end case;

         end if;

         return CIFX_NO_ERROR;

      else

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Packet Time out !",
            Log_Level => A4A.Logger.Level_Verbose);

         return CIFX_DEV_GET_TIMEOUT;

      end if;

   end Channel_Get_Packet_Wrap;

end A4A.Protocols.HilscherX.cifX_Wrap;
