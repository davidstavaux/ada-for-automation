
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;

package body A4A.Protocols.HilscherX.Indication_FB is

   procedure Initialise
     (Function_Block : in out Instance'Class;
     Channel_Access  : in Channel_Messaging.Instance_Access) is
   begin

      Function_Block.My_Channel := Channel_Access;

      Function_Block.Initialised := True;

   end Initialise;

   procedure Cyclic
     (Function_Block : in out Instance'Class;
      Got_Indication : out Boolean) is
   begin

      case Function_Block.Status is

         when X00 =>

            if Function_Block.Indication_Got and Function_Block.Initialised
            then

               Function_Block.Status := X01;
               Function_Block.Indication_Got := False;

            end if;

         when X01 =>

            if Function_Block.Answer_Got then

               A4A.Log.Logger.Put
                 (Who  => Function_Block.Get_Ident & ".Cyclic",
                  What => "Answer Got");

               Function_Block.Status := X02;
               Function_Block.Answer_Got := False;

            end if;

         when X02 =>

            Function_Block.My_Channel.Send
              (Item   => Function_Block.Response,
               Result => Function_Block.Packet_Sent);

            if Function_Block.Packet_Sent then

               A4A.Log.Logger.Put
                 (Who  => Function_Block.Get_Ident & ".Cyclic",
                  What => "Response Sent");

               Function_Block.Status := X00;

            end if;

      end case;

      Got_Indication := Function_Block.Status = X01;

   end Cyclic;

   function Is_Ready
     (Function_Block : in Instance'Class) return Boolean is
   begin

      return Function_Block.Status = X00;

   end Is_Ready;

   function Get_Ident
     (Function_Block : in out Instance) return String
      is (raise Program_Error with "Get_Ident not implemented");

end A4A.Protocols.HilscherX.Indication_FB;
