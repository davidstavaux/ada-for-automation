
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body A4A.Protocols.HilscherX.Packet_Queue_Map is

   protected body Packet_Queue_Map_Type is

      procedure Put
        (Item   : in Packet_Queue_Access;
         Index  : out DWord;
         Result : out Boolean) is

         The_Index : Positive;

      begin

         if Empty then
            Result := False;
         else
            The_Index := Queue_Map_Indexes (Queue_Map_Index);
            Queue_Map (The_Index) := Item;
            Index := DWord (The_Index);
            Result := True;

            if Queue_Map_Index > Queue_Map_Indexes'First (1) then
               Queue_Map_Index := Queue_Map_Index - 1;
            else
               Empty := True;
            end if;

         end if;

      end Put;

      procedure Get
        (Index  : in DWord;
         Item   : out Packet_Queue_Access;
         Result : out Boolean) is

         The_Index : Positive;

      begin

         The_Index := Positive (Index);

         if The_Index not in Queue_Map'Range then
            Result := False;
         else

            Item := Queue_Map (The_Index);

            Result := True;

         end if;

      end Get;

      procedure Delete
        (Index  : in DWord;
         Result : out Boolean) is

         The_Index : Positive;

      begin

         The_Index := Positive (Index);

         if The_Index not in Queue_Map'Range then
            Result := False;
         else

            if Empty then
               Empty := False;
            else
               Queue_Map_Index := Queue_Map_Index + 1;
            end if;

            Queue_Map_Indexes (Queue_Map_Index) := The_Index;

            Queue_Map (The_Index) := null;

            Result := True;

         end if;

      end Delete;

      procedure Initialise is
      begin

         for Index in Queue_Map'Range loop
            Queue_Map_Indexes (Index) := Index;
         end loop;

         Queue_Map_Index := Queue_Map_Indexes'Last (1);
         Empty := False;

      end Initialise;

   end Packet_Queue_Map_Type;

end A4A.Protocols.HilscherX.Packet_Queue_Map;
