
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;

package body A4A.Protocols.HilscherX.Request_FB is

   procedure Initialise
     (Function_Block : in out Instance'Class;
     Channel_Access  : in Channel_Messaging.Instance_Access) is
   begin

      Function_Block.My_Channel := Channel_Access;

      Function_Block.My_Channel.Put
        (Item   => Function_Block.My_Receive_Queue'Unrestricted_Access,
         Index  => Function_Block.My_Receive_Queue_ID,
         Result => Function_Block.Queue_ID_Got);

      if not Function_Block.Queue_ID_Got then
         A4A.Log.Logger.Put
           (Who  => Function_Block.Get_Ident & ".Initialise",
            What => "Could not Register my Queue !");

      else

         Function_Block.Initialised := True;

      end if;

   end Initialise;

   procedure Cyclic
     (Function_Block : in out Instance'Class;
      Do_Command     : in Boolean;
      Done           : out Boolean;
      Error          : out Boolean) is
   begin

      case Function_Block.Status is

         when X00 =>

            if Do_Command and Function_Block.Initialised then
               Function_Block.Error_Flag := False;
               Function_Block.Status     := X01;
               Function_Block.Message_Id := Function_Block.Message_Id + 1;
            end if;

         when X01 =>

            Function_Block.My_Channel.Get_Packet
              (Item   => Function_Block.Request,
               Result => Function_Block.Packet_Got);

            if Function_Block.Packet_Got then
               Function_Block.Status := X02;
            end if;

         when X02 =>

            --  Filling the generic part

            Function_Block.Request.Header.Dest    := 16#20#;

            Function_Block.Request.Header.Src     :=
              Function_Block.My_Receive_Queue_App;

            Function_Block.Request.Header.Dest_Id := 0;

            Function_Block.Request.Header.Src_Id  :=
              Function_Block.My_Receive_Queue_ID;

            Function_Block.Request.Header.Id      :=
              Function_Block.Message_Id;

            Function_Block.Request.Header.State   := 0;

            Function_Block.Request.Header.Ext     := 0;
            Function_Block.Request.Header.Rout    := 0;

            --  Filling the specific part

            Function_Block.Fill_Request (Function_Block.Request);

            Function_Block.Status := X03;

         when X03 =>

            Function_Block.My_Channel.Send
              (Item   => Function_Block.Request,
               Result => Function_Block.Packet_Sent);

            if Function_Block.Packet_Sent then
               Function_Block.Status := X04;
            end if;

         when X04 =>

            Function_Block.My_Receive_Queue.Get
              (Item   => Function_Block.Confirmation,
               Result => Function_Block.Answer_Got);

            if Function_Block.Answer_Got then

               if Function_Block.Confirmation.Header.Id /=
                 Function_Block.Message_Id
               then

                  Function_Block.Error_Flag := True;

                  Function_Block.Type_Of_Error := Error_Id;

                  A4A.Log.Logger.Put
                    (Who  => Function_Block.Get_Ident & ".Cyclic",
                     What => "Wrong Message Id received");

               else

                  Function_Block.Store_Result
                    (Confirmation  => Function_Block.Confirmation,
                     Error         => Function_Block.Error_Flag,
                     Type_Of_Error => Function_Block.Type_Of_Error,
                     Last_Error    => Function_Block.Last_Error);

                  if Function_Block.Error_Flag
                    and then Function_Block.Type_Of_Error = Error_Cmd
                  then

                     A4A.Log.Logger.Put
                       (Who  => Function_Block.Get_Ident & ".Cyclic",
                        What => "Wrong Command received");

                  end if;

               end if;

               Function_Block.My_Channel.Return_Packet
                 (Item => Function_Block.Confirmation);

               Function_Block.Status := X05;
            end if;

         when X05 =>

            if not Do_Command then
               Function_Block.Status := X00;
            end if;

      end case;

      Done := (Function_Block.Status = X05);

      Error := Function_Block.Error_Flag;

   end Cyclic;

   function Get_Type_Of_Error
     (Function_Block : in Instance) return Error_Type is
   begin

      return Function_Block.Type_Of_Error;

   end Get_Type_Of_Error;

   function Get_Last_Error
     (Function_Block : in Instance) return DWord is
   begin

      return Function_Block.Last_Error;

   end Get_Last_Error;

   procedure Show_Error
     (Function_Block : in Instance'Class;
      Who            : in String) is

      Error_String  : String (1 .. 20);

      First_Non_Space : Positive;

   begin

      case Function_Block.Get_Type_Of_Error is

      when Request_FB.Error_None =>

         null;

      when Request_FB.Error_Id =>

         A4A.Log.Logger.Put (Who  => Who,
                             What => "Got an ID error");

      when Request_FB.Error_Cmd =>

         A4A.Log.Logger.Put (Who  => Who,
                             What => "Got a Cmd error");

      when Request_FB.Error_Status =>

         DWord_Text_IO.Put
           (To   => Error_String,
            Item => Function_Block.Get_Last_Error,
            Base => 16);

         for Index in Error_String'Range loop
            First_Non_Space := Index;
            exit when Error_String (Index) /= ' ';
         end loop;

         A4A.Log.Logger.Put
           (Who  => Who,
            What => "Got a Status error : "
            & Error_String (First_Non_Space .. Error_String'Last));

      end case;

   end Show_Error;

   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access) is
   begin
      raise Program_Error with "Fill_Request not implemented";
   end Fill_Request;

   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord) is
   begin
      raise Program_Error with "Store_Result not implemented";
   end Store_Result;

   function Get_Ident
     (Function_Block : in out Instance) return String
     is (raise Program_Error with "Get_Ident not implemented");

end A4A.Protocols.HilscherX.Request_FB;
