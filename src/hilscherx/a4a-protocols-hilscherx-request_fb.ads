
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - Request Function Block
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

with A4A.Protocols.HilscherX.Channel_Messaging;

package A4A.Protocols.HilscherX.Request_FB is

   type Error_Type is
     (Error_None,
      Error_Id,
      Error_Cmd,
      Error_Status);

   type Instance is abstract tagged limited private;
   type Instance_Access is access all Instance;

   procedure Initialise
     (Function_Block : in out Instance'Class;
     Channel_Access  : in Channel_Messaging.Instance_Access);

   procedure Cyclic
     (Function_Block : in out Instance'Class;
      Do_Command     : in Boolean;
      Done           : out Boolean;
      Error          : out Boolean);

   function Get_Type_Of_Error
     (Function_Block : in Instance) return Error_Type;

   function Get_Last_Error
     (Function_Block : in Instance) return DWord;

   procedure Show_Error
     (Function_Block : in Instance'Class;
      Who            : in String);

private

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Get packet from pool

      X02,
      --  Fill packet

      X03,
      --  Send packet

      X04,
      --  Get answer

      X05
      --  Done
     );

   type Instance is abstract tagged limited
      record

         My_Channel   : Channel_Messaging.Instance_Access;

         Status       : Block_Status := X00;

         Request      : cifX_Packet_Access;
         Confirmation : cifX_Packet_Access;

         Message_Id   : DWord := 0;

         Initialised  : Boolean := False;

         Packet_Got   : Boolean := False;
         Packet_Sent  : Boolean := False;
         Answer_Got   : Boolean := False;

         My_Receive_Queue : aliased Packet_Queue_Type (Queue_Size => 1);
         My_Receive_Queue_App : DWord := Channel_Messaging.A4A_App_0;
         My_Receive_Queue_ID  : DWord;

         Queue_ID_Got : Boolean := False;

         Error_Flag    : Boolean    := False;
         Type_Of_Error : Error_Type := Error_None;
         Last_Error    : DWord      := 0;

      end record;

   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access);

   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord);

   function Get_Ident
     (Function_Block : in out Instance) return String;

end A4A.Protocols.HilscherX.Request_FB;
