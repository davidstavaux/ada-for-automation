
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time;

with A4A.Memory.MBTCP_IOServer;
with A4A.Memory.Hilscher_Fieldbus1;
use A4A.Memory;

with A4A.Library.Conversion; use A4A.Library.Conversion;
with A4A.User_Objects_Fieldbus1; use A4A.User_Objects_Fieldbus1;

package body A4A.User_Functions_Fieldbus1 is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
   begin

      Cmd_Byte     := Hilscher_Fieldbus1.Inputs (0);
      Pattern_Byte := Hilscher_Fieldbus1.Inputs (1);

   end Map_Inputs;

   procedure Map_Outputs is
   begin

      Hilscher_Fieldbus1.Outputs (0) := Cmd_Byte;
      Hilscher_Fieldbus1.Outputs (1) := Output_Byte;

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      Bytes_To_Word (LSB_Byte => Hilscher_Fieldbus1.Inputs (1),
                     MSB_Byte => Hilscher_Fieldbus1.Inputs (0),
                     Word_out => MBTCP_IOServer.Input_Registers (100));

      Bytes_To_Word (LSB_Byte => Hilscher_Fieldbus1.Outputs (1),
                     MSB_Byte => Hilscher_Fieldbus1.Outputs (0),
                     Word_out => MBTCP_IOServer.Input_Registers (101));

   end Map_HMI_Outputs;

   procedure Process_IO is

      Elapsed_TON_1 : Ada.Real_Time.Time_Span;

   begin

      if First_Cycle then

         Output_Byte := Pattern_Byte;

         First_Cycle := False;

      end if;

      Tempo_TON_1.Cyclic (Start   => not TON_1_Q,
                          Preset  => Ada.Real_Time.Milliseconds (1000),
                          Elapsed => Elapsed_TON_1,
                          Q       => TON_1_Q);

      if TON_1_Q then

         case Cmd_Byte is

         when 0 =>
            Output_Byte := ROR (Value => Output_Byte, Amount => 1);

         when 1 =>
            Output_Byte := ROL (Value => Output_Byte, Amount => 1);

         when others => Output_Byte := Pattern_Byte;

         end case;

      end if;

   end Process_IO;

end A4A.User_Functions_Fieldbus1;
