
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Calendar.Arithmetic; use Ada.Calendar.Arithmetic;

with Gtk.Enums;

with A4A.Configuration; use A4A.Configuration;
with A4A.Application.Configuration; use A4A.Application.Configuration;

with A4A.Log;
with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Fieldbus1;
with A4A.Kernel.Fieldbus2;
with Ada.Calendar.Formatting;

package body A4A.GUI_Elements.General_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  General Status Page
   --------------------------------------------------------------------

   procedure Create_Application_UI
     (GUI_Element   : in out Instance;
      Start_Time_In : in Ada.Calendar.Time)
   is
      Application_UI : constant access Application_UI_Type :=
        GUI_Element.Application_UI'Access;
   begin
      Start_Time := Start_Time_In;

      Gtk_New (Application_UI.Frame, "Application");
      Gtk_Label (Application_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Application</b>");
      Application_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Application_UI.VBox);

      Gtk_New_Hbox (Application_UI.HBox);

      Gtk_New (Application_UI.Start_Time_Label,
               "<big>Start time:</big>" & CRLF
               & Ada.Calendar.Formatting.Image
                 (Date                  => Start_Time,
                  Include_Time_Fraction => False,
                  Time_Zone             => The_Time_Offset));
      Application_UI.Start_Time_Label.Set_Use_Markup (True);

      Application_UI.HBox.Pack_Start
        (Application_UI.Start_Time_Label, Expand => False, Padding => 10);

      Gtk_New (Application_UI.Up_Time_Label,
               "<big>Up time:</big>" & CRLF
               & "hh:mm:ss");
      Application_UI.Up_Time_Label.Set_Use_Markup (True);

      Application_UI.HBox.Pack_Start
        (Application_UI.Up_Time_Label, Expand => False, Padding => 10);

      Application_UI.VBox.Pack_Start
        (Application_UI.HBox, Expand => False, Padding => 10);

      Application_UI.Frame.Add (Application_UI.VBox);

   end Create_Application_UI;

   procedure Create_Main_Task_UI (GUI_Element : in out Instance) is
      Main_Task_UI : constant access Main_Task_UI_Type :=
        GUI_Element.Main_UI'Access;
   begin
      Gtk_New (Main_Task_UI.Frame, "Main Task");
      Gtk_Label (Main_Task_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Main Task</b>");
      Main_Task_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Main_Task_UI.VBox);

      Gtk_New_Hbox (Main_Task_UI.Config_HBox);

      Gtk_New (Main_Task_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF);
      Main_Task_UI.Config_Label.Set_Use_Markup (True);

      if Application_Main_Task_Type = Cyclic then
         Main_Task_UI.Config_Label.Set_Markup
           ("<big>Task Configuration</big>" & CRLF
            & "Cyclic type | "
            & "Delay : " & Integer'Image (Application_Main_Task_Delay_MS)
            & " ms");
      elsif Application_Main_Task_Type = Periodic then
         Main_Task_UI.Config_Label.Set_Markup
           ("<big>Task Configuration</big>" & CRLF
            & "Periodic type | "
            & "Period : " & Integer'Image (Application_Main_Task_Period_MS)
            & " ms");
      end if;

      Main_Task_UI.Config_HBox.Pack_Start
        (Main_Task_UI.Config_Label, Expand => False, Padding => 10);

      Main_Task_UI.VBox.Pack_Start
        (Main_Task_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Main_Task_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Main_Task_UI.Watchdog_VBox);

      Gtk_New (Main_Task_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Main_Task_UI.Watchdog_Image, "gtk-yes",
               Gtk.Enums.Icon_Size_Button);

      Main_Task_UI.Watchdog_VBox.Pack_Start
        (Main_Task_UI.Watchdog_Label, Expand => False, Padding => 10);

      Main_Task_UI.Watchdog_VBox.Pack_Start
        (Main_Task_UI.Watchdog_Image, Expand => False);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Main_Task_UI.Running_VBox);

      Gtk_New (Main_Task_UI.Running_Label, "Running");

      Gtk_New (Main_Task_UI.Running_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Main_Task_UI.Running_VBox.Pack_Start
        (Main_Task_UI.Running_Label, Expand => False, Padding => 10);

      Main_Task_UI.Running_VBox.Pack_Start
        (Main_Task_UI.Running_Image, Expand => False);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Running_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Main_Task_UI.Stats_VBox);

      Gtk_New (Main_Task_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Main_Task_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Main_Task_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Main_Task_UI.Stats_HBox);

      Gtk_New (Main_Task_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Main_Task_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Main_Task_UI.Stats_Values_Label, "Values");

      Main_Task_UI.Stats_HBox.Pack_Start
        (Main_Task_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Main_Task_UI.Stats_HBox.Pack_Start
        (Main_Task_UI.Stats_Values_Label, Expand => False);

      Main_Task_UI.Stats_Frame.Add (Main_Task_UI.Stats_HBox);

      Main_Task_UI.Stats_VBox.Pack_Start
        (Main_Task_UI.Stats_Frame, Expand => False, Padding => 10);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Stats_VBox, Expand => False, Padding => 10);

      --  Scheduling Stats

      Gtk_New_Vbox (Main_Task_UI.Sched_Stats_VBox);

      Gtk_New (Main_Task_UI.Sched_Stats_Frame, "Scheduling Stats (delay)");
      Gtk_Label (Main_Task_UI.Sched_Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Scheduling Stats (delay)</u>" & CRLF);
      Main_Task_UI.Sched_Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Main_Task_UI.Sched_Stats_HBox);

      Gtk_New (Main_Task_UI.Sched_Stats_Items_Label,
               "+ 100 µs : " & CRLF
               & "+ 01 ms : " & CRLF
               & "+ 10 ms : " & CRLF
               & "+ 20 ms : " & CRLF
               & "+ 30 ms : " & CRLF
               & "+ 40 ms : " & CRLF
               & "+ 50 ms : " & CRLF
               & "+ 60 ms : " & CRLF
               & "+ 70 ms : " & CRLF
               & "+ 80 ms : ");
      Main_Task_UI.Sched_Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Main_Task_UI.Sched_Stats_Values_Label, "Values");

      Main_Task_UI.Sched_Stats_HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Items_Label,
         Expand => False, Padding => 10);

      Main_Task_UI.Sched_Stats_HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Values_Label, Expand => False);

      Main_Task_UI.Sched_Stats_Frame.Add (Main_Task_UI.Sched_Stats_HBox);

      Main_Task_UI.Sched_Stats_VBox.Pack_Start
        (Main_Task_UI.Sched_Stats_Frame, Expand => False, Padding => 10);

      Main_Task_UI.HBox.Pack_Start
        (Main_Task_UI.Sched_Stats_VBox, Expand => False, Padding => 10);

      Main_Task_UI.VBox.Pack_Start
        (Main_Task_UI.HBox, Expand => False);

      Main_Task_UI.Frame.Add (Main_Task_UI.VBox);

   end Create_Main_Task_UI;

   procedure Create_Periodic_Task1_UI (GUI_Element : in out Instance) is
      Periodic_Task1_UI : constant access Periodic_Task1_UI_Type :=
        GUI_Element.Periodic1_UI'Access;
   begin
      Gtk_New (Periodic_Task1_UI.Frame, "Periodic Task 1");
      Gtk_Label (Periodic_Task1_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Periodic Task 1</b>");
      Periodic_Task1_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Periodic_Task1_UI.VBox);

      Gtk_New_Hbox (Periodic_Task1_UI.Config_HBox);

      Gtk_New (Periodic_Task1_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF
               & "Period : xxxx ms");
      Periodic_Task1_UI.Config_Label.Set_Use_Markup (True);

      Periodic_Task1_UI.Config_Label.Set_Markup
        ("<big>Task Configuration</big>" & CRLF
         & "Period : "
         & Integer'Image (Application_Periodic_Task_1_Period_MS)
         & " ms");

      Periodic_Task1_UI.Config_HBox.Pack_Start
        (Periodic_Task1_UI.Config_Label,
         Expand => False, Padding => 10);

      Periodic_Task1_UI.VBox.Pack_Start
        (Periodic_Task1_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Periodic_Task1_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Periodic_Task1_UI.Watchdog_VBox);

      Gtk_New (Periodic_Task1_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Periodic_Task1_UI.Watchdog_Image, "gtk-yes",
               Gtk.Enums.Icon_Size_Button);

      Periodic_Task1_UI.Watchdog_VBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_Label, Expand => False, Padding => 10);

      Periodic_Task1_UI.Watchdog_VBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_Image, Expand => False);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Periodic_Task1_UI.Running_VBox);

      Gtk_New (Periodic_Task1_UI.Running_Label, "Running");

      Gtk_New (Periodic_Task1_UI.Running_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Periodic_Task1_UI.Running_VBox.Pack_Start
        (Periodic_Task1_UI.Running_Label, Expand => False, Padding => 10);

      Periodic_Task1_UI.Running_VBox.Pack_Start
        (Periodic_Task1_UI.Running_Image, Expand => False);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Running_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Periodic_Task1_UI.Stats_VBox);

      Gtk_New (Periodic_Task1_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Periodic_Task1_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Periodic_Task1_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Periodic_Task1_UI.Stats_HBox);

      Gtk_New (Periodic_Task1_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Periodic_Task1_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Periodic_Task1_UI.Stats_Values_Label,
               "Values");

      Periodic_Task1_UI.Stats_HBox.Pack_Start
        (Periodic_Task1_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Periodic_Task1_UI.Stats_HBox.Pack_Start
        (Periodic_Task1_UI.Stats_Values_Label, Expand => False);

      Periodic_Task1_UI.Stats_Frame.Add (Periodic_Task1_UI.Stats_HBox);

      Periodic_Task1_UI.Stats_VBox.Pack_Start
        (Periodic_Task1_UI.Stats_Frame, Expand => False, Padding => 10);

      Periodic_Task1_UI.HBox.Pack_Start
        (Periodic_Task1_UI.Stats_VBox, Expand => False, Padding => 10);

      Periodic_Task1_UI.VBox.Pack_Start
        (Periodic_Task1_UI.HBox, Expand => False);

      Periodic_Task1_UI.Frame.Add (Periodic_Task1_UI.VBox);

   end Create_Periodic_Task1_UI;

   procedure Create_Fieldbus1_Task_UI (GUI_Element : in out Instance) is
      Fieldbus1_Task_UI : constant access Fieldbus_Task_UI_Type :=
        GUI_Element.Fieldbus1_UI'Access;
   begin
      Gtk_New (Fieldbus1_Task_UI.Frame, "Fieldbus1 Task");
      Gtk_Label (Fieldbus1_Task_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Fieldbus1 Task</b>");
      Fieldbus1_Task_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Fieldbus1_Task_UI.VBox);

      Gtk_New_Hbox (Fieldbus1_Task_UI.Config_HBox);

      Gtk_New (Fieldbus1_Task_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF
               & "Period : xxxx ms");
      Fieldbus1_Task_UI.Config_Label.Set_Use_Markup (True);

      Fieldbus1_Task_UI.Config_Label.Set_Markup
        ("<big>Task Configuration</big>" & CRLF
         & "Period : "
         & Integer'Image (Fieldbus1_Task_Period_MS)
         & " ms");

      Fieldbus1_Task_UI.Config_HBox.Pack_Start
        (Fieldbus1_Task_UI.Config_Label,
         Expand => False, Padding => 10);

      Fieldbus1_Task_UI.VBox.Pack_Start
        (Fieldbus1_Task_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Fieldbus1_Task_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Fieldbus1_Task_UI.Watchdog_VBox);

      Gtk_New (Fieldbus1_Task_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Fieldbus1_Task_UI.Watchdog_Image, "gtk-yes",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus1_Task_UI.Watchdog_VBox.Pack_Start
        (Fieldbus1_Task_UI.Watchdog_Label, Expand => False, Padding => 10);

      Fieldbus1_Task_UI.Watchdog_VBox.Pack_Start
        (Fieldbus1_Task_UI.Watchdog_Image, Expand => False);

      Fieldbus1_Task_UI.HBox.Pack_Start
        (Fieldbus1_Task_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Fieldbus1_Task_UI.Running_VBox);

      Gtk_New (Fieldbus1_Task_UI.Running_Label, "Running");

      Gtk_New (Fieldbus1_Task_UI.Running_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus1_Task_UI.Running_VBox.Pack_Start
        (Fieldbus1_Task_UI.Running_Label, Expand => False, Padding => 10);

      Fieldbus1_Task_UI.Running_VBox.Pack_Start
        (Fieldbus1_Task_UI.Running_Image, Expand => False);

      Fieldbus1_Task_UI.HBox.Pack_Start
        (Fieldbus1_Task_UI.Running_VBox, Expand => False, Padding => 10);

      --  Communicating

      Gtk_New_Vbox (Fieldbus1_Task_UI.Communicating_VBox);

      Gtk_New (Fieldbus1_Task_UI.Communicating_Label, "Communicating");

      Gtk_New (Fieldbus1_Task_UI.Communicating_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus1_Task_UI.Communicating_VBox.Pack_Start
        (Fieldbus1_Task_UI.Communicating_Label,
         Expand => False, Padding => 10);

      Fieldbus1_Task_UI.Communicating_VBox.Pack_Start
        (Fieldbus1_Task_UI.Communicating_Image, Expand => False);

      Fieldbus1_Task_UI.HBox.Pack_Start
        (Fieldbus1_Task_UI.Communicating_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Fieldbus1_Task_UI.Stats_VBox);

      Gtk_New (Fieldbus1_Task_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Fieldbus1_Task_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Fieldbus1_Task_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Fieldbus1_Task_UI.Stats_HBox);

      Gtk_New (Fieldbus1_Task_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Fieldbus1_Task_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Fieldbus1_Task_UI.Stats_Values_Label,
               "Values");

      Fieldbus1_Task_UI.Stats_HBox.Pack_Start
        (Fieldbus1_Task_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Fieldbus1_Task_UI.Stats_HBox.Pack_Start
        (Fieldbus1_Task_UI.Stats_Values_Label, Expand => False);

      Fieldbus1_Task_UI.Stats_Frame.Add (Fieldbus1_Task_UI.Stats_HBox);

      Fieldbus1_Task_UI.Stats_VBox.Pack_Start
        (Fieldbus1_Task_UI.Stats_Frame, Expand => False, Padding => 10);

      Fieldbus1_Task_UI.HBox.Pack_Start
        (Fieldbus1_Task_UI.Stats_VBox, Expand => False, Padding => 10);

      Fieldbus1_Task_UI.VBox.Pack_Start
        (Fieldbus1_Task_UI.HBox, Expand => False);

      Fieldbus1_Task_UI.Frame.Add (Fieldbus1_Task_UI.VBox);

   end Create_Fieldbus1_Task_UI;

   procedure Create_Fieldbus2_Task_UI (GUI_Element : in out Instance) is
      Fieldbus2_Task_UI : constant access Fieldbus_Task_UI_Type :=
        GUI_Element.Fieldbus2_UI'Access;
   begin
      Gtk_New (Fieldbus2_Task_UI.Frame, "Fieldbus2 Task");
      Gtk_Label (Fieldbus2_Task_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Fieldbus2 Task</b>");
      Fieldbus2_Task_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Fieldbus2_Task_UI.VBox);

      Gtk_New_Hbox (Fieldbus2_Task_UI.Config_HBox);

      Gtk_New (Fieldbus2_Task_UI.Config_Label,
               "<big>Task Configuration</big>" & CRLF
               & "Period : xxxx ms");
      Fieldbus2_Task_UI.Config_Label.Set_Use_Markup (True);

      Fieldbus2_Task_UI.Config_Label.Set_Markup
        ("<big>Task Configuration</big>" & CRLF
         & "Period : "
         & Integer'Image (Fieldbus2_Task_Period_MS)
         & " ms");

      Fieldbus2_Task_UI.Config_HBox.Pack_Start
        (Fieldbus2_Task_UI.Config_Label,
         Expand => False, Padding => 10);

      Fieldbus2_Task_UI.VBox.Pack_Start
        (Fieldbus2_Task_UI.Config_HBox, Expand => False, Padding => 10);

      Gtk_New_Hbox (Fieldbus2_Task_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (Fieldbus2_Task_UI.Watchdog_VBox);

      Gtk_New (Fieldbus2_Task_UI.Watchdog_Label, "Watchdog");

      Gtk_New (Fieldbus2_Task_UI.Watchdog_Image, "gtk-yes",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus2_Task_UI.Watchdog_VBox.Pack_Start
        (Fieldbus2_Task_UI.Watchdog_Label, Expand => False, Padding => 10);

      Fieldbus2_Task_UI.Watchdog_VBox.Pack_Start
        (Fieldbus2_Task_UI.Watchdog_Image, Expand => False);

      Fieldbus2_Task_UI.HBox.Pack_Start
        (Fieldbus2_Task_UI.Watchdog_VBox, Expand => False, Padding => 10);

      --  Running

      Gtk_New_Vbox (Fieldbus2_Task_UI.Running_VBox);

      Gtk_New (Fieldbus2_Task_UI.Running_Label, "Running");

      Gtk_New (Fieldbus2_Task_UI.Running_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus2_Task_UI.Running_VBox.Pack_Start
        (Fieldbus2_Task_UI.Running_Label, Expand => False, Padding => 10);

      Fieldbus2_Task_UI.Running_VBox.Pack_Start
        (Fieldbus2_Task_UI.Running_Image, Expand => False);

      Fieldbus2_Task_UI.HBox.Pack_Start
        (Fieldbus2_Task_UI.Running_VBox, Expand => False, Padding => 10);

      --  Communicating

      Gtk_New_Vbox (Fieldbus2_Task_UI.Communicating_VBox);

      Gtk_New (Fieldbus2_Task_UI.Communicating_Label, "Communicating");

      Gtk_New (Fieldbus2_Task_UI.Communicating_Image, "gtk-no",
               Gtk.Enums.Icon_Size_Button);

      Fieldbus2_Task_UI.Communicating_VBox.Pack_Start
        (Fieldbus2_Task_UI.Communicating_Label,
         Expand => False, Padding => 10);

      Fieldbus2_Task_UI.Communicating_VBox.Pack_Start
        (Fieldbus2_Task_UI.Communicating_Image, Expand => False);

      Fieldbus2_Task_UI.HBox.Pack_Start
        (Fieldbus2_Task_UI.Communicating_VBox, Expand => False, Padding => 10);

      --  Stats

      Gtk_New_Vbox (Fieldbus2_Task_UI.Stats_VBox);

      Gtk_New (Fieldbus2_Task_UI.Stats_Frame, "Task duration (ms)");
      Gtk_Label (Fieldbus2_Task_UI.Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Task duration (ms)</u>" & CRLF);
      Fieldbus2_Task_UI.Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Fieldbus2_Task_UI.Stats_HBox);

      Gtk_New (Fieldbus2_Task_UI.Stats_Items_Label,
               "Min :" & CRLF
               & "Max :" & CRLF
               & "Avg :");
      Fieldbus2_Task_UI.Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (Fieldbus2_Task_UI.Stats_Values_Label,
               "Values");

      Fieldbus2_Task_UI.Stats_HBox.Pack_Start
        (Fieldbus2_Task_UI.Stats_Items_Label,
         Expand => False, Padding => 10);

      Fieldbus2_Task_UI.Stats_HBox.Pack_Start
        (Fieldbus2_Task_UI.Stats_Values_Label, Expand => False);

      Fieldbus2_Task_UI.Stats_Frame.Add (Fieldbus2_Task_UI.Stats_HBox);

      Fieldbus2_Task_UI.Stats_VBox.Pack_Start
        (Fieldbus2_Task_UI.Stats_Frame, Expand => False, Padding => 10);

      Fieldbus2_Task_UI.HBox.Pack_Start
        (Fieldbus2_Task_UI.Stats_VBox, Expand => False, Padding => 10);

      Fieldbus2_Task_UI.VBox.Pack_Start
        (Fieldbus2_Task_UI.HBox, Expand => False);

      Fieldbus2_Task_UI.Frame.Add (Fieldbus2_Task_UI.VBox);

   end Create_Fieldbus2_Task_UI;

   procedure Application_UI_Update
     (GUI_Element : in out Instance)
   is
      Application_UI : constant access Application_UI_Type :=
        GUI_Element.Application_UI'Access;

      Days         : Ada.Calendar.Arithmetic.Day_Count;
      Seconds      : Duration;
      Leap_Seconds : Ada.Calendar.Arithmetic.Leap_Seconds_Count;

      Up_Time_Hours   : Integer;
      Up_Time_Minutes : Integer;
      Up_Time_Seconds : Integer;

      Sub_Seconds : Duration;

   begin

      Ada.Calendar.Arithmetic.Difference
        (Left         => Ada.Calendar.Clock,
         Right        => Start_Time,
         Days         => Days,
         Seconds      => Seconds,
         Leap_Seconds => Leap_Seconds);

      Ada.Calendar.Formatting.Split
        (Seconds    => Seconds,
         Hour       => Up_Time_Hours,
         Minute     => Up_Time_Minutes,
         Second     => Up_Time_Seconds,
         Sub_Second => Sub_Seconds);

      Up_Time_Hours := Up_Time_Hours + Integer (Days) * 24;

      Application_UI.Up_Time_Label.Set_Markup
        ("<big>Up time:</big>" & CRLF
         & Up_Time_Hours'Img & "h, "
         & Up_Time_Minutes'Img & "m,"
         & Up_Time_Seconds'Img & "s");

   end Application_UI_Update;

   procedure Main_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean)
   is
      Main_Task_UI : constant access Main_Task_UI_Type :=
        GUI_Element.Main_UI'Access;
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Main_Task_UI_Update";
   begin

      if not Main_Watching then
         The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_Main_Task_Interface.Control.Start_Watching (True);
         Main_Watching := True;
      end if;

      The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => Main_Task_Watchdog_TON_Q);

      if Main_Task_Watchdog_TON_Q and not Main_Task_Watchdog_Error then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task Watchdog Time Out elapsed!");
         Main_Task_Watchdog_Error := True;
         Main_Task_UI.Watchdog_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
      end if;

      if not Main_Running and The_Main_Task_Interface.Status.Running then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task started!");
         Main_Task_UI.Running_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         Main_Running := True;
      end if;

      if Main_Running and not The_Main_Task_Interface.Status.Running then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task stopped!");
         Main_Task_UI.Running_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         Main_Running := False;
      end if;

      Main_Task_Status := The_Main_Task_Interface.Status.Get_Status;
      Main_Task_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (Main_Task_Status.Min_Duration * 1000)
         & CRLF
         & Duration'Image (Main_Task_Status.Max_Duration * 1000)
         & CRLF
         & Duration'Image (Main_Task_Status.Avg_Duration * 1000));

      Main_Task_UI.Sched_Stats_Values_Label.Set_Markup
        (Integer'Image (Main_Task_Status.Sched_Stats (1))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (2))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (3))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (4))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (5))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (6))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (7))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (8))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (9))
         & CRLF
         & Integer'Image (Main_Task_Status.Sched_Stats (10)));

   end Main_Task_UI_Update;

   procedure Periodic_Task1_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean)
   is
      Periodic_Task1_UI : constant access Periodic_Task1_UI_Type :=
        GUI_Element.Periodic1_UI'Access;
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Periodic_Task1_UI_Update";
   begin

      if not Period1_Watching then
         The_GP_Task1_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_GP_Task1_Interface.Control.Start_Watching (True);
         Period1_Watching := True;
      end if;

      The_GP_Task1_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_GP_Task1_Interface.Status_Watchdog.Value,
         Error           => Period1_Task_Watchdog_TON_Q);

      if Period1_Task_Watchdog_TON_Q and not Period1_Task_Watchdog_Error then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Periodic task 1 Watchdog Time Out elapsed!");
         Period1_Task_Watchdog_Error := True;
         Periodic_Task1_UI.Watchdog_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
      end if;

      if not Period1_Running and The_GP_Task1_Interface.Status.Running then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Periodic task 1 started!");
         Periodic_Task1_UI.Running_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         Period1_Running := True;
      end if;

      if Period1_Running and not The_GP_Task1_Interface.Status.Running then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Periodic task 1 stopped!");
         Periodic_Task1_UI.Running_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         Period1_Running := False;
      end if;

      Period1_Task_Status := The_GP_Task1_Interface.Status.Get_Status;
      Periodic_Task1_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (Period1_Task_Status.Min_Duration * 1000)
         & CRLF
         & Duration'Image (Period1_Task_Status.Max_Duration * 1000)
         & CRLF
         & Duration'Image (Period1_Task_Status.Avg_Duration * 1000));

   end Periodic_Task1_UI_Update;

   procedure Fieldbus1_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean)
   is
      Fieldbus1_Task_UI : constant access Fieldbus_Task_UI_Type :=
        GUI_Element.Fieldbus1_UI'Access;
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Fieldbus1_Task_UI_Update";
   begin

      if not FB1_Data.Main_Watching then
         Fieldbus1.The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         Fieldbus1.The_Main_Task_Interface.Control.Start_Watching (True);
         FB1_Data.Main_Watching := True;
      end if;

      Fieldbus1.The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog =>
           Fieldbus1.The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => FB1_Data.Main_Task_Watchdog_TON_Q);

      if FB1_Data.Main_Task_Watchdog_TON_Q
        and not FB1_Data.Main_Task_Watchdog_Error
      then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Fieldbus1 task Watchdog Time Out elapsed!");
         FB1_Data.Main_Task_Watchdog_Error := True;
         Fieldbus1_Task_UI.Watchdog_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
      end if;

      if not FB1_Data.Main_Running
        and Fieldbus1.The_Main_Task_Interface.Status.Running
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus1 task started!");
         Fieldbus1_Task_UI.Running_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         FB1_Data.Main_Running := True;
      end if;

      if FB1_Data.Main_Running
        and not Fieldbus1.The_Main_Task_Interface.Status.Running
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus1 task stopped!");
         Fieldbus1_Task_UI.Running_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         FB1_Data.Main_Running := False;
      end if;

      if not FB1_Data.Communicating
        and Fieldbus1.The_Main_Task_Interface.cifX_Board_Status.Communicating
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus1 communication started!");
         Fieldbus1_Task_UI.Communicating_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         FB1_Data.Communicating := True;
      end if;

      if FB1_Data.Communicating and
          not Fieldbus1.The_Main_Task_Interface.cifX_Board_Status.Communicating
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus1 communication stopped!");
         Fieldbus1_Task_UI.Communicating_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         FB1_Data.Communicating := False;
      end if;

      FB1_Data.Main_Task_Status :=
        Fieldbus1.The_Main_Task_Interface.Status.Get_Status;
      Fieldbus1_Task_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (FB1_Data.Main_Task_Status.Min_Duration * 1000)
         & CRLF
         & Duration'Image (FB1_Data.Main_Task_Status.Max_Duration * 1000)
         & CRLF
         & Duration'Image (FB1_Data.Main_Task_Status.Avg_Duration * 1000));

   end Fieldbus1_Task_UI_Update;

   procedure Fieldbus2_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean)
   is
      Fieldbus2_Task_UI : constant access Fieldbus_Task_UI_Type :=
        GUI_Element.Fieldbus2_UI'Access;
      My_Ident : constant String :=
        "A4A.GUI_Elements.General_Status_Page.Fieldbus2_Task_UI_Update";
   begin

      if not FB2_Data.Main_Watching then
         Fieldbus2.The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         Fieldbus2.The_Main_Task_Interface.Control.Start_Watching (True);
         FB2_Data.Main_Watching := True;
      end if;

      Fieldbus2.The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog =>
           Fieldbus2.The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => FB2_Data.Main_Task_Watchdog_TON_Q);

      if FB2_Data.Main_Task_Watchdog_TON_Q
        and not FB2_Data.Main_Task_Watchdog_Error
      then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Fieldbus2 task Watchdog Time Out elapsed!");
         FB2_Data.Main_Task_Watchdog_Error := True;
         Fieldbus2_Task_UI.Watchdog_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
      end if;

      if not FB2_Data.Main_Running
        and Fieldbus2.The_Main_Task_Interface.Status.Running
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus2 task started!");
         Fieldbus2_Task_UI.Running_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         FB2_Data.Main_Running := True;
      end if;

      if FB2_Data.Main_Running
        and not Fieldbus2.The_Main_Task_Interface.Status.Running
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus2 task stopped!");
         Fieldbus2_Task_UI.Running_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         FB2_Data.Main_Running := False;
      end if;

      if not FB2_Data.Communicating
        and Fieldbus2.The_Main_Task_Interface.cifX_Board_Status.Communicating
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus2 communication started!");
         Fieldbus2_Task_UI.Communicating_Image.Set
           ("gtk-yes", Gtk.Enums.Icon_Size_Button);
         FB2_Data.Communicating := True;
      end if;

      if FB2_Data.Communicating and
          not Fieldbus2.The_Main_Task_Interface.cifX_Board_Status.Communicating
      then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Fieldbus2 communication stopped!");
         Fieldbus2_Task_UI.Communicating_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         FB2_Data.Communicating := False;
      end if;

      FB2_Data.Main_Task_Status :=
        Fieldbus2.The_Main_Task_Interface.Status.Get_Status;
      Fieldbus2_Task_UI.Stats_Values_Label.Set_Markup
        (Duration'Image (FB2_Data.Main_Task_Status.Min_Duration * 1000)
         & CRLF
         & Duration'Image (FB2_Data.Main_Task_Status.Max_Duration * 1000)
         & CRLF
         & Duration'Image (FB2_Data.Main_Task_Status.Avg_Duration * 1000));

   end Fieldbus2_Task_UI_Update;

   overriding function Create
     return Instance
   is

   begin

      return Create (Ada.Calendar.Clock);

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance;
      Terminating : in Boolean)
   is

   begin

      GUI_Element.Application_UI_Update;
      GUI_Element.Main_Task_UI_Update (Terminating);
      GUI_Element.Periodic_Task1_UI_Update (Terminating);
      GUI_Element.Fieldbus1_Task_UI_Update (Terminating);
      GUI_Element.Fieldbus2_Task_UI_Update (Terminating);

   end Update;

   function Create
     (Start_Time : Ada.Calendar.Time) return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "General Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.Create_Application_UI (Start_Time);

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Application_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Create_Main_Task_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Main_UI.Frame,
         Expand => False);

      GUI_Element.Create_Periodic_Task1_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Periodic1_UI.Frame,
         Expand => False);

      GUI_Element.Create_Fieldbus1_Task_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Fieldbus1_UI.Frame,
         Expand => False);

      GUI_Element.Create_Fieldbus2_Task_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Fieldbus2_UI.Frame,
         Expand => False);

      GUI_Element.Scrolled_Window.Add_With_Viewport
        (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

end A4A.GUI_Elements.General_Status_Page;
