
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Memory.MBTCP_IOScan;
use A4A.Memory;

with A4A.Dual_Port_Memory;

with A4A.MBTCP_Client; use A4A.MBTCP_Client;

with A4A.Task_Interfaces; use A4A.Task_Interfaces;
with A4A.Task_Watchdog; use A4A.Task_Watchdog;

with A4A.Application;

with A4A.Application.MBTCP_Server_Config;
use  A4A.Application.MBTCP_Server_Config;
with A4A.Application.MBTCP_Clients_Config;
use A4A.Application.MBTCP_Clients_Config;
with A4A.Library.Timers; use A4A.Library.Timers;

package A4A.Kernel.Main is

   Watchdog_Time_Out_MS : constant Natural := 2000;

   procedure Create_Main_Task;

   type Main_Task_Status_Type is
      record
         Ready        : Boolean  := False;
         Terminated   : Boolean  := False;
         Running      : Boolean  := False;

         Min_Duration : Duration := 0.0;
         Max_Duration : Duration := 0.0;
         Avg_Duration : Duration := 0.0;
         Sched_Stats  : Sched_Stats_Array := (others => 0);
      end record;

   protected type Main_Task_Status is

      procedure Ready (Value : in Boolean);
      function Ready return Boolean;

      procedure Terminated (Value : in Boolean);
      function Terminated return Boolean;

      procedure Running (Value : in Boolean);
      function Running return Boolean;

      procedure Min_Duration (Value : in Duration);
      procedure Max_Duration (Value : in Duration);
      procedure Avg_Duration (Value : in Duration);
      procedure Sched_Stats  (Value : in Sched_Stats_Array);

      function Get_Status return Main_Task_Status_Type;

   private

      Status : Main_Task_Status_Type;

   end Main_Task_Status;

   type MBTCP_Server_Status_Type is
      record
         WD_Error : Boolean  := False;
         --  Watchdog error
      end record;

   protected type MBTCP_Server_Status is

      procedure WD_Error (Value : in Boolean);

      function WD_Error return Boolean;

   private
      Status : MBTCP_Server_Status_Type;

   end MBTCP_Server_Status;

   type MBTCP_Client_Status_Type is
      record
         WD_Error : Boolean  := False;
         --  Watchdog error
      end record;

   protected type MBTCP_Client_Status is

      procedure WD_Error (Value : in Boolean);

      function WD_Error return Boolean;

   private
      Status : MBTCP_Client_Status_Type;

   end MBTCP_Client_Status;

   type MBTCP_Clients_Status_Type is array (MBTCP_Clients_Configuration'Range)
     of MBTCP_Client_Status;

   type Main_Task_Interface is
      record
         Control : Task_Control;
         Status  : Main_Task_Status;

         Control_Watchdog  : Control_Watchdog_Type;
         Status_Watchdog   : Status_Watchdog_Type;

         MServer_Status    : MBTCP_Server_Status;
         MClients_Status   : MBTCP_Clients_Status_Type;
      end record;

   type Main_Task_Itf_Access is access all Main_Task_Interface;

   task type Main_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Main_Task_Itf_Access
     ) is
      pragma Priority (Task_Priority);
   end Main_Task;
   type Main_Task_Access is access Main_Task;

   --------------------------------------------------------------------
   --  Main Task Management
   --------------------------------------------------------------------

   The_Main_Task_Interface     : aliased Main_Task_Interface;

   --------------------------------------------------------------------
   --  Clock Management
   --------------------------------------------------------------------

   Clock_Handler_Interface : aliased A4A.Library.Timers.Task_Interface;

   --------------------------------------------------------------------
   --  Modbus TCP Server Management
   --------------------------------------------------------------------

   MBTCP_Server_Task_Interface : aliased Server.Task_Interface;

   --------------------------------------------------------------------
   --  Modbus TCP Clients Management
   --------------------------------------------------------------------

   My_Bool_DPM : aliased A4A.Dual_Port_Memory.Bool_Dual_Port_Memory
     (First => A4A.Memory.MBTCP_IOScan.Bool_IO_Range'First,
      Last  => A4A.Memory.MBTCP_IOScan.Bool_IO_Range'Last);

   My_Word_DPM : aliased A4A.Dual_Port_Memory.Word_Dual_Port_Memory
     (First => A4A.Memory.MBTCP_IOScan.Word_IO_Range'First,
      Last  => A4A.Memory.MBTCP_IOScan.Word_IO_Range'Last);

   MBTCP_Clients_Tasks_Itf : array (MBTCP_Clients_Configuration'Range)
     of aliased A4A.MBTCP_Client.Task_Itf_Access;

private

   The_Main_Task               : Main_Task_Access;
   Main_Task_Created           : Boolean := False;

   MBTCP_Server_Task           : Server.Periodic_Task_Access;

   MBTCP_Clients_Tasks         : Periodic_Task_Access_Array
     (MBTCP_Clients_Configuration'Range);

end A4A.Kernel.Main;
