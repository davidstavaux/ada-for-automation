
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;
with A4A.Protocols.HilscherX.netXTransport;

package body A4A.Protocols.HilscherX.Driver is

   My_Ident : constant String := "A4A.Protocols.HilscherX.Driver";

   function Driver_Init return DInt is

      Result : DInt;

   begin

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Initialising netXTransport...");

      Result := A4A.Protocols.HilscherX.netXTransport.Initialise;

      if Result = 0 then
         A4A.Protocols.HilscherX.netXTransport.Start;
      end if;

      return Result;

   end Driver_Init;

   procedure Driver_Deinit is
   begin
      A4A.Protocols.HilscherX.netXTransport.Quit;

      loop
         delay 1.0;
         exit when A4A.Protocols.HilscherX.netXTransport.is_Terminated;
      end loop;

   end Driver_Deinit;

end A4A.Protocols.HilscherX.Driver;
