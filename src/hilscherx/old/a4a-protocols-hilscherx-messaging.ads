
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Message_Queue;
with A4A.Protocols.HilscherX.Message_Pool;

package A4A.Protocols.HilscherX.Messaging is

   procedure Quit;
   procedure Initialise (Channel_Handle : in Channel_Handle_Type);

   package Pool_Package is new
     A4A.Protocols.HilscherX.Message_Pool (Pool_Size => 20);

   Message_Pool : Pool_Package.Message_Pool_Type;

   package Queue_Package is new
     A4A.Protocols.HilscherX.Message_Queue (Queue_Size => 10);

   Send_Queue : Queue_Package.Message_Queue_Type;
   Receive_Queue : Queue_Package.Message_Queue_Type;

   task Send_Msg;

   task Receive_Msg;

private

   Quit_Flag : Boolean := False;
   Init_Flag : Boolean := False;

   The_Channel_Handle : Channel_Handle_Type;

end A4A.Protocols.HilscherX.Messaging;
