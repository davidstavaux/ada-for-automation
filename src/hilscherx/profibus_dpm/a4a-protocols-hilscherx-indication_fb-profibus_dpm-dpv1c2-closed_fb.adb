
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Log;

package body
  A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2.Closed_FB is

   overriding
   procedure Handle_Indication
     (Function_Block : in out Instance;
      Indication     : in cifX_Packet_Access) is

      Ind   : constant PROFIBUS_FSPMM2_PACKET_CLOSED_IND_T_Access :=
        From_cifX_Packet (Indication);

   begin

      Function_Block.Ind_Data := Ind.Data;

      --  We use the same packet for the response
      --  So we don't need to return it to the pool nor get a new one

      A4A.Log.Logger.Put
        (Who  => My_Ident & ".Handle_Indication",
         What => "Indication received");

      Function_Block.Response := Indication;

      Function_Block.Indication_Got := True;

   end Handle_Indication;

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String is
      pragma Unreferenced (Function_Block);
   begin
      return My_Ident;
   end Get_Ident;

   procedure Answer
     (Function_Block : in out Instance) is

      Res   : constant PROFIBUS_FSPMM2_PACKET_CLOSED_RES_T_Access :=
        From_cifX_Packet (Function_Block.Response);

   begin

      --  We change only relevant fields
      Res.Head.Dest := 16#20#;
      Res.Head.Cmd  := PROFIBUS_FSPMM2_CMD_CLOSED_RES;

      --  The response is ready to be sent back
      Function_Block.Answer_Got := True;

   end Answer;

   function Get_Data
     (Function_Block : in Instance)
      return PROFIBUS_FSPMM2_CLOSED_IND_T is
   begin

      return Function_Block.Ind_Data;

   end Get_Data;

end A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2.Closed_FB;
