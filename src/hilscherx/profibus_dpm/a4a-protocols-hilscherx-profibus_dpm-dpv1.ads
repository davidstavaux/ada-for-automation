
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP Master data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Register_App.FB;
with A4A.Protocols.HilscherX.rcX_Public.rcX_Unregister_App.FB;
use A4A.Protocols.HilscherX.rcX_Public;

with A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2.Closed_FB;

package A4A.Protocols.HilscherX.Profibus_DPM.DPV1 is

   package DPV1C2_Ind_FB renames
     A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2;

   type Instance is tagged limited private;
   type Instance_Access is access all Instance;

   procedure Initialise
     (Self           : in out Instance;
      Channel_Access : in Channel_Messaging.Instance_Access;
      DPV1C2_Closed_Access : in DPV1C2_Ind_FB.Closed_FB.Instance_Access);

   procedure Quit
     (Self : in out Instance);

   function Is_Ready
     (Self : in Instance) return Boolean;

   function Is_Faulty
     (Self : in Instance) return Boolean;

   function Is_Terminated
     (Self : in Instance) return Boolean;

private

   Package_Ident : String := "A4A.Protocols.HilscherX.Profibus_DPM.DPV1";

   task type DPV1_Task_Type
     (Self : access Instance);

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Initialized

      X02,
      --  Registered / Ready

      X03,
      --  Unregistering

      X04,
      --  Unregistered

      X05,
      --  Faulty

      X06
      --  Terminated
     );

   type Instance is tagged limited
      record

         Quit_Flag : Boolean := False;
         Init_Flag : Boolean := False;

         My_Channel   : Channel_Messaging.Instance_Access;

         Status       : Block_Status := X00;

         Register_App_FB : rcX_Register_App.FB.Instance;

         Do_Register_App    : Boolean := False;
         Register_App_Done  : Boolean := False;
         Register_App_Error : Boolean := False;

         Unregister_App_FB : rcX_Unregister_App.FB.Instance;

         Do_Unregister_App    : Boolean := False;
         Unregister_App_Done  : Boolean := False;
         Unregister_App_Error : Boolean := False;

         DPV1C2_Closed_FB : DPV1C2_Ind_FB.Closed_FB.Instance_Access;

         Received_Packet : cifX_Packet_Access;

         Packet_Got   : Boolean := False;

         My_Receive_Queue : aliased Packet_Queue_Type
           (Queue_Size => Channel_Messaging.Default_Queue_Size);
         My_Receive_Queue_App : DWord := Channel_Messaging.A4A_App_0;
         My_Receive_Queue_ID  : DWord;

         Queue_ID_Got : Boolean := False;

         Error_Flag : Boolean;
         Last_Error : DWord;

         DPV1_Task    : DPV1_Task_Type (Instance'Access);

      end record;

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1;
