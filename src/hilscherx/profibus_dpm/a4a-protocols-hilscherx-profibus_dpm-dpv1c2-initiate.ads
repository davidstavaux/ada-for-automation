
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.rcX_Public;
use A4A.Protocols.HilscherX.rcX_Public;

package A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Initiate is

   --------------------------------------------------------------------
   --  from ProfibusFspmm2_Public.h
   --------------------------------------------------------------------

   --  #define PROFIBUS_FSPMM2_CMD_INITIATE_REQ                     0x00004404
   PROFIBUS_FSPMM2_CMD_INITIATE_REQ             : constant := 16#00004404#;

   --  #define PROFIBUS_FSPMM2_CMD_INITIATE_CNF                     0x00004405
   PROFIBUS_FSPMM2_CMD_INITIATE_CNF             : constant := 16#00004405#;

   --------------------------------------------------------------------
   --  Packet : PROFIBUS_FSPMM2_INITIATE_REQ / PROFIBUS_FSPMM2_INITIATE_CNF
   --  This function initiates a DPV1 Class 2 Connection
   --------------------------------------------------------------------

   --  /***** request packet *****/

   --  #define PROFIBUS_FSPMM2_ADD_ADDR_TABLE_LEN         228
   PROFIBUS_FSPMM2_ADD_ADDR_TABLE_LEN           : constant := 228;

   type PROFIBUS_FSPMM2_ADD_ADDR_PARAM is
      record
         S_Type       : Byte;

         S_Len        : Byte;

         D_Type       : Byte;

         D_Len        : Byte;

         AddParam : Byte_Array (1 .. PROFIBUS_FSPMM2_ADD_ADDR_TABLE_LEN);

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_ADD_ADDR_PARAM);

   type PROFIBUS_FSPMM2_INITIATE_REQ_T is
      record
         Remote_Address       : DWord;
         --  Remote Address

         Send_Timeout         : Word;
         --  Send timeout value for periodic supervision of connection to be
         --  established

         Features_Supported1  : Byte;
         --  Bit field containing information about the service functionality

         Features_Supported2  : Byte;
         --  Bit field containing additional information about the service
         --  functionality

         Profile_Features_Supported1  : Byte;
         --  Bit field containing information about the profile

         Profile_Features_Supported2  : Byte;
         --  Bit field containing additional information about the profile
         --  features

         Profile_Ident_Number         : Word;
         --  Identification number for profile
         --  (A value of 0 indicates no profile is currently used.)

         AddAddrParam : PROFIBUS_FSPMM2_ADD_ADDR_PARAM;
         --  Additional Address Parameter Table

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_INITIATE_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_INITIATE_REQ_T;
         --  packet data
      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T_Access);

   --  /***** confirmation packet *****/

   type PROFIBUS_FSPMM2_INITIATE_CNF_POS_T is
      record
         Remote_Address       : DWord;
         --  Remote Address

         Com_Ref              : DWord;
         --  Communication Reference for uniquely identifying the connection

         Max_Len_Data_Unit    : Byte;
         --  Maximum length of data unit to transfer

         Features_Supported1  : Byte;
         --  Bit field containing information about the service functionality
         --  actually supported by the DP V1 slave

         Features_Supported2  : Byte;
         --  Bit field containing additional information about the service
         --  functionality actually supported by the DP V1 slave

         Profile_Features_Supported1  : Byte;
         --  Bit field containing information about the profile actually
         --  supported by the DP V1 slave

         Profile_Features_Supported2  : Byte;
         --  Bit field containing additional information about the
         --  profile features actually supported by the DP V1 slave

         Profile_Ident_Number         : Word;
         --  Identification number for the actually chosen profile

         AddAddrParam : PROFIBUS_FSPMM2_ADD_ADDR_PARAM;
         --  Additional Address Parameter Table

      end record;
   --  Positive confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_INITIATE_CNF_POS_T);

   type PROFIBUS_FSPMM2_INITIATE_CNF_NEG_T is
      record
         Remote_Address       : DWord;
         --  Remote Address

         Error_Decode         : Byte;
         --  A value of 128 here indicates DP V1 error handling is applied.

         Error_Code1          : Byte;
         --  ErrorCode1

         Error_Code2          : Byte;
         --  ErrorCode2, meaning depends on bErrorCode1

         Reserved_byte        : Byte;
         --  Reserved

         Detail               : Word;
         --  Additional detailed error information

         Reserved_word        : Word;
         --  Reserved

      end record;
   --  Negative confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_INITIATE_CNF_NEG_T);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_INITIATE_CNF_POS_T;
         --  packet data

      end record;
   --  Positive confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T_Access);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_INITIATE_CNF_NEG_T;
         --  packet data

      end record;
   --  Negative confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T);

   type PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T_Access);

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Initiate;
