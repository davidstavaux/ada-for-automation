
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.rcX_Public;
use A4A.Protocols.HilscherX.rcX_Public;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C1;
use A4A.Protocols.HilscherX.Profibus_DPM.DPV1C1;

package A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Read is

   --------------------------------------------------------------------
   --  from ProfibusFspmm2_Public.h
   --------------------------------------------------------------------

   --  #define PROFIBUS_FSPMM2_CMD_READ_REQ                     0x00004406
   PROFIBUS_FSPMM2_CMD_READ_REQ             : constant := 16#00004406#;

   --  #define PROFIBUS_FSPMM2_CMD_READ_CNF                     0x00004407
   PROFIBUS_FSPMM2_CMD_READ_CNF             : constant := 16#00004407#;

   --------------------------------------------------------------------
   --  Packet : PROFIBUS_FSPMM2_READ_REQ / PROFIBUS_FSPMM2_READ_CNF
   --  This packet transfers a read request for a data block from the device
   --  acting as a PROFIBUS DP V1 Master Class 2 to a DP V1-Slave.
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type PROFIBUS_FSPMM2_READ_REQ_T is
      record
         Com_Ref              : DWord;
         --  Communication Reference

         Slot                 : DWord;
         --  Slot

         Index                : DWord;
         --  Instance number (4 bits)

         Length               : DWord;
         --  Number of bytes of the data block that has to be read.

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_READ_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_READ_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_READ_REQ_T;
         --  packet data
      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_READ_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_READ_REQ_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_READ_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_READ_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_READ_REQ_T_Access);

   --  /***** confirmation packet *****/

   type PROFIBUS_FSPMM2_READ_CNF_POS_T is
      record
         Com_Ref              : DWord;
         --  Communication Reference

         Slot                 : DWord;
         --  Slot

         Index                : DWord;
         --  Index

         Length               : DWord;
         --  Number of bytes of the data block that has to be read.

         Data                 :
         Byte_Array (1 .. PROFIBUS_FSPM_MAX_IO_DATA_LEN);
         --  Number of bytes of the data block that has to be read.

      end record;
   --  Positive confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_READ_CNF_POS_T);

   type PROFIBUS_FSPMM2_READ_CNF_NEG_T is
      record
         Com_Ref              : DWord;
         --  Communication Reference

         Slot                 : DWord;
         --  Slot

         Index                : DWord;
         --  Index

         Length               : DWord;
         --  Number of bytes of the data block that has to be read.

         Error_Decode         : Byte;
         --  A value of 128 here indicates DP V1 error handling is applied.

         Error_Code1          : Byte;
         --  ErrorCode1

         Error_Code2          : Byte;
         --  ErrorCode2, meaning depends on bErrorCode1

         Reserved_byte        : Byte;
         --  Reserved

      end record;
   --  Negative confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_READ_CNF_NEG_T);

   type PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_READ_CNF_POS_T;
         --  packet data

      end record;
   --  Positive confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T);

   type PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_READ_CNF_POS_T_Access);

   type PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_READ_CNF_NEG_T;
         --  packet data

      end record;
   --  Negative confirmation
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T);

   type PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_READ_CNF_NEG_T_Access);

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Read;
