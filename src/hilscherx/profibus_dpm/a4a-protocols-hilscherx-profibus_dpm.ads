
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP Master data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.Profibus_DPM is

   --------------------------------------------------------------------
   --  from ProfibusApm_Public.h
   --------------------------------------------------------------------

   type Global_Bits_Type is
      record
         Control_Error               : Boolean;
         --  CONTROL-ERROR :
         --  This error is caused by incorrect parameterization.

         Auto_Clear_Error            : Boolean;
         --  AUTO-CLEAR-ERROR :
         --  The device stopped the communication to all slaves
         --  and reached the auto-clear end state.

         Non_Exchange_Error          : Boolean;
         --  NON-EXCHANGE-ERROR :
         --  At least one slave has not reached the data exchange
         --  state and no process data are exchanged with it.

         Fatal_Error                 : Boolean;
         --  FATAL-ERROR :
         --  Because of a severe bus error, no bus communication
         --  is possible any more.

         Host_Not_Ready_Notification : Boolean;
         --  Host-NOT-READY-NOTIFICATION :
         --  Indicates if the host program has set its state to
         --  operative or not. If the bit is set the host
         --  program is not ready to communicate.

         Timeout_Error               : Boolean;
         --  TIMEOUT-ERROR :
         --  The Device has detected an overstepped timeout
         --  supervision time because of rejected PROFIBUS
         --  telegrams. It's an indication for bus short circuits while
         --  the master interrupts the communication. The number
         --  of detected timeouts are fixed in the Time_out_cnt
         --  variable. The bit will be set when the first timeout
         --  was detected and will not be deleted any
         --  more.

      end record;
   --  Global Bits

   for Global_Bits_Type use
      record
         Control_Error                at 0 range 0 .. 0;
         Auto_Clear_Error             at 0 range 1 .. 1;
         Non_Exchange_Error           at 0 range 2 .. 2;
         Fatal_Error                  at 0 range 3 .. 3;
         Host_Not_Ready_Notification  at 0 range 4 .. 4;
         Timeout_Error                at 0 range 5 .. 5;
      end record;

   for Global_Bits_Type'Size use Byte'Size;

   type DPM_State_Type is
     (OFFLINE,
      STOP,
      CLEAR,
      OPERATE);
   --  Master main state

   for DPM_State_Type use
     (OFFLINE   => 16#00#,
      STOP      => 16#40#,
      CLEAR     => 16#80#,
      OPERATE   => 16#C0#);

   for DPM_State_Type'Size use Byte'Size;

   subtype abReserved_8 is Byte_Array (0 .. 7);

   subtype abSlaves_Type is Byte_Array (0 .. 15);

   type Global_State_Field_Type is
      record
         Global_Bits          : Global_Bits_Type;
         --  Global Bits

         DPM_State            : DPM_State_Type;
         --  Master main state

         Error_Remote_Address : Byte;
         --  bErr_Rem_Adr / Location of error (not used)

         Error_Event          : Byte;
         --  bErr_Event / Error code (not used)

         Bus_Error_Count      : Word;
         --  usBus_Error_Cnt / Counter for the bus error events
         --  This variable is a counter for severe bus errors,
         --  for example short circuits on the bus.

         Timeout_Count        : Word;
         --  usTime_Out_Cnt / Counter for bus timeouts
         --  This variable is a counter for the number of rejected PROFIBUS
         --  telegrams because of heavy bus error.

         Reserved             : abReserved_8;
         --  abReserved[8] / Reserved area
         --  This data block is reserved.

         abSlaves_Config      : abSlaves_Type;
         --  abSl_cfg[16] / Slave configuration area
         --  This variable is a field of 16 bytes and contains the
         --  parameterization state of each slave station.

         abSlaves_State       : abSlaves_Type;
         --  abSl_state[16] / Slave state information area
         --  This variable is a field of 16 bytes and contains the state
         --  of each slave station.

         abSlaves_Diag        : abSlaves_Type;
         --  abSl_diag[16] / Slave diagnostic area
         --  This variable is a field of 16 bytes containing the diagnostic
         --  bit of each slave.

      end record;

   for Global_State_Field_Type'Size use 64 * Byte'Size;

   type Extended_Status_Block_Type is
      record
         Global_State_Field : Global_State_Field_Type;
      end record;

   for Extended_Status_Block_Type'Size use 64 * Byte'Size;
   pragma Convention (C, Extended_Status_Block_Type);

   procedure Channel_Extended_Status_Block
     (Channel_Handle        : in Channel_Handle_Type;
      Extended_Status_Block : out Extended_Status_Block_Type;
      Error                 : out DInt);
   --  int32_t APIENTRY xChannelExtendedStatusBlock
   --       ( CIFXHANDLE  hChannel, uint32_t ulCmd, uint32_t ulOffset,
   --        uint32_t ulDataLen, void* pvData);

   --------------------------------------------------------------------
   --  from ProfibusFspmm_Public.h
   --------------------------------------------------------------------

   type Station_Status_1_Type is
      record
         Station_Non_Existent       : Boolean;
         --  no response

         Station_Not_Ready          : Boolean;
         --  station not ready

         Cfg_Fault                  : Boolean;
         --  configuration fault

         Ext_Diag                   : Boolean;
         --  extended diagnostic

         Not_Supported              : Boolean;
         --  sync, freeze not supported

         Invalid_Response           : Boolean;
         --  response faulty

         Prm_Fault                  : Boolean;
         --  parameters faulty

         Master_Lock                : Boolean;
         --  locked by a master

      end record;
   --  Station Status 1

   for Station_Status_1_Type use
      record
         Station_Non_Existent         at 0 range 0 .. 0;
         Station_Not_Ready            at 0 range 1 .. 1;
         Cfg_Fault                    at 0 range 2 .. 2;
         Ext_Diag                     at 0 range 3 .. 3;
         Not_Supported                at 0 range 4 .. 4;
         Invalid_Response             at 0 range 5 .. 5;
         Prm_Fault                    at 0 range 6 .. 6;
         Master_Lock                  at 0 range 7 .. 7;
      end record;

   for Station_Status_1_Type'Size use Byte'Size;

   type Station_Status_2_Type is
      record
         Prm_Req                    : Boolean;
         --  request new parameters

         Stat_Diag                  : Boolean;
         --  static diagnostic

         bTrue                      : Boolean;
         --  set to 1 by a slave

         Wd_On                      : Boolean;
         --  watchdog function on/off

         Freeze_Mode                : Boolean;
         --  freeze mode active

         Sync_Mode                  : Boolean;
         --  sync mode active

         Reserved                   : Boolean;
         --  reserved

         Deactivated                : Boolean;
         --  slave deactivated

      end record;
   --  Station Status 2

   for Station_Status_2_Type use
      record
         Prm_Req                      at 0 range 0 .. 0;
         Stat_Diag                    at 0 range 1 .. 1;
         bTrue                        at 0 range 2 .. 2;
         Wd_On                        at 0 range 3 .. 3;
         Freeze_Mode                  at 0 range 4 .. 4;
         Sync_Mode                    at 0 range 5 .. 5;
         Reserved                     at 0 range 6 .. 6;
         Deactivated                  at 0 range 7 .. 7;
      end record;

   for Station_Status_2_Type'Size use Byte'Size;

   type Station_Status_3_Type is
      record
         Reserved_1                 : Boolean;
         --  reserved

         Reserved_2                 : Boolean;
         --  reserved

         Reserved_3                 : Boolean;
         --  reserved

         Reserved_4                 : Boolean;
         --  reserved

         Reserved_5                 : Boolean;
         --  reserved

         Reserved_6                 : Boolean;
         --  reserved

         Reserved_7                 : Boolean;
         --  reserved

         Ext_Diag_Overflow          : Boolean;
         --  ext. diagnostic overflow

      end record;
   --  Station Status 3

   for Station_Status_3_Type use
      record
         Reserved_1                   at 0 range 0 .. 0;
         Reserved_2                   at 0 range 1 .. 1;
         Reserved_3                   at 0 range 2 .. 2;
         Reserved_4                   at 0 range 3 .. 3;
         Reserved_5                   at 0 range 4 .. 4;
         Reserved_6                   at 0 range 5 .. 5;
         Reserved_7                   at 0 range 6 .. 6;
         Ext_Diag_Overflow            at 0 range 7 .. 7;
      end record;

   for Station_Status_3_Type'Size use Byte'Size;

   Diag_Bytes_Num : constant := 6;

   type PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T is
      record
         Station_Status_1          : Station_Status_1_Type;
         --  Station Status 1

         Station_Status_2          : Station_Status_2_Type;
         --  Station Status 2

         Station_Status_3          : Station_Status_3_Type;
         --  Station Status 3

         Master_Add                : Byte;
         --  corresponding master address

         Ident_Number              : Word;
         --  ident number, motorola format

      end record;
   for PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T'Size use Diag_Bytes_Num * Byte'Size;
   pragma Convention (C, PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T);

   type PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T_Access is
     access all PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T;

   type PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T is
      record
         Bytes          : Byte_Array (1 .. Diag_Bytes_Num);
         --  Station Status bytes

      end record;
   for PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T'Size
   use Diag_Bytes_Num * Byte'Size;
   pragma Convention (C, PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T);

   type PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T_Access is
     access all PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T;

   function From_Byte_Array is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM_DIAGNOSTIC_DATA_BYTES_T_Access,
      Target => PROFIBUS_FSPMM_DIAGNOSTIC_DATA_T_Access);

end A4A.Protocols.HilscherX.Profibus_DPM;
