
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body
  A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Initiate_FB is

   overriding
   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access) is

      Req  : constant PROFIBUS_FSPMM2_PACKET_INITIATE_REQ_T_Access :=
        From_cifX_Packet (Request);

   begin

      Req.Head.Len :=
        ((PROFIBUS_FSPMM2_INITIATE_REQ_T'Size
         - PROFIBUS_FSPMM2_ADD_ADDR_PARAM'Size) / 8)
        + 4 + 4;

      Req.Head.Cmd := PROFIBUS_FSPMM2_CMD_INITIATE_REQ;

      Req.Data.Remote_Address := Function_Block.Remote_Address;

      Req.Data.Send_Timeout := 100;

      Req.Data.Features_Supported1 := 1;
      Req.Data.Features_Supported2 := 0;
      Req.Data.Profile_Features_Supported1 := 0;
      Req.Data.Profile_Features_Supported2 := 0;

      Req.Data.Profile_Ident_Number := 0;

      Req.Data.AddAddrParam.S_Type := 0;
      Req.Data.AddAddrParam.S_Len  := 2;
      Req.Data.AddAddrParam.D_Type := 0;
      Req.Data.AddAddrParam.D_Len  := 2;

      Req.Data.AddAddrParam.AddParam (1) := 0;
      Req.Data.AddAddrParam.AddParam (2) := 0;
      Req.Data.AddAddrParam.AddParam (3) := 0;
      Req.Data.AddAddrParam.AddParam (4) := 0;

   end Fill_Request;

   overriding
   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord) is

      Cnf_Pos : PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_POS_T_Access;
      Cnf_Neg : PROFIBUS_FSPMM2_PACKET_INITIATE_CNF_NEG_T_Access;

   begin

      if Confirmation.Header.Cmd /= PROFIBUS_FSPMM2_CMD_INITIATE_CNF then

         Error := True;

         Type_Of_Error := Error_Cmd;

      elsif Confirmation.Header.State /= 0 then

         Error := True;

         Type_Of_Error := Error_Status;

         Last_Error := Confirmation.Header.State;

         Cnf_Neg := From_cifX_Packet (Confirmation);

         Function_Block.Cnf_Neg_Data := Cnf_Neg.Data;

      else

         Error := False;

         Type_Of_Error := Error_None;

         Cnf_Pos := From_cifX_Packet (Confirmation);

         Function_Block.Cnf_Pos_Data := Cnf_Pos.Data;

      end if;

   end Store_Result;

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String is
      pragma Unreferenced (Function_Block);
   begin
      return My_Ident;
   end Get_Ident;

   procedure Set_Parameters
     (Function_Block : in out Instance;
      Remote_Address : in DWord) is
   begin

      Function_Block.Remote_Address := Remote_Address;

   end Set_Parameters;

   function Get_Data_Pos
     (Function_Block : in Instance)
      return PROFIBUS_FSPMM2_INITIATE_CNF_POS_T is
   begin

      return Function_Block.Cnf_Pos_Data;

   end Get_Data_Pos;

   function Get_Data_Neg
     (Function_Block : in Instance)
      return PROFIBUS_FSPMM2_INITIATE_CNF_NEG_T is
   begin

      return Function_Block.Cnf_Neg_Data;

   end Get_Data_Neg;

end A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Initiate_FB;
