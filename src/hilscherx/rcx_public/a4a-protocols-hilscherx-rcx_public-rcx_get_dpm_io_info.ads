
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Get_DPM_IO_Info is

   --------------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

   --  #define RCX_GET_DPM_IO_INFO_REQ                       0x00002F0C
   RCX_GET_DPM_IO_INFO_REQ             : constant := 16#00002F0C#;

   --  #define RCX_GET_DPM_IO_INFO_CNF                       0x00002F0D
   RCX_GET_DPM_IO_INFO_CNF             : constant := 16#00002F0D#;

   --------------------------------------------------------------------
   --  Packet : RCX_GET_DPM_IO_INFO_REQ / RCX_GET_DPM_IO_INFO_CNF
   --  This packet is used to obtain offset and length of the used I/O data
   --  space of all process data areas for the requested channel.
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type RCX_GET_DPM_IO_INFO_REQ_T is new TLR_EMPTY_PACKET_T;
   pragma Convention (C, RCX_GET_DPM_IO_INFO_REQ_T);

   type RCX_GET_DPM_IO_INFO_REQ_T_Access is
     access all RCX_GET_DPM_IO_INFO_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_GET_DPM_IO_INFO_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_GET_DPM_IO_INFO_REQ_T_Access);

   --  /***** confirmation packet *****/

   type RCX_DPM_IO_BLOCK_INFO_T is
      record
         Sub_Block_Index    : DWord;
         --  number of sub block

         Type_Of_Sub_Block  : DWord;
         --  type of sub block

         Flags              : Word;
         --  flags of the sub block

         Reserved           : Word;
         --  reserved

         Offset             : DWord;
         --  start offset of the IO data

         Length             : DWord;
         --  length of used IO data

      end record;
   pragma Convention (C, RCX_DPM_IO_BLOCK_INFO_T);

   type RCX_DPM_IO_BLOCK_INFO_T_Array is array (Natural range <>) of
     RCX_DPM_IO_BLOCK_INFO_T;
   pragma Convention (C, RCX_DPM_IO_BLOCK_INFO_T_Array);

   type RCX_GET_DPM_IO_INFO_CNF_DATA_T is
      record
         Num_IO_Block_Info  : DWord;
         --  Number of IO Block Info

         IO_Block_Info : RCX_DPM_IO_BLOCK_INFO_T_Array (0 .. 1);
         --  Array of I/O Block information
      end record;
   pragma Convention (C, RCX_GET_DPM_IO_INFO_CNF_DATA_T);

   type RCX_GET_DPM_IO_INFO_CNF_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_GET_DPM_IO_INFO_CNF_DATA_T;
         --  packet data

      end record;
   pragma Convention (C, RCX_GET_DPM_IO_INFO_CNF_T);

   type RCX_GET_DPM_IO_INFO_CNF_T_Access is
     access all RCX_GET_DPM_IO_INFO_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_GET_DPM_IO_INFO_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_GET_DPM_IO_INFO_CNF_T_Access);

end A4A.Protocols.HilscherX.rcX_Public.rcX_Get_DPM_IO_Info;
