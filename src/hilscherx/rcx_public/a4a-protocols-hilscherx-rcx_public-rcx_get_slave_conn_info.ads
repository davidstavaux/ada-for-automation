
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Conn_Info is

   --------------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

   --  #define RCX_GET_SLAVE_CONN_INFO_REQ                   0x00002F0A
   RCX_GET_SLAVE_CONN_INFO_REQ             : constant := 16#00002F0A#;

   --  #define RCX_GET_SLAVE_CONN_INFO_CNF                   0x00002F0B
   RCX_GET_SLAVE_CONN_INFO_CNF             : constant := 16#00002F0B#;

   --------------------------------------------------------------------
   --  Packet :
   --  RCX_PACKET_GET_SLAVE_CONN_INFO_REQ / RCX_PACKET_GET_SLAVE_CONN_INFO_CNF
   --  This packet allows retrieving detail information of a slave
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_DATA_T is
      record
         Handle       : DWord;
         --  Slave Handle

      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_DATA_T);

   type RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_DATA_T;
         --  packet data
      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T);

   type RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T_Access is
     access all RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T_Access);

   --  /***** confirmation packet *****/

   STATE_BYTES_MAX : constant := CIFX_MAX_DATA_SIZE - 8;

   type RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_DATA_T is
      record
         Handle       : DWord;
         --  Slave Handle

         Struct_ID    : DWord;
         --  Structure Identification Number

         State        : Byte_Array (0 .. STATE_BYTES_MAX - 1);
         --  Fieldbus Specific Slave Status Information
         --  (Refer to Fieldbus Documentation)

      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_DATA_T);

   type RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_DATA_T;
         --  packet data

      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T);

   type RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T_Access is
     access all RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T_Access);

end A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Conn_Info;
