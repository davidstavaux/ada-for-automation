
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Handle is

   --------------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

   --  #define RCX_GET_SLAVE_HANDLE_REQ                      0x00002F08
   RCX_GET_SLAVE_HANDLE_REQ             : constant := 16#00002F08#;

   --  #define RCX_GET_SLAVE_HANDLE_CNF                      0x00002F09
   RCX_GET_SLAVE_HANDLE_CNF             : constant := 16#00002F09#;

   --------------------------------------------------------------------
   --  Packet :
   --       RCX_PACKET_GET_SLAVE_HANDLE_REQ / RCX_PACKET_GET_SLAVE_HANDLE_CNF
   --  This packet allows retrieving diagnostic information of the connected
   --  devices
   --------------------------------------------------------------------

   --  /***** request packet *****/

   --  #define RCX_LIST_CONF_SLAVES                0x00000001
   RCX_LIST_CONF_SLAVES                 : constant := 16#00000001#;

   --  #define RCX_LIST_ACTV_SLAVES                0x00000002
   RCX_LIST_ACTV_SLAVES                 : constant := 16#00000002#;

   --  #define RCX_LIST_FAULTED_SLAVES             0x00000003
   RCX_LIST_FAULTED_SLAVES              : constant := 16#00000003#;

   type RCX_PACKET_GET_SLAVE_HANDLE_REQ_DATA_T is
      record
         Param       : DWord;
         --  requested list of slaves
      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_HANDLE_REQ_DATA_T);

   type RCX_PACKET_GET_SLAVE_HANDLE_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_PACKET_GET_SLAVE_HANDLE_REQ_DATA_T;
         --  packet data
      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_HANDLE_REQ_T);

   type RCX_PACKET_GET_SLAVE_HANDLE_REQ_T_Access is
     access all RCX_PACKET_GET_SLAVE_HANDLE_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_PACKET_GET_SLAVE_HANDLE_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_PACKET_GET_SLAVE_HANDLE_REQ_T_Access);

   --  /***** confirmation packet *****/

   HANDLES_MAX : constant := (CIFX_MAX_DATA_SIZE / 4) - 1;

   type Handles_Array is array (Natural range <>) of DWord;

   type RCX_PACKET_GET_SLAVE_HANDLE_CNF_DATA_T is
      record
         Param       : DWord;
         --  requested list of slaves

         Handles     : Handles_Array (0 .. HANDLES_MAX - 1);
         --  Slave Handles

      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_HANDLE_CNF_DATA_T);

   type RCX_PACKET_GET_SLAVE_HANDLE_CNF_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_PACKET_GET_SLAVE_HANDLE_CNF_DATA_T;
         --  packet data

      end record;
   pragma Convention (C, RCX_PACKET_GET_SLAVE_HANDLE_CNF_T);

   type RCX_PACKET_GET_SLAVE_HANDLE_CNF_T_Access is
     access all RCX_PACKET_GET_SLAVE_HANDLE_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_PACKET_GET_SLAVE_HANDLE_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_PACKET_GET_SLAVE_HANDLE_CNF_T_Access);

end A4A.Protocols.HilscherX.rcX_Public.rcX_Get_Slave_Handle;
