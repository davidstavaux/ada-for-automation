
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body
  A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_DPM_IO_Info_FB is

   overriding
   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access) is
      pragma Unreferenced (Function_Block);

      Req  : constant RCX_GET_DPM_IO_INFO_REQ_T_Access :=
        From_cifX_Packet (Request);

   begin

      Req.Head.Len := 0;

      Req.Head.Cmd := RCX_GET_DPM_IO_INFO_REQ;

   end Fill_Request;

   overriding
   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord) is

      Cnf  : RCX_GET_DPM_IO_INFO_CNF_T_Access;

   begin

      if Confirmation.Header.Cmd /= RCX_GET_DPM_IO_INFO_CNF then

         Error := True;

         Type_Of_Error := Error_Cmd;

      elsif Confirmation.Header.State /= 0 then

         Error := True;

         Type_Of_Error := Error_Status;

         Last_Error := Confirmation.Header.State;

      else

         Error := False;

         Type_Of_Error := Error_None;

         Cnf := From_cifX_Packet (Confirmation);

         Function_Block.IO_Info := Cnf.Data;

      end if;

   end Store_Result;

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String is
      pragma Unreferenced (Function_Block);
   begin
      return My_Ident;
   end Get_Ident;

   function Get_Data
     (Function_Block : in Instance) return RCX_GET_DPM_IO_INFO_CNF_DATA_T is
   begin

      return Function_Block.IO_Info;

   end Get_Data;

end A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_DPM_IO_Info_FB;
