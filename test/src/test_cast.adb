
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Unchecked_Conversion;

with Ada.Text_IO;

with A4A; use A4A;

--  with A4A.Protocols.HilscherX;

procedure Test_Cast is

   subtype My_Byte_Array is Byte_Array (0 .. 19);
   subtype My_Word_Array is Word_Array (0 .. 9);

   function To_Bytes is new Ada.Unchecked_Conversion
     (Source => My_Word_Array,
      Target => My_Byte_Array);

   function To_Words is new Ada.Unchecked_Conversion
     (Source => My_Byte_Array,
      Target => My_Word_Array);
   --     My_Word_Array := Word_Array(My_Byte_Array); -- Merche p� !
   --     My_Byte_Array := Byte_Array(My_Word_Array);

   My_Byte : Byte := 0;
   My_Word : Word := 0;
   My_SInt : SInt := 0;
   My_Int  : Int  := 0;

   My_Bytes : My_Byte_Array := (1, 2, 3, 4, others => 0);
   My_Words : My_Word_Array := (others => 0);

begin

   My_Byte := 16#FF#;
   Ada.Text_IO.Put ("My_Byte : ");
   Byte_Text_IO.Put (My_Byte, Base => 16);
   Ada.Text_IO.New_Line;
   Ada.Text_IO.Put ("My_Byte : ");
   Byte_Text_IO.Put (My_Byte, Base => 10);
   Ada.Text_IO.New_Line;
   Ada.Text_IO.Put ("My_Byte : ");
   Byte_Text_IO.Put (My_Byte, Base => 2);
   Ada.Text_IO.New_Line;

   My_Word := Word (My_Byte);
   Ada.Text_IO.Put ("My_Word : ");
   Word_Text_IO.Put (My_Word, Base => 16);
   Ada.Text_IO.New_Line;

   My_Byte := Byte (My_Word);
   Ada.Text_IO.Put ("My_Byte : ");
   Byte_Text_IO.Put (My_Byte, Base => 16);
   Ada.Text_IO.New_Line;

   My_Word := SHL (My_Word, 8);
   Ada.Text_IO.Put ("My_Word : ");
   Word_Text_IO.Put (My_Word, Base => 16);
   Ada.Text_IO.New_Line;

   --     My_Byte := Byte(My_Word); << Constraint error !

   My_SInt := 127;
   My_Int  := Int (My_SInt);
   My_SInt := SInt (My_Int);
   My_Byte := Byte (My_SInt);

   My_SInt := -128;
   My_Int  := Int (My_SInt);
   My_SInt := SInt (My_Int);
   --  My_Int  := My_Int - 1;
   --  My_SInt := SInt(My_Int); -- << Constraint error !

   --  My_Byte := Byte(My_SInt); -- << Constraint error !
   My_Byte := SInt_To_Byte (My_SInt);
   Ada.Text_IO.Put ("My_Byte : ");
   Byte_Text_IO.Put (My_Byte, Base => 16);
   Ada.Text_IO.New_Line;

   --  My_SInt := SInt(My_Byte); -- << Constraint error !

   for Index in My_Bytes'First .. 5 loop
      Ada.Text_IO.Put ("My_Bytes(" & Index'Img & ") : ");
      Byte_Text_IO.Put (My_Bytes (Index), Base => 16);
      Ada.Text_IO.New_Line;
   end loop;

   My_Words := To_Words (My_Bytes);

   for Index in My_Words'First .. 5 loop
      Ada.Text_IO.Put ("My_Words(" & Index'Img & ") : ");
      Word_Text_IO.Put (My_Words (Index), Base => 16);
      Ada.Text_IO.New_Line;
   end loop;

   for Index in My_Words'First .. 5 loop
      My_Words (Index) := ROL (My_Words (Index), 8);
   end loop;

   for Index in My_Words'First .. 5 loop
      Ada.Text_IO.Put ("My_Words(" & Index'Img & ") : ");
      Word_Text_IO.Put (My_Words (Index), Base => 16);
      Ada.Text_IO.New_Line;
   end loop;

   My_Bytes := To_Bytes (My_Words);

   for Index in My_Bytes'First .. 5 loop
      Ada.Text_IO.Put ("My_Bytes(" & Index'Img & ") : ");
      Byte_Text_IO.Put (My_Bytes (Index), Base => 16);
      Ada.Text_IO.New_Line;
   end loop;

end Test_Cast;
