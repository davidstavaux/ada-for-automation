
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with A4A.Generic_Dual_Port_Memory; use A4A;

procedure Test_Generic_DPM is

   type My_Inputs is
      record
         A : Boolean := False;
         B : Word    :=     0;
         C : Byte    :=     0;
      end record;

   type My_Outputs is
      record
         X : Boolean := False;
         Y : Word    :=     0;
         Z : Byte    :=     0;
      end record;

   package DPM is new
     A4A.Generic_Dual_Port_Memory
       (Inputs_Type  => My_Inputs,
        Outputs_Type => My_Outputs);

   Quit : Boolean := False;

   My_DPM : DPM.Dual_Port_Memory;

   task Task_Ping is
   end Task_Ping;

   task body Task_Ping is
      My_Inputs_Buffer  : My_Inputs;
      My_Outputs_Buffer : My_Outputs;

      procedure Print;

      procedure Print is
         Word_Buffer : String (1 .. 30);
         Byte_Buffer : String (1 .. 30);
      begin

         A4A.Word_Text_IO.Put (To   => Word_Buffer,
                               Item => My_Inputs_Buffer.B, Base => 16);
         A4A.Byte_Text_IO.Put (To   => Byte_Buffer,
                               Item => My_Inputs_Buffer.C, Base => 16);
         Ada.Text_IO.Put_Line ("Task_Ping :"
                               & " A = " & My_Inputs_Buffer.A'Img
                               & " B = " & Word_Buffer
                               & " C = " & Byte_Buffer);

      end Print;

   begin

      Ada.Text_IO.Put_Line ("Task_Ping : Started");

      delay 2.0;

      loop
         Ada.Text_IO.Put_Line ("Task_Ping : Get Inputs");
         My_Inputs_Buffer := My_DPM.Inputs.Get_Data;

         Print;

         Ada.Text_IO.Put_Line ("Task_Ping : Calculate");
         My_Outputs_Buffer.X := My_Inputs_Buffer.A;
         My_Outputs_Buffer.Y := My_Inputs_Buffer.B;
         My_Outputs_Buffer.Z := My_Inputs_Buffer.C;

         Ada.Text_IO.Put_Line ("Task_Ping : Set Outputs");
         My_DPM.Outputs.Set_Data (My_Outputs_Buffer);

         exit when Quit;
         delay 1.0;

      end loop;

      Ada.Text_IO.Put_Line ("Task_Ping : Ended");

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

   end Task_Ping;

   task Task_Pong is
   end Task_Pong;

   task body Task_Pong is
      My_Inputs_Buffer  : My_Inputs;
      My_Outputs_Buffer : My_Outputs;
   begin

      Ada.Text_IO.Put_Line ("Task_Pong : Started");

      loop
         Ada.Text_IO.Put_Line ("Task_Pong : Get Outputs");
         My_Outputs_Buffer := My_DPM.Outputs.Get_Data;

         Ada.Text_IO.Put_Line ("Task_Pong : Calculate");
         My_Inputs_Buffer.A := not My_Outputs_Buffer.X;
         My_Inputs_Buffer.B := My_Outputs_Buffer.Y + 2;
         My_Inputs_Buffer.C := My_Outputs_Buffer.Z + 1;

         Ada.Text_IO.Put_Line ("Task_Pong : Set Inputs");
         My_DPM.Inputs.Set_Data (My_Inputs_Buffer);

         exit when Quit;
         delay 1.0;

      end loop;

      Ada.Text_IO.Put_Line ("Task_Pong : Ended");

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

   end Task_Pong;

begin

   Ada.Text_IO.Put_Line ("Test_Generic_DPM started..");

   delay 30.0;

   Quit := True;

   Ada.Text_IO.Put_Line ("Test_Generic_DPM ended.");

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

end Test_Generic_DPM;
