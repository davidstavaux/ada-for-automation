
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Integer_Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;
with A4A.Protocols.LibModbus;

procedure Test_LibModbus_Client2 is
   package LibModbus renames A4A.Protocols.LibModbus;
   MyContext : LibModbus.Context_Type;
   Input_Registers   : Word_Array (0 .. 9);
   Holding_Registers : Word_Array (0 .. 9);
   My_Timer          : Integer := 60; -- 1 minute
   Context_Ok : Boolean := False;
   Connect_Ok : Boolean := False;

   procedure Close;
   procedure Close is
   begin
      if Connect_Ok then
         LibModbus.Close (Context => MyContext);
      end if;
      if Context_Ok then
         LibModbus.Free (Context => MyContext);
      end if;
      Ada.Text_IO.Put (Item => "Closing gracefully.");
   end Close;

begin
   Ada.Text_IO.Put_Line (Item => "Test_LibModbus_Client...");

   MyContext := LibModbus.New_TCP (IP_Address => "127.0.0.1",
                                   Port       => 1502);
   Context_Ok := True;

   LibModbus.Connect (Context => MyContext);
   Connect_Ok := True;

   loop
      My_Timer := My_Timer - 1;
      LibModbus.Read_Input_Registers (Context => MyContext,
                                      Offset  => 0,
                                      Number  => Input_Registers'Length,
                                      Dest    => Input_Registers);

      Ada.Text_IO.Put (Item => "Input Registers(0) := ");
      Ada.Integer_Text_IO.Put
        (Item => Integer (Input_Registers (0)), Base => 16);
      Ada.Text_IO.New_Line;

      Holding_Registers (0 .. 9) := Input_Registers (0 .. 9);

      LibModbus.Write_Registers (Context     => MyContext,
                                 Offset      => 0,
                                 Number      => Holding_Registers'Length,
                                 Data        => Holding_Registers);
      delay 1.0;
      exit when My_Timer < 0;

   end loop;

   Close;

exception

   when Error : LibModbus.Context_Error | LibModbus.Connect_Error =>
      Ada.Text_IO.Put_Line ("libmodbus exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

   when Error : others =>
      Ada.Text_IO.Put_Line ("Unexpected exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

end Test_LibModbus_Client2;
