
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2020, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Real_Time; use Ada.Real_Time;

procedure Test_RT is

   procedure cyclic;

   procedure cyclic is
      real1 : constant Float := 10.0;
      real2 : constant Float := 20.0;
      real3 : Float :=  0.0;
      pragma Unreferenced (real3);
   begin
      for Index in 0 .. 1000000 loop -- should do something useful instead
         real3 := real1 * real2;
      end loop;
   end cyclic;

   task type Periodic_Task
     (Task_Priority           : System.Priority;
      Period_In_Milliseconds  : Natural) is
      pragma Priority (Task_Priority);
   end Periodic_Task;

   task body Periodic_Task is
      Next_Time  : Ada.Real_Time.Time := Clock;
      MyPeriod   : constant Time_Span := Milliseconds (Period_In_Milliseconds);
      Max_Times  : constant Integer := 10000 / Period_In_Milliseconds;
      --  we want the test to run 10 s
      Times      : Integer := 0;
      Stats      : array (1 .. 10) of Integer;
      Thresholds : array (1 .. 10) of Duration;
      MyDuration       : Duration;
      MyMinDuration    : Duration;
      MyMaxDuration    : Duration;
      MyPeriodDuration : constant Duration := To_Duration (MyPeriod);
   begin
      Put_Line ("MyPeriod_Duration    : " & Duration'Image (MyPeriodDuration));

      for Index in Stats'Range loop
         Stats (Index) := 0;
      end loop;

      Thresholds (1) := MyPeriodDuration *  0.001;
      Thresholds (2) := MyPeriodDuration *  0.005;
      Thresholds (3) := MyPeriodDuration *  0.010;
      Thresholds (4) := MyPeriodDuration *  0.050;
      Thresholds (5) := MyPeriodDuration *  0.100;
      Thresholds (6) := MyPeriodDuration *  0.500;
      Thresholds (7) := MyPeriodDuration *  1.000;
      Thresholds (8) := MyPeriodDuration *  5.000;
      Thresholds (9) := MyPeriodDuration * 10.000;
      Thresholds (10) := MyPeriodDuration * 20.000;
      loop
         Next_Time := Next_Time + MyPeriod;

         cyclic; -- Do something useful

         delay until Next_Time;

         MyDuration := To_Duration (Clock - Next_Time);
         if (Times = 0) then
            MyMinDuration := MyDuration;
            MyMaxDuration := MyDuration;
         else
            if MyMinDuration > MyDuration then
               MyMinDuration := MyDuration;
            elsif MyMaxDuration < MyDuration then
                  MyMaxDuration := MyDuration;
            end if;
         end if;

         for Index in Stats'Range loop
            if MyDuration < Thresholds (Index) then
               Stats (Index) := Stats (Index) + 1;
               exit;
            end if;
         end loop;

         Times := Times + 1;

         exit when Times > Max_Times;

      end loop;
      Put_Line ("Et Hop ! Times : " & Times'Img
                & " Duration Min : " & Duration'Image (MyMinDuration)
                & " Max : " & Duration'Image (MyMaxDuration));
      for Index in Stats'Range loop
         Put_Line (Integer'Image (Index)
                   & " | Thresholds := " & Duration'Image (Thresholds (Index))
                   & " | Stats := " & Integer'Image (Stats (Index)));
      end loop;
      Put_Line ("Periodic_Task finished !");
   end Periodic_Task;

   My_Periodic_Task : Periodic_Task (Task_Priority          => 97,
                                     Period_In_Milliseconds => 10);
begin
   Put_Line ("Real Time test...");

end Test_RT;
