
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2022, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;

procedure Tutorial_00 is

   Valve1_Is_Open   : Boolean := True;
   Valve2_Is_Closed : Boolean := False;

   Pump1_On         : Boolean := False;
   Pump2_On         : Boolean := False;
   Pump3_On         : Boolean := False;

   Pump4_On         : Boolean := False;

   Counter : Integer := 10;

begin

   Put_Line ("This tutorial is about playing with booleans");
   New_Line;

   Pump1_On := Valve1_Is_Open and Valve2_Is_Closed;
   Pump2_On := not Valve1_Is_Open or Valve2_Is_Closed;
   Pump3_On := Valve1_Is_Open xor Valve2_Is_Closed;

   Pump4_On := Valve1_Is_Open and Counter > 5;

   Put_Line ("The ""not"" operator truth table :");
   for A in False .. True loop
      Put_Line ("not " & A'Img & " = " & Boolean'Image (not A));
      delay 1.0;
   end loop;
   New_Line;

   Put_Line ("The ""and"" operator truth table :");
   for A in Boolean'Range loop
      for B in Boolean'Range loop
         Put_Line (A'Img & " and " & B'Img & " = " & Boolean'Image (A and B));
         delay 1.0;
      end loop;
   end loop;
   New_Line;

   Put_Line ("The ""or"" operator truth table :");
   for A in Boolean'Range loop
      for B in Boolean'Range loop
         Put_Line (A'Img & " or " & B'Img & " = " & Boolean'Image (A or B));
         delay 1.0;
      end loop;
   end loop;
   New_Line;

   Put_Line ("The ""xor"" operator truth table :");
   for A in Boolean'Range loop
      for B in Boolean'Range loop
         Put_Line (A'Img & " xor " & B'Img & " = " & Boolean'Image (A xor B));
         delay 1.0;
      end loop;
   end loop;
   New_Line;

   Put_Line ("Tutorial_00 finished !");
end Tutorial_00;
